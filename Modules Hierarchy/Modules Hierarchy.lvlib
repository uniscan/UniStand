﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="OVERRIDE ONLY &quot;GET HIERARCHY PATH&quot; METHOD" Type="Folder"/>
	<Item Name="Root" Type="Folder">
		<Item Name="Children" Type="Folder">
			<Item Name="Computer Processing" Type="Folder">
				<Item Name="Computer Processing.lvclass" Type="LVClass" URL="../Computer Processing Class/Computer Processing.lvclass"/>
			</Item>
			<Item Name="Database" Type="Folder">
				<Item Name="Database.lvclass" Type="LVClass" URL="../Database Class/Database.lvclass"/>
			</Item>
			<Item Name="Device" Type="Folder">
				<Item Name="Children" Type="Folder">
					<Item Name="Complex" Type="Folder">
						<Item Name="Complex.lvclass" Type="LVClass" URL="../Complex Class/Complex.lvclass"/>
					</Item>
					<Item Name="Digital Multimeter" Type="Folder">
						<Item Name="Digital Multimeter.lvclass" Type="LVClass" URL="../Digital Multimeter Class/Digital Multimeter.lvclass"/>
					</Item>
					<Item Name="Electronic Load" Type="Folder">
						<Item Name="Electronic Load.lvclass" Type="LVClass" URL="../Electronic Load Class/Electronic Load.lvclass"/>
					</Item>
					<Item Name="Interface Adapter" Type="Folder">
						<Item Name="Interface Adapter.lvclass" Type="LVClass" URL="../Interface Adapter Class/Interface Adapter.lvclass"/>
					</Item>
					<Item Name="Multiplexer" Type="Folder">
						<Item Name="Multiplexer.lvclass" Type="LVClass" URL="../Multiplexer Class/Multiplexer.lvclass"/>
					</Item>
					<Item Name="Power Supply Unit" Type="Folder">
						<Item Name="Power Supply Unit.lvclass" Type="LVClass" URL="../Power Supply Unit Class/Power Supply Unit.lvclass"/>
					</Item>
					<Item Name="Programmer" Type="Folder">
						<Item Name="Programmer.lvclass" Type="LVClass" URL="../Programmer Class/Programmer.lvclass"/>
					</Item>
					<Item Name="RF Instruments" Type="Folder">
						<Item Name="Children" Type="Folder">
							<Item Name="RF Power Meter" Type="Folder">
								<Item Name="RF Power Meter.lvclass" Type="LVClass" URL="../RF Power Meter Class/RF Power Meter.lvclass"/>
							</Item>
							<Item Name="Spectrum Analyzer" Type="Folder">
								<Item Name="RF Spectrum Analyzer.lvclass" Type="LVClass" URL="../RF Spectrum Analyzer Class/RF Spectrum Analyzer.lvclass"/>
							</Item>
						</Item>
						<Item Name="RF Instrument.lvclass" Type="LVClass" URL="../RF Instrument Class/RF Instrument.lvclass"/>
					</Item>
					<Item Name="Step Motor" Type="Folder">
						<Item Name="Step Motor.lvclass" Type="LVClass" URL="../Step Motor Class/Step Motor.lvclass"/>
					</Item>
				</Item>
				<Item Name="Device.lvclass" Type="LVClass" URL="../Device Class/Device.lvclass"/>
			</Item>
			<Item Name="Test Modules" Type="Folder">
				<Item Name="Test Modules.lvclass" Type="LVClass" URL="../Test Modules Class/Test Modules.lvclass"/>
			</Item>
			<Item Name="User Interface" Type="Folder">
				<Item Name="User Interface.lvclass" Type="LVClass" URL="../User Interface Class/User Interface.lvclass"/>
			</Item>
		</Item>
		<Item Name="Root Module Directory.lvclass" Type="LVClass" URL="../Root Module Directory Class/Root Module Directory.lvclass"/>
	</Item>
</Library>
