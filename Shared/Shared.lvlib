﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Changelog Tools" Type="Folder">
		<Item Name="Changelog File Add Build.vi" Type="VI" URL="../Changelog Tools/Changelog File Add Build.vi"/>
		<Item Name="Changelog File Readout.vi" Type="VI" URL="../Changelog Tools/Changelog File Readout.vi"/>
		<Item Name="Changelog New Build Window.vi" Type="VI" URL="../Changelog Tools/Changelog New Build Window.vi"/>
		<Item Name="Check Build Update.vi" Type="VI" URL="../Changelog Tools/Check Build Update.vi"/>
		<Item Name="Post-Build Window Call.vi" Type="VI" URL="../Changelog Tools/Post-Build Window Call.vi"/>
		<Item Name="Version.ctl" Type="VI" URL="../Changelog Tools/Version.ctl"/>
	</Item>
	<Item Name="Debug" Type="Folder">
		<Item Name="High Precision Timer (AnyWire).vim" Type="VI" URL="../Debug/High Precision Timer (AnyWire).vim"/>
		<Item Name="Wait (AnyWire).vim" Type="VI" URL="../Debug/Wait (AnyWire).vim"/>
	</Item>
	<Item Name="Error Management" Type="Folder">
		<Item Name="Build Error.vi" Type="VI" URL="../Error Management/Build Error.vi"/>
		<Item Name="Clear All Errors.vi" Type="VI" URL="../Error Management/Clear All Errors.vi"/>
		<Item Name="Convert Error.vi" Type="VI" URL="../Error Management/Convert Error.vi"/>
		<Item Name="Filter Error Code.vi" Type="VI" URL="../Error Management/Filter Error Code.vi"/>
		<Item Name="Filter Multiple Error Codes.vi" Type="VI" URL="../Error Management/Filter Multiple Error Codes.vi"/>
		<Item Name="Wait.vi" Type="VI" URL="../Error Management/Wait.vi"/>
	</Item>
	<Item Name="Mouse" Type="Folder">
		<Item Name="Mouse Re-Click.vi" Type="VI" URL="../Mouse/Mouse Re-Click.vi"/>
	</Item>
	<Item Name="Other" Type="Folder">
		<Item Name="BCD To Decimal.vi" Type="VI" URL="../Other/BCD To Decimal.vi"/>
		<Item Name="Miliseconds to TimeString.vi" Type="VI" URL="../Other/Miliseconds to TimeString.vi"/>
	</Item>
	<Item Name="Paths" Type="Folder">
		<Item Name="Compiled Libs Path.vi" Type="VI" URL="../Paths/Compiled Libs Path.vi"/>
		<Item Name="Fix Path.vi" Type="VI" URL="../Paths/Fix Path.vi"/>
	</Item>
	<Item Name="Registry" Type="Folder">
		<Item Name="Backlash Fix.vi" Type="VI" URL="../Registry/Backlash Fix.vi"/>
		<Item Name="Enum Registry Key.vi" Type="VI" URL="../Registry/Enum Registry Key.vi"/>
		<Item Name="Read Registry Value (Binary).vi" Type="VI" URL="../Registry/Read Registry Value (Binary).vi"/>
		<Item Name="Unistand Enum Directory.vi" Type="VI" URL="../Registry/Unistand Enum Directory.vi"/>
		<Item Name="UniStand Format Local Variable Path.vi" Type="VI" URL="../Registry/UniStand Format Local Variable Path.vi"/>
		<Item Name="Unistand Read Local Variable.vi" Type="VI" URL="../Registry/Unistand Read Local Variable.vi"/>
		<Item Name="Unistand Write Local Variable.vi" Type="VI" URL="../Registry/Unistand Write Local Variable.vi"/>
		<Item Name="Write Registry Value (Binary).vi" Type="VI" URL="../Registry/Write Registry Value (Binary).vi"/>
	</Item>
	<Item Name="String" Type="Folder">
		<Item Name="Hex String to Normal String.vi" Type="VI" URL="../String/Hex String to Normal String.vi"/>
		<Item Name="Normal String to Hex String.vi" Type="VI" URL="../String/Normal String to Hex String.vi"/>
		<Item Name="String Levenshtein Distance.vi" Type="VI" URL="../String/String Levenshtein Distance.vi"/>
	</Item>
	<Item Name="UI Refs" Type="Folder">
		<Item Name="Get Array Active Element Index.vi" Type="VI" URL="../UI Refs/Get Array Active Element Index.vi"/>
	</Item>
	<Item Name="User Messages" Type="Folder">
		<Item Name="Button Dialog.vi" Type="VI" URL="../User Messages/Button Dialog.vi"/>
		<Item Name="MaxDisplaySizes.vi" Type="VI" URL="../User Messages/Support/MaxDisplaySizes.vi"/>
		<Item Name="Non-Blocking Message CORE.vi" Type="VI" URL="../User Messages/Non-Blocking Message CORE.vi"/>
		<Item Name="Non-Blocking Message.vi" Type="VI" URL="../User Messages/Non-Blocking Message.vi"/>
		<Item Name="TipStrip.vi" Type="VI" URL="../User Messages/TipStrip.vi"/>
	</Item>
	<Item Name="VISA" Type="Folder">
		<Item Name="VISA Clear Port Data.vi" Type="VI" URL="../VISA/VISA Clear Port Data.vi"/>
		<Item Name="VISA Set DisableErrorReplacement.vi" Type="VI" URL="../VISA/VISA Set DisableErrorReplacement.vi"/>
	</Item>
</Library>
