﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16761898</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*+!!!*Q(C=\&gt;5`&lt;B.2%-@R(SA&amp;WMZ&gt;UE67/E1V6X$$!8S&amp;O9,&lt;.%BO89Y%"35_!:*0%'E;$O!L_!L/^YU(EUA).Y!1SOY_:`@X`HX]&gt;LW2;HMD8?FY;:N_O9X[Y_G$5M=Z[`-;I;J/&amp;U_0[6H8Y`@4[6F^6UU`/7L);P/DS&gt;48*].&amp;`Y6.,^P,^H^P^R&gt;`"(`]&gt;`"+YU6%35J1H');UR[40-G40-G40-G$0-C$0-C$0-C&gt;X-G&gt;X-G&gt;X-G.X-C.X-C.X-BL*R?ZS%5/+:E]G3A:."EA;1R&amp;S5@C34S**`&amp;QK=34?"*0YEE].&amp;(C34S**`%E(LIJ]33?R*.Y%A^$F32L*]?4?"B?A3@Q"*\!%XC95I%H!!34"1-(A]"15"G="*\!%XAY6?!*0)%H]!1?KB6Y!E`A#4S"BS[V+F';M:0D92AZ(M@D?"S0YW&amp;I/2\(YXA=D_.B/DE?R_-AH!G&gt;Q3()[?1U=#Y=D_0BDRS0YX%]DM@R5&amp;6XS'NFBG&lt;MZ(A-D_%R0)&lt;(]$#%$)`B-4S'R`!QL!S0Y4%]BM@Q-*5-D_%R0!&lt;%G*4J:1RG&gt;$1;'9(BY;CHR?IO25GM&gt;KE@8P6$K8\9V!_2_O&amp;1XX4VT64@*08CKR&gt;6P6DK26"`/46;D6&amp;0IOY]'OL!ZZ[SIWQJ;]K+MK1M+(0+&lt;(4^T1U0BY0W_\VWOZWWW[X7[\67KZ77S[57CY8G]\FGM^HZ.8$,@HYBH.Z,^ZR`@,DZ^PH&gt;X3&lt;?XGY_P,`&lt;@0VS`7HE@_(`]T`Q&lt;N2L0:W$.8I%56Z*Z1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Sequence Controller.lvclass" Type="LVClass" URL="../Sequence Controller Class/Sequence Controller.lvclass"/>
	<Item Name="Executor.lvclass" Type="LVClass" URL="../Executor Class/Executor.lvclass"/>
</Library>
