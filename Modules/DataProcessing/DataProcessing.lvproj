﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="utf.calculate.project.code.coverage" Type="Bool">true</Property>
	<Property Name="utf.create.arraybrackets" Type="Str">[]</Property>
	<Property Name="utf.create.arraythreshold" Type="UInt">100</Property>
	<Property Name="utf.create.captureinputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.captureoutputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.codecoverage.flag" Type="Bool">false</Property>
	<Property Name="utf.create.codecoverage.value" Type="UInt">100</Property>
	<Property Name="utf.create.editor.flag" Type="Bool">false</Property>
	<Property Name="utf.create.editor.path" Type="Path"></Property>
	<Property Name="utf.create.nameseparator" Type="Str">/</Property>
	<Property Name="utf.create.precision" Type="UInt">6</Property>
	<Property Name="utf.create.repetitions" Type="UInt">1</Property>
	<Property Name="utf.create.testpath.flag" Type="Bool">false</Property>
	<Property Name="utf.create.testpath.path" Type="Path"></Property>
	<Property Name="utf.create.timeout.flag" Type="Bool">false</Property>
	<Property Name="utf.create.timeout.value" Type="UInt">0</Property>
	<Property Name="utf.create.type" Type="UInt">0</Property>
	<Property Name="utf.enable.RT.VI.server" Type="Bool">false</Property>
	<Property Name="utf.passwords" Type="Bin">&amp;Q#!!!!!!!)!%%!Q`````Q:4&gt;(*J&lt;G=!!":!1!!"`````Q!!#6"B=X.X&lt;X*E=Q!"!!%!!!!"!!!!#F652E&amp;-4&amp;.516)!!!!!</Property>
	<Property Name="utf.report.atml.create" Type="Bool">false</Property>
	<Property Name="utf.report.atml.path" Type="Path">ATML report.xml</Property>
	<Property Name="utf.report.atml.view" Type="Bool">false</Property>
	<Property Name="utf.report.details.errors" Type="Bool">false</Property>
	<Property Name="utf.report.details.failed" Type="Bool">false</Property>
	<Property Name="utf.report.details.passed" Type="Bool">false</Property>
	<Property Name="utf.report.errors" Type="Bool">true</Property>
	<Property Name="utf.report.failed" Type="Bool">true</Property>
	<Property Name="utf.report.html.create" Type="Bool">false</Property>
	<Property Name="utf.report.html.path" Type="Path">HTML report.html</Property>
	<Property Name="utf.report.html.view" Type="Bool">false</Property>
	<Property Name="utf.report.passed" Type="Bool">true</Property>
	<Property Name="utf.report.skipped" Type="Bool">true</Property>
	<Property Name="utf.report.sortby" Type="UInt">1</Property>
	<Property Name="utf.report.stylesheet.flag" Type="Bool">false</Property>
	<Property Name="utf.report.stylesheet.path" Type="Path"></Property>
	<Property Name="utf.report.summary" Type="Bool">true</Property>
	<Property Name="utf.report.txt.create" Type="Bool">false</Property>
	<Property Name="utf.report.txt.path" Type="Path">ASCII report.txt</Property>
	<Property Name="utf.report.txt.view" Type="Bool">false</Property>
	<Property Name="utf.run.changed.days" Type="UInt">1</Property>
	<Property Name="utf.run.changed.outdated" Type="Bool">false</Property>
	<Property Name="utf.run.changed.timestamp" Type="Bin">&amp;Q#!!!!!!!%!%%"5!!9*2'&amp;U:3^U;7VF!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	<Property Name="utf.run.days.flag" Type="Bool">false</Property>
	<Property Name="utf.run.includevicallers" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.overwrite" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.path" Type="Path">test execution log.txt</Property>
	<Property Name="utf.run.modified.last.run.flag" Type="Bool">true</Property>
	<Property Name="utf.run.priority.flag" Type="Bool">false</Property>
	<Property Name="utf.run.priority.value" Type="UInt">5</Property>
	<Property Name="utf.run.statusfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.statusfile.path" Type="Path">test status log.txt</Property>
	<Property Name="utf.run.timestamp.flag" Type="Bool">false</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Core Libs" Type="Folder">
			<Item Name="Data Parser.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp">
				<Item Name="Private" Type="Folder">
					<Item Name="Cluster Toolkit" Type="Folder">
						<Item Name="(CT) Type Desc Enum Strings.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Type Desc Enum Strings.ctl"/>
						<Item Name="(CT) Type Desc Enum.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Type Desc Enum.ctl"/>
						<Item Name="(CT) Waveform Subtype Code.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Waveform Subtype Code.ctl"/>
						<Item Name="(CT) Name from Type Desc.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Name from Type Desc.vi"/>
						<Item Name="(CT) Get Object Name.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Get Object Name.vi"/>
						<Item Name="(CT) Delete Name from Object.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Delete Name from Object.vi"/>
						<Item Name="(CT) Get Object Type.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Get Object Type.vi"/>
						<Item Name="(CT) Cluster to Array of Variants.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Cluster to Array of Variants.vi"/>
						<Item Name="(CT) Number of Cluster Elements.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Number of Cluster Elements.vi"/>
						<Item Name="(CT) Reshape n-Dimension Array.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Reshape n-Dimension Array.vi"/>
						<Item Name="(CT) Array of Variants to Cluster.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Array of Variants to Cluster.vi"/>
						<Item Name="(CT) Get Array Element Type String.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Get Array Element Type String.vi"/>
						<Item Name="(CT) Get Array Element Type String (From String).vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Get Array Element Type String (From String).vi"/>
						<Item Name="(CT) Array Subtype and Size.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Array Subtype and Size.vi"/>
						<Item Name="(CT) Name to Type Desc.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Name to Type Desc.vi"/>
						<Item Name="(CT) Set Object Name.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Set Object Name.vi"/>
						<Item Name="(CT) Replace Array Element Type String.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Replace Array Element Type String.vi"/>
						<Item Name="(CT) VArray Size.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) VArray Size.vi"/>
						<Item Name="(CT) Type Check Error Msg.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Type Check Error Msg.vi"/>
						<Item Name="(CT) Name from Enum Desc.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Name from Enum Desc.vi"/>
						<Item Name="(CT) Enum Type and Item Check (Type String).vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Enum Type and Item Check (Type String).vi"/>
						<Item Name="(CT) Enum Type and Item Check.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Enum Type and Item Check.vi"/>
						<Item Name="(CT) Enum Type Check.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Enum Type Check.vi"/>
						<Item Name="(CT) Object Type (String) &amp; Subtype Check.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Object Type (String) &amp; Subtype Check.vi"/>
						<Item Name="(CT) Object Type &amp; Subtype Check.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Object Type &amp; Subtype Check.vi"/>
						<Item Name="(CT) Type &amp; Subtype Check.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Type &amp; Subtype Check.vi"/>
						<Item Name="(CT) Waveform Type String to Cluster Type String.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Waveform Type String to Cluster Type String.vi"/>
						<Item Name="(CT) Insert Into 1D Array.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Insert Into 1D Array.vi"/>
						<Item Name="(CT) Delete Name from Type Desc.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Delete Name from Type Desc.vi"/>
						<Item Name="(CT) Type Check Type String Elements (All).vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Type Check Type String Elements (All).vi"/>
						<Item Name="(CT) Calculate Index (1D).vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Calculate Index (1D).vi"/>
						<Item Name="(CT) Cluster Index Error.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Cluster Index Error.vi"/>
						<Item Name="(CT) Index Cluster.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Index Cluster.vi"/>
						<Item Name="(CT) Is Integer Check.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Is Integer Check.vi"/>
						<Item Name="(CT) Array Index from String.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Array Index from String.vi"/>
						<Item Name="(CT) Get Cluster Element (by Name).vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Get Cluster Element (by Name).vi"/>
						<Item Name="(CT) Get Cluster Element Data Types.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Get Cluster Element Data Types.vi"/>
						<Item Name="(CT) Get Array Element.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Get Array Element.vi"/>
						<Item Name="(CT) Number of Cluster Elements in Array Element.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Number of Cluster Elements in Array Element.vi"/>
						<Item Name="(CT) Get Cluster Element Names.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Cluster Toolkit/(CT) Get Cluster Element Names.vi"/>
					</Item>
					<Item Name="Image.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Image.ctl"/>
					<Item Name="Value ID.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Value ID.vi"/>
					<Item Name="Add Attribute.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Add Attribute.vi"/>
					<Item Name="Strip Name Root RegEx.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Strip Name Root RegEx.vi"/>
					<Item Name="Build Name.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Build Name.vi"/>
					<Item Name="Strip Cluster Name.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Strip Cluster Name.vi"/>
					<Item Name="Compare Names.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Compare Names.vi"/>
					<Item Name="Prepend Cluster Name.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Prepend Cluster Name.vi"/>
					<Item Name="Get Enum Strings.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Get Enum Strings.vi"/>
					<Item Name="Get Type with No Labels.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Get Type with No Labels.vi"/>
					<Item Name="Write Cluster to Cluster.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Write Cluster to Cluster.vi"/>
					<Item Name="Write Element to Array.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Write Element to Array.vi"/>
					<Item Name="Write Element to Cluster.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Write Element to Cluster.vi"/>
					<Item Name="Write to Variant.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Write to Variant.vi"/>
					<Item Name="Convert String to Integer Array.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Convert String to Integer Array.vi"/>
					<Item Name="Write Array to Array.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Write Array to Array.vi"/>
					<Item Name="String Array to Spreadsheet String.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/String Array to Spreadsheet String.vi"/>
					<Item Name="Trim TrailingZeros from String.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Trim TrailingZeros from String.vi"/>
					<Item Name="Fix Parent Label.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Fix Parent Label.vi"/>
				</Item>
				<Item Name="Dynamic Type Definition.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Dynamic Type Definition.vi"/>
				<Item Name="Get Data Type String.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Get Data Type String.vi"/>
				<Item Name="Set Variant Value.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Private/Set Variant Value.vi"/>
				<Item Name="Convert Data To Image.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Convert Data To Image.vi"/>
				<Item Name="Fill Data From Image.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Fill Data From Image.vi"/>
				<Item Name="Get Image Names.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Get Image Names.vi"/>
				<Item Name="Get Image Values.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Get Image Values.vi"/>
				<Item Name="Get Data Names.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Get Data Names.vi"/>
				<Item Name="Set Data Values.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Set Data Values.vi"/>
				<Item Name="Rename Variant Cluster.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Rename Variant Cluster.vi"/>
				<Item Name="Get Array of Name Strings.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Get Array of Name Strings.vi"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/1abvi3w/vi.lib/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="whitespace.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
			</Item>
			<Item Name="Actor Framework.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp">
				<Item Name="AF Debug" Type="Folder">
					<Item Name="Client Interface" Type="Folder">
						<Item Name="Get Actor Handles.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Get Actor Handles.vi"/>
						<Item Name="Get Registry Update Event.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Get Registry Update Event.vi"/>
						<Item Name="Protected Actor Handle.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Protected Actor Handle/Protected Actor Handle.lvclass"/>
					</Item>
					<Item Name="Debug Messages" Type="Folder">
						<Item Name="Ping Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Ping msg/Ping Msg.lvclass"/>
						<Item Name="Register Actor Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Register Actor Msg/Register Actor Msg.lvclass"/>
					</Item>
					<Item Name="support" Type="Folder">
						<Item Name="Actor Registry.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Actor Registry.vi"/>
						<Item Name="Generate Trace.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace.vi"/>
						<Item Name="Get Clone Name.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Get Clone Name.vi"/>
						<Item Name="Localized Strings.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Localized Strings.vi"/>
						<Item Name="Registration Mode.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Registration Mode.ctl"/>
						<Item Name="TDM Registration Mode.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/TDM Registration Mode.ctl"/>
						<Item Name="TDM Registry.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/TDM Registry.vi"/>
						<Item Name="This Actor.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/This Actor.ctl"/>
					</Item>
					<Item Name="Trace Generation" Type="Folder">
						<Item Name="Generate Custom Trace.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Custom Trace.vi"/>
						<Item Name="Generate Trace for Dropped Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for Dropped Message.vi"/>
						<Item Name="Generate Trace for Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for Message.vi"/>
						<Item Name="Generate Trace for New Actor.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for New Actor.vi"/>
						<Item Name="Generate Trace for New Time Delayed Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for New Time Delayed Message.vi"/>
						<Item Name="Generate Trace for Received Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for Received Message.vi"/>
						<Item Name="Generate Trace for Skipped Time-Delayed Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for Skipped Time-Delayed Message.vi"/>
						<Item Name="Generate Trace for Stopped Actor.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for Stopped Actor.vi"/>
						<Item Name="Generate Trace for Stopped Time-Delayed Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for Stopped Time-Delayed Message.vi"/>
					</Item>
				</Item>
				<Item Name="Messages" Type="Folder">
					<Item Name="Time-Delayed Send Message" Type="Folder">
						<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
						<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
						<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
					</Item>
					<Item Name="Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Message/Message.lvclass"/>
					<Item Name="Stop Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Stop Msg/Stop Msg.lvclass"/>
					<Item Name="Last Ack.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/LastAck/Last Ack.lvclass"/>
					<Item Name="Launch Nested Actor Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Launch Nested Actor Msg/Launch Nested Actor Msg.lvclass"/>
					<Item Name="Batch Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Batch Msg/Batch Msg.lvclass"/>
					<Item Name="Reply Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Reply Msg/Reply Msg.lvclass"/>
					<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Report Error Msg/Report Error Msg.lvclass"/>
					<Item Name="Self-Addressed Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Self-Addressed Msg/Self-Addressed Msg.lvclass"/>
				</Item>
				<Item Name="Actor.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Actor/Actor.lvclass"/>
				<Item Name="Message Priority Queue.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Message Priority Queue/Message Priority Queue.lvclass"/>
				<Item Name="Message Enqueuer.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Message Enqueuer/Message Enqueuer.lvclass"/>
				<Item Name="Message Dequeuer.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Message Dequeuer/Message Dequeuer.lvclass"/>
				<Item Name="Message Queue.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Message Queue/Message Queue.lvclass"/>
				<Item Name="Init Actor Queues FOR TESTING ONLY.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Actor/Init Actor Queues FOR TESTING ONLY.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/1abvi3w/vi.lib/Utility/High Resolution Relative Seconds.vi"/>
			</Item>
			<Item Name="Thread.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Thread.lvlibp">
				<Item Name="Messages to Thread" Type="Folder">
					<Item Name="Command.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Thread.lvlibp/Messages to Thread/Command/Command.lvclass"/>
					<Item Name="Command with Reply.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Thread.lvlibp/Messages to Thread/Command with Reply/Command with Reply.lvclass"/>
					<Item Name="Report-to-Caller.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Thread.lvlibp/Messages to Thread/Report-to-Caller/Report-to-Caller.lvclass"/>
				</Item>
				<Item Name="Thread.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Thread.lvlibp/Thread_class/Thread.lvclass"/>
			</Item>
			<Item Name="Module Core.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Module Core.lvlibp">
				<Item Name="Messages to Task" Type="Folder">
					<Item Name="Init Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Init Message/Init Message.lvclass"/>
					<Item Name="Prepare-to-Exec Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Prepare-to-Exec Message/Prepare-to-Exec Message.lvclass"/>
					<Item Name="Execute Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Execute Message/Execute Message.lvclass"/>
					<Item Name="Free Memory Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Free Memory Message/Free Memory Message.lvclass"/>
					<Item Name="Terminate Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Terminate Message/Terminate Message.lvclass"/>
					<Item Name="Read Self-Actor Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Read Self-Actor Message/Read Self-Actor Message.lvclass"/>
					<Item Name="Write Self-Actor Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Write Self-Actor Message/Write Self-Actor Message.lvclass"/>
					<Item Name="Set CloneFPName Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Set CloneFPName Message/Set CloneFPName Message.lvclass"/>
				</Item>
				<Item Name="Task.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Task_class/Task.lvclass"/>
				<Item Name="Module.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Module_class/Module.lvclass"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Name.vi"/>
			</Item>
			<Item Name="Modules Hierarchy.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp">
				<Item Name="OVERRIDE ONLY &quot;GET HIERARCHY PATH&quot; METHOD" Type="Folder"/>
				<Item Name="Root" Type="Folder">
					<Item Name="Children" Type="Folder">
						<Item Name="Computer Processing" Type="Folder">
							<Item Name="Computer Processing.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Computer Processing Class/Computer Processing.lvclass"/>
						</Item>
						<Item Name="Database" Type="Folder">
							<Item Name="Database.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Database Class/Database.lvclass"/>
						</Item>
						<Item Name="Device" Type="Folder">
							<Item Name="Children" Type="Folder">
								<Item Name="Complex" Type="Folder">
									<Item Name="Complex.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Complex Class/Complex.lvclass"/>
								</Item>
								<Item Name="Digital Multimeter" Type="Folder">
									<Item Name="Digital Multimeter.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Digital Multimeter Class/Digital Multimeter.lvclass"/>
								</Item>
								<Item Name="Electronic Load" Type="Folder">
									<Item Name="Electronic Load.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Electronic Load Class/Electronic Load.lvclass"/>
								</Item>
								<Item Name="Interface Adapter" Type="Folder">
									<Item Name="Interface Adapter.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Interface Adapter Class/Interface Adapter.lvclass"/>
								</Item>
								<Item Name="Multiplexer" Type="Folder">
									<Item Name="Multiplexer.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Multiplexer Class/Multiplexer.lvclass"/>
								</Item>
								<Item Name="Power Supply Unit" Type="Folder">
									<Item Name="Power Supply Unit.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Power Supply Unit Class/Power Supply Unit.lvclass"/>
								</Item>
								<Item Name="Programmer" Type="Folder">
									<Item Name="Programmer.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Programmer Class/Programmer.lvclass"/>
								</Item>
								<Item Name="RF Instruments" Type="Folder">
									<Item Name="Children" Type="Folder">
										<Item Name="RF Power Meter" Type="Folder">
											<Item Name="RF Power Meter.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/RF Power Meter Class/RF Power Meter.lvclass"/>
										</Item>
										<Item Name="Spectrum Analyzer" Type="Folder">
											<Item Name="RF Spectrum Analyzer.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/RF Spectrum Analyzer Class/RF Spectrum Analyzer.lvclass"/>
										</Item>
									</Item>
									<Item Name="RF Instrument.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/RF Instrument Class/RF Instrument.lvclass"/>
								</Item>
								<Item Name="Step Motor" Type="Folder">
									<Item Name="Step Motor.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Step Motor Class/Step Motor.lvclass"/>
								</Item>
							</Item>
							<Item Name="Device.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Device Class/Device.lvclass"/>
						</Item>
						<Item Name="Test Modules" Type="Folder">
							<Item Name="Test Modules.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Test Modules Class/Test Modules.lvclass"/>
						</Item>
						<Item Name="User Interface" Type="Folder">
							<Item Name="User Interface.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/User Interface Class/User Interface.lvclass"/>
						</Item>
					</Item>
					<Item Name="Root Module Directory.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Root Module Directory Class/Root Module Directory.lvclass"/>
				</Item>
			</Item>
		</Item>
		<Item Name="Post-Build Action.vi" Type="VI" URL="../Post-Build Action.vi"/>
		<Item Name="DataProcessing.lvlib" Type="Library" URL="../DataProcessing.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_MABase.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MABase.lvlib"/>
				<Item Name="NI_MAPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MAPro.lvlib"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_Gmath.lvlib" Type="Library" URL="/&lt;vilib&gt;/gmath/NI_Gmath.lvlib"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
			</Item>
			<Item Name="Post-Build. Move Build (.lvlibp).vi" Type="VI" URL="../../../Post-Build Action VIs/Post-Build. Move Build (.lvlibp).vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="DataProcessing" Type="Packed Library">
				<Property Name="Bld_buildCacheID" Type="Str">{E882488C-0F99-4060-82D8-8CB6D40A3187}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">DataProcessing</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../Modules</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_postActionVIID" Type="Ref">/My Computer/Post-Build Action.vi</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{68A38F2B-5226-4401-98AA-2CDA05EACD8F}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">DataProcessing.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">../Modules/NI_AB_PROJECTNAME.lvlibp</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../Dependencies</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[2].destName" Type="Str">Core Libs</Property>
				<Property Name="Destination[2].path" Type="Path">..</Property>
				<Property Name="Destination[2].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{DBEFE093-5611-4D46-A828-E5489CA21DED}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[1].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[1].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Core Libs</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Core Libs/Actor Framework.lvlibp</Property>
				<Property Name="Source[2].preventRename" Type="Bool">true</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Core Libs/Thread.lvlibp</Property>
				<Property Name="Source[3].preventRename" Type="Bool">true</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Core Libs/Module Core.lvlibp</Property>
				<Property Name="Source[4].preventRename" Type="Bool">true</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Core Libs/Modules Hierarchy.lvlibp</Property>
				<Property Name="Source[5].preventRename" Type="Bool">true</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/DataProcessing.lvlib</Property>
				<Property Name="Source[6].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[6].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[6].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[6].preventRename" Type="Bool">true</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[6].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">7</Property>
				<Property Name="TgtF_fileDescription" Type="Str">DataProcessing</Property>
				<Property Name="TgtF_internalName" Type="Str">DataProcessing</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2016 </Property>
				<Property Name="TgtF_productName" Type="Str">DataProcessing</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{87C8F61C-58B6-4975-9749-E7BE763F6654}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">DataProcessing.lvlibp</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
