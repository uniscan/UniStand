﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Tasks" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Math" Type="Folder">
			<Item Name="ArrayMean.lvclass" Type="LVClass" URL="../Task Classes/ArrayMean/ArrayMean.lvclass"/>
			<Item Name="Pulse Measurements.lvclass" Type="LVClass" URL="../Task Classes/Pulse Measurements/Pulse Measurements.lvclass"/>
			<Item Name="AmplitudeLevels.lvclass" Type="LVClass" URL="../Task Classes/AmplitudeLevels/AmplitudeLevels.lvclass"/>
			<Item Name="ArrayMinMax.lvclass" Type="LVClass" URL="../Task Classes/ArrayMinMax/ArrayMinMax.lvclass"/>
			<Item Name="ANDBoolArray.lvclass" Type="LVClass" URL="../Task Classes/ANDBoolArray/ANDBoolArray.lvclass"/>
			<Item Name="ORBoolArray.lvclass" Type="LVClass" URL="../Task Classes/ORBoolArray/ORBoolArray.lvclass"/>
			<Item Name="NOTBool.lvclass" Type="LVClass" URL="../Task Classes/NOTBool/NOTBool.lvclass"/>
			<Item Name="Arithmetics.lvclass" Type="LVClass" URL="../Task Classes/Arithmetics/Arithmetics.lvclass"/>
			<Item Name="RoundDbl.lvclass" Type="LVClass" URL="../Task Classes/RoundDbl/RoundDbl.lvclass"/>
			<Item Name="FormulaMath.lvclass" Type="LVClass" URL="../Task Classes/FormulaMath/FormulaMath.lvclass"/>
		</Item>
		<Item Name="Conversion" Type="Folder">
			<Item Name="ArrayIndex2DDbl.lvclass" Type="LVClass" URL="../Task Classes/ArrayIndex2DDbl/ArrayIndex2DDbl.lvclass"/>
			<Item Name="ArrayIndex2DBool.lvclass" Type="LVClass" URL="../Task Classes/ArrayIndex2DBool/ArrayIndex2DBool.lvclass"/>
			<Item Name="ArrayIndex1DStr.lvclass" Type="LVClass" URL="../Task Classes/ArrayIndex1DStr/ArrayIndex1DStr.lvclass"/>
			<Item Name="ArrayIndex1DDbl.lvclass" Type="LVClass" URL="../Task Classes/ArrayIndex1DDbl/ArrayIndex1DDbl.lvclass"/>
			<Item Name="ArrayIndex1DU8.lvclass" Type="LVClass" URL="../Task Classes/ArrayIndex1DU8/ArrayIndex1DU8.lvclass"/>
			<Item Name="ArrayIndex1DU64.lvclass" Type="LVClass" URL="../Task Classes/ArrayIndex1DU64/ArrayIndex1DU64.lvclass"/>
			<Item Name="ArrayU8ToU64.lvclass" Type="LVClass" URL="../Task Classes/ArrayU8ToU64/ArrayU8ToU64.lvclass"/>
			<Item Name="ArrayBuild1DStr.lvclass" Type="LVClass" URL="../Task Classes/ArrayBuild1DStr/ArrayBuild1DStr.lvclass"/>
			<Item Name="ArrayBuild1DDbl.lvclass" Type="LVClass" URL="../Task Classes/ArrayBuild1DDbl/ArrayBuild1DDbl.lvclass"/>
			<Item Name="ArrayBuild1DU8.lvclass" Type="LVClass" URL="../Task Classes/ArrayBuild1DU8/ArrayBuild1DU8.lvclass"/>
			<Item Name="ArrayBuild1DU64.lvclass" Type="LVClass" URL="../Task Classes/ArrayBuild1DU64/ArrayBuild1DU64.lvclass"/>
			<Item Name="ArrayThresholdSplit1DDbl.lvclass" Type="LVClass" URL="../Task Classes/ArrayThresholdSplit1DDbl/ArrayThresholdSplit1DDbl.lvclass"/>
			<Item Name="U64ToArrayU8.lvclass" Type="LVClass" URL="../Task Classes/U64ToArrayU8/U64ToArrayU8.lvclass"/>
			<Item Name="ArrayU8ToI64.lvclass" Type="LVClass" URL="../Task Classes/ArrayU8ToI64/ArrayU8ToI64.lvclass"/>
			<Item Name="I64ToArrayU8.lvclass" Type="LVClass" URL="../Task Classes/I64ToArrayU8/I64ToArrayU8.lvclass"/>
			<Item Name="ArrayU8ToSgl.lvclass" Type="LVClass" URL="../Task Classes/ArrayU8ToSgl/ArrayU8ToSgl.lvclass"/>
			<Item Name="SglToArrayU8.lvclass" Type="LVClass" URL="../Task Classes/SglToArrayU8/SglToArrayU8.lvclass"/>
			<Item Name="NumberToString.lvclass" Type="LVClass" URL="../Task Classes/NumberToString/NumberToString.lvclass"/>
			<Item Name="StringToNumber.lvclass" Type="LVClass" URL="../Task Classes/StringToNumber/StringToNumber.lvclass"/>
			<Item Name="FormatToString.lvclass" Type="LVClass" URL="../Task Classes/FormatToString/FormatToString.lvclass"/>
		</Item>
		<Item Name="Comparison" Type="Folder">
			<Item Name="NodeStatusCheck.lvclass" Type="LVClass" URL="../Task Classes/NodeStatusCheck/NodeStatusCheck.lvclass"/>
			<Item Name="InRangeAndCoerce.lvclass" Type="LVClass" URL="../Task Classes/InRangeAndCoerce/InRangeAndCoerce.lvclass"/>
			<Item Name="CompareNumbers.lvclass" Type="LVClass" URL="../Task Classes/CompareNumbers/CompareNumbers.lvclass"/>
			<Item Name="CompareBooleans.lvclass" Type="LVClass" URL="../Task Classes/CompareBooleans/CompareBooleans.lvclass"/>
			<Item Name="StringMatchRegularExpression.lvclass" Type="LVClass" URL="../Task Classes/StringMatchRegularExpression/StringMatchRegularExpression.lvclass"/>
		</Item>
		<Item Name="Constants" Type="Folder">
			<Item Name="ConstantBoolean.lvclass" Type="LVClass" URL="../Task Classes/ConstantBoolean/ConstantBoolean.lvclass"/>
			<Item Name="ConstantNumeric.lvclass" Type="LVClass" URL="../Task Classes/ConstantNumeric/ConstantNumeric.lvclass"/>
			<Item Name="ConstantStringPath.lvclass" Type="LVClass" URL="../Task Classes/ConstantStringPath/ConstantStringPath.lvclass"/>
		</Item>
		<Item Name="Time" Type="Folder">
			<Item Name="GetTimerValue.lvclass" Type="LVClass" URL="../Task Classes/GetTimerValue/GetTimerValue.lvclass"/>
		</Item>
	</Item>
	<Item Name="Module.lvclass" Type="LVClass" URL="../Module Class/Module.lvclass"/>
</Library>
