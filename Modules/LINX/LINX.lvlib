﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Support" Type="Folder"/>
	<Item Name="Tasks" Type="Folder">
		<Item Name="DAQ" Type="Folder">
			<Item Name="AI" Type="Folder">
				<Item Name="AI Config Channels.lvclass" Type="LVClass" URL="../Task Classes/AI Config Channels/AI Config Channels.lvclass"/>
				<Item Name="AI Read.lvclass" Type="LVClass" URL="../Task Classes/AI Read/AI Read.lvclass"/>
			</Item>
			<Item Name="AO" Type="Folder">
				<Item Name="AO Config Channels.lvclass" Type="LVClass" URL="../Task Classes/AO Config Channels/AO Config Channels.lvclass"/>
				<Item Name="AO Write.lvclass" Type="LVClass" URL="../Task Classes/AO Write/AO Write.lvclass"/>
			</Item>
			<Item Name="DIO" Type="Folder">
				<Item Name="DIO Config Channels.lvclass" Type="LVClass" URL="../Task Classes/DIO Config Channels/DIO Config Channels.lvclass"/>
				<Item Name="DIO Read.lvclass" Type="LVClass" URL="../Task Classes/DIO Read/DIO Read.lvclass"/>
				<Item Name="DIO Write.lvclass" Type="LVClass" URL="../Task Classes/DIO Write/DIO Write.lvclass"/>
				<Item Name="DIO Write Square Wave.lvclass" Type="LVClass" URL="../Task Classes/DIO Write Square Wave/DIO Write Square Wave.lvclass"/>
				<Item Name="DIO Read Pulse Width.lvclass" Type="LVClass" URL="../Task Classes/DIO Read Pulse Width/DIO Read Pulse Width.lvclass"/>
			</Item>
			<Item Name="PWM" Type="Folder">
				<Item Name="PWM Config Channels.lvclass" Type="LVClass" URL="../Task Classes/PWM Config Channels/PWM Config Channels.lvclass"/>
				<Item Name="PWM Write.lvclass" Type="LVClass" URL="../Task Classes/PWM Write/PWM Write.lvclass"/>
			</Item>
		</Item>
		<Item Name="Interfaces" Type="Folder">
			<Item Name="SPI" Type="Folder">
				<Item Name="SPI Init.lvclass" Type="LVClass" URL="../Task Classes/SPI Init/SPI Init.lvclass"/>
				<Item Name="SPI Write Read.lvclass" Type="LVClass" URL="../Task Classes/SPI Write Read/SPI Write Read.lvclass"/>
			</Item>
			<Item Name="UART" Type="Folder">
				<Item Name="UART Init.lvclass" Type="LVClass" URL="../Task Classes/UART Init/UART Init.lvclass"/>
				<Item Name="UART Deinit.lvclass" Type="LVClass" URL="../Task Classes/UART Deinit/UART Deinit.lvclass"/>
				<Item Name="UART Read.lvclass" Type="LVClass" URL="../Task Classes/UART Read/UART Read.lvclass"/>
				<Item Name="UART Write.lvclass" Type="LVClass" URL="../Task Classes/UART Write/UART Write.lvclass"/>
				<Item Name="UART Bytes Available.lvclass" Type="LVClass" URL="../Task Classes/UART Bytes Available/UART Bytes Available.lvclass"/>
			</Item>
			<Item Name="I2C" Type="Folder">
				<Item Name="I2C Init.lvclass" Type="LVClass" URL="../Task Classes/I2C Init/I2C Init.lvclass"/>
				<Item Name="I2C Deinit.lvclass" Type="LVClass" URL="../Task Classes/I2C Deinit/I2C Deinit.lvclass"/>
				<Item Name="I2C Write.lvclass" Type="LVClass" URL="../Task Classes/I2C Write/I2C Write.lvclass"/>
				<Item Name="I2C Read.lvclass" Type="LVClass" URL="../Task Classes/I2C Read/I2C Read.lvclass"/>
			</Item>
		</Item>
		<Item Name="LINX Deinit.lvclass" Type="LVClass" URL="../Task Classes/LINX Deinit/LINX Deinit.lvclass"/>
		<Item Name="LINX Init.lvclass" Type="LVClass" URL="../Task Classes/LINX Init/LINX Init.lvclass"/>
	</Item>
	<Item Name="Module.lvclass" Type="LVClass" URL="../Module Class/Module.lvclass"/>
</Library>
