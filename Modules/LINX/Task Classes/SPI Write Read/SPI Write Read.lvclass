﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassBodyIcon" Type="Str">Actor</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6618880</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">LINX.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../LINX.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+Q!!!*Q(C=T:3^&lt;F."%%;`CU"+[Z)C%DA.%B7;CA["*4I[P],UK6RSX@E6ZB8]#KZ#!]5U3?^8="],S:R&gt;&lt;SQ,`#/E",'&lt;O8@^T=\MO&lt;/&lt;F7K\E*ZL=[Z&gt;HWT\`D+Z"GQ/_D@NM4HCL_-4]&gt;?\U#0Z&gt;[&amp;(YQ`RHWNP4\:`ZT_-N?@8@DPKH[Z0_8^K/O8RJ``K[KI%NX9UPLC+(@;PK[NE/?S@0PD89LW`TX_#\_\O\O4X\?KTHJ[OX`([HNG@Q_XJ`7=;X^+@`S&gt;YWC:V+B=2FFBADJG+;Z0IC:\IC:\IC2\IA2\IA2\IA?\IDO\IDO\IDG\IBG\IBG\IBFY\ON#&amp;,H3KECS?,*1E42)EEU&amp;2]EBY%J[%*_(BJR+?B#@B38A3(K9IY5FY%J[%*_%B4!F0QJ0Q*$Q*$[EK3&gt;;/$E`#1XI&amp;0!&amp;0Q"0Q"$QMK9!H!!A7#R)(3="1Y!Q'!5`!%`!Q6-!4]!1]!5`!AVM"4]!4]!1]!1]BN3J2;5J(BY=U=HA=(I@(Y8&amp;Y3#W(R_&amp;R?"Q?BY@FZ0!Y0![%M[#4(!1Z1=Y%ZY@$Y`$QEM0D]$A]$I`$A[POE.@+&amp;*L3U?%R?!Q?A]@A-8B))90(Y$&amp;Y$"[$B\1S?!Q?A]@A-8B93A;0Q70Q'#$'ICQP)ZE2;%QS")/(PXJ;L/Z36"+LP6Q2^@#K(5KVQ[:WC.1/B^KGKWWGWC;J&amp;6_NK'L&amp;5CO#WM?J1;P"K#WC&amp;FQG;M6TC3WQ/4&lt;$*NA9'W&amp;$&lt;&amp;"#(XHC;L83=LH59L(1@$\8&lt;$&lt;4:$,2?$T7;$43=$D59$$9X;KP[.P70&gt;R,0?0_]E@8PX\4^3^ZP\DI_P=9_M[`&lt;V]X8@_BQW[[`O.N.`V_X`7@XH8^F]NC.X8]_&lt;[&lt;@LP&gt;TCFT3]RP?&lt;&lt;XUP^Q._K:NO^&gt;D8Y",P-Q'Q!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.2</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"5\5F.31QU+!!.-6E.$4%*76Q!!%1!!!!1\!!!!)!!!%/!!!!!G!!!!!AJ-35Z9,GRW&lt;'FC&amp;F.133"8=GFU:3"3:7&amp;E,GRW9WRB=X-!!!!!!+!8!)!!!$!!!!A!"!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!VQ3LBX#[-%KTJ\BJ4BBCGA!!!"!!!!!1!!!!!,,J$U]CN:R+OV9]HKB';3,5(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!#UG?E[IP_01,B5&amp;SXP[XQ6!1!!!0`````5(9T:DQ#S"/G!#:DM_%*_!!!!%/SDX&amp;@!*VJ^E1$Z'C_??+U!!!!%!!!!!!!!!#=!!5R71U-!!!!"!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!-!!!!!!A!"!!!!!!!C!!!!((C=9W"D9'JAO-!!R)R!T.4!^!0)`A$C-Q!!;!%).A!!!!!!21!!!2BYH'.AQ!4`A1")-4)Q-*U!UCRIYG!;RK9GQ'5O,LOAYMR1.\*#2"G"9ER\A!QGE"SK8J"`Q(Z#!7R9T!9!&gt;P5I&amp;1!!!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!&amp;E!!!#T(C=3W"E9-AUND$&lt;!+3:A6C=I9%B/4]FF9M"S'?!A#V-$"3$!+BZ7GDCBA=/JQ'"(L^]#ZD@`);HWU6&amp;I,F'29+J6+4&lt;2U7EUU?&amp;J:.&amp;Z=7@````.R`B/&gt;TNE80=U1;ENJM$+(\=295$R!(3,#$[@W!'3"8-P!#A;2Q.&amp;=I-*3S'"[)/(W]Q9929$$-S#N8_(3$JQ]DOY1![1O$A1Z&lt;O2AUAPX=CC!1+]83'=%A=&gt;_(1%10S'5^U!KXPZ)'ZEA.O@RD)A")6A5Y4E%N:1+;$V83T(8@1!,P&lt;112#:5#I#AB6!(9-W!6(//)/Q].L\?N\OU$BS)95BAZ!X!$%I$B%RHI-D!QA#ZG!:#V5L1W1T116A]5&amp;C(U"SN:!UP-&amp;S8S1(J$-'KA9C,U*SG[!OA=E^B&gt;)4Y#S1&lt;Z.A,+ZA?Q&amp;5,91E#U!:5M#W1_A&lt;$EI?Q-UCH$2TPYOLED?B[&gt;0!$5&gt;=I!!!!!-&amp;Q#!%1!!"$%X,D!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Q8!)!2!!!%-4=O-!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$"=!A"%!!!1R.SYQ!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"A"X)!9!B+!'!'=A"A!5)!9!Z#!'!!!!"`````Y!!!!'!!!!"A!!!!9!!!!'!@`]"A%!"!9"!Q1'!1N%"A=@Z!9"$-1'!4BU"A%Y&gt;Q9"$-1'!2`E"A=,2!9"!Q1'!1!%"A(``!9!!!!'!!!!"A!!!!9!!!!(`````!!!#!0`````````````````````YC)C)C)C)C)C)C)C)C)C0_)C)C)C0`Y``C0C)C)C)D`C)C)C)_)C0C0DYC)C)C)`YC)C)C)`YD`_)_)C)C)C0_)C)C)C)DY_)C0C)C)C)D`C)C)C)``C0C)DYC)C)C)`YC)C)C)C)C)C)C)C)C)C0``````````````````````T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-T-T-T-T-T-T0`-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T-T-T-T-T-T-`]T-T-T``````````-T-T0`-T-T-]!!!!!!!!0T-T-T`T-T-T0!!!!X1!!$]T-T-`]T-T-TQ!!].U0!!`-T-T0`-T-QT-!$&gt;X&gt;X&gt;!0T-T-T`T-T-T0!!$&gt;!.U!$]T-T-`]T-T-TQ$&gt;U!!.X1`-T-T0`-T-T-]!X&gt;!!$&gt;U$-]T-T`T-T-T0!!$&gt;!.U!$]T-T-`]T-T-TQ!.X&gt;X&gt;U!`-T-T0`-T-RG9!!0$&gt;$Q!0T-T-T`T-T-T0!!!!X1!!$]T-T-`]T-T-TQ!!!!!!!!`-T-T0`-T-T-``````````T-T-T`T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-T-T-T-T-T-T0`-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T-T-T-T-T-T-``````````````````````!!!%!0```````````````````````````````````````````X&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=@``=8&amp;R=8&amp;R=8&amp;R=@```X(```^R=@^R=8&amp;R=8&amp;R=8&amp;R``^R=8&amp;R=8&amp;R=8(`=8&amp;R=@^R=@^R`X&amp;R=8&amp;R=8&amp;R=8(``X&amp;R=8&amp;R=8&amp;R=8(``X&amp;R````=8(`=8&amp;R=8&amp;R=8&amp;R=@``=8&amp;R=8&amp;R=8&amp;R=8&amp;R`X(`=8&amp;R=@^R=8&amp;R=8&amp;R=8&amp;R``^R=8&amp;R=8&amp;R=8(```^R=@^R=8&amp;R`X&amp;R=8&amp;R=8&amp;R=8(``X&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=@````````````````````````````````````````````]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML````````````````````+SML+SML+```+SML+SML+SP`!!!!!!!!!!!!!!!!!0]L+SML+SML``]L+SML+SML+`]!!!!!!!#"_A!!!!!!`SML+SML+SP``SML+SML+SML`Q!!!!$]!)'"!0Q!!!$`+SML+SML+```+SML+SML)S-D!!!!A9'"A9'"A9%!!0]L+SML+SML``]L+SML+SML+`]!!!!!A9%!!)'"!!!!`SML+SML+SP``SML+SML+SML`Q!!_I'"!!!!!)'"A1$`+SML+SML+```+SML+SML+SP`!!#"A9%!!!!!A9([!#-D)SML+SML``]L+SML+SML+`]!!!!!A9%!!)'"!!!!`SML+SML+SP``SML+SML+SML`Q!!!)'"A9'"A9'"!!$`+SML+SML+```+SML+SMLUN,3!!!!!0Q!A9%!`!!!!0]L+SML+SML``]L+SML+SML+`]!!!!!!!$[A1!!!!!!`SML+SML+SP``SML+SML+SML`Q!!!!!!!!!!!!!!!!$`+SML+SML+```+SML+SML+SP```````````````````]L+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP```````````````````````````````````````````]!!!!-!!&amp;'5%B1!!!!!!!$!!!#AQ!!"/FYH+W547A4523&amp;\QV4?1ENPKGN&lt;;!BM5RD%1/CK$8Y5_SLN"K+%A/OKM(%(SB'EN1)1LM:J!%,9D%,I3N$Q*7,5-3N"$?TM#M&amp;OYA.O(&gt;4&amp;/REP$0*Z+]9.W&lt;R'-,\THHPHM-$E,\T95=6FH2!PE-@)2V=-1U"3A%'^:``+@!)`A9==+-/EST#NRR6(.'B.[&lt;ZW4'2B2_UW`BGP)#8C(S&lt;NO\D&lt;B*T[&lt;!`JAX*FZ5S6^[/+.E?7V5'$V`&amp;KO/'YPX*6N5%'9)[&lt;KZS!+O!YJ!EK;.8IQ`CKG,_[QQQNS8JV)%,L3_JF!_4)FF`M#2R(P+9M#7"*-?B7#QW)&lt;E'_;VD4*B-HG1X=,Y,US]U4UIJ(\59F]71TY&lt;N)\T6.@0M*N3*(B!;*Z3YR`;6]\;8]/K4&amp;F?J6)CDN=\&gt;UW&amp;!+6^E&lt;L9&gt;_^L4&gt;UV^"QB9OMG-,]9&lt;?)_PZ4ET"7NP0Q5R&amp;E1?JO_Q$E&gt;5T8%,*.PL/9;M'#1\BL-5!V[S9H#+,)&gt;G$GP`S%&amp;)J6(0V-*C+BV0_B*X@,=8IKG5\W(S`K.I/O[,2&gt;02P1G&gt;%ZLTB(F\U]QK"QS#"-^QJH8;#3A5#D1!7JPI?5+(F(+$E_OXM3=?;U\=&gt;'V/\A*.4PS[@N?=XFD198=7GGU^4GW6???"WNJ[]P_X^236;+GTL1)S'/H3P.-VK+7N':*&gt;1&gt;'&amp;G3"GO;WN'@*:M8W[N@8-XL:G&lt;+^'7X/Z8"N(-Q]WWCIBVNKK&lt;BK\RCZVFM9LQ2/=M4I,TLG7E\/NDYO.RY7&amp;&amp;7%Q^^]X]'ES7^:B5%TV@_+TJ.&lt;,:NEU8[&gt;5[4E[S.&lt;:4OFTY`EJB2KEOAE*P++]]L,B0\M6:UA!!!!!"!!!!#!!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!##!!!!!9!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!&lt;B=!A!!!!!!"!!A!-0````]!!1!!!!!!5A!!!!-!&amp;%!Q`````QJ$5S"$;'&amp;O&lt;G6M!!!51$$`````#E2B&gt;'%A9HFU:8-!!#*!5!!#!!!!!2:45%EA6X*J&gt;'5A5G6B:#ZM&gt;G.M98.T!!!"!!)!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!*2=!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!!!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!W'=-6A!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$9:QR7!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!%)8!)!!!!!!!1!)!$$`````!!%!!!!!!#9!!!!"!"Z!5!!!&amp;F.133"8=GFU:3"3:7&amp;E,GRW9WRB=X-!!!%!!!!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'2=!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!!!!!%!!)!#!!!!!1!!!"!!!!!+!!!!!)!!!1!!!!!&amp;A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$I!!!";(C=D9`.3A-R&amp;)7`.,5`U[KV"8&gt;#&amp;CZ=O@%&amp;"A3B5'3II#Y&lt;GUQ:#)T-J-7FD_FD["NY/[U)OCE(,LF@\DUH!3:YPDZ7QU&gt;!*\0J`@.VW)4CZ@QBGZKHKID?T,VV!J@"VP8Y$V\'Q&amp;7;Q=6N7.@26[&lt;-44.J8KNC9W8/W7AZ%J(A1(WCJ.(\FBMZ^N-O/A]L?OG\,O&gt;R"XS:-R*P,?-&gt;OHLN=J*NFIZP4L9T7P4I=XF1@IP'2IJ#VB93X?:/4!R$DA6O&gt;3*B0V*\`3?`.[?.&gt;B^2D-12?2!3VB9[Y)SRV-%X"#E[2Q!!!'Q!!1!#!!-!"!!!!%A!$Q)!!!!!$Q$9!.5!!!"2!!]#!!!!!!]!W!$6!!!!7A!0!A!!!!!0!.A!V1!!!'-!$Q1!!!!!$Q$9!.5)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65F35V*$$1I!!UR71U.-1F:8!!!2!!!!"$M!!!!A!!!1Y!!!!!!!!!!!!!!!)!!!!$1!!!1I!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!!!!!"W%2'2&amp;-!!!!!!!!"\%R*:(-!!!!!!!!#!&amp;:*1U1!!!!!!!!#&amp;(:F=H-!!!!%!!!#+&amp;.$5V)!!!!!!!!#D%&gt;$5&amp;)!!!!!!!!#I%F$4UY!!!!!!!!#N'FD&lt;$1!!!!!!!!#S'FD&lt;$A!!!!!!!!#X%R*:H!!!!!!!!!#]%:13')!!!!!!!!$"%:15U5!!!!!!!!$'&amp;:12&amp;!!!!!!!!!$,%R*9G1!!!!!!!!$1%*%3')!!!!!!!!$6%*%5U5!!!!!!!!$;&amp;:*6&amp;-!!!!!!!!$@%253&amp;!!!!!!!!!$E%V6351!!!!!!!!$J%B*5V1!!!!!!!!$O&amp;:$6&amp;!!!!!!!!!$T%:515)!!!!!!!!$Y!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!,!!!!!!!!!!!`````Q!!!!!!!!$1!!!!!!!!!!$`````!!!!!!!!!/1!!!!!!!!!!0````]!!!!!!!!!\!!!!!!!!!!!`````Q!!!!!!!!%9!!!!!!!!!!$`````!!!!!!!!!3!!!!!!!!!!!0````]!!!!!!!!"3!!!!!!!!!!!`````Q!!!!!!!!'5!!!!!!!!!!$`````!!!!!!!!!;1!!!!!!!!!"0````]!!!!!!!!$$!!!!!!!!!!(`````Q!!!!!!!!-=!!!!!!!!!!D`````!!!!!!!!!SQ!!!!!!!!!#@````]!!!!!!!!$0!!!!!!!!!!+`````Q!!!!!!!!.-!!!!!!!!!!$`````!!!!!!!!!VQ!!!!!!!!!!0````]!!!!!!!!$&gt;!!!!!!!!!!!`````Q!!!!!!!!/)!!!!!!!!!!$`````!!!!!!!!"!Q!!!!!!!!!!0````]!!!!!!!!'%!!!!!!!!!!!`````Q!!!!!!!!I5!!!!!!!!!!$`````!!!!!!!!#C1!!!!!!!!!!0````]!!!!!!!!-L!!!!!!!!!!!`````Q!!!!!!!!SU!!!!!!!!!!$`````!!!!!!!!$,Q!!!!!!!!!!0````]!!!!!!!!-T!!!!!!!!!!!`````Q!!!!!!!!UU!!!!!!!!!!$`````!!!!!!!!$4Q!!!!!!!!!!0````]!!!!!!!!03!!!!!!!!!!!`````Q!!!!!!!!^1!!!!!!!!!!$`````!!!!!!!!$VA!!!!!!!!!!0````]!!!!!!!!0B!!!!!!!!!#!`````Q!!!!!!!""Q!!!!!"*45%EA6X*J&gt;'5A5G6B:#ZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!AJ-35Z9,GRW&lt;'FC&amp;F.133"8=GFU:3"3:7&amp;E,GRW9WRB=X.16%AQ!!!!!!!!!!!!!!!!!!=!!1!!!!!!!!%!!!!"!"B!5!!!%52*4S"8=GFU:3ZM&gt;G.M98.T!!%!!!!!!!!!!!!!!!!#%EVP:(6M:3"$&lt;X*F,GRW&lt;'FC=!R598.L,GRW9WRB=X.16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!!#!!!!!1!91&amp;!!!"&amp;%35]A6X*J&gt;'5O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!"`````A!!!!!!!!)347^E&gt;7RF)%.P=G5O&lt;(:M;7*Q$&amp;2B=WMO&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!!!!1!!!!!!!!-!!!!"!"B!5!!!%52*4S"8=GFU:3ZM&gt;G.M98.T!!%!!!!!!!(````_!!!!!!!!!B*.&lt;W2V&lt;'5A1W^S:3ZM&gt;GRJ9H!-6'&amp;T;SZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!!!"!!!!!!!!"!!!!!%!'%"1!!!22%F0)&amp;&gt;S;82F,GRW9WRB=X-!!1!!!!!!!@````Y!!!!!!!!#%EVP:(6M:3"$&lt;X*F,GRW&lt;'FC=!R598.L,GRW9WRB=X.16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!!!!!!!!1!91&amp;!!!"&amp;%35]A6X*J&gt;'5O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!"`````A!!!!!!!!)347^E&gt;7RF)%.P=G5O&lt;(:M;7*Q$&amp;2B=WMO&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!!!!1!!!!!!!1!!!!!$!"2!-0````]+1V-A1WBB&lt;GZF&lt;!!!&amp;%!Q`````QJ%982B)'*Z&gt;'6T!!"J!0(9:QP_!!!!!QJ-35Z9,GRW&lt;'FC&amp;F.133"8=GFU:3"3:7&amp;E,GRW9WRB=X-35V"*)&amp;&gt;S;82F)&amp;*F971O9X2M!#R!5!!#!!!!!2V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!)!!!!#``````````]!!!!!!!!!!!!!!!!!!B*.&lt;W2V&lt;'5A1W^S:3ZM&gt;GRJ9H!-6'&amp;T;SZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!!!"!!!!!!!#!!!!!!%!:1$RW'=-6A!!!!-+4%F/7#ZM&gt;GRJ9B:45%EA6X*J&gt;'5A5G6B:#ZM&gt;G.M98.T%F.133"8=GFU:3"3:7&amp;E,G.U&lt;!!I1&amp;!!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!!!!!!!!!!!!!!#%EVP:(6M:3"$&lt;X*F,GRW&lt;'FC=!R598.L,GRW9WRB=X.16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!1!!!!H2'FH;7RF&lt;H2@151S8UZ*,GRW&lt;'FC/E2*4S"8=GFU:3ZM&gt;G.M98.T!!!!(%R*4FAO&lt;(:M;7)[2%F0)&amp;&gt;S;82F,GRW9WRB=X-!!!!22%F0)&amp;&gt;S;82F,GRW9WRB=X-!!!!75V"*)&amp;&gt;S;82F)&amp;*F971O&lt;(:D&lt;'&amp;T=Q</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"Z!!!!!B*.&lt;W2V&lt;'5A1W^S:3ZM&gt;GRJ9H!-6'&amp;T;SZM&gt;G.M98.T5&amp;2)-!!!!%U!!1!*!!!!!!!9)5.P&lt;8"J&lt;'6E)%:S97VF&gt;W^S;S"-;7*T%EVP:(6M:3"$&lt;X*F,GRW&lt;'FC=!J598.L8W.M98.T$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="SPI Write Read.ctl" Type="Class Private Data" URL="SPI Write Read.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Data Types" Type="Folder">
		<Item Name="Constructor Data.ctl" Type="VI" URL="../Constructor Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="Return Data.ctl" Type="VI" URL="../Return Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0(7P:DP!!!!!Q]B6%6.5%R"6%5O&lt;(:M;7)7)62&amp;46"-162&amp;8V2"5UMO&lt;(:D&lt;'&amp;T=RR$&lt;WZT&gt;(*V9X2P=C"%982B)*=A[O\P[0]O9X2M!"J!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082401280</Property>
		</Item>
		<Item Name="Used Local Variables.ctl" Type="VI" URL="../Used Local Variables.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0(7P:DP!!!!!Q]B6%6.5%R"6%5O&lt;(:M;7)7)62&amp;46"-162&amp;8V2"5UMO&lt;(:D&lt;'&amp;T=RR$&lt;WZT&gt;(*V9X2P=C"%982B)*=A[O\P[0]O9X2M!"J!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082401280</Property>
		</Item>
		<Item Name="Used Sequence Variables.ctl" Type="VI" URL="../Used Sequence Variables.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0(7P:DP!!!!!Q]B6%6.5%R"6%5O&lt;(:M;7)7)62&amp;46"-162&amp;8V2"5UMO&lt;(:D&lt;'&amp;T=RR$&lt;WZT&gt;(*V9X2P=C"%982B)*=A[O\P[0]O9X2M!"J!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082401280</Property>
		</Item>
		<Item Name="Created Sequence Variables.ctl" Type="VI" URL="../Created Sequence Variables.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="Get Data Types.vi" Type="VI" URL="../Get Data Types.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'`!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!11&amp;-,5G6U&gt;8*O)%2B&gt;'%!'E"4&amp;&amp;6T:71A4'^D97QA6G&amp;S;7&amp;C&lt;'6T!!!=1&amp;-868.F:#"4:8&amp;V:7ZD:3"798*J97*M:8-!)%"4'E.S:7&amp;U:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!!!=1&amp;!!"1!&amp;!!9!"Q!)!!E+2'&amp;U93"5?8"F=Q!!.E"Q!"Y!!#-+4%F/7#ZM&gt;GRJ9B:45%EA6X*J&gt;'5A5G6B:#ZM&gt;G.M98.T!!B598.L)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!U1(!!(A!!)QJ-35Z9,GRW&lt;'FC&amp;F.133"8=GFU:3"3:7&amp;E,GRW9WRB=X-!"V2B=WMA;7Y!6!$Q!!Q!!Q!%!!I!#Q!%!!1!"!!%!!Q!"!!%!!U#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!/!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574592</Property>
	</Item>
	<Item Name="Get Hierarchy Path.vi" Type="VI" URL="../Get Hierarchy Path.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!=1%!!!@````]!"1Z);76S98*D;(EA='&amp;U;!!!.E"Q!"Y!!#-+4%F/7#ZM&gt;GRJ9B:45%EA6X*J&gt;'5A5G6B:#ZM&gt;G.M98.T!!B598.L)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!U1(!!(A!!)QJ-35Z9,GRW&lt;'FC&amp;F.133"8=GFU:3"3:7&amp;E,GRW9WRB=X-!"V2B=WMA;7Y!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!%!!E$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!+!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Get Properties.vi" Type="VI" URL="../Get Properties.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'V!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!)1&gt;)98-A2F!`!":!-0````]-68.F=C"'5#"/97VF!!!51$$`````#U2F=W.S;8"U;7^O!&amp;A!]1!!!!!!!!!$%EVP:(6M:3"$&lt;X*F,GRW&lt;'FC=!R598.L,GRW9WRB=X-46'&amp;T;S"1=G^Q:8*U;76T,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ1=G^Q:8*U;76T!!!W1(!!(A!!)QJ-35Z9,GRW&lt;'FC&amp;F.133"8=GFU:3"3:7&amp;E,GRW9WRB=X-!#&amp;2B=WMA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!D#ER*4FAO&lt;(:M;7)75V"*)&amp;&gt;S;82F)&amp;*F971O&lt;(:D&lt;'&amp;T=Q!(6'&amp;T;S"J&lt;A"5!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!Q!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
	</Item>
	<Item Name="Constructor.vi" Type="VI" URL="../Constructor.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;_!!!!#Q!%!!!!)%"4'E.S:7&amp;U:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!!!;1&amp;-54G6X)%.P&lt;H.U=H6D&gt;'^S)%2B&gt;'%!!$*!=!!?!!!D#ER*4FAO&lt;(:M;7)75V"*)&amp;&gt;S;82F)&amp;*F971O&lt;(:D&lt;'&amp;T=Q!%6'&amp;T;Q!!%E!B$6.B&gt;G5A9WBB&lt;G&gt;F=T]!)%"1!!%!""&gt;$&lt;WZT&gt;(*V9X2P=C"$&lt;'^T:3"&amp;&gt;G6O&gt;!!C1(!!'1!"!!581W^O=X2S&gt;7.U&lt;X)A1WRP=W5A28:F&lt;H1!,%"Q!"=!!!!"!!%!!!0I!!9:28:F&lt;H1A5G6H;8.U=G&amp;U;7^O)&amp;*F:GZV&lt;1!=1&amp;-868.F:#"4:8&amp;V:7ZD:3"798*J97*M:8-!&amp;E"4%%.P&lt;H.U=H6D&gt;'^S)%2B&gt;'%!!&amp;1!]!!-!!!!!!!"!!)!!!!!!!!!!Q!!!!=!#!!*!Q!!?!!!!!!!!!!!!!!*!!!!#1!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!%!!!!")!!!!3!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">256</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="Execute.vi" Type="VI" URL="../Execute.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'B!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!=1&amp;!!!Q!!!!%!!AZ3:82V=GZF:#"&amp;=H*P=A!!"!!!!"*!5QV3:82V=GZF:#"%982B!$:!=!!?!!!D#ER*4FAO&lt;(:M;7)75V"*)&amp;&gt;S;82F)&amp;*F971O&lt;(:D&lt;'&amp;T=Q!)6'&amp;T;S"P&gt;81!!":!-0````]-68.F=C"'5#"/97VF!!!-1#%(5WBP&gt;S"'5!!71&amp;-11W^O=X2S&gt;7.U&lt;X)A2'&amp;U91!!&amp;%"4$URP9W&amp;M)&amp;:B=GFB9GRF=Q!91&amp;-35W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!!!?1&amp;!!!Q!*!!I!#R""9X2V97QA6'&amp;T;S"%982B!!!U1(!!(A!!)QJ-35Z9,GRW&lt;'FC&amp;F.133"8=GFU:3"3:7&amp;E,GRW9WRB=X-!"V2B=WMA;7Y!6!$Q!!Q!!Q!%!!5!"A!(!!1!#!!%!!1!"!!-!!U$!!"Y!!!*!!!!!!!!!!E!!!#.#Q!#%!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!!%!!!!*)!!!!!!1!/!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="Form SPI packet.vi" Type="VI" URL="../Form SPI packet.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
	</Item>
</LVClass>
