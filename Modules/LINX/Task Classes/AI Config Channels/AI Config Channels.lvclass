﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassBodyIcon" Type="Str">Actor</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6618880</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">LINX.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../LINX.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,&amp;!!!*Q(C=T:1^&lt;J."%)&lt;@$V(1_A")S'YC)3("&gt;&amp;1%,.(2_1ID5&lt;JSC&gt;UZYA2T"2==A&amp;3BI:EG[8W&amp;^)G1T,0LD5G)@Y2%%,O?\_?&gt;H&gt;HHGVWP6.M4[&lt;&amp;7B^JY&lt;[P_V&lt;C-K\:Z'[^O_^NP8)?N8_\\GXQ`@PQL@LQ*X2+`OO.@L7\&amp;\_9`V*\P&lt;@`/PRXLFF_XWU\`\(K@`Y&gt;G-S\X`90"I!3XND/_O)JN^V^86]GSX4_\]6_,_@Y]`R[_CYO,P&gt;_XK=`V&lt;(`^&gt;N@XQ0JM&lt;Q`P0^$YFOHB0]($.KF4/9CQR!*TT&amp;2=KU20^%20^%20^%!0^%!0^%!0&gt;%&gt;X&gt;%&gt;X&gt;%&gt;X&gt;%-X&gt;%-X&gt;%-X^.L2B3ZUI6/6:0*EIC2JEC!:$)K33]+4]#1]#1_P3HA3HI1HY5FY'++%*_&amp;*?"+?B)=Q*4Q*4]+4]#1]J+IE74M[0!E0[28Q"$Q"4]!4]$#F!JY!)*AM3"QE!5/"-XA)?!+?A)&gt;("4Q"4]!4]!1]O"8Q"$Q"4]!4]""3KR+6JH2U?%ADB]@B=8A=(I?(V(*Y("[(R_&amp;R?*B/$I`$YU!Y%TL*1:!4Z!RQ8BQ?BY?&lt;("[(R_&amp;R?"Q?8(7&amp;P&amp;;GU*3/$I`"9`!90!;0Q5-+'4Q'D]&amp;D]"A]J*8"9`!90!;0Q=.5-HA-(I0(!$%G:8I:S9R!9Z!B'$T][G[ROEJ23;TW=E45T;OW+&gt;5WG^IG5NM=;IOONJBKC[27@,7CKB6,L1BK([='L1;D.IF;="GI3[Z,\"2&lt;9(.MAIWQ)&gt;&lt;(?C8U,Q_]P,T5=LH5[?GJ&amp;IO&amp;ZP/Z*J/*2K/2BM/B_PW_?LX?ZF2^2F_X\O:=GP,]_?O,YZ0"R_/46V_/:`WDYZ082_/CX`DPW+&gt;6.XX497@&gt;^/VZ.`N_V5X@P?SG(ZY7/[P0\[_[W&lt;@T^:ASNM4]FG&gt;^,PU0:[-?;8X@V/AHE/&gt;!Q1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"M&lt;5F.31QU+!!.-6E.$4%*76Q!!&amp;K!!!!2\!!!!)!!!&amp;I!!!!!K!!!!!AJ-35Z9,GRW&lt;'FC'E&amp;*)%.P&lt;G:J:S"$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!!!!!#A&amp;Q#!!!!Q!!!)!!1!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!-^&lt;EDAO;+:,HA!IQ%%7/#]!!!!1!!!!%!!!!!!_QH49\_TO3+MI+=?76,4&amp;V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!G]=VNN.E0U3AC(RB\X/F9Q%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"#@=@W!;)P\_C0&amp;B5]R8X#I!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!)1!!!"BYH'.A9W"K9,D!!-3-$EQ.4"F!VA?'!!9!0[%&amp;FA!!!!!!!")!!!!+?*RD9'&lt;A!%-'!!#&gt;!"Q!!!!!!%A!!!%9?*RD9-!%`Y%!3$%S-$"&gt;!.)M;/*A'M;G*M"F,C[\I/,-5$?SQI3"\NY$J*F!=F!V5$]QH1(C%_DG]%0J"UBC!)M4+1U!!!!-!!&amp;73524!!!!!!!$!!!"H!!!!WRYH.P!S-#1;7RBRM$%Q-!-:)MT.$!EZ[?E]D)!_1Q1I!.D5!!#I/:JI9E&lt;(DC="A2[`0)N9(\T'ZZO&amp;R7"ZBI6#;:3E7Y@&amp;:&amp;/(R774B;6&amp;X`_```@@)4H=,&gt;(TH&amp;('Z$;&lt;A[A_(%8&amp;1Y1"UCTA/D`A2EA6;DGS81#:9'U"*)'O)%I^A=!68%U6#ATF,!9(IA[@,T"B"(C5*A4IL#ZFXDTG^^Q!$UF=0!B3X?D"J$@/R&amp;%!I6Y/E-Y*)[\=/C)!@G-*U!'&gt;P,!@-U"^U]9S)!3&amp;9&amp;/%Z"&amp;,)QQC\L:DDNIA-0"112#:5#I#AB6!+*WA&amp;VQB#0O-$T]V\[_NYM63,-BR9E$%$?!'%SI7)_"E9%2T'2E7!N6;Q.E-U(&amp;9(%,9CN!AYW2Q2[OZT:58A0*("&gt;'G"[%OGIE&gt;T#"T7"E_--!-Q^I(V20!^4&gt;)$&amp;@I.A"+$M%S*Y!:5=$W2_A\#1A7Q$+TA3S$2AB\$QI'WQ:!W\;W&gt;`&amp;&amp;3G9Q0E#FD6!_35ZN]$!1+][WF!H/,97S/="C25EF]'%!!6^E;E!!!$[!!!"9(C==W"A9-AUND"D9'2A9!:C=99'BO4]F&amp;1'*'$#S)!4B!;(BT7`5?HW5&lt;(I^&amp;&amp;2[(:2M?FU5:(JZGQ^73,9&lt;&gt;FNXFGC)A(%!MX(76[,((&gt;2%1$J!N)3).LQG_(`&lt;I&lt;7!S83X2,&gt;T*9@/L`7MA'V3X1?&lt;4\-IHE-J!_M#;,Y\7O/#5"RI%G_L7`L&amp;,L:O\G"&gt;L*UMKC]_00````G(YQF$Y!%`Z1$`.M/P":I`1:GH&gt;A"=D[`[](G)SJR]&gt;'RBV(=P`&lt;VP6UA?71P/A#R%I-%7)Q*C.O2R%(!W&gt;`&amp;&amp;4V]1'JZA4AZN]$!1+][WF!H/,97S!=!KG63QQ!!!!!"+1!!!&lt;2YH(.A9'$).,9Q-W"E9'!'9H''"I&lt;E`*25"C41Q]C!%Y3(.&lt;`2[8:2M?BU5&gt;(I^F'R[@225?EW;0V7)NBNVOP'!B37[2&lt;L$&amp;&amp;2?-VVX%6&amp;!;4(]&amp;)X9_O"TO-F,.U7X@;7PUK-?I%S1*5C1&amp;I%30-!$?%!KG9"78S=217E[=7@````NRYI41&gt;;JN,.U(KAB,7&lt;I@E!S!)*I,U+1#U#L3@LX)"MG&lt;1U08\Z&amp;L$\ODG"#FA[?64!_DO0N8YL615J;4\)!N1B!T2-!G:)C)K!&amp;^#B!KW8[E];@OP]U(R%*S\_-*J`V\[_NYM:3#-(C1-1:Q&amp;&amp;1'*-1,Q+&amp;GY-"G!R%)9&amp;;!33OCQE`3$A\/`CCB\O),5]1*R=E&amp;SG6RVNK"-=7QM!;4FAGQ!!!!!!!!Q8!)!2!!!%-4=O-!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$"=!A"%!!!1R.SYQ!!!!!!Q8!)!!!!!%-4=O-!!!!!!-&amp;Q#!%1!!"$%X,D!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!',OCZ"C3.J19ESLE'")CB"C4II=9!!!!(`````A!!!!9!!!!'!!!!"A!!!!9"``Q'!1!%"A%$"!9"#U1'"R`E"A%-R!9"/(1'!4BX"A%-R!9"(_1'"QN%"A%$"!9"!!1'!@`]"A!!!!9!!!!'!!!!"A!!!!@````]!!!)!``````````````````````C)C)C)C)C)C)C)C)C)C)`YC0D`_0`Y_)DY``C0C)C0_)DYDYDYC0_0_0C0DYC)D`C)_)_)`YDY_0D`_)_)C)`YC)C0C0C)_)DY_)C0C)C0_)DYDYD`_0C)_0C)D`_)D`C)C)C)C)C)C)C)C)C)C)```````````````````````-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-T-T-T-T-T-T0`-T-T-T-T-T-T-T-T-T-T`T-T-T0`````````]T-T-`]T-T-TQ!!!!!!!!`-T-T0`-T-T-]!!!$&gt;!!!0T-T-T`T-T-T0!!$QX1]!$]T-T-`]T-T$-Q!.X&gt;X&gt;U!`-T-T0`-T-T-]!!.U!X1!0T-T-T`T-T-T0!.X1!!X&gt;$]T-T-`]T-T-TQ$&gt;U!!.X1-TT-T0`-T-T-]!!.U!X1!0T-T-T`T-T-T0!!X&gt;X&gt;X1$]T-T-`]T-T':A!!].U0!!`-T-T0`-T-T-]!!!$&gt;!!!0T-T-T`T-T-T0!!!!!!!!$]T-T-`]T-T-T``````````-T-T0`-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-T-T-T-T-T-T0`-T-T-T-T-T-T-T-T-T-T`````````````````````]!!!1!````````````````````````````````````````````=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R``^R=8(`=@```X(```^R`X&amp;R=@^R````=8(`=8&amp;R=8(``X&amp;R=@^R=@^R=@^R=8(``X(``X(`=8(`=@^R=8&amp;R=@``=8&amp;R`X&amp;R`X&amp;R``^R=@^R`X(`=@```X&amp;R`X&amp;R=8&amp;R``^R=8&amp;R=8(`=8(`=8&amp;R`X&amp;R=@^R`X&amp;R=8(`=8&amp;R=8(``X&amp;R=@^R=@^R=@```X(`=8&amp;R`X(`=8&amp;R=@```X&amp;R=@``=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R`````````````````````````````````````````````SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SP```````````````````]L+SML+SML``]L+SML+SML+`]!!!!!!!!!!!!!!!!!`SML+SML+SP``SML+SML+SML`Q!!!!!!!)([!!!!!!$`+SML+SML+```+SML+SML+SP`!!!!!0Q!A9%!`!!!!0]L+SML+SML``]L+SML+SMD)S-!!!#"A9'"A9'"A1!!`SML+SML+SP``SML+SML+SML`Q!!!!#"A1!!A9%!!!$`+SML+SML+```+SML+SML+SP`!!$[A9%!!!!!A9'"!0]L+SML+SML``]L+SML+SML+`]!!)'"A1!!!!#"A@I!)S-D+SML+SP``SML+SML+SML`Q!!!!#"A1!!A9%!!!$`+SML+SML+```+SML+SML+SP`!!!!A9'"A9'"A9%!!0]L+SML+SML``]L+SML+SP3UN)!!!!!`!#"A1$]!!!!`SML+SML+SP``SML+SML+SML`Q!!!!!!!0K"!!!!!!$`+SML+SML+```+SML+SML+SP`!!!!!!!!!!!!!!!!!0]L+SML+SML``]L+SML+SML+````````````````````SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+````````````````````````````````````````````Q!!!!Q!!5:13&amp;!!!!!!!!-!!!3V!!!,*XC=L6:,&lt;"N6&amp;,VP0);R]_H9L?.93GL(':M).41KBJ;)4UEG":@7*$7"NE"&lt;%[?EEIGLW#E)2.P&amp;+&amp;+%OI#),*!KM9ACM?ICKNBE53'X))U189&amp;IE&gt;RGA=1#Q39+#BU0^\XRD'?=R+F1P8B[MN[ZZ\ZTTZQ:A+:OM:WLQ%5.C,C+GS-;?,-K!3DV#F$^R7&gt;!(#8`!NE6)BI=&amp;%&lt;&amp;OVS&amp;&gt;'D1H&amp;8D1J]]#X`D;@W?`A6]39BY(Y]_*I;QG&amp;?$(6EV[$MMF58J7I=U[T;L_K"4P%)KX(%JP#:=5@*)#%I088W^J!*%\O*Z*4K=G2R8*0KPJV=)M:)?$52:&lt;:G3SE^A2;4_DJ5EJW#"Z-W3A#6\9'FJK1&lt;S';!Y;_-!R3RAW?PE6!/-8V9\#V*Z$].Y'1:ZLJM]=LAS2XOHI(LI4FE6%9KYD]QL,ZB==FA\S(!L+SO)Q\7+G^"AFV1?%%,#`?Q&gt;&gt;]O)]CU1)+84AP[,`AX&amp;_V*U#OSM(Q=2[S&gt;C'P&gt;J$:Z56/Y-]#&lt;85:BD9_$.-4S09S#(W"A]]KQ)N4H-&lt;4-(G3^&amp;/Q&gt;TUY8C_&amp;1E@T9SFMM5#J(T5_=O:)LDE7SGG.EYI2&gt;EV&lt;/@XJ[3-8.!!(B)QHG\WHF98&amp;R%!8#N16^%;&amp;!K7TA@OUX25DR&lt;5ZSSVJ2\#:74`XHD@;J?L*^T?H:PT&lt;0\U,-_M&lt;YNBW?@?@3?@2;N&gt;,(/MZ#$"(T;Q(`\$:$.MQF%$5'O!?9!9CYZ0*N!HC'4JZ&amp;HH^PIW94*:8FW@H\?A50F_SX0]I19HF6O[Q`U"^3Z;`I@S0[:T&lt;H(="QU,P\#_NV9HULP-;5@X.+H!YWVD`"!^3^&amp;GVZ/2A9H-J/4Y\G#-1=R:H$D/:"[#,9;MU9TL)'=6:N$(]PF])"U&lt;&lt;=UG[#5IPYD\%%E\=X.$N\4Y&amp;"7Z5/9)6[&lt;L2`(TNJBLSG3\G-CL;_PIUCY'MUJ88#:ZR3*`L.J-UIR^F4@W7J,T2K])KOB@6,:R9ACB"+VYE5[I"7,,S]P9X&amp;=@7%MR&gt;&amp;3LEAJ&lt;/D5CE=C\%A88=6I&gt;?DP#6&amp;;@O.2/M]OODK0?M+V&lt;FYVOO&amp;9.XX73&amp;L`@T&gt;D$^`.G+-&lt;&gt;'E3O`H1&amp;B!O;%&amp;BHNY_)0#"(IC&gt;A.B*Q*AY:]3%+T\$V5I@R&gt;+HJ@*OP'DVM4&gt;]Y(+GVC;04AJ4K_!-P#!G3R^-V$FDE]2\X9I'DRE.18CLBD5=8U=Y&lt;%7$JRI.94P@JJA2+RI]V7A)/XF7N\L?-&lt;T?K%XTH=ZIW&amp;LTN&amp;XT#5.T0D\D9K_N%1X?R-A"OVK/($Z/=TDF#!0^4Q#^!NX&lt;B!%(^-7F2&amp;/:$_L$O%G$%TBH$+U&gt;2FG"]LO2XQ&gt;?P-7P&gt;X\$[U`0@-U-T6/8UDOO`7!YFOW`N_VPW@9X&lt;6:&amp;HJ-'TQ7$ZQ&lt;FY:!HQ(C1Y:(QI%ZP'Q^IE]V^0/-Z]R#ZR$@)J:I$XE%+_WO:`E+1X&gt;Y"\^I&gt;=$0W#7?9Q)UG%.,RS`$\Z`Y!:::#Y!XYW]RNGT^I&lt;I.YW:4N#24OXJKW0EW&amp;N#4LD1[)1^D(*1U#]K$`*T'*2:O&amp;J$!E8M7P!@QQ;"/O#KOFH[W0V^)2#[H=BDRZ4@IK,,4`"`&amp;`+&lt;1!!!!!!!!%!!!!/A!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!+1!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!"'&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!!K!!!!!1!C1&amp;!!!"J"33"$&lt;WZG;7=A1WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!!!1!!!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!#E8!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!%!!!!!!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$99VP+!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.BD7]I!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!!&lt;B=!A!!!!!!"!!A!-0````]!!1!!!!!!5A!!!!-!$E!Q`````Q2/97VF!!!91%!!!@````]!!!N"33"$;'&amp;O&lt;G6M=Q!E1&amp;!!!1!"'E&amp;*)%.P&lt;G:J:S"$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!!"!!)!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E8!)!!!!!!!1!&amp;!!-!!!%!!!!!!!1!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!!8B=!A!!!!!!$!!Z!-0````]%4G&amp;N:1!!'%"!!!(`````!!!,15EA1WBB&lt;GZF&lt;(-!*%"1!!%!!2J"33"$&lt;WZG;7=A1WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!!!1!#!!!!!!!!!!!!!!!%!!5!#Q!!!!1!!!"&gt;!!!!+!!!!!)!!!1!!!!!#A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%R!!!"\XC=D:(,3M.1&amp;%68?GP@NP86VI+3E1-()PA$E9*1E&amp;*U)DARZF%$VU3;W_,1\`-,`!T^!DV*;BXIQ'QY._??Q]\;""D3&gt;EY`Z3F0X-=!"I[$F@810"`&lt;IQ=XDA/&gt;EP$RZNW_!KJR/:\=H/CFDO[(W5I3B^&amp;MP3E$4\NJWPNDZ"H.M40&amp;QDI9[56KALG&gt;B(;_&lt;T`.I[6L!NNXD=O'C!9_7/]I;&gt;3KZ5S;OF.&amp;B8J'T8F2S:5J,I)EJ#POCCIV[GLBBX,GY7DR(;N*+S&gt;I+@0MC_?5EN2.DP\*65*+*3M79H]H3'5OR-;GSZ:]0..W(K#17OHXT=^E*V=2U'*8(*%I/8LFWMSD?!;([T`4\U;R*ZB2%B@$.-/J1E@ISA,8FL=^?ATI3_W)^D0;,X*/;-U!!!!!!!"_!!%!!A!$!!5!!!"9!!]#!!!!!!]!W!$6!!!!91!0!A!!!!!0!.A!V1!!!'I!$Q)!!!!!$Q$9!.5!!!"T!!]%!!!!!!]!W!$6!!!!@)!!B!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!"35V*$$1I!!UR71U.-1F:8!!!7I!!!"(M!!!!A!!!7A!!!!!!!!!!!!!!!)!!!!$1!!!2E!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!"!!!"W%2'2&amp;-!!!!!!!!#!%R*:(-!!!!!!!!#&amp;&amp;:*1U1!!!!#!!!#+(:F=H-!!!!%!!!#:&amp;.$5V)!!!!!!!!#S%&gt;$5&amp;)!!!!!!!!#X%F$4UY!!!!!!!!#]'FD&lt;$1!!!!!!!!$"'FD&lt;$A!!!!!!!!$'%R*:H!!!!!!!!!$,%:13')!!!!!!!!$1%:15U5!!!!!!!!$6&amp;:12&amp;!!!!!!!!!$;%R*9G1!!!!!!!!$@%*%3')!!!!!!!!$E%*%5U5!!!!!!!!$J&amp;:*6&amp;-!!!!!!!!$O%253&amp;!!!!!!!!!$T%V6351!!!!!!!!$Y%B*5V1!!!!!!!!$^&amp;:$6&amp;!!!!!!!!!%#%:515)!!!!!!!!%(!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-!!!!!!!!!!!`````Q!!!!!!!!$5!!!!!!!!!!$`````!!!!!!!!!/A!!!!!!!!!!0````]!!!!!!!!!]!!!!!!!!!!!`````Q!!!!!!!!%=!!!!!!!!!!$`````!!!!!!!!!31!!!!!!!!!!P````]!!!!!!!!"4!!!!!!!!!!!`````Q!!!!!!!!&amp;E!!!!!!!!!!$`````!!!!!!!!!&lt;!!!!!!!!!!!0````]!!!!!!!!"Q!!!!!!!!!!"`````Q!!!!!!!!.A!!!!!!!!!!,`````!!!!!!!!"'!!!!!!!!!!"0````]!!!!!!!!&amp;E!!!!!!!!!!(`````Q!!!!!!!!7A!!!!!!!!!!D`````!!!!!!!!"&lt;!!!!!!!!!!#@````]!!!!!!!!&amp;Q!!!!!!!!!!+`````Q!!!!!!!!81!!!!!!!!!!$`````!!!!!!!!"?!!!!!!!!!!!0````]!!!!!!!!&amp;_!!!!!!!!!!!`````Q!!!!!!!!9-!!!!!!!!!!$`````!!!!!!!!"J!!!!!!!!!!!0````]!!!!!!!!)F!!!!!!!!!!!`````Q!!!!!!!!S9!!!!!!!!!!$`````!!!!!!!!$+A!!!!!!!!!!0````]!!!!!!!!2:!!!!!!!!!!!`````Q!!!!!!!"&amp;M!!!!!!!!!!$`````!!!!!!!!%81!!!!!!!!!!0````]!!!!!!!!2B!!!!!!!!!!!`````Q!!!!!!!"(M!!!!!!!!!!$`````!!!!!!!!%@1!!!!!!!!!!0````]!!!!!!!!5C!!!!!!!!!!!`````Q!!!!!!!"31!!!!!!!!!!$`````!!!!!!!!&amp;*A!!!!!!!!!!0````]!!!!!!!!5R!!!!!!!!!#!`````Q!!!!!!!"8]!!!!!":"33"$&lt;WZG;7=A1WBB&lt;GZF&lt;(-O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!AJ-35Z9,GRW&lt;'FC'E&amp;*)%.P&lt;G:J:S"$;'&amp;O&lt;G6M=SZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!!!!!!!%!!%!!!!!!!!"!!!!!1!?1&amp;!!!"9B6%6.5%R"6%6@6%&amp;43SZM&gt;G.M98.T!!!"!!!!!!!!!!!!!!)347^E&gt;7RF)%.P=G5O&lt;(:M;7*Q$&amp;2B=WMO&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!!!!1!!!!!!!!)!!!!"!"Z!5!!!&amp;C&amp;525V14%&amp;526^516.,,GRW9WRB=X-!!!%!!!!!!!(````_!!!!!!)347^E&gt;7RF)%.P=G5O&lt;(:M;7*Q$&amp;2B=WMO&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!"!"Z!5!!!&amp;C&amp;525V14%&amp;526^516.,,GRW9WRB=X-!!!%!!!!!!!(````_!!!!!!)347^E&gt;7RF)%.P=G5O&lt;(:M;7*Q$&amp;2B=WMO&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!!!!1!!!!!!!1!!!!!$!!Z!-0````]%4G&amp;N:1!!'%"!!!(`````!!!,15EA1WBB&lt;GZF&lt;(-!&lt;Q$RW'.&lt;SA!!!!-+4%F/7#ZM&gt;GRJ9BJ"33"$&lt;WZG;7=A1WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=R:"33"$&lt;WZG;7=A1WBB&lt;GZF&lt;(-O9X2M!#J!5!!"!!%&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!#!!!!!@````]!!!!!!!!!!!!#%EVP:(6M:3"$&lt;X*F,GRW&lt;'FC=!R598.L,GRW9WRB=X.16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!)!!!!G)62&amp;46"-162&amp;,GRW&lt;'FC/C&amp;525V14%&amp;526^516.,,GRW9WRB=X-!!!!B4%F/7#ZM&gt;GRJ9DIB6%6.5%R"6%6@6%&amp;43SZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"Z!!!!!B*.&lt;W2V&lt;'5A1W^S:3ZM&gt;GRJ9H!-6'&amp;T;SZM&gt;G.M98.T5&amp;2)-!!!!%U!!1!*!!!!!!!9)5.P&lt;8"J&lt;'6E)%:S97VF&gt;W^S;S"-;7*T%EVP:(6M:3"$&lt;X*F,GRW&lt;'FC=!J598.L8W.M98.T$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="AI Config Channels.ctl" Type="Class Private Data" URL="AI Config Channels.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Data Types" Type="Folder">
		<Item Name="Constructor Data.ctl" Type="VI" URL="../Constructor Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="Return Data.ctl" Type="VI" URL="../Return Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0(7P:DP!!!!!Q]B6%6.5%R"6%5O&lt;(:M;7)7)62&amp;46"-162&amp;8V2"5UMO&lt;(:D&lt;'&amp;T=RR$&lt;WZT&gt;(*V9X2P=C"%982B)*=A[O\P[0]O9X2M!"J!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082401280</Property>
		</Item>
		<Item Name="Used Local Variables.ctl" Type="VI" URL="../Used Local Variables.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0(7P:DP!!!!!Q]B6%6.5%R"6%5O&lt;(:M;7)7)62&amp;46"-162&amp;8V2"5UMO&lt;(:D&lt;'&amp;T=RR$&lt;WZT&gt;(*V9X2P=C"%982B)*=A[O\P[0]O9X2M!"J!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082401280</Property>
		</Item>
		<Item Name="Used Sequence Variables.ctl" Type="VI" URL="../Used Sequence Variables.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0(7P:DP!!!!!Q]B6%6.5%R"6%5O&lt;(:M;7)7)62&amp;46"-162&amp;8V2"5UMO&lt;(:D&lt;'&amp;T=RR$&lt;WZT&gt;(*V9X2P=C"%982B)*=A[O\P[0]O9X2M!"J!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082401280</Property>
		</Item>
		<Item Name="Created Sequence Variables.ctl" Type="VI" URL="../Created Sequence Variables.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="Get Data Types.vi" Type="VI" URL="../Get Data Types.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!((!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!11&amp;-,5G6U&gt;8*O)%2B&gt;'%!'E"4&amp;&amp;6T:71A4'^D97QA6G&amp;S;7&amp;C&lt;'6T!!!=1&amp;-868.F:#"4:8&amp;V:7ZD:3"798*J97*M:8-!)%"4'E.S:7&amp;U:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!!!=1&amp;!!"1!&amp;!!9!"Q!)!!E+2'&amp;U93"5?8"F=Q!!/E"Q!"Y!!#=+4%F/7#ZM&gt;GRJ9BJ"33"$&lt;WZG;7=A1WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!)6'&amp;T;S"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/%"Q!"Y!!#=+4%F/7#ZM&gt;GRJ9BJ"33"$&lt;WZG;7=A1WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!(6'&amp;T;S"J&lt;A"5!0!!$!!$!!1!#A!,!!1!"!!%!!1!$!!%!!1!$1)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!Y!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574592</Property>
	</Item>
	<Item Name="Get Hierarchy Path.vi" Type="VI" URL="../Get Hierarchy Path.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&lt;!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!=1%!!!@````]!"1Z);76S98*D;(EA='&amp;U;!!!/E"Q!"Y!!#=+4%F/7#ZM&gt;GRJ9BJ"33"$&lt;WZG;7=A1WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!)6'&amp;T;S"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/%"Q!"Y!!#=+4%F/7#ZM&gt;GRJ9BJ"33"$&lt;WZG;7=A1WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!(6'&amp;T;S"J&lt;A"5!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!1!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!I!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Get Properties.vi" Type="VI" URL="../Get Properties.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'^!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!)1&gt;)98-A2F!`!":!-0````]-68.F=C"'5#"/97VF!!!51$$`````#U2F=W.S;8"U;7^O!&amp;A!]1!!!!!!!!!$%EVP:(6M:3"$&lt;X*F,GRW&lt;'FC=!R598.L,GRW9WRB=X-46'&amp;T;S"1=G^Q:8*U;76T,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ1=G^Q:8*U;76T!!![1(!!(A!!*QJ-35Z9,GRW&lt;'FC'E&amp;*)%.P&lt;G:J:S"$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!B598.L)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Y1(!!(A!!*QJ-35Z9,GRW&lt;'FC'E&amp;*)%.P&lt;G:J:S"$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!&gt;598.L)'FO!&amp;1!]!!-!!-!"!!)!!E!"!!%!!1!"!!+!!1!"!!,!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!$!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
	</Item>
	<Item Name="Constructor.vi" Type="VI" URL="../Constructor.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'#!!!!#Q!%!!!!)%"4'E.S:7&amp;U:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!!!;1&amp;-54G6X)%.P&lt;H.U=H6D&gt;'^S)%2B&gt;'%!!$:!=!!?!!!H#ER*4FAO&lt;(:M;7);15EA1W^O:GFH)%.I97ZO:7RT,GRW9WRB=X-!"&amp;2B=WM!!"*!)1V498:F)'.I97ZH:8-`!#"!5!!"!!181W^O=X2S&gt;7.U&lt;X)A1WRP=W5A28:F&lt;H1!)E"Q!"E!!1!&amp;&amp;U.P&lt;H.U=H6D&gt;'^S)%.M&lt;X.F)%6W:7ZU!#R!=!!8!!!!!1!"!!!$[!!''56W:7ZU)&amp;*F:WFT&gt;(*B&gt;'FP&lt;C"3:7:O&gt;7U!(%"4&amp;V6T:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!":!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!"5!0!!$!!!!!!!!1!#!!!!!!!!!!-!!!!(!!A!#1-!!(A!!!!!!!!!!!!!#1!!!!E!!!!!!!!!!!!!!!!!!!#1!!!!!!!!!"!!!!!3!!!!%A!!!!!"!!I!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="Execute.vi" Type="VI" URL="../Execute.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'J!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!=1&amp;!!!Q!!!!%!!AZ3:82V=GZF:#"&amp;=H*P=A!!"!!!!"*!5QV3:82V=GZF:#"%982B!$J!=!!?!!!H#ER*4FAO&lt;(:M;7);15EA1W^O:GFH)%.I97ZO:7RT,GRW9WRB=X-!#&amp;2B=WMA&lt;X6U!!!71$$`````$&amp;6T:8)A2F!A4G&amp;N:1!!$%!B"V.I&lt;X=A2F!!&amp;E"4%%.P&lt;H.U=H6D&gt;'^S)%2B&gt;'%!!"2!5Q^-&lt;W.B&lt;#"798*J97*M:8-!'%"4%F.F=86F&lt;G.F)&amp;:B=GFB9GRF=Q!!(E"1!!-!#1!+!!M117.U&gt;7&amp;M)&amp;2B=WMA2'&amp;U91!!/%"Q!"Y!!#=+4%F/7#ZM&gt;GRJ9BJ"33"$&lt;WZG;7=A1WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!(6'&amp;T;S"J&lt;A"5!0!!$!!$!!1!"1!'!!=!"!!)!!1!"!!%!!Q!$1-!!(A!!!E!!!!!!!!!#1!!!)U,!!)1!!!!!!!!!"!!!!!!!!!!!!!!!!!!!!!1!!!!EA!!!!!"!!Y!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350836752</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="Menu Tags.ctl" Type="VI" URL="../Menu Tags.ctl">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
	</Item>
</LVClass>
