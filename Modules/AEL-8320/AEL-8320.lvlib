﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Aktakom" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Support" Type="Folder">
			<Item Name="AEL-8320 Ranges.vi" Type="VI" URL="../Aktakom/AEL-8320 Ranges.vi"/>
			<Item Name="CMD.ctl" Type="VI" URL="../Aktakom/CMD.ctl"/>
			<Item Name="Mode.ctl" Type="VI" URL="../Aktakom/Mode.ctl"/>
			<Item Name="Par.ctl" Type="VI" URL="../Aktakom/Par.ctl"/>
			<Item Name="Send Cmd.vi" Type="VI" URL="../Aktakom/Send Cmd.vi"/>
			<Item Name="Short Mode.ctl" Type="VI" URL="../Aktakom/Short Mode.ctl"/>
		</Item>
		<Item Name="Init.vi" Type="VI" URL="../Aktakom/Init.vi"/>
		<Item Name="Deinit.vi" Type="VI" URL="../Aktakom/Deinit.vi"/>
		<Item Name="Set Mode.vi" Type="VI" URL="../Aktakom/Set Mode.vi"/>
		<Item Name="Set Value.vi" Type="VI" URL="../Aktakom/Set Value.vi"/>
		<Item Name="Output.vi" Type="VI" URL="../Aktakom/Output.vi"/>
		<Item Name="Test.vi" Type="VI" URL="../Aktakom/Test.vi"/>
		<Item Name="Short-Circuit mode.vi" Type="VI" URL="../Aktakom/Short-Circuit mode.vi"/>
		<Item Name="System Error Read.vi" Type="VI" URL="../Aktakom/System Error Read.vi"/>
	</Item>
	<Item Name="Tasks" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="UI TypeDefs" Type="Folder">
			<Item Name="Parameters.ctl" Type="VI" URL="../Task Classes/UI TypeDefs/Parameters.ctl"/>
			<Item Name="Short-Circuit Modes.ctl" Type="VI" URL="../Task Classes/UI TypeDefs/Short-Circuit Modes.ctl"/>
		</Item>
		<Item Name="Init.lvclass" Type="LVClass" URL="../Task Classes/Init/Init.lvclass"/>
		<Item Name="Deinit.lvclass" Type="LVClass" URL="../Task Classes/Deinit/Deinit.lvclass"/>
		<Item Name="Disable.lvclass" Type="LVClass" URL="../Task Classes/Disable/Disable.lvclass"/>
		<Item Name="Enable.lvclass" Type="LVClass" URL="../Task Classes/Enable/Enable.lvclass"/>
		<Item Name="Measure.lvclass" Type="LVClass" URL="../Task Classes/Measure/Measure.lvclass"/>
		<Item Name="Set Current.lvclass" Type="LVClass" URL="../Task Classes/Set Current/Set Current.lvclass"/>
		<Item Name="Set Power.lvclass" Type="LVClass" URL="../Task Classes/Set Power/Set Power.lvclass"/>
		<Item Name="Set Resistance.lvclass" Type="LVClass" URL="../Task Classes/Set Resistance/Set Resistance.lvclass"/>
		<Item Name="Set Voltage.lvclass" Type="LVClass" URL="../Task Classes/Set Voltage/Set Voltage.lvclass"/>
		<Item Name="Short-circuit.lvclass" Type="LVClass" URL="../Task Classes/Short-circuit/Short-circuit.lvclass"/>
	</Item>
	<Item Name="Module.lvclass" Type="LVClass" URL="../Module Class/Module.lvclass"/>
</Library>
