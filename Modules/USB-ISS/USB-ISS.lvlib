﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Support" Type="Folder">
		<Item Name="USB-ISS API" Type="Folder">
			<Item Name="Init.vi" Type="VI" URL="../Support/USB-ISS API/Init.vi"/>
			<Item Name="Deinit.vi" Type="VI" URL="../Support/USB-ISS API/Deinit.vi"/>
			<Item Name="Send CMD.vi" Type="VI" URL="../Support/USB-ISS API/Send CMD.vi"/>
			<Item Name="I2C Speed.ctl" Type="VI" URL="../Support/USB-ISS API/I2C Speed.ctl"/>
			<Item Name="Mode.ctl" Type="VI" URL="../Support/USB-ISS API/Mode.ctl"/>
			<Item Name="SPI Mode.ctl" Type="VI" URL="../Support/USB-ISS API/SPI Mode.ctl"/>
			<Item Name="I2C_Init.vi" Type="VI" URL="../Support/USB-ISS API/I2C_Init.vi"/>
			<Item Name="I2C_AD1_AD2.vi" Type="VI" URL="../Support/USB-ISS API/I2C_AD1_AD2.vi"/>
			<Item Name="Check device on I2C bus.vi" Type="VI" URL="../Support/USB-ISS API/Check device on I2C bus.vi"/>
			<Item Name="SPI_Init.vi" Type="VI" URL="../Support/USB-ISS API/SPI_Init.vi"/>
			<Item Name="SPI_Send.vi" Type="VI" URL="../Support/USB-ISS API/SPI_Send.vi"/>
			<Item Name="IO Pin State.ctl" Type="VI" URL="../Support/USB-ISS API/IO Pin State.ctl"/>
		</Item>
		<Item Name="VIP Specific" Type="Folder">
			<Item Name="VIP I2C Address.vi" Type="VI" URL="../Support/VIP Specific/VIP I2C Address.vi"/>
			<Item Name="VIP Send CMD and Read Data.vi" Type="VI" URL="../Support/VIP Specific/VIP Send CMD and Read Data.vi"/>
		</Item>
		<Item Name="CRC16-CCITT-FALSE.vi" Type="VI" URL="../Support/CRC16-CCITT-FALSE.vi"/>
	</Item>
	<Item Name="Tasks" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="UI TypeDefs" Type="Folder">
			<Item Name="Data_bytes.ctl" Type="VI" URL="../Task Classes/UI TypeDefs/Data_bytes.ctl"/>
			<Item Name="I2C_Address.ctl" Type="VI" URL="../Task Classes/UI TypeDefs/I2C_Address.ctl"/>
			<Item Name="I2C_Speed.ctl" Type="VI" URL="../Task Classes/UI TypeDefs/I2C_Speed.ctl"/>
			<Item Name="Reg_Address.ctl" Type="VI" URL="../Task Classes/UI TypeDefs/Reg_Address.ctl"/>
			<Item Name="SPI Mode (UI).ctl" Type="VI" URL="../Task Classes/UI TypeDefs/SPI Mode (UI).ctl"/>
		</Item>
		<Item Name="VIP Protocol" Type="Folder">
			<Item Name="VIP_Ping.lvclass" Type="LVClass" URL="../Task Classes/VIP_Ping/VIP_Ping.lvclass"/>
			<Item Name="VIP_GetStatusData.lvclass" Type="LVClass" URL="../Task Classes/VIP_GetStatusData/VIP_GetStatusData.lvclass"/>
			<Item Name="VIP_GetInfo.lvclass" Type="LVClass" URL="../Task Classes/VIP_GetInfo/VIP_GetInfo.lvclass"/>
			<Item Name="VIP_GetResetsCount.lvclass" Type="LVClass" URL="../Task Classes/VIP_GetResetsCount/VIP_GetResetsCount.lvclass"/>
			<Item Name="VIP_GetFirmware.lvclass" Type="LVClass" URL="../Task Classes/VIP_GetFirmware/VIP_GetFirmware.lvclass"/>
			<Item Name="VIP_GetHardware.lvclass" Type="LVClass" URL="../Task Classes/VIP_GetHardware/VIP_GetHardware.lvclass"/>
			<Item Name="VIP_SwitchCell.lvclass" Type="LVClass" URL="../Task Classes/VIP_SwitchCell/VIP_SwitchCell.lvclass"/>
			<Item Name="VIP_GetConsumptionCurrent.lvclass" Type="LVClass" URL="../Task Classes/VIP_GetConsumptionCurrent/VIP_GetConsumptionCurrent.lvclass"/>
			<Item Name="VIP_BootloaderEnter.lvclass" Type="LVClass" URL="../Task Classes/VIP_BootloaderEnter/VIP_BootloaderEnter.lvclass"/>
			<Item Name="VIP_BootloaderWriteFlash.lvclass" Type="LVClass" URL="../Task Classes/VIP_BootloaderWriteFlash/VIP_BootloaderWriteFlash.lvclass"/>
			<Item Name="VIP_BootloaderReadFlash.lvclass" Type="LVClass" URL="../Task Classes/VIP_BootloaderReadFlash/VIP_BootloaderReadFlash.lvclass"/>
			<Item Name="VIP_BootloaderClearFlash.lvclass" Type="LVClass" URL="../Task Classes/VIP_BootloaderClearFlash/VIP_BootloaderClearFlash.lvclass"/>
			<Item Name="VIP_BootloaderExit.lvclass" Type="LVClass" URL="../Task Classes/VIP_BootloaderExit/VIP_BootloaderExit.lvclass"/>
		</Item>
		<Item Name="Init I2C Mode.lvclass" Type="LVClass" URL="../Task Classes/Init I2C Mode/Init I2C Mode.lvclass"/>
		<Item Name="Init SPI Mode.lvclass" Type="LVClass" URL="../Task Classes/Init SPI Mode/Init SPI Mode.lvclass"/>
		<Item Name="Deinit.lvclass" Type="LVClass" URL="../Task Classes/Deinit/Deinit.lvclass"/>
		<Item Name="Check I2C Address.lvclass" Type="LVClass" URL="../Task Classes/Check I2C Address/Check I2C Address.lvclass"/>
		<Item Name="Find I2C Address.lvclass" Type="LVClass" URL="../Task Classes/Find I2C Address/Find I2C Address.lvclass"/>
		<Item Name="Read I2C.lvclass" Type="LVClass" URL="../Task Classes/Read I2C/Read I2C.lvclass"/>
		<Item Name="Write I2C.lvclass" Type="LVClass" URL="../Task Classes/Write I2C/Write I2C.lvclass"/>
		<Item Name="SPI Data.lvclass" Type="LVClass" URL="../Task Classes/SPI Data/SPI Data.lvclass"/>
	</Item>
	<Item Name="Module.lvclass" Type="LVClass" URL="../Module Class/Module.lvclass"/>
</Library>
