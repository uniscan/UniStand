﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassBodyIcon" Type="Str">Actor</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6618880</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">USB-ISS.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../USB-ISS.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,!!!!*Q(C=T:3R&lt;B."%)&lt;`1R3U,CEC)5&gt;56'B?!)%F/DK`QK2/Z2+\]Q/EG4:3'D]!D;P1U%S4^(Y&amp;^\'1T,@LN:5%RR931?T?X*X`W:HZ&lt;H?^5GWPJ*&gt;;(WPH"RP_-G*&gt;X]\L](J6M4BX6V08^`T&lt;_06DVT&lt;_8PZ$]1`L\^Z;`30]R^K\A_X@_@&gt;DX@0L@HP30VE&gt;]P`5:-,N&gt;``J[7E*&lt;OX*_/)KNN_`KK[3:&lt;^`MP7P2,U`TX_!\`&lt;W^O$X\?:H.4E]@U`0\Z(VW&gt;_?XX_E]3XDYX_#ZWV3JX)199E&amp;ZJCJO.;*HOC*HOC*HOC"(OC"(OC"(OC/\OC/\OC/\OC'&lt;OC'&lt;OC'&lt;OCVIQN&gt;[%*H6J,C3;%E;:)A'1S+EFP#E`!E0!E00Z8Q*$Q*4]+4]$"%#5`#E`!E0!E095JY%J[%*_&amp;*?%B63&lt;*W&gt;(A3(N)LY!FY!J[!*_#BJ!+?!#!I&amp;C1/EI#BQ"G]"$Q"4]$$KQ+?A#@A#8A#(NQ+?!+?A#@A#8A)K&lt;-3F;:U&gt;(B))Y@(Y8&amp;Y("[(B^2S?"Q?B]@B=8AI*Y@(Y8%AH)*/=B$E"$E$H"]/D]0$1Q[0Q_0Q/$Q/$[[[1FZHJN#5DA[0Q70Q'$Q'D]&amp;$#BE]"I`"9`!90+36Q70Q'$Q'D]&amp;$+2E]"I`"9Y!923EP)ZE2;!QS")/(K_Y7K[M5F=2K,U&gt;%X&lt;RKGV*NM[FN)L8.I&lt;&lt;I;IOJNEBKE[]WK7K4J49*;B_H"KU'IV:%,&lt;A-V*,\!JND-WS+D&lt;!B.M$[7+_%`O7"S_63C]6#]`F=M^F-U_F5I^&amp;)Q_&amp;1A]&amp;!`8Z@P6ZP&gt;[K_I7^;NTW8RLR@HLU^_8:RV:N&gt;8,W_Z(FZ^HJ2^+X`A8V&gt;&gt;_-0(8&lt;&gt;D4`?&gt;*-@&gt;^XYU`NO`/7EW(6^`XT84&lt;\@&lt;-;5M38G5:\.O@1`H)V[I=VT.U?`!,YV30%!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"B@5F.31QU+!!.-6E.$4%*76Q!!&amp;!Q!!!24!!!!)!!!%_Q!!!!B!!!!!AV65U)N36.4,GRW&lt;'FC$EVP:(6M:3ZM&gt;G.M98.T!!!!!!!!I"=!A!!!-!!!#!!%!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!#Q2;)K[UD=4)?%MYFT)OH*!!!!%!!!!"!!!!!!'VFL*S)#:USZ38SL.OVM(&gt;1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!'`ITWT#LMJ#A"],"??8[B)"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1;:Y&lt;S`V%P,91&amp;6Y4I.2;4!!!!!1!!!!!!!!!*Q!"4&amp;:$1Q!!!!%!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!Q!!!!!#!!%!!!!!!#9!!!!G?*RDY'2A;G#YQ!$%D%$-V-$U!]D_!/)T#("!:2A!R`1+]1!!!!!!3!!!!2BYH'.AQ!4`A1")-4)Q-&amp;U!UCRIYG!;RK9GQ'5O,LOAYMR1.\*#2"G"9ER\A!QGE"R5$&gt;104'?!_!3[/=R9T!9!@_EI)A!!!!Q!!6:*2&amp;-!!!!!!!-!!!'&gt;!!!$)(C=7]$)Q*"J&lt;'(W!5AT!\%Y1Q.$=HZ++B=$E-]!!2QQ"A5A!'K?&amp;JKYY9($;5#ARS`@!O9XP_(J&gt;F%2;+Z2E7!K&amp;?HW52(J^&amp;&amp;B[722?@(H````T5&gt;Y$H&gt;\Z"RXN!'J\?9!CB^X5?%!=9!U#YD_(ZA"5A5T,Q"I'E&gt;$B4*$#9PBA;D$RRN-'#%7QYS-1L&gt;@!+B$JDM%;('4$.$&amp;1#=I&gt;(9J!&amp;F1BQ!NYI&amp;;#,99&lt;%LL!@YJ"`CXH&gt;A"-JX@^7$T%1'A%VR!ZH%!42!Y_*#FOV%$+.=\%51#B8A[1TAEDLNQ[)A"_9QH/I(/\_3"_:)$\PYQE!%F+A+&gt;*C#,72BB&amp;H;T(8@1!$P$112#:5#I#AB6!+*WA(VUB#0O-$S]V\[_NQM54GR)=?!!R!V!$%I$S&amp;C0A:%":#%4E+S&amp;KL5"MJGA9L#Y",%&amp;I-%KB+1(:KM'EBAM_"X!@!;QT"OI/B$\%Z4&gt;!(5D3-Q7K'=#F/U#:#&gt;!W&gt;Z!^A-I/QD)&amp;I#S)Y&amp;M"59)/Q\+`A#V&amp;R@N\/`CCB1E]$10!0^9C,A!!!!!!!!-&amp;Q#!"1!!"$%X,D!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Q8!)!&amp;!!!%-4=O-!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$"=!A!5!!!1R.SYQ!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"IG=F(&lt;;5J2'KF+5:IJ3F%;*H'&gt;W!!!!"`````Y!!!!'!!!!"A!!!!9!!!!'!@`]"A%!"!9"!Q1'!1N%"A=@Z!9"$-1'!4BU"A%Y&gt;Q9"$-1'!2`E"A=,2!9"!Q1'!1!%"A(``!9!!!!'!!!!"A!!!!9!!!!(`````!!!#!0`````````````````````YC)C)C)C)C)C)C)C)C)C0_0C)_)`YD`_)_)_0C)``D`D`D`DYDY_)_0C0DYC0C)`Y_0DY_)_0C0DYDY_)D`C0_0C)_0C0DYDY_)_0C)_)D`DYC0C0_)``C)`YD`_0`Y`YC)C)C)C)C)C)C)C)C)C0``````````````````````T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-T-T-T-T-T-T0`-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T-T-T-T-T-T-`]T-T-T``````````-T-T0`-T-T-]!!!!!!!!0T-T-T`T-T-T0!!!!X1!!$]T-T-`]T-T-TQ!!].U0!!`-T-T0`-T-QT-!$&gt;X&gt;X&gt;!0T-T-T`T-T-T0!!$&gt;!.U!$]T-T-`]T-T-TQ$&gt;U!!.X1`-T-T0`-T-T-]!X&gt;!!$&gt;U$-]T-T`T-T-T0!!$&gt;!.U!$]T-T-`]T-T-TQ!.X&gt;X&gt;U!`-T-T0`-T-RG9!!0$&gt;$Q!0T-T-T`T-T-T0!!!!X1!!$]T-T-`]T-T-TQ!!!!!!!!`-T-T0`-T-T-``````````T-T-T`T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-T-T-T-T-T-T0`-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T-T-T-T-T-T-``````````````````````!!!%!0```````````````````````````````````````````X&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=@``=@^R=8(`=8(``X&amp;R````=8(`=8(`=@^R=8(```^R``^R``^R``^R`X&amp;R`X(`=8(`=@^R=@^R`X&amp;R=@^R=8(``X(`=@^R`X(`=8(`=@^R=@^R`X&amp;R`X(`=8&amp;R``^R=@``=@^R=8(`=@^R=@^R`X&amp;R`X(`=8(`=@^R=8(`=8&amp;R``^R`X&amp;R=@^R=@``=8(```^R=8(``X&amp;R````=@```X(``X&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=@````````````````````````````````````````````]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML````````````````````+SML+SML+```+SML+SML+SP`!!!!!!!!!!!!!!!!!0]L+SML+SML``]L+SML+SML+`]!!!!!!!#"_A!!!!!!`SML+SML+SP``SML+SML+SML`Q!!!!$]!)'"!0Q!!!$`+SML+SML+```+SML+SML)S-D!!!!A9'"A9'"A9%!!0]L+SML+SML``]L+SML+SML+`]!!!!!A9%!!)'"!!!!`SML+SML+SP``SML+SML+SML`Q!!_I'"!!!!!)'"A1$`+SML+SML+```+SML+SML+SP`!!#"A9%!!!!!A9([!#-D)SML+SML``]L+SML+SML+`]!!!!!A9%!!)'"!!!!`SML+SML+SP``SML+SML+SML`Q!!!)'"A9'"A9'"!!$`+SML+SML+```+SML+SMLUN,3!!!!!0Q!A9%!`!!!!0]L+SML+SML``]L+SML+SML+`]!!!!!!!$[A1!!!!!!`SML+SML+SP``SML+SML+SML`Q!!!!!!!!!!!!!!!!$`+SML+SML+```+SML+SML+SP```````````````````]L+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP```````````````````````````````````````````]!!!!-!!&amp;'5%B1!!!!!!!$!!!!K2=!A!!!!!!"!!1!!!!"!!!!!!!$!!!!#W.M98.T5X2S;7ZH&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!!30SI[/CB*4F.55HR44U.,261J!!!!!!!!!!VE;8.Q&lt;'&amp;Z2GFM&gt;'6S&amp;Q#!!!!!!!%!"1!(!!!"!!!!!3!!!!!!!!!!!!FU?8"F1WRB=X-8!)!!!!!!!1!)!$$`````!!%!!!!!!!6*&lt;H.U=A!!!!!!!!!!!!/M!!!(EXC=L66.4".&lt;&amp;$[XH?IN&amp;LG$#*2)/J!:",82G#B;@7LEAO)0`F4CTU*N,)I*%=+0U9W[G*$Q8MR&lt;Y/PC'&lt;;Y@!M7&lt;WN-QW&lt;CTUI4843S-&lt;L2$&gt;'IU`(=_7WLVIUTS=X.Z(THO_=\XTU$M0*PVBQKQCU4#&amp;P'T2%4;L)'!=AH+&lt;B0RT3Q1@)&amp;3%/=G,#0$L*8I3*::U)M;X41,8Q'0G#U^&gt;3[$P/EF&lt;X'U"5MDMFK4+D,'EXS)&lt;8!V0`7K4-2,[M-L?QO+9&lt;/K)G0^+Y_CI3A&gt;YJ64J)C%.YG38L\]=SV)6U68[.*'L&gt;42EVAX+A&gt;6QPL-3.30\*4EBQMEA&gt;?3M#5H&lt;#QM"#!:!@592^DB]!M9NI8*&amp;=&amp;5]_.VAGVM-H'V.A9Z(HB]@"%=6;=89!KI7OYQ2#+O"N?S9M?&amp;U_9_WT=UN)3YH"V==-G.+C&amp;`42/8W&gt;@2GJ0[0]$!:+`)+3^,`$SA/C#(6O0D&gt;"3B+6RHT:BAW[%,I,E=?W(7&lt;M.EN?'X&gt;A'UG?X)=JH'!2^G0V&amp;(\C5&lt;W`N':G;G"Q;6U9P+Z&gt;'-B-4SNDYV?O:S3%FGZH-@._B0\A2\2&lt;6#T,&lt;(,!7*.A$)[6KD],]`$Q+A'M!X901*L8AYW3\GD&amp;@]7SAO'!.F.O,SP&amp;0J[Y)^&lt;25K.STGQ00&lt;E80SKTS7'7?X@&lt;\0&lt;M&gt;L83LQL-Q$&amp;VQMYL`OBV1C7?\%*7#Y3K9(9CZ8?&lt;:,O2*?4T60,PT?]^W?6S_:X/Z8"E/F5`ZHJ5)=4SL0\/_7F_&amp;=T^&lt;\[!&lt;JEO=/YDNY)9C,'C^2YZGZ"$SBTXZ?X\I69,[\[KOPR)#R[_UZ^D2Z.DI_'2&amp;)_[:Q('YU1:?3*TQ,&amp;$HVBF99*5*@7D$L7LBG/.AOV]23%-D:*XKZ12'BVD=V@1P1?$O`[4R;-)F`-?%!VF$%;F7F6S'#+T'6'F07EOWJ&lt;5M#Z0D[J3DN]%&gt;.*1KPD$.E1_`A^J*5'UNGF1@VLAU-2-/YHR$FIW/:D("%E;?&amp;ICZ%PE($CPZB.0&gt;*FCJ+W+DNYE1VOZ7-%8&lt;H8J_''K@JST5,R&gt;.V-_.#&amp;Z^YJTDI4C(B']DV(K^7`[:`QZ68(XRN!1'`0H60YQ'V-[#&gt;AZQ!,RR"E#Y9TL=?VZJUICEK:*W/B2Z((E31!:+)7]&gt;C)1T!YM:+,F&gt;^.8CF0]&lt;J'G67^5#7#]GPWX#7NZ4`Z4VI_!RWE^\W2R/(BR#D83/,O?@_T`+`"%@K4_$E[2&amp;`4&gt;"G\]"^.-?21!!!!1!!!!N!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!LA!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!$I8!)!!!!!!!1!)!$$`````!!%!!!!!!"Y!!!!"!":!5!!!$EVP:(6M:3ZM&gt;G.M98.T!!!"!!!!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!+2=!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!1!!!!!!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.&lt;;-UE!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!VNIT31!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!#)&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!"M!!!!!Q!?!$@`````!!18!)!!!!!!!1!%!!!!!1!!!!!!!!!O1(!!$A6*&lt;H.U=A!"!!!8!)!!!!!!!1!%!!!!!1!!!!!!!!B$4UUN='^S&gt;!!!'%"1!!%!!1Z.&lt;W2V&lt;'5O&lt;(:D&lt;'&amp;T=Q!!!1!#!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:&amp;Q#!!!!!!!%!"1!$!!!"!!!!!!!%!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U91!!!(A8!)!!!!!!!Q!?!$@`````!!18!)!!!!!!!1!%!!!!!1!!!!!!!!!O1(!!$A6*&lt;H.U=A!"!!!8!)!!!!!!!1!%!!!!!1!!!!!!!!B$4UUN='^S&gt;!!!'%"1!!%!!1Z.&lt;W2V&lt;'5O&lt;(:D&lt;'&amp;T=Q!!!1!#!!!!!!!!!!!!!!!%!!1!#Q!!!!1!!!"/!!!!+!!!!!)!!!1!!!!!"1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%=!!!"Z8C=D5^.3].1%*TUJ4;N&lt;7T6WJ/3AXA1WEM08E-,1A\&amp;9P$CS:A0+4R-3&amp;[+R`Z+$`Y+`15[?1FY5.!&gt;7(:GF^F&gt;!"/=Y?K4!8/#(2A'4*XLG,E:\,&lt;X8+C]UH[&lt;M:9XKWG7ZAKYR]@LW^SD+!:X`G,K_@Z-&lt;O8GU6[F53FDEF!'2&gt;&amp;L;+AE,NUVH9T4J3Q,&amp;?&gt;/GDB[RMHST4:1M2-&amp;+E#&lt;1!]2N\Z$E)C'9E\3&gt;4M1C8S#Z?Z%?KNK)5Y4$/EOU)'&amp;LCCD"/@Y]R`U^58\1LV%X,&amp;'C\G0CX`?W1,48J8I:_'"BC;O;?0A!%-?5W'E([IB'PR5PDO('P8$"IZ9B&gt;KX0NTG5J-\"[S/-;9[RAFLOZL_!C^&gt;2NA!!!"_!!%!!A!$!!5!!!"9!!]#!!!!!!]!W!$6!!!!91!0!A!!!!!0!.A!V1!!!'I!$Q)!!!!!$Q$9!.5!!!"T!!]%!!!!!!]!W!$6!!!!@)!!B!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!"35V*$$1I!!UR71U.-1F:8!!!5$!!!"&amp;-!!!!A!!!4\!!!!!!!!!!!!!!!)!!!!$1!!!2)!!!!(5R*1EY!!!!!!!!"&lt;%R75V)!!!!!!!!"A&amp;*55U=!!!!!!!!"F%.$5V1!!!!!!!!"K%R*&gt;GE!!!!!!!!"P%.04F!!!!!!!!!"U&amp;2./$!!!!!!!!!"Z%2'2&amp;-!!!!!!!!"_%R*:(-!!!!!!!!#$&amp;:*1U1!!!!!!!!#)(:F=H-!!!!%!!!#.&amp;.$5V)!!!!!!!!#G%&gt;$5&amp;)!!!!!!!!#L%F$4UY!!!!!!!!#Q'FD&lt;$1!!!!!!!!#V'FD&lt;$A!!!!!!!!#[%R*:H!!!!!!!!!#`&amp;.55C!!!!!!!!!$%%:13')!!!!!!!!$*%:15U5!!!!!!!!$/&amp;:12&amp;!!!!!!!!!$4%R*9G1!!!!!!!!$9%*%3')!!!!!!!!$&gt;%*%5U5!!!!!!!!$C&amp;:*6&amp;-!!!!!!!!$H%253&amp;!!!!!!!!!$M%V6351!!!!!!!!$R%B*5V1!!!!!!!!$W&amp;:$6&amp;!!!!!!!!!$\%:515)!!!!!!!!%!!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!`````Q!!!!!!!!$-!!!!!!!!!!$`````!!!!!!!!!/!!!!!!!!!!!0````]!!!!!!!!![!!!!!!!!!!!`````Q!!!!!!!!%5!!!!!!!!!!$`````!!!!!!!!!2Q!!!!!!!!!!0````]!!!!!!!!"3!!!!!!!!!!!`````Q!!!!!!!!'5!!!!!!!!!!$`````!!!!!!!!!;1!!!!!!!!!"0````]!!!!!!!!$3!!!!!!!!!!(`````Q!!!!!!!!.9!!!!!!!!!!D`````!!!!!!!!!WA!!!!!!!!!#@````]!!!!!!!!$?!!!!!!!!!!+`````Q!!!!!!!!/)!!!!!!!!!!$`````!!!!!!!!!ZA!!!!!!!!!!0````]!!!!!!!!$M!!!!!!!!!!!`````Q!!!!!!!!0%!!!!!!!!!!$`````!!!!!!!!"%A!!!!!!!!!!0````]!!!!!!!!'4!!!!!!!!!!!`````Q!!!!!!!!J1!!!!!!!!!!,`````!!!!!!!!#G!!!!!!!!!!!0````]!!!!!!!!,%!!!!!!!!!!!`````Q!!!!!!!!\!!!!!!!!!!!$`````!!!!!!!!$MA!!!!!!!!!!0````]!!!!!!!!/U!!!!!!!!!!!`````Q!!!!!!!!\A!!!!!!!!!!$`````!!!!!!!!$UA!!!!!!!!!!0````]!!!!!!!!05!!!!!!!!!!!`````Q!!!!!!!")-!!!!!!!!!!$`````!!!!!!!!%B1!!!!!!!!!!0````]!!!!!!!!3(!!!!!!!!!!!`````Q!!!!!!!"*)!!!!!!!!!)$`````!!!!!!!!%WA!!!!!#EVP:(6M:3ZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.2</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#M!!!!!BB.&lt;W2V&lt;'6T)%BJ:8*B=G.I?3ZM&gt;GRJ9H!:37ZU:8*G97.F)%&amp;E98"U:8)O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!&lt;!!"!!A!!!!!'#&amp;$&lt;WVQ;7RF:#"'=G&amp;N:8&gt;P=GMA4'FC=RB.&lt;W2V&lt;'6T)%BJ:8*B=G.I?3ZM&gt;GRJ9H!837ZU:8*G97.F)%&amp;E98"U:8)A1WRB=X-:37ZU:8*G97.F)%&amp;E98"U:8)O&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Module.ctl" Type="Class Private Data" URL="Module.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="TypeDefs" Type="Folder">
		<Item Name="Module CMDs.ctl" Type="VI" URL="../TypeDefs/Module CMDs.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="I2C Read Data.ctl" Type="VI" URL="../TypeDefs/I2C Read Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="I2C Write Data.ctl" Type="VI" URL="../TypeDefs/I2C Write Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="Get Hierarchy Path.vi" Type="VI" URL="../Get Hierarchy Path.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;0!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!=1%!!!@````]!"1Z);76S98*D;(EA='&amp;U;!!!.%"Q!"Y!!"Y.66.#,5F45SZM&gt;GRJ9AZ.&lt;W2V&lt;'5O&lt;(:D&lt;'&amp;T=Q!!#EVP:(6M:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!-E"Q!"Y!!"Y.66.#,5F45SZM&gt;GRJ9AZ.&lt;W2V&lt;'5O&lt;(:D&lt;'&amp;T=Q!!#5VP:(6M:3"J&lt;A"5!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!1!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!I!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1352671760</Property>
	</Item>
	<Item Name="Init.vi" Type="VI" URL="../Init.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%$!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!?$6641CV*5V-O&lt;(:M;7)/47^E&gt;7RF,GRW9WRB=X-!!!J.&lt;W2V&lt;'5A&lt;X6U!!!S1(!!(A!!(AV65U)N36.4,GRW&lt;'FC$EVP:(6M:3ZM&gt;G.M98.T!!!*47^E&gt;7RF)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!%!!1!"!!'!Q!!?!!!#1!!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#3!!!!!!%!"Q!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1352671760</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="Deinit.vi" Type="VI" URL="../Deinit.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$[!!!!"A!&lt;1!-!&amp;':J&lt;G&amp;M)'6S=G^S)'.P:'5A&lt;X6U!!!%!!!!.%"Q!"Y!!"Y.66.#,5F45SZM&gt;GRJ9AZ.&lt;W2V&lt;'5O&lt;(:D&lt;'&amp;T=Q!!#EVP:(6M:3"P&gt;81!!"F!!Q!4:GFO97QA:8*S&lt;X)A9W^E:3"J&lt;A!S1(!!(A!!(AV65U)N36.4,GRW&lt;'FC$EVP:(6M:3ZM&gt;G.M98.T!!!*47^E&gt;7RF)'FO!&amp;1!]!!-!!!!!1!"!!)!!1!"!!%!!1!$!!%!!1!%!Q!!?!!!$1A!!!!!!!!!!!!!C1!!!!!!!!!!!!!!!!!!!!!!!!!3!!!!!!!!!!!!!!#1!!!!!!%!"1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="Handle Command.vi" Type="VI" URL="../Handle Command.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!J!5Q63:8"M?1!U1(!!(A!!(AV65U)N36.4,GRW&lt;'FC$EVP:(6M:3ZM&gt;G.M98.T!!!+47^E&gt;7RF)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!91$$`````$E:M982U:7ZF:#"%982B!!!*1!9!!U..2!!S1(!!(A!!(AV65U)N36.4,GRW&lt;'FC$EVP:(6M:3ZM&gt;G.M98.T!!!*47^E&gt;7RF)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%!!!!"!!!!#3!!!!!!%!#Q!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
	</Item>
	<Item Name="Actor Core.vi" Type="VI" URL="../Actor Core.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%@!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!G&amp;E&amp;D&gt;'^S)%:S97VF&gt;W^S;SZM&gt;GRJ9H!.17.U&lt;X)O&lt;(:D&lt;'&amp;T=Q!!#5&amp;D&gt;'^S)'^V&gt;!!71&amp;!!!Q!!!!%!!ABF=H*P=C"J&lt;A!!-E"Q!"Y!!"Y.66.#,5F45SZM&gt;GRJ9AZ.&lt;W2V&lt;'5O&lt;(:D&lt;'&amp;T=Q!!#5VP:(6M:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1352671760</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
</LVClass>
