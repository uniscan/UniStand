﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassBodyIcon" Type="Str">Actor</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6618880</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">USB-ISS.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../USB-ISS.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,&amp;!!!*Q(C=T:1^&lt;J."%)&lt;@$V(1_A")S'YC)3("&gt;&amp;1%,.(2_1ID5&lt;JSC&gt;UZYA2T"2==A&amp;3BI:EG[8W&amp;^)G1T,0LD5G)@Y2%%,O?\_?&gt;H&gt;HHGVWP6.M4[&lt;&amp;7B^JY&lt;[P_V&lt;C-K\:Z'[^O_^NP8)?N8_\\GXQ`@PQL@LQ*X2+`OO.@L7\&amp;\_9`V*\P&lt;@`/PRXLFF_XWU\`\(K@`Y&gt;G-S\X`90"I!3XND/_O)JN^V^86]GSX4_\]6_,_@Y]`R[_CYO,P&gt;_XK=`V&lt;(`^&gt;N@XQ0JM&lt;Q`P0^$YFOHB0]($.KF4/9CQR!*TT&amp;2=KU20^%20^%20^%!0^%!0^%!0&gt;%&gt;X&gt;%&gt;X&gt;%&gt;X&gt;%-X&gt;%-X&gt;%-X^.L2B3ZUI6/6:0*EIC2JEC!:$)K33]+4]#1]#1_P3HA3HI1HY5FY'++%*_&amp;*?"+?B)=Q*4Q*4]+4]#1]J+IE74M[0!E0[28Q"$Q"4]!4]$#F!JY!)*AM3"QE!5/"-XA)?!+?A)&gt;("4Q"4]!4]!1]O"8Q"$Q"4]!4]""3KR+6JH2U?%ADB]@B=8A=(I?(V(*Y("[(R_&amp;R?*B/$I`$YU!Y%TL*1:!4Z!RQ8BQ?BY?&lt;("[(R_&amp;R?"Q?8(7&amp;P&amp;;GU*3/$I`"9`!90!;0Q5-+'4Q'D]&amp;D]"A]J*8"9`!90!;0Q=.5-HA-(I0(!$%G:8I:S9R!9Z!B'$T][G[ROEJ23;TW=E45T;OW+&gt;5WG^IG5NM=;IOONJBKC[27@,7CKB6,L1BK([='L1;D.IF;="GI3[Z,\"2&lt;9(.MAIWQ)&gt;&lt;(?C8U,Q_]P,T5=LH5[?GJ&amp;IO&amp;ZP/Z*J/*2K/2BM/B_PW_?LX?ZF2^2F_X\O:=GP,]_?O,YZ0"R_/46V_/:`WDYZ082_/CX`DPW+&gt;6.XX497@&gt;^/VZ.`N_V5X@P?SG(ZY7/[P0\[_[W&lt;@T^:ASNM4]FG&gt;^,PU0:[-?;8X@V/AHE/&gt;!Q1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"695F.31QU+!!.-6E.$4%*76Q!!%0A!!!2A!!!!)!!!%.A!!!!O!!!!!AV65U)N36.4,GRW&lt;'FC'V:*5&amp;^#&lt;W^U&lt;'^B:'6S27ZU:8)O&lt;(:D&lt;'&amp;T=Q!!!!!!I"=!A!!!-!!!#!!%!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!!UNI`C_!ZK1JO:GN^`1-(T!!!!%!!!!"!!!!!!BA//"RIB"5OK(DU8+3`FS&gt;1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!-A^OB,(D8N%MGU&gt;@]Q2W51"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1\+0=6]!H7HW2!0E;,ZZYL1!!!!1!!!!!!!!!*Q!"4&amp;:$1Q!!!!%!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!Q!!!!!#!!%!!!!!!#)!!!!;?*RD9'.A;G#YQ!$%D%$-V-$U!]D_Q-$!!!"7F1&lt;G!!!!!!"&amp;!!!"'(C=9W$!"0_"!%AR-D!QH1$3,'DC9"L'JC&lt;!:3YOO[$CT&amp;!XME*%'9&amp;C4(O!$#;1(+J?E(`!@E)"&lt;&amp;D-"A"W^3A6!!!!!!!!$!!"6EF%5Q!!!!!!!Q!!!71!!!,-?*R,9'2AS$3W-.M!J*G"7*SBA3%Z0S76CQ():Y#!,5Q-&amp;)-!K(F;;/+'"Q[H!9%?PXQ,G.`]BK@&lt;257AO5:&amp;AKF5J.N(2;442Y7FEU8FR:````]X(_%ZX/W2=^T2"K3WGQ-I@NR&amp;B10%!&gt;)M)0J`9!:)&amp;=S]!+"J(!U6SAQF,)9(IAY@&lt;T"BB&amp;A--T)+V@Y&gt;)/H$S/\B!$J#Y/"$FOZ'$3#`&gt;S+)"!LR&gt;):Q3"RXY&gt;!2!`):4X1#L?`EA&lt;G3!WZ`'-C!%B7"4B/13VF!JI06&gt;,-&gt;&gt;^!!O^N""%*F1+A+#&amp;5!&gt;AT9"5=YYA\$QWPN[XO\1/()BB3'$E$=!-3A/%4'?AS-$#!,G9"E,63N$:$.""7$R17)@1(+VE$3]Q8*@*!?E-Q;K"C)P1H+&lt;I#["S4W&amp;UB0A,*"PEW!MLG"\!61NB#1,1"F3Q,:$["M/3B\!T3+=.(/`C[O3.[(JU]!.2VSA!!!!!Q8!)!2!!!%-4=O-!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$"=!A"%!!!1R.SYQ!!!!!!Q8!)!!!!!%-4=O-!!!!!!-&amp;Q#!%1!!"$%X,D!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!',OCZ"C3.J19ESLE'")CB"C4II=9!!!!(`````A!!!!9!!!!'!!!!"A!!!!9"``Q'!1!%"A%$"!9"#U1'"R`E"A%-R!9"/(1'!4BX"A%-R!9"(_1'"QN%"A%$"!9"!!1'!@`]"A!!!!9!!!!'!!!!"A!!!!@````]!!!)!``````````````````````C)C)C)C)C)C)C)C)C)C)`YC0D`_0`Y_)DY``C0C)C0_)DYDYDYC0_0_0C0DYC)D`C)_)_)`YDY_0D`_)_)C)`YC)C0C0C)_)DY_)C0C)C0_)DYDYD`_0C)_0C)D`_)D`C)C)C)C)C)C)C)C)C)C)```````````````````````-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-T-T-T-T-T-T0`-T-T-T-T-T-T-T-T-T-T`T-T-T0`````````]T-T-`]T-T-TQ!!!!!!!!`-T-T0`-T-T-]!!!$&gt;!!!0T-T-T`T-T-T0!!$QX1]!$]T-T-`]T-T$-Q!.X&gt;X&gt;U!`-T-T0`-T-T-]!!.U!X1!0T-T-T`T-T-T0!.X1!!X&gt;$]T-T-`]T-T-TQ$&gt;U!!.X1-TT-T0`-T-T-]!!.U!X1!0T-T-T`T-T-T0!!X&gt;X&gt;X1$]T-T-`]T-T':A!!].U0!!`-T-T0`-T-T-]!!!$&gt;!!!0T-T-T`T-T-T0!!!!!!!!$]T-T-`]T-T-T``````````-T-T0`-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-T-T-T-T-T-T0`-T-T-T-T-T-T-T-T-T-T`````````````````````]!!!1!````````````````````````````````````````````=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R``^R=8(`=@```X(```^R`X&amp;R=@^R````=8(`=8&amp;R=8(``X&amp;R=@^R=@^R=@^R=8(``X(``X(`=8(`=@^R=8&amp;R=@``=8&amp;R`X&amp;R`X&amp;R``^R=@^R`X(`=@```X&amp;R`X&amp;R=8&amp;R``^R=8&amp;R=8(`=8(`=8&amp;R`X&amp;R=@^R`X&amp;R=8(`=8&amp;R=8(``X&amp;R=@^R=@^R=@```X(`=8&amp;R`X(`=8&amp;R=@```X&amp;R=@``=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R`````````````````````````````````````````````SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SP```````````````````]L+SML+SML``]L+SML+SML+`]!!!!!!!!!!!!!!!!!`SML+SML+SP``SML+SML+SML`Q!!!!!!!)([!!!!!!$`+SML+SML+```+SML+SML+SP`!!!!!0Q!A9%!`!!!!0]L+SML+SML``]L+SML+SMD)S-!!!#"A9'"A9'"A1!!`SML+SML+SP``SML+SML+SML`Q!!!!#"A1!!A9%!!!$`+SML+SML+```+SML+SML+SP`!!$[A9%!!!!!A9'"!0]L+SML+SML``]L+SML+SML+`]!!)'"A1!!!!#"A@I!)S-D+SML+SP``SML+SML+SML`Q!!!!#"A1!!A9%!!!$`+SML+SML+```+SML+SML+SP`!!!!A9'"A9'"A9%!!0]L+SML+SML``]L+SML+SP3UN)!!!!!`!#"A1$]!!!!`SML+SML+SP``SML+SML+SML`Q!!!!!!!0K"!!!!!!$`+SML+SML+```+SML+SML+SP`!!!!!!!!!!!!!!!!!0]L+SML+SML``]L+SML+SML+````````````````````SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+````````````````````````````````````````````Q!!!!)!!1!!!!!!$!!"2F")5!!!!!!!!Q!!!H]!!!4J?*SNF%VI%U%5R^_%L5R#C\/RN1UU**:N,'*!&amp;,5'0YK&gt;#N63F,4A)&gt;1%NXZ!;T1@YKG^,%)/P2D-1?AN"$RZ#-7\"#^\M#=&amp;?YA.?0&gt;3&amp;/RG@4PJ\C9JRIO\-#T,`.\`T@P`'1$J/RPR.'(.!-,W]'0/!*_K%Y";F-,"%XE*&lt;*(]"D)9)!:-U57WYWG3510[64V#T`!#`-$&gt;ZD@T&amp;&lt;QGB/XCVC-MA-6]"BR6^7(ZJF*HSLN2J&gt;"H6Z5BS$:)UX.8#@WE'VI;"5'&lt;M&amp;9Z3JJ!_!F*UM:OJZYM;YLVVROF!6(3;Q$D_E"'K:`%CCD^1:1E3V!G;&lt;ME9-E*K&amp;;L,C3XI)BI9^*CSFBWCSTV90R=$W;6_GH"_!3$/FOW$A]VCV&lt;P&amp;N3.(O-[1R3Z&amp;`;2S\97$RF4AGMU'MDB?M!^-G"1K6_H!&lt;KL@OU&lt;O+/^"Q+E&gt;I_;8]SX&amp;C`07S[)P8YU9DR'7"S`YQ;=UH20EEGW6B[+QA&lt;*NO'S:=-.99/8&amp;RCY0B4`Y1/8;G0"[:6].L?=#;=@B/_PJ,,:].0-Y_?JX(*94?63BRW[QP8!/?PUFJA)"QS""-]AW4\N.&amp;1K&amp;2Q!LCZ[&amp;&gt;&amp;BJ?ZQMDC.[ER=&gt;3&gt;OK&lt;K4OY;4Y\]7(FL4'Y^Z\-S#G^;TG&amp;;:&gt;4@5E&gt;&lt;T`T_N&amp;T"%;VVJB13_KTW3&gt;\%&amp;N;5VA&gt;1K*(IQE]CM&gt;[26K.A[P&gt;*[[8";%\;7E^:3K&gt;4"Y=RD4FIF1FJJV&lt;&lt;.@8-@-YPDF3!*3:&amp;:]-[X&gt;5ZX0O;&gt;SY8'&amp;7\3Q.]XM"E57T&gt;AC%`\0\&amp;:L.:0:_E-WU28]4I[4D@J8OWT=`X5ZBR3WY9UO;7]#&gt;'20Z9&amp;:T%!!!!!"!!!!#!!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!"Z!!!!!9!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!2B=!A!!!!!!"!!A!-0````]!!1!!!!!!+A!!!!%!)E"1!!!&lt;6EF18U*P&lt;X2M&lt;W&amp;E:8*&amp;&lt;H2F=CZM&gt;G.M98.T!!%!!!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!F&amp;Q#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!!!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$7P9SO!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.;^D+Y!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!!2B=!A!!!!!!"!!A!-0````]!!1!!!!!!+A!!!!%!)E"1!!!&lt;6EF18U*P&lt;X2M&lt;W&amp;E:8*&amp;&lt;H2F=CZM&gt;G.M98.T!!%!!!!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'2=!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!!!!!%!!)!#!!!!!1!!!"!!!!!+!!!!!)!!!1!!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$V!!!"&gt;8C=D9]^4A-R%)5`RY(]%5CAI%"),CBII/%#KS#1UKV911MGXE72,)RWH9C3!X!7&lt;M!V/!@=A-EGC))5[%EDTXMT\YW"05K_0NZ@XQ$&gt;P]Z'*_-M/`6T0\U`O"GHN[-1IA`7Z?8&amp;9]R,53&lt;?6N8_/GU30=&gt;*#I@H@F9*9U*B[H(T6%\H.O&lt;'W7D:%.$&amp;A@J%3;.8,7@S\#1N&gt;/%@;#=P/FT&amp;*:'(AI&amp;Y;RH@J+6HLK#\S.,RW=FW3I-W(9\_F&gt;_ANJ'CE,5\C7ZS+3;',@J#,L!N94^1+`RF@J7&gt;'MO0+!&lt;CC"S%B$7&amp;\4&amp;E6WLP'QB?1;!!!!!!!!"M!!%!!A!$!!1!!!")!!]#!!!!!!]!W!$6!!!!51!0!A!!!!!0!.A!V1!!!&amp;I!$Q)!!!!!$Q$9!.5!!!"D!!]%!!!!!!]!W!$6#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*5F.31QU+!!.-6E.$4%*76Q!!%0A!!!2A!!!!)!!!%.A!!!!!!!!!!!!!!#!!!!!U!!!%3!!!!"V-35*/!!!!!!!!!7R-6F.3!!!!!!!!!9"36&amp;.(!!!!!!!!!:2$1V.5!!!!!!!!!;B-38:J!!!!!!!!!&lt;R$4UZ1!!!!!!!!!&gt;"544AQ!!!!!!!!!?2%2E24!!!!!!!!!@B-372T!!!!!!!!!AR735.%!!!!!!!!!C"W:8*T!!!!"!!!!D241V.3!!!!!!!!!JB(1V"3!!!!!!!!!KR*1U^/!!!!!!!!!M"J9WQU!!!!!!!!!N2J9WQY!!!!!!!!!OB$5%-S!!!!!!!!!PR-37:Q!!!!!!!!!R"'5%BC!!!!!!!!!S2'5&amp;.&amp;!!!!!!!!!TB75%21!!!!!!!!!UR-37*E!!!!!!!!!W"#2%BC!!!!!!!!!X2#2&amp;.&amp;!!!!!!!!!YB73624!!!!!!!!!ZR%6%B1!!!!!!!!!\".65F%!!!!!!!!!]2)36.5!!!!!!!!!^B71V21!!!!!!!!!_R'6%&amp;#!!!!!!!!"!!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$1!!!!!!!!!!0````]!!!!!!!!!W!!!!!!!!!!!`````Q!!!!!!!!$M!!!!!!!!!!$`````!!!!!!!!!01!!!!!!!!!!0````]!!!!!!!!")!!!!!!!!!!!`````Q!!!!!!!!%I!!!!!!!!!!$`````!!!!!!!!!6!!!!!!!!!!!0````]!!!!!!!!"H!!!!!!!!!!!`````Q!!!!!!!!'M!!!!!!!!!!4`````!!!!!!!!!R1!!!!!!!!!"`````]!!!!!!!!$*!!!!!!!!!!)`````Q!!!!!!!!-U!!!!!!!!!!H`````!!!!!!!!!U1!!!!!!!!!#P````]!!!!!!!!$6!!!!!!!!!!!`````Q!!!!!!!!.E!!!!!!!!!!$`````!!!!!!!!!XQ!!!!!!!!!!0````]!!!!!!!!$E!!!!!!!!!!!`````Q!!!!!!!!15!!!!!!!!!!$`````!!!!!!!!"BA!!!!!!!!!!0````]!!!!!!!!+(!!!!!!!!!!!`````Q!!!!!!!!IE!!!!!!!!!!$`````!!!!!!!!#D1!!!!!!!!!!0````]!!!!!!!!-O!!!!!!!!!!!`````Q!!!!!!!!T!!!!!!!!!!!$`````!!!!!!!!$-A!!!!!!!!!!0````]!!!!!!!!-W!!!!!!!!!!!`````Q!!!!!!!!V!!!!!!!!!!!$`````!!!!!!!!$5A!!!!!!!!!!0````]!!!!!!!!0-!!!!!!!!!!!`````Q!!!!!!!!]Y!!!!!!!!!!$`````!!!!!!!!$U!!!!!!!!!!!0````]!!!!!!!!0&lt;!!!!!!!!!#!`````Q!!!!!!!""I!!!!!"&gt;736"@1G^P&gt;'RP972F=E6O&gt;'6S,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"Z!!!!!B*.&lt;W2V&lt;'5A1W^S:3ZM&gt;GRJ9H!-6'&amp;T;SZM&gt;G.M98.T5&amp;2)-!!!!%U!!1!*!!!!!!!9)5.P&lt;8"J&lt;'6E)%:S97VF&gt;W^S;S"-;7*T%EVP:(6M:3"$&lt;X*F,GRW&lt;'FC=!J598.L8W.M98.T$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="VIP_BootloaderEnter.ctl" Type="Class Private Data" URL="VIP_BootloaderEnter.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Data Types" Type="Folder">
		<Item Name="Constructor Data.ctl" Type="VI" URL="../Constructor Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="Return Data.ctl" Type="VI" URL="../Return Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0(7P:DP!!!!!Q]B6%6.5%R"6%5O&lt;(:M;7)7)62&amp;46"-162&amp;8V2"5UMO&lt;(:D&lt;'&amp;T=RR$&lt;WZT&gt;(*V9X2P=C"%982B)*=A[O\P[0]O9X2M!"J!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082401280</Property>
		</Item>
		<Item Name="Used Local Variables.ctl" Type="VI" URL="../Used Local Variables.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0(7P:DP!!!!!Q]B6%6.5%R"6%5O&lt;(:M;7)7)62&amp;46"-162&amp;8V2"5UMO&lt;(:D&lt;'&amp;T=RR$&lt;WZT&gt;(*V9X2P=C"%982B)*=A[O\P[0]O9X2M!"J!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082401280</Property>
		</Item>
		<Item Name="Used Sequence Variables.ctl" Type="VI" URL="../Used Sequence Variables.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0(7P:DP!!!!!Q]B6%6.5%R"6%5O&lt;(:M;7)7)62&amp;46"-162&amp;8V2"5UMO&lt;(:D&lt;'&amp;T=RR$&lt;WZT&gt;(*V9X2P=C"%982B)*=A[O\P[0]O9X2M!"J!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082401280</Property>
		</Item>
		<Item Name="Created Sequence Variables.ctl" Type="VI" URL="../Created Sequence Variables.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="Get Data Types.vi" Type="VI" URL="../Get Data Types.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(0!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!11&amp;-,5G6U&gt;8*O)%2B&gt;'%!'E"4&amp;&amp;6T:71A4'^D97QA6G&amp;S;7&amp;C&lt;'6T!!!=1&amp;-868.F:#"4:8&amp;V:7ZD:3"798*J97*M:8-!)%"4'E.S:7&amp;U:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!!!=1&amp;!!"1!&amp;!!9!"Q!)!!E+2'&amp;U93"5?8"F=Q!!0E"Q!"Y!!#M.66.#,5F45SZM&gt;GRJ9BN736"@1G^P&gt;'RP972F=E6O&gt;'6S,GRW9WRB=X-!#&amp;2B=WMA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$R!=!!?!!!L$6641CV*5V-O&lt;(:M;7)&lt;6EF18U*P&lt;X2M&lt;W&amp;E:8*&amp;&lt;H2F=CZM&gt;G.M98.T!!&gt;598.L)'FO!&amp;1!]!!-!!-!"!!+!!M!"!!%!!1!"!!-!!1!"!!.!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!$A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574592</Property>
	</Item>
	<Item Name="Get Hierarchy Path.vi" Type="VI" URL="../Get Hierarchy Path.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;D!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!=1%!!!@````]!"1Z);76S98*D;(EA='&amp;U;!!!0E"Q!"Y!!#M.66.#,5F45SZM&gt;GRJ9BN736"@1G^P&gt;'RP972F=E6O&gt;'6S,GRW9WRB=X-!#&amp;2B=WMA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$R!=!!?!!!L$6641CV*5V-O&lt;(:M;7)&lt;6EF18U*P&lt;X2M&lt;W&amp;E:8*&amp;&lt;H2F=CZM&gt;G.M98.T!!&gt;598.L)'FO!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!"!!*!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Get Properties.vi" Type="VI" URL="../Get Properties.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(&amp;!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!)1&gt;)98-A2F!`!":!-0````]-68.F=C"'5#"/97VF!!!51$$`````#U2F=W.S;8"U;7^O!&amp;A!]1!!!!!!!!!$%EVP:(6M:3"$&lt;X*F,GRW&lt;'FC=!R598.L,GRW9WRB=X-46'&amp;T;S"1=G^Q:8*U;76T,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ1=G^Q:8*U;76T!!!_1(!!(A!!+QV65U)N36.4,GRW&lt;'FC'V:*5&amp;^#&lt;W^U&lt;'^B:'6S27ZU:8)O&lt;(:D&lt;'&amp;T=Q!)6'&amp;T;S"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!0%"Q!"Y!!#M.66.#,5F45SZM&gt;GRJ9BN736"@1G^P&gt;'RP972F=E6O&gt;'6S,GRW9WRB=X-!"V2B=WMA;7Y!6!$Q!!Q!!Q!%!!A!#1!%!!1!"!!%!!I!"!!%!!M#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!-!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
	</Item>
	<Item Name="Execute.vi" Type="VI" URL="../Execute.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'R!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!=1&amp;!!!Q!!!!%!!AZ3:82V=GZF:#"&amp;=H*P=A!!"!!!!"*!5QV3:82V=GZF:#"%982B!$Z!=!!?!!!L$6641CV*5V-O&lt;(:M;7)&lt;6EF18U*P&lt;X2M&lt;W&amp;E:8*&amp;&lt;H2F=CZM&gt;G.M98.T!!B598.L)'^V&gt;!!!&amp;E!Q`````QR6=W6S)%:1)%ZB&lt;75!!!R!)1&gt;4;'^X)%:1!":!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!51&amp;-04'^D97QA6G&amp;S;7&amp;C&lt;'6T!"B!5R*4:8&amp;V:7ZD:3"798*J97*M:8-!!"Z!5!!$!!E!#A!,%%&amp;D&gt;(6B&lt;#"598.L)%2B&gt;'%!!$R!=!!?!!!L$6641CV*5V-O&lt;(:M;7)&lt;6EF18U*P&lt;X2M&lt;W&amp;E:8*&amp;&lt;H2F=CZM&gt;G.M98.T!!&gt;598.L)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"Q!%!!A!"!!%!!1!$!!.!Q!!?!!!#1!!!!!!!!!*!!!!D1M!!B!!!!!!!!!!%!!!!!!!!!!!!!!!!!!!!"!!!!#3!!!!!!%!$A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
</LVClass>
