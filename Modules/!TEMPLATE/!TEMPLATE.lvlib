﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="INSTRUCTIONS" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Rename everything to the real module name" Type="Folder">
			<Item Name="Rename Project" Type="Folder"/>
			<Item Name="Rename Library" Type="Folder"/>
			<Item Name="Rename Build Specification Name" Type="Folder"/>
			<Item Name="Rename Build Specidication Target Filename" Type="Folder"/>
		</Item>
		<Item Name="Modify Module.lvclass" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Change inheritance to a meaningful hierarchy class" Type="Folder"/>
			<Item Name="Change a name string in Get Hierarchy Path.vi" Type="Folder"/>
			<Item Name="Implement code in Init.vi (if needed)" Type="Folder"/>
			<Item Name="Implement code in Deinit.vi (if needed)" Type="Folder"/>
			<Item Name="Implement code in Handle Command.vi" Type="Folder"/>
			<Item Name="Handle error codes in Handle Error.vi to avoid actor closing" Type="Folder"/>
			<Item Name="Implement code in Actor Core.vi (if needed)" Type="Folder"/>
		</Item>
		<Item Name="Creating New Task Classes" Type="Folder">
			<Item Name="!TEMPLATE_TASK.lvclass -&gt; Save -&gt;Save As... -&gt; Copy" Type="Folder"/>
			<Item Name="Modify Data TypeDefs (which are used by the task)" Type="Folder"/>
			<Item Name="Change a name string array in Get Hierarchy Path.vi" Type="Folder"/>
			<Item Name="Change properties in Get Properties.vi (if needed)" Type="Folder"/>
			<Item Name="Implement code in Constructor.vi (if needed)" Type="Folder"/>
			<Item Name="Make sure everything on Front Panel is localised and pretty" Type="Folder"/>
			<Item Name="Delete Constructor.vi from project and disk if not used" Type="Folder"/>
			<Item Name="Implement code in Execute.vi" Type="Folder"/>
		</Item>
		<Item Name="When done.." Type="Folder">
			<Item Name="Put any external dependency files to the top folder" Type="Folder"/>
			<Item Name="Delete !TEMPLATE_TASK.lvclass from project" Type="Folder"/>
			<Item Name="Delete !TEMPLATE_TASK.lvclass from disk" Type="Folder"/>
			<Item Name="Delete these instructions&apos; folder" Type="Folder"/>
		</Item>
	</Item>
	<Item Name="Support" Type="Folder"/>
	<Item Name="Tasks" Type="Folder">
		<Item Name="!TEMPLATE_TASK.lvclass" Type="LVClass" URL="../Task Classes/!TEMPLATE_TASK/!TEMPLATE_TASK.lvclass"/>
	</Item>
	<Item Name="Module.lvclass" Type="LVClass" URL="../Module Class/Module.lvclass"/>
</Library>
