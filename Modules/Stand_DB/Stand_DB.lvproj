﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="utf.calculate.project.code.coverage" Type="Bool">true</Property>
	<Property Name="utf.create.arraybrackets" Type="Str">[]</Property>
	<Property Name="utf.create.arraythreshold" Type="UInt">100</Property>
	<Property Name="utf.create.captureinputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.captureoutputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.codecoverage.flag" Type="Bool">false</Property>
	<Property Name="utf.create.codecoverage.value" Type="UInt">100</Property>
	<Property Name="utf.create.editor.flag" Type="Bool">false</Property>
	<Property Name="utf.create.editor.path" Type="Path"></Property>
	<Property Name="utf.create.nameseparator" Type="Str">/</Property>
	<Property Name="utf.create.precision" Type="UInt">6</Property>
	<Property Name="utf.create.repetitions" Type="UInt">1</Property>
	<Property Name="utf.create.testpath.flag" Type="Bool">false</Property>
	<Property Name="utf.create.testpath.path" Type="Path"></Property>
	<Property Name="utf.create.timeout.flag" Type="Bool">false</Property>
	<Property Name="utf.create.timeout.value" Type="UInt">0</Property>
	<Property Name="utf.create.type" Type="UInt">0</Property>
	<Property Name="utf.enable.RT.VI.server" Type="Bool">false</Property>
	<Property Name="utf.passwords" Type="Bin">&amp;Q#!!!!!!!)!%%!Q`````Q:4&gt;(*J&lt;G=!!":!1!!"`````Q!!#6"B=X.X&lt;X*E=Q!"!!%!!!!"!!!!#F652E&amp;-4&amp;.516)!!!!!</Property>
	<Property Name="utf.report.atml.create" Type="Bool">false</Property>
	<Property Name="utf.report.atml.path" Type="Path">ATML report.xml</Property>
	<Property Name="utf.report.atml.view" Type="Bool">false</Property>
	<Property Name="utf.report.details.errors" Type="Bool">false</Property>
	<Property Name="utf.report.details.failed" Type="Bool">false</Property>
	<Property Name="utf.report.details.passed" Type="Bool">false</Property>
	<Property Name="utf.report.errors" Type="Bool">true</Property>
	<Property Name="utf.report.failed" Type="Bool">true</Property>
	<Property Name="utf.report.html.create" Type="Bool">false</Property>
	<Property Name="utf.report.html.path" Type="Path">HTML report.html</Property>
	<Property Name="utf.report.html.view" Type="Bool">false</Property>
	<Property Name="utf.report.passed" Type="Bool">true</Property>
	<Property Name="utf.report.skipped" Type="Bool">true</Property>
	<Property Name="utf.report.sortby" Type="UInt">1</Property>
	<Property Name="utf.report.stylesheet.flag" Type="Bool">false</Property>
	<Property Name="utf.report.stylesheet.path" Type="Path"></Property>
	<Property Name="utf.report.summary" Type="Bool">true</Property>
	<Property Name="utf.report.txt.create" Type="Bool">false</Property>
	<Property Name="utf.report.txt.path" Type="Path">ASCII report.txt</Property>
	<Property Name="utf.report.txt.view" Type="Bool">false</Property>
	<Property Name="utf.run.changed.days" Type="UInt">1</Property>
	<Property Name="utf.run.changed.outdated" Type="Bool">false</Property>
	<Property Name="utf.run.changed.timestamp" Type="Bin">&amp;Q#!!!!!!!%!%%"5!!9*2'&amp;U:3^U;7VF!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	<Property Name="utf.run.days.flag" Type="Bool">false</Property>
	<Property Name="utf.run.includevicallers" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.overwrite" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.path" Type="Path">test execution log.txt</Property>
	<Property Name="utf.run.modified.last.run.flag" Type="Bool">true</Property>
	<Property Name="utf.run.priority.flag" Type="Bool">false</Property>
	<Property Name="utf.run.priority.value" Type="UInt">5</Property>
	<Property Name="utf.run.statusfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.statusfile.path" Type="Path">test status log.txt</Property>
	<Property Name="utf.run.timestamp.flag" Type="Bool">false</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Core Libs" Type="Folder">
			<Item Name="Shared.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Shared.lvlibp"/>
			<Item Name="Data Parser.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp"/>
			<Item Name="Actor Framework.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp"/>
			<Item Name="Thread.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Thread.lvlibp"/>
			<Item Name="Module Core.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Module Core.lvlibp"/>
			<Item Name="Modules Hierarchy.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp"/>
			<Item Name="DB API.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/DB API.lvlibp"/>
		</Item>
		<Item Name="Post-Build Action.vi" Type="VI" URL="../Post-Build Action.vi"/>
		<Item Name="Stand_DB.lvlib" Type="Library" URL="../Stand_DB.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
			</Item>
			<Item Name="Post-Build. Move Build (.lvlibp).vi" Type="VI" URL="../../../Post-Build Action VIs/Post-Build. Move Build (.lvlibp).vi"/>
			<Item Name="Stand ID.ctl" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/ID TypeDefs/Stand ID.ctl"/>
			<Item Name="Stand Test Type ID.ctl" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/ID TypeDefs/Stand Test Type ID.ctl"/>
			<Item Name="Device ID.ctl" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/ID TypeDefs/Device ID.ctl"/>
			<Item Name="Conn ObjectReference.ctl" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/1abvi3w/vi.lib/addons/database/Connection.llb/Conn ObjectReference.ctl"/>
			<Item Name="Test ID.ctl" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/ID TypeDefs/Test ID.ctl"/>
			<Item Name="MF DB Close.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/MF DB Close.vi"/>
			<Item Name="CA DB Close.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/CityAir DB/CA DB Close.vi"/>
			<Item Name="FT DB Close.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/FirTwig DB/FT DB Close.vi"/>
			<Item Name="G1 DB Close.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/G1 DB/G1 DB Close.vi"/>
			<Item Name="MF DB Open.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/MF DB Open.vi"/>
			<Item Name="Task Properties.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Task_class/Task Properties.ctl"/>
			<Item Name="Send Command Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Thread.lvlibp/Thread_class/Send Command Message.vi"/>
			<Item Name="Task.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Task_class/Task.lvclass"/>
			<Item Name="MFDB_tests INSERT.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Tests/MFDB_tests INSERT.vi"/>
			<Item Name="MFDB_UniStand Identify User and Workstation.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/UniStand/MFDB_UniStand Identify User and Workstation.vi"/>
			<Item Name="MFDB_tests UPDATE.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Tests/MFDB_tests UPDATE.vi"/>
			<Item Name="MFDB_testTables INSERT Value.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Test Tables/MFDB_testTables INSERT Value.vi"/>
			<Item Name="Dynamic Type Definition.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Dynamic Type Definition.vi"/>
			<Item Name="MFDB_complex Check ID by Test Type.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Complex/MFDB_complex Check ID by Test Type.vi"/>
			<Item Name="MFDB_complex Check SN by Test Type.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Complex/MFDB_complex Check SN by Test Type.vi"/>
			<Item Name="MFDB_complex Check IDs as Components.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Complex/MFDB_complex Check IDs as Components.vi"/>
			<Item Name="MFDB_complex Check SNs as Components.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Complex/MFDB_complex Check SNs as Components.vi"/>
			<Item Name="MFDB_complex Search ID by SN.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Complex/MFDB_complex Search ID by SN.vi"/>
			<Item Name="MFDB_complex New ID by Test Type.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Complex/MFDB_complex New ID by Test Type.vi"/>
			<Item Name="MFDB_deviceAssembly INSERT.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Devices/MFDB_deviceAssembly INSERT.vi"/>
			<Item Name="MFDB_deviceSerialNumbers INSERT.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Devices/MFDB_deviceSerialNumbers INSERT.vi"/>
			<Item Name="Clear All Errors.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Error Management/Clear All Errors.vi"/>
			<Item Name="Actor.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Actor/Actor.lvclass"/>
			<Item Name="Database.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Database Class/Database.lvclass"/>
			<Item Name="Get Array Active Element Index.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/UI Refs/Get Array Active Element Index.vi"/>
			<Item Name="Normal String to Hex String.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/String/Normal String to Hex String.vi"/>
			<Item Name="Hex String to Normal String.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/String/Hex String to Normal String.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Stand_DB" Type="Packed Library">
				<Property Name="Bld_buildCacheID" Type="Str">{E882488C-0F99-4060-82D8-8CB6D40A3187}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Stand_DB</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../Modules</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_postActionVIID" Type="Ref">/My Computer/Post-Build Action.vi</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{68A38F2B-5226-4401-98AA-2CDA05EACD8F}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Stand_DB.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">../Modules/NI_AB_PROJECTNAME.lvlibp</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../Dependencies</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[2].destName" Type="Str">Core Libs</Property>
				<Property Name="Destination[2].path" Type="Path">..</Property>
				<Property Name="Destination[2].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{AA5B4E81-50EF-40EE-B3A8-53505CE80F0F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[1].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[1].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Core Libs</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/Stand_DB.lvlib/Module.lvclass/Handle Command.vi</Property>
				<Property Name="Source[10].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[10].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[10].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[10].type" Type="Str">VI</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/Stand_DB.lvlib/Module.lvclass/Handle Error.vi</Property>
				<Property Name="Source[11].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[11].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[11].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[11].type" Type="Str">VI</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/Stand_DB.lvlib/Module.lvclass/Actor Core.vi</Property>
				<Property Name="Source[12].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[12].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[12].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[12].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Core Libs/Actor Framework.lvlibp</Property>
				<Property Name="Source[2].preventRename" Type="Bool">true</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Core Libs/Thread.lvlibp</Property>
				<Property Name="Source[3].preventRename" Type="Bool">true</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Core Libs/Module Core.lvlibp</Property>
				<Property Name="Source[4].preventRename" Type="Bool">true</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Core Libs/Modules Hierarchy.lvlibp</Property>
				<Property Name="Source[5].preventRename" Type="Bool">true</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Stand_DB.lvlib</Property>
				<Property Name="Source[6].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[6].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[6].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[6].preventRename" Type="Bool">true</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[6].type" Type="Str">Library</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/Stand_DB.lvlib/Module.lvclass/Get Hierarchy Path.vi</Property>
				<Property Name="Source[7].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[7].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[7].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[7].type" Type="Str">VI</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/Stand_DB.lvlib/Module.lvclass/Init.vi</Property>
				<Property Name="Source[8].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[8].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[8].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[8].type" Type="Str">VI</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/Stand_DB.lvlib/Module.lvclass/Deinit.vi</Property>
				<Property Name="Source[9].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[9].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[9].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">13</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Stand_DB</Property>
				<Property Name="TgtF_internalName" Type="Str">Stand_DB</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2016 </Property>
				<Property Name="TgtF_productName" Type="Str">Stand_DB</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{87C8F61C-58B6-4975-9749-E7BE763F6654}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Stand_DB.lvlibp</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
