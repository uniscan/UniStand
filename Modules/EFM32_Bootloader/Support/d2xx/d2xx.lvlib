﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">EFM32_Bootloader.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../EFM32_Bootloader.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5Q&lt;/07RB7W!,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@PWW`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"\Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"XC-_N!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="FT_Clear_DTR.vi" Type="VI" URL="../FT_Clear_DTR.vi"/>
	<Item Name="FT_Clear_RTS.vi" Type="VI" URL="../FT_Clear_RTS.vi"/>
	<Item Name="FT_Close_Device.vi" Type="VI" URL="../FT_Close_Device.vi"/>
	<Item Name="FT_Create_Device_Info_List.vi" Type="VI" URL="../FT_Create_Device_Info_List.vi"/>
	<Item Name="FT_Cycle_Port.vi" Type="VI" URL="../FT_Cycle_Port.vi"/>
	<Item Name="FT_Erase_EEPROM.vi" Type="VI" URL="../FT_Erase_EEPROM.vi"/>
	<Item Name="FT_Get_Bit_Mode.vi" Type="VI" URL="../FT_Get_Bit_Mode.vi"/>
	<Item Name="FT_Get_COM_Port_Number.vi" Type="VI" URL="../FT_Get_COM_Port_Number.vi"/>
	<Item Name="FT_Get_Device_Description_By_Index.vi" Type="VI" URL="../FT_Get_Device_Description_By_Index.vi"/>
	<Item Name="FT_Get_Device_Info.vi" Type="VI" URL="../FT_Get_Device_Info.vi"/>
	<Item Name="FT_Get_Device_Info_Detail.vi" Type="VI" URL="../FT_Get_Device_Info_Detail.vi"/>
	<Item Name="FT_Get_Device_Info_List.vi" Type="VI" URL="../FT_Get_Device_Info_List.vi"/>
	<Item Name="FT_Get_Device_LocationID_By_Index.vi" Type="VI" URL="../FT_Get_Device_LocationID_By_Index.vi"/>
	<Item Name="FT_Get_Device_Serial_Number_By_Index.vi" Type="VI" URL="../FT_Get_Device_Serial_Number_By_Index.vi"/>
	<Item Name="FT_Get_Driver_Version.vi" Type="VI" URL="../FT_Get_Driver_Version.vi"/>
	<Item Name="FT_Get_EEPROM_User_Area_Size.vi" Type="VI" URL="../FT_Get_EEPROM_User_Area_Size.vi"/>
	<Item Name="FT_Get_Event_Status.vi" Type="VI" URL="../FT_Get_Event_Status.vi"/>
	<Item Name="FT_Get_Latency_Timer.vi" Type="VI" URL="../FT_Get_Latency_Timer.vi"/>
	<Item Name="FT_Get_Library_Version.vi" Type="VI" URL="../FT_Get_Library_Version.vi"/>
	<Item Name="FT_Get_Modem_Status.vi" Type="VI" URL="../FT_Get_Modem_Status.vi"/>
	<Item Name="FT_Get_Number_of_Devices.vi" Type="VI" URL="../FT_Get_Number_of_Devices.vi"/>
	<Item Name="FT_Get_Queue_Status.vi" Type="VI" URL="../FT_Get_Queue_Status.vi"/>
	<Item Name="FT_Get_Queue_Status_Ex.vi" Type="VI" URL="../FT_Get_Queue_Status_Ex.vi"/>
	<Item Name="FT_Get_Status.vi" Type="VI" URL="../FT_Get_Status.vi"/>
	<Item Name="FT_Open_Device_By_Description.vi" Type="VI" URL="../FT_Open_Device_By_Description.vi"/>
	<Item Name="FT_Open_Device_By_Index.vi" Type="VI" URL="../FT_Open_Device_By_Index.vi"/>
	<Item Name="FT_Open_Device_By_Location.vi" Type="VI" URL="../FT_Open_Device_By_Location.vi"/>
	<Item Name="FT_Open_Device_By_Serial_Number.vi" Type="VI" URL="../FT_Open_Device_By_Serial_Number.vi"/>
	<Item Name="FT_Purge.vi" Type="VI" URL="../FT_Purge.vi"/>
	<Item Name="FT_Read_Byte_Data.vi" Type="VI" URL="../FT_Read_Byte_Data.vi"/>
	<Item Name="FT_Read_EEPROM_User_Area.vi" Type="VI" URL="../FT_Read_EEPROM_User_Area.vi"/>
	<Item Name="FT_Read_FT232-FT245B_EEPROM.vi" Type="VI" URL="../FT_Read_FT232-FT245B_EEPROM.vi"/>
	<Item Name="FT_Read_FT232-FT245R_EEPROM.vi" Type="VI" URL="../FT_Read_FT232-FT245R_EEPROM.vi"/>
	<Item Name="FT_Read_FT232H_EEPROM.vi" Type="VI" URL="../FT_Read_FT232H_EEPROM.vi"/>
	<Item Name="FT_Read_FT2232_EEPROM.vi" Type="VI" URL="../FT_Read_FT2232_EEPROM.vi"/>
	<Item Name="FT_Read_FT2232H_EEPROM.vi" Type="VI" URL="../FT_Read_FT2232H_EEPROM.vi"/>
	<Item Name="FT_Read_FT4232H_EEPROM.vi" Type="VI" URL="../FT_Read_FT4232H_EEPROM.vi"/>
	<Item Name="FT_Read_String_Data.vi" Type="VI" URL="../FT_Read_String_Data.vi"/>
	<Item Name="FT_Reload.vi" Type="VI" URL="../FT_Reload.vi"/>
	<Item Name="FT_Rescan.vi" Type="VI" URL="../FT_Rescan.vi"/>
	<Item Name="FT_Reset_Device.vi" Type="VI" URL="../FT_Reset_Device.vi"/>
	<Item Name="FT_Reset_Port.vi" Type="VI" URL="../FT_Reset_Port.vi"/>
	<Item Name="FT_Restart_InTask.vi" Type="VI" URL="../FT_Restart_InTask.vi"/>
	<Item Name="FT_Set_Baud_Rate.vi" Type="VI" URL="../FT_Set_Baud_Rate.vi"/>
	<Item Name="FT_Set_Baud_Rate_Divisor.vi" Type="VI" URL="../FT_Set_Baud_Rate_Divisor.vi"/>
	<Item Name="FT_Set_Bit_Mode.vi" Type="VI" URL="../FT_Set_Bit_Mode.vi"/>
	<Item Name="FT_Set_Break_Off.vi" Type="VI" URL="../FT_Set_Break_Off.vi"/>
	<Item Name="FT_Set_Break_On.vi" Type="VI" URL="../FT_Set_Break_On.vi"/>
	<Item Name="FT_Set_Characters.vi" Type="VI" URL="../FT_Set_Characters.vi"/>
	<Item Name="FT_Set_Data_Characteristics.vi" Type="VI" URL="../FT_Set_Data_Characteristics.vi"/>
	<Item Name="FT_Set_Deadman_Timeout.vi" Type="VI" URL="../FT_Set_Deadman_Timeout.vi"/>
	<Item Name="FT_Set_DTR.vi" Type="VI" URL="../FT_Set_DTR.vi"/>
	<Item Name="FT_Set_Event_Notification.vi" Type="VI" URL="../FT_Set_Event_Notification.vi"/>
	<Item Name="FT_Set_Flow_Control.vi" Type="VI" URL="../FT_Set_Flow_Control.vi"/>
	<Item Name="FT_Set_Latency_Timer.vi" Type="VI" URL="../FT_Set_Latency_Timer.vi"/>
	<Item Name="FT_Set_Reset_Pipe_Retry_Count.vi" Type="VI" URL="../FT_Set_Reset_Pipe_Retry_Count.vi"/>
	<Item Name="FT_Set_RTS.vi" Type="VI" URL="../FT_Set_RTS.vi"/>
	<Item Name="FT_Set_Timeouts.vi" Type="VI" URL="../FT_Set_Timeouts.vi"/>
	<Item Name="FT_Set_USB_Parameters.vi" Type="VI" URL="../FT_Set_USB_Parameters.vi"/>
	<Item Name="FT_Stop_InTask.vi" Type="VI" URL="../FT_Stop_InTask.vi"/>
	<Item Name="FT_Write_Byte_Data.vi" Type="VI" URL="../FT_Write_Byte_Data.vi"/>
	<Item Name="FT_Write_EEPROM_User_Area.vi" Type="VI" URL="../FT_Write_EEPROM_User_Area.vi"/>
	<Item Name="FT_Write_FT232-FT245B_EEPROM.vi" Type="VI" URL="../FT_Write_FT232-FT245B_EEPROM.vi"/>
	<Item Name="FT_Write_FT232-FT245R_EEPROM.vi" Type="VI" URL="../FT_Write_FT232-FT245R_EEPROM.vi"/>
	<Item Name="FT_Write_FT232H_EEPROM.vi" Type="VI" URL="../FT_Write_FT232H_EEPROM.vi"/>
	<Item Name="FT_Write_FT2232_EEPROM.vi" Type="VI" URL="../FT_Write_FT2232_EEPROM.vi"/>
	<Item Name="FT_Write_FT2232H_EEPROM.vi" Type="VI" URL="../FT_Write_FT2232H_EEPROM.vi"/>
	<Item Name="FT_Write_FT4232H_EEPROM.vi" Type="VI" URL="../FT_Write_FT4232H_EEPROM.vi"/>
	<Item Name="FT_Write_String_Data.vi" Type="VI" URL="../FT_Write_String_Data.vi"/>
	<Item Name="FTDI_Status_Explanation.vi" Type="VI" URL="../FTDI_Status_Explanation.vi"/>
</Library>
