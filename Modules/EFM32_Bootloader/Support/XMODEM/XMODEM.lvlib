﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6618880</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">EFM32_Bootloader.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../EFM32_Bootloader.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)[!!!*Q(C=\&gt;5R4E*"%-&lt;R4W.BSQU-6ZAL9'&gt;F]KYQH156L35(-#:T!"OOQ"7G-M;/WIYLY(_8%&gt;%#'D8'O-^&amp;_.\O\-^^D[@5W\FUJMWR.DX9WI"W4&amp;P@&lt;,:"J3WL]^O@[@4^Z3X&lt;*2`/PW&gt;\^4`.X[^`90WD`M..`_W``?VW?`2,]/X@AR/V"R%^[5&amp;XOKENOUHS*%`S*%`S*!`S)!`S)!`S)(&gt;S*X&gt;S*X&gt;S*T&gt;S)T&gt;S)T&gt;S)_](O=B&amp;,H*)S?,*1EH2J%!S')K3F]34?"*0YO'D%E`C34S**`%Q2)EH]33?R*.YG+&lt;%EXA34_**0*4KEOQ(/:\%1XE&amp;HM!4?!*0Y'&amp;*":Y!%#Q7&amp;![+Q&amp;"Q-HA4?!*0Y/'N!E`A#4S"*`"Q7I%H]!3?Q".YG.*X*&lt;KG(?2Y+#0(YXA=D_.R0*37YX%]DM@R/"[7E_.R0!\#7&gt;!J$E(/*'?!]](R/"Z_S@%Y(M@D?"Q0J`I6]LYT4&gt;-/=DS'R`!9(M.D?#ABQW.Y$)`B-4S5F?%R0)&lt;(]"A?FJ,B-4S'RY!9C\+]D','2'/1%2A?@PL&gt;9PUK2:&gt;90[3[?65XJ?JG5^V%KJN$&gt;&gt;&amp;6&amp;V.VE63&lt;L^J5V7;J.E(VR[H1+IRK%&gt;8E.F"L8F@U*8V"H^.H^)%_I9`JIT&lt;VCQ?OVWON6CMNFUMN&amp;AP.ZX0.:D-.Q[$*:+,R?+T2;,2\$&amp;RQ\"Y)W_@3,?_@LFYOHR`P&lt;B[O\Y?H[\P,^LHF0`$`_2=]'X7K`489IV@E/:%M!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="XMODEM Build Packet.vi" Type="VI" URL="../XMODEM Build Packet.vi"/>
	<Item Name="XMODEM Build Packets.vi" Type="VI" URL="../XMODEM Build Packets.vi"/>
	<Item Name="XMODEM CRC-16.vi" Type="VI" URL="../XMODEM CRC-16.vi"/>
</Library>
