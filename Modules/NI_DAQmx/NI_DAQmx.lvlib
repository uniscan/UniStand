﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="DAQmx Finder" Type="Folder">
		<Item Name="Association.ctl" Type="VI" URL="../DAQmx Finder/Association.ctl"/>
		<Item Name="Find HW.vi" Type="VI" URL="../DAQmx Finder/Find HW.vi"/>
		<Item Name="GetCDAQChassis.vi" Type="VI" URL="../DAQmx Finder/GetCDAQChassis.vi"/>
		<Item Name="GetCDAQModules.vi" Type="VI" URL="../DAQmx Finder/GetCDAQModules.vi"/>
		<Item Name="GetDeviceAlias.vi" Type="VI" URL="../DAQmx Finder/GetDeviceAlias.vi"/>
		<Item Name="GetDeviceNames.vi" Type="VI" URL="../DAQmx Finder/GetDeviceNames.vi"/>
		<Item Name="Module Info.ctl" Type="VI" URL="../DAQmx Finder/Module Info.ctl"/>
		<Item Name="SelectDevPopup.vi" Type="VI" URL="../DAQmx Finder/SelectDevPopup.vi"/>
		<Item Name="SelectModulePopup.vi" Type="VI" URL="../DAQmx Finder/SelectModulePopup.vi"/>
	</Item>
	<Item Name="Tasks" Type="Folder">
		<Item Name="Support" Type="Folder">
			<Item Name="Check for Duplicates.vim" Type="VI" URL="../Task Classes/Support/Check for Duplicates.vim"/>
		</Item>
		<Item Name="Init.lvclass" Type="LVClass" URL="../Task Classes/Init/Init.lvclass"/>
		<Item Name="Deinit.lvclass" Type="LVClass" URL="../Task Classes/Deinit/Deinit.lvclass"/>
		<Item Name="Express.InitDAQFinder.lvclass" Type="LVClass" URL="../Task Classes/Express.InitDAQFinder/Express.InitDAQFinder.lvclass"/>
		<Item Name="Express.Task.SetChannels.lvclass" Type="LVClass" URL="../Task Classes/Express.Task.SetChannels/Express.Task.SetChannels.lvclass"/>
		<Item Name="AI.Task.SetChannels.lvclass" Type="LVClass" URL="../Task Classes/AI.Task.SetChannels/AI.Task.SetChannels.lvclass"/>
		<Item Name="AO.Task.SetChannels.lvclass" Type="LVClass" URL="../Task Classes/AO.Task.SetChannels/AO.Task.SetChannels.lvclass"/>
		<Item Name="DI.Task.SetChannels.lvclass" Type="LVClass" URL="../Task Classes/DI.Task.SetChannels/DI.Task.SetChannels.lvclass"/>
		<Item Name="DO.Task.SetChannels.lvclass" Type="LVClass" URL="../Task Classes/DO.Task.SetChannels/DO.Task.SetChannels.lvclass"/>
		<Item Name="Task.SetTiming.lvclass" Type="LVClass" URL="../Task Classes/Task.SetTiming/Task.SetTiming.lvclass"/>
		<Item Name="Task.Start.lvclass" Type="LVClass" URL="../Task Classes/Task.Start/Task.Start.lvclass"/>
		<Item Name="Task.Stop.lvclass" Type="LVClass" URL="../Task Classes/Task.Stop/Task.Stop.lvclass"/>
		<Item Name="AI.Task.ReadNCNS.lvclass" Type="LVClass" URL="../Task Classes/AI.Task.ReadNCNS/AI.Task.ReadNCNS.lvclass"/>
		<Item Name="AO.Task.WriteNCNS(Array).lvclass" Type="LVClass" URL="../Task Classes/AO.Task.WriteNCNS(Array)/AO.Task.WriteNCNS(Array).lvclass"/>
		<Item Name="AO.Task.WriteNCNS(Formula).lvclass" Type="LVClass" URL="../Task Classes/AO.Task.WriteNCNS(Formula)/AO.Task.WriteNCNS(Formula).lvclass"/>
		<Item Name="DI.Task.ReadNCNS.lvclass" Type="LVClass" URL="../Task Classes/DI.Task.ReadNCNS/DI.Task.ReadNCNS.lvclass"/>
		<Item Name="DO.Task.WriteNC1S.lvclass" Type="LVClass" URL="../Task Classes/DO.Task.WriteNC1S/DO.Task.WriteNC1S.lvclass"/>
	</Item>
	<Item Name="Module.lvclass" Type="LVClass" URL="../Module Class/Module.lvclass"/>
</Library>
