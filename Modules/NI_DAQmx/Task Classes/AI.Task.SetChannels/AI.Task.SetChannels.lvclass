﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassBodyIcon" Type="Str">Actor</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6618880</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">NI_DAQmx.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../NI_DAQmx.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,&amp;!!!*Q(C=T:1^&lt;J."%)&lt;@$V(1_A")S'YC)3("&gt;&amp;1%,.(2_1ID5&lt;JSC&gt;UZYA2T"2==A&amp;3BI:EG[8W&amp;^)G1T,0LD5G)@Y2%%,O?\_?&gt;H&gt;HHGVWP6.M4[&lt;&amp;7B^JY&lt;[P_V&lt;C-K\:Z'[^O_^NP8)?N8_\\GXQ`@PQL@LQ*X2+`OO.@L7\&amp;\_9`V*\P&lt;@`/PRXLFF_XWU\`\(K@`Y&gt;G-S\X`90"I!3XND/_O)JN^V^86]GSX4_\]6_,_@Y]`R[_CYO,P&gt;_XK=`V&lt;(`^&gt;N@XQ0JM&lt;Q`P0^$YFOHB0]($.KF4/9CQR!*TT&amp;2=KU20^%20^%20^%!0^%!0^%!0&gt;%&gt;X&gt;%&gt;X&gt;%&gt;X&gt;%-X&gt;%-X&gt;%-X^.L2B3ZUI6/6:0*EIC2JEC!:$)K33]+4]#1]#1_P3HA3HI1HY5FY'++%*_&amp;*?"+?B)=Q*4Q*4]+4]#1]J+IE74M[0!E0[28Q"$Q"4]!4]$#F!JY!)*AM3"QE!5/"-XA)?!+?A)&gt;("4Q"4]!4]!1]O"8Q"$Q"4]!4]""3KR+6JH2U?%ADB]@B=8A=(I?(V(*Y("[(R_&amp;R?*B/$I`$YU!Y%TL*1:!4Z!RQ8BQ?BY?&lt;("[(R_&amp;R?"Q?8(7&amp;P&amp;;GU*3/$I`"9`!90!;0Q5-+'4Q'D]&amp;D]"A]J*8"9`!90!;0Q=.5-HA-(I0(!$%G:8I:S9R!9Z!B'$T][G[ROEJ23;TW=E45T;OW+&gt;5WG^IG5NM=;IOONJBKC[27@,7CKB6,L1BK([='L1;D.IF;="GI3[Z,\"2&lt;9(.MAIWQ)&gt;&lt;(?C8U,Q_]P,T5=LH5[?GJ&amp;IO&amp;ZP/Z*J/*2K/2BM/B_PW_?LX?ZF2^2F_X\O:=GP,]_?O,YZ0"R_/46V_/:`WDYZ082_/CX`DPW+&gt;6.XX497@&gt;^/VZ.`N_V5X@P?SG(ZY7/[P0\[_[W&lt;@T^:ASNM4]FG&gt;^,PU0:[-?;8X@V/AHE/&gt;!Q1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"5Q5F.31QU+!!.-6E.$4%*76Q!!%0!!!!2!!!!!)!!!%.!!!!!P!!!!!AZ/36^%16&amp;N?#ZM&gt;GRJ9BN"33Z598.L,F.F&gt;%.I97ZO:7RT,GRW9WRB=X-!!!!!I"=!A!!!-!!!#!!%!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!!UNI`C_!ZK1JO:GN^`1-(T!!!!%!!!!"!!!!!!BA//"RIB"5OK(DU8+3`FS&gt;1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!-A^OB,(D8N%MGU&gt;@]Q2W51"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1\+0=6]!H7HW2!0E;,ZZYL1!!!!1!!!!!!!!!*Q!"4&amp;:$1Q!!!!%!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!Q!!!!!#!!%!!!!!!#)!!!!;?*RD9'.A;G#YQ!$%D%$-V-$U!]D_Q-$!!!"7F1&lt;G!!!!!!"&amp;!!!"'(C=9W$!"0_"!%AR-D!QH1$3,'DC9"L'JC&lt;!:3YOO[$CT&amp;!XME*%'9&amp;C4(O!$#;1(+J?E(`!@E)"&lt;&amp;D-"A"W^3A6!!!!!!!!$!!"6EF%5Q!!!!!!!Q!!!71!!!,-?*R,9'2AS$3W-.M!J*G"7*SBA3%Z0S76CQ():Y#!,5Q-&amp;)-!K(F;;/+'"Q[H!9%?PXQ,G.`]BK@&lt;257AO5:&amp;AKF5J.N(2;442Y7FEU8FR:````]X(_%ZX/W2=^T2"K3WGQ-I@NR&amp;B10%!&gt;)M)0J`9!:)&amp;=S]!+"J(!U6SAQF,)9(IAY@&lt;T"BB&amp;A--T)+V@Y&gt;)/H$S/\B!$J#Y/"$FOZ'$3#`&gt;S+)"!LR&gt;):Q3"RXY&gt;!2!`):4X1#L?`EA&lt;G3!WZ`'-C!%B7"4B/13VF!JI06&gt;,-&gt;&gt;^!!O^N""%*F1+A+#&amp;5!&gt;AT9"5=YYA\$QWPN[XO\1/()BB3'$E$=!-3A/%4'?AS-$#!,G9"E,63N$:$.""7$R17)@1(+VE$3]Q8*@*!?E-Q;K"C)P1H+&lt;I#["S4W&amp;UB0A,*"PEW!MLG"\!61NB#1,1"F3Q,:$["M/3B\!T3+=.(/`C[O3.[(JU]!.2VSA!!!!!Q8!)!2!!!%-4=O-!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$"=!A"%!!!1R.SYQ!!!!!!Q8!)!!!!!%-4=O-!!!!!!-&amp;Q#!%1!!"$%X,D!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!',OCZ"C3.J19ESLE'")CB"C4II=9!!!!(`````A!!!!9!!!!'!!!!"A!!!!9"``Q'!1!%"A%$"!9"#U1'"R`E"A%-R!9"/(1'!4BX"A%-R!9"(_1'"QN%"A%$"!9"!!1'!@`]"A!!!!9!!!!'!!!!"A!!!!@````]!!!)!``````````````````````C)C)C)C)C)C)C)C)C)C)`YC0D`_0`Y_)DY``C0C)C0_)DYDYDYC0_0_0C0DYC)D`C)_)_)`YDY_0D`_)_)C)`YC)C0C0C)_)DY_)C0C)C0_)DYDYD`_0C)_0C)D`_)D`C)C)C)C)C)C)C)C)C)C)```````````````````````-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-T-T-T-T-T-T0`-T-T-T-T-T-T-T-T-T-T`T-T-T0`````````]T-T-`]T-T-TQ!!!!!!!!`-T-T0`-T-T-]!!!$&gt;!!!0T-T-T`T-T-T0!!$QX1]!$]T-T-`]T-T$-Q!.X&gt;X&gt;U!`-T-T0`-T-T-]!!.U!X1!0T-T-T`T-T-T0!.X1!!X&gt;$]T-T-`]T-T-TQ$&gt;U!!.X1-TT-T0`-T-T-]!!.U!X1!0T-T-T`T-T-T0!!X&gt;X&gt;X1$]T-T-`]T-T':A!!].U0!!`-T-T0`-T-T-]!!!$&gt;!!!0T-T-T`T-T-T0!!!!!!!!$]T-T-`]T-T-T``````````-T-T0`-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-T-T-T-T-T-T0`-T-T-T-T-T-T-T-T-T-T`````````````````````]!!!1!````````````````````````````````````````````=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R``^R=8(`=@```X(```^R`X&amp;R=@^R````=8(`=8&amp;R=8(``X&amp;R=@^R=@^R=@^R=8(``X(``X(`=8(`=@^R=8&amp;R=@``=8&amp;R`X&amp;R`X&amp;R``^R=@^R`X(`=@```X&amp;R`X&amp;R=8&amp;R``^R=8&amp;R=8(`=8(`=8&amp;R`X&amp;R=@^R`X&amp;R=8(`=8&amp;R=8(``X&amp;R=@^R=@^R=@```X(`=8&amp;R`X(`=8&amp;R=@```X&amp;R=@``=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R`````````````````````````````````````````````SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SP```````````````````]L+SML+SML``]L+SML+SML+`]!!!!!!!!!!!!!!!!!`SML+SML+SP``SML+SML+SML`Q!!!!!!!)([!!!!!!$`+SML+SML+```+SML+SML+SP`!!!!!0Q!A9%!`!!!!0]L+SML+SML``]L+SML+SMD)S-!!!#"A9'"A9'"A1!!`SML+SML+SP``SML+SML+SML`Q!!!!#"A1!!A9%!!!$`+SML+SML+```+SML+SML+SP`!!$[A9%!!!!!A9'"!0]L+SML+SML``]L+SML+SML+`]!!)'"A1!!!!#"A@I!)S-D+SML+SP``SML+SML+SML`Q!!!!#"A1!!A9%!!!$`+SML+SML+```+SML+SML+SP`!!!!A9'"A9'"A9%!!0]L+SML+SML``]L+SML+SP3UN)!!!!!`!#"A1$]!!!!`SML+SML+SP``SML+SML+SML`Q!!!!!!!0K"!!!!!!$`+SML+SML+```+SML+SML+SP`!!!!!!!!!!!!!!!!!0]L+SML+SML``]L+SML+SML+````````````````````SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+````````````````````````````````````````````Q!!!!Q!!5:13&amp;!!!!!!!!-!!!*`!!!%[8C=L:2.;"."&amp;-@@B+V-1IOTM&lt;5..#37&lt;3RC1"3V"D_+H1L65J3UY#(5",&gt;_1'MU(_+JP3R#$LU9T%(I,11]?1D&amp;OQ1P?\!H"8O)$8DX5B4M:HU\[?YG+=;,OT!MS`T?`]X\`RE![4M&lt;]42BT1$#^P"DTA#@KB/!7J4#Q2.Z#7S2`!9S'#!'4.&amp;&amp;NO.JEF%$_F5^1M`Q!PT!X?9X]R7]*I4NYN9D,)$&amp;@!9=6@6B_;:3:]K\5;81:V?6)=AW3..T6QH^J"N;'A6"G\"7/5K;10A*3&gt;,'&lt;K??,'O+^&gt;=&lt;J1&amp;2UGM!Y`J!2KG@R)II`5'5*%N1*GG\*'$*#;B7KSYEN[#);'039MJ9&gt;IMM^7$]8!^GF@JJQ@A%ATJ&lt;NAY0.9N7\R&lt;5D2\D/E-5O2@WE=OW&amp;A]:5Y*L."L)Y8L!04*A5+F@JQ'[KX\N'\CDP1=#J(;0GF`-NR9PTVMOC,V_.')]2FA=P_-'H.*U4Z**NF9?CM)'S&lt;&lt;BMG8$$7'$FR=9O$Y5`_%$FWJDQ?G6@$;XH!GH(Y4PL[3SW@$4T/0HK&gt;RS7%XF5I=&gt;OM,VQ$HL^*;9#!=-A140).E_\425+B5=!+YO?B829;8O=,)YD?J-8(5H&lt;KG[E\O'E_/`&amp;BZ;URO0??T-AJP7MZB7G85XV*(7]`]`L2=Q2'N&gt;;95%PKM^EH?R"&lt;7F.9(5+C2[-*0)L(?E6;D9/LX3?OFQ7B/WFJ075KH5Q?(-9UZ;*5*;;&gt;7WT8VT(T/,YZ5A#5G27@$/NX6/&gt;T\GH=O&amp;RB6OUM$@.\!:&amp;&amp;MX9)B0_T_R7;T74W@J$.N%6`%[/EYX[6\NMX0^V/9=5NO'.,GFP!H2E4_7"7=R!!!!!!1!!!!A!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!?1!!!!'!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!%98!)!!!!!!!1!)!$$`````!!%!!!!!!#I!!!!"!#*!5!!!'U&amp;*,F2B=WMO5W6U1WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!*2=!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!!!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!VLW-LA!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$7P9SO!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!%98!)!!!!!!!1!)!$$`````!!%!!!!!!#I!!!!"!#*!5!!!'U&amp;*,F2B=WMO5W6U1WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E8!)!!!!!!!1!&amp;!!-!!!%!!!!!!!!!!!!!!!!!"!!#!!A!!!!%!!!!1!!!!#A!!!!#!!!%!!!!!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!_!!!!8:YH)W005Y$-2#&amp;0]?"`%%)5&amp;!A*"=56.NQA9W#E.+A]./$S8IBQG22\%1J/1"HY1:=AX0!$:BMACCA1%];?&gt;[&lt;?7]-\",Y@(^\?16U_[R`@&gt;)^@ZQH@O:(N`P&gt;@H*FQU.S[7,PXI\(TA&gt;2BN['M0?8.IS?IX1!"TU`$&gt;&amp;.4*'&lt;=NQ]459T'ZX*&lt;,3M#7C3A@J!3;.8,=@S&lt;+1V&gt;/\PK+@0OLC)3])6/2XRVD+_4EV0MZTG)EP(?3&lt;&lt;!SL5;8$YL`Q+J9U5B;T&gt;3(365T%R&lt;,!JZ!*N#@O'7O%X][.MF6B_2.%22_1A*+QK&lt;)NN&gt;K3WPA#J?%&amp;"!!!!&lt;!!"!!)!!Q!%!!!!3!!0!A!!!!!0!.A!V1!!!&amp;%!$Q)!!!!!$Q$9!.5!!!";!!]#!!!!!!]!W!$6!!!!9Q!0"!!!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"636*45E-.#A!$4&amp;:$1UR#6F=!!"$Q!!!%1!!!!#!!!"$1!!!!!!!!!!!!!!!A!!!!.!!!"#A!!!!=4%F#4A!!!!!!!!&amp;A4&amp;:45A!!!!!!!!&amp;U5F242Q!!!!!!!!')1U.46!!!!!!!!!'=4%FW;1!!!!!!!!'Q1U^/5!!!!!!!!!(%6%UY-!!!!!!!!!(92%:%5Q!!!!!!!!(M4%FE=Q!!!!!!!!)!6EF$2!!!!!!!!!)5&gt;G6S=Q!!!!1!!!)I5U.45A!!!!!!!!+-2U.15A!!!!!!!!+A35.04A!!!!!!!!+U;7.M.!!!!!!!!!,);7.M/!!!!!!!!!,=4%FG=!!!!!!!!!,Q2F")9A!!!!!!!!-%2F"421!!!!!!!!-96F"%5!!!!!!!!!-M4%FC:!!!!!!!!!.!1E2)9A!!!!!!!!.51E2421!!!!!!!!.I6EF55Q!!!!!!!!.]2&amp;2)5!!!!!!!!!/1466*2!!!!!!!!!/E3%F46!!!!!!!!!/Y6E.55!!!!!!!!!0-2F2"1A!!!!!!!!0A!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!U!!!!!!!!!!$`````!!!!!!!!!.A!!!!!!!!!!0````]!!!!!!!!!\!!!!!!!!!!!`````Q!!!!!!!!$U!!!!!!!!!!$`````!!!!!!!!!3!!!!!!!!!!!0````]!!!!!!!!"+!!!!!!!!!!!`````Q!!!!!!!!&amp;1!!!!!!!!!!$`````!!!!!!!!!:Q!!!!!!!!!!0````]!!!!!!!!"L!!!!!!!!!!%`````Q!!!!!!!!-5!!!!!!!!!!@`````!!!!!!!!!S1!!!!!!!!!#0````]!!!!!!!!$.!!!!!!!!!!*`````Q!!!!!!!!.%!!!!!!!!!!L`````!!!!!!!!!V1!!!!!!!!!!0````]!!!!!!!!$:!!!!!!!!!!!`````Q!!!!!!!!.]!!!!!!!!!!$`````!!!!!!!!!Z!!!!!!!!!!!0````]!!!!!!!!%&amp;!!!!!!!!!!!`````Q!!!!!!!!99!!!!!!!!!!$`````!!!!!!!!#BQ!!!!!!!!!!0````]!!!!!!!!+,!!!!!!!!!!!`````Q!!!!!!!!SQ!!!!!!!!!!$`````!!!!!!!!$,A!!!!!!!!!!0````]!!!!!!!!-Q!!!!!!!!!!!`````Q!!!!!!!!T1!!!!!!!!!!$`````!!!!!!!!$4A!!!!!!!!!!0````]!!!!!!!!.1!!!!!!!!!!!`````Q!!!!!!!!]I!!!!!!!!!!$`````!!!!!!!!$T!!!!!!!!!!!0````]!!!!!!!!0/!!!!!!!!!!!`````Q!!!!!!!!^E!!!!!!!!!)$`````!!!!!!!!%'!!!!!!&amp;U&amp;*,F2B=WMO5W6U1WBB&lt;GZF&lt;(-O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"Z!!!!!B*.&lt;W2V&lt;'5A1W^S:3ZM&gt;GRJ9H!-6'&amp;T;SZM&gt;G.M98.T5&amp;2)-!!!!%U!!1!*!!!!!!!9)5.P&lt;8"J&lt;'6E)%:S97VF&gt;W^S;S"-;7*T%EVP:(6M:3"$&lt;X*F,GRW&lt;'FC=!J598.L8W.M98.T$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="AI.Task.SetChannels.ctl" Type="Class Private Data" URL="AI.Task.SetChannels.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Data Types" Type="Folder">
		<Item Name="Constructor Data.ctl" Type="VI" URL="../Constructor Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="Return Data.ctl" Type="VI" URL="../Return Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0(7P:DP!!!!!Q]B6%6.5%R"6%5O&lt;(:M;7)7)62&amp;46"-162&amp;8V2"5UMO&lt;(:D&lt;'&amp;T=RR$&lt;WZT&gt;(*V9X2P=C"%982B)*=A[O\P[0]O9X2M!"J!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082401280</Property>
		</Item>
		<Item Name="Used Local Variables.ctl" Type="VI" URL="../Used Local Variables.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0(7P:DP!!!!!Q]B6%6.5%R"6%5O&lt;(:M;7)7)62&amp;46"-162&amp;8V2"5UMO&lt;(:D&lt;'&amp;T=RR$&lt;WZT&gt;(*V9X2P=C"%982B)*=A[O\P[0]O9X2M!"J!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082401280</Property>
		</Item>
		<Item Name="Used Sequence Variables.ctl" Type="VI" URL="../Used Sequence Variables.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0(7P:DP!!!!!Q]B6%6.5%R"6%5O&lt;(:M;7)7)62&amp;46"-162&amp;8V2"5UMO&lt;(:D&lt;'&amp;T=RR$&lt;WZT&gt;(*V9X2P=C"%982B)*=A[O\P[0]O9X2M!"J!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082401280</Property>
		</Item>
		<Item Name="Created Sequence Variables.ctl" Type="VI" URL="../Created Sequence Variables.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="Menu Tags.ctl" Type="VI" URL="../Menu Tags.ctl">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#!!!!!!1"Y!0%!!!!!!!!!!QZ/36^%16&amp;N?#ZM&gt;GRJ9BN"33Z598.L,F.F&gt;%.I97ZO:7RT,GRW9WRB=X-.476O&gt;3"597&gt;T,G.U&lt;!!X1"5!!QF":'2#:7:P=G5.2'6M:82F27RF&lt;76O&gt;!F$&lt;'6B=ERJ=X1!#%VF&lt;H5A6'&amp;H!!!"!!!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
	</Item>
	<Item Name="Get Data Types.vi" Type="VI" URL="../Get Data Types.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(4!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!11&amp;-,5G6U&gt;8*O)%2B&gt;'%!'E"4&amp;&amp;6T:71A4'^D97QA6G&amp;S;7&amp;C&lt;'6T!!!=1&amp;-868.F:#"4:8&amp;V:7ZD:3"798*J97*M:8-!)%"4'E.S:7&amp;U:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!!!=1&amp;!!"1!&amp;!!9!"Q!)!!E+2'&amp;U93"5?8"F=Q!!1%"Q!"Y!!#Q/4EF@2%&amp;2&lt;8AO&lt;(:M;7)&lt;15EO6'&amp;T;SZ4:82$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!!)6'&amp;T;S"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!0E"Q!"Y!!#Q/4EF@2%&amp;2&lt;8AO&lt;(:M;7)&lt;15EO6'&amp;T;SZ4:82$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!!(6'&amp;T;S"J&lt;A"5!0!!$!!$!!1!#A!,!!1!"!!%!!1!$!!%!!1!$1)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!Y!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">278929408</Property>
	</Item>
	<Item Name="Get Hierarchy Path.vi" Type="VI" URL="../Get Hierarchy Path.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;H!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!=1%!!!@````]!"1Z);76S98*D;(EA='&amp;U;!!!1%"Q!"Y!!#Q/4EF@2%&amp;2&lt;8AO&lt;(:M;7)&lt;15EO6'&amp;T;SZ4:82$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!!)6'&amp;T;S"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!0E"Q!"Y!!#Q/4EF@2%&amp;2&lt;8AO&lt;(:M;7)&lt;15EO6'&amp;T;SZ4:82$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!!(6'&amp;T;S"J&lt;A"5!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!1!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!I!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">278929424</Property>
	</Item>
	<Item Name="Get Properties.vi" Type="VI" URL="../Get Properties.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!)1&gt;)98-A2F!`!":!-0````]-68.F=C"'5#"/97VF!!!51$$`````#U2F=W.S;8"U;7^O!&amp;A!]1!!!!!!!!!$%EVP:(6M:3"$&lt;X*F,GRW&lt;'FC=!R598.L,GRW9WRB=X-46'&amp;T;S"1=G^Q:8*U;76T,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ1=G^Q:8*U;76T!!"!1(!!(A!!,!Z/36^%16&amp;N?#ZM&gt;GRJ9BN"33Z598.L,F.F&gt;%.I97ZO:7RT,GRW9WRB=X-!!!B598.L)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!_1(!!(A!!,!Z/36^%16&amp;N?#ZM&gt;GRJ9BN"33Z598.L,F.F&gt;%.I97ZO:7RT,GRW9WRB=X-!!!&gt;598.L)'FO!&amp;1!]!!-!!-!"!!)!!E!"!!%!!1!"!!+!!1!"!!,!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!$!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">10493952</Property>
	</Item>
	<Item Name="Constructor.vi" Type="VI" URL="../Constructor.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!')!!!!#Q!%!!!!)%"4'E.S:7&amp;U:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!!!;1&amp;-54G6X)%.P&lt;H.U=H6D&gt;'^S)%2B&gt;'%!!$R!=!!?!!!M$EZ*8U2"57VY,GRW&lt;'FC'U&amp;*,F2B=WMO5W6U1WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!!"&amp;2B=WM!!"*!)1V498:F)'.I97ZH:8-`!#"!5!!"!!181W^O=X2S&gt;7.U&lt;X)A1WRP=W5A28:F&lt;H1!)E"Q!"E!!1!&amp;&amp;U.P&lt;H.U=H6D&gt;'^S)%.M&lt;X.F)%6W:7ZU!#R!=!!8!!!!!1!"!!!$[!!''56W:7ZU)&amp;*F:WFT&gt;(*B&gt;'FP&lt;C"3:7:O&gt;7U!(%"4&amp;V6T:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!":!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!"5!0!!$!!!!!!!!1!#!!!!!!!!!!-!!!!(!!A!#1-!!(A!!!!!!!!!!!!!#1!!!!E!!!!!!!!!!!!!!!!!!!#1!!!!!!!!!"!!!!!3!!!!%A!!!!!"!!I!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073742080</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="Execute.vi" Type="VI" URL="../Execute.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'V!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!=1&amp;!!!Q!!!!%!!AZ3:82V=GZF:#"&amp;=H*P=A!!"!!!!"*!5QV3:82V=GZF:#"%982B!%"!=!!?!!!M$EZ*8U2"57VY,GRW&lt;'FC'U&amp;*,F2B=WMO5W6U1WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!!#&amp;2B=WMA&lt;X6U!!!71$$`````$&amp;6T:8)A2F!A4G&amp;N:1!!$%!B"V.I&lt;X=A2F!!&amp;E"4%%.P&lt;H.U=H6D&gt;'^S)%2B&gt;'%!!"2!5Q^-&lt;W.B&lt;#"798*J97*M:8-!'%"4%F.F=86F&lt;G.F)&amp;:B=GFB9GRF=Q!!(E"1!!-!#1!+!!M117.U&gt;7&amp;M)&amp;2B=WMA2'&amp;U91!!0E"Q!"Y!!#Q/4EF@2%&amp;2&lt;8AO&lt;(:M;7)&lt;15EO6'&amp;T;SZ4:82$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!!(6'&amp;T;S"J&lt;A"5!0!!$!!$!!1!"1!'!!=!"!!)!!1!"!!%!!Q!$1-!!(A!!!E!!!!!!!!!#1!!!)U,!!)1!!!!!!!!!"!!!!!!!!!!!!!!!!!!!!!1!!!!EA!!!!!"!!Y!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">278929424</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
</LVClass>
