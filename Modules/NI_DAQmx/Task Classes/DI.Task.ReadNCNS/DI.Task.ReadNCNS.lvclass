﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassBodyIcon" Type="Str">Actor</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6618880</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">NI_DAQmx.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../NI_DAQmx.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,&amp;!!!*Q(C=T:1^&lt;J."%)&lt;@$V(1_A")S'YC)3("&gt;&amp;1%,.(2_1ID5&lt;JSC&gt;UZYA2T"2==A&amp;3BI:EG[8W&amp;^)G1T,0LD5G)@Y2%%,O?\_?&gt;H&gt;HHGVWP6.M4[&lt;&amp;7B^JY&lt;[P_V&lt;C-K\:Z'[^O_^NP8)?N8_\\GXQ`@PQL@LQ*X2+`OO.@L7\&amp;\_9`V*\P&lt;@`/PRXLFF_XWU\`\(K@`Y&gt;G-S\X`90"I!3XND/_O)JN^V^86]GSX4_\]6_,_@Y]`R[_CYO,P&gt;_XK=`V&lt;(`^&gt;N@XQ0JM&lt;Q`P0^$YFOHB0]($.KF4/9CQR!*TT&amp;2=KU20^%20^%20^%!0^%!0^%!0&gt;%&gt;X&gt;%&gt;X&gt;%&gt;X&gt;%-X&gt;%-X&gt;%-X^.L2B3ZUI6/6:0*EIC2JEC!:$)K33]+4]#1]#1_P3HA3HI1HY5FY'++%*_&amp;*?"+?B)=Q*4Q*4]+4]#1]J+IE74M[0!E0[28Q"$Q"4]!4]$#F!JY!)*AM3"QE!5/"-XA)?!+?A)&gt;("4Q"4]!4]!1]O"8Q"$Q"4]!4]""3KR+6JH2U?%ADB]@B=8A=(I?(V(*Y("[(R_&amp;R?*B/$I`$YU!Y%TL*1:!4Z!RQ8BQ?BY?&lt;("[(R_&amp;R?"Q?8(7&amp;P&amp;;GU*3/$I`"9`!90!;0Q5-+'4Q'D]&amp;D]"A]J*8"9`!90!;0Q=.5-HA-(I0(!$%G:8I:S9R!9Z!B'$T][G[ROEJ23;TW=E45T;OW+&gt;5WG^IG5NM=;IOONJBKC[27@,7CKB6,L1BK([='L1;D.IF;="GI3[Z,\"2&lt;9(.MAIWQ)&gt;&lt;(?C8U,Q_]P,T5=LH5[?GJ&amp;IO&amp;ZP/Z*J/*2K/2BM/B_PW_?LX?ZF2^2F_X\O:=GP,]_?O,YZ0"R_/46V_/:`WDYZ082_/CX`DPW+&gt;6.XX497@&gt;^/VZ.`N_V5X@P?SG(ZY7/[P0\[_[W&lt;@T^:ASNM4]FG&gt;^,PU0:[-?;8X@V/AHE/&gt;!Q1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"5F5F.31QU+!!.-6E.$4%*76Q!!%/A!!!1^!!!!)!!!%-A!!!!M!!!!!AZ/36^%16&amp;N?#ZM&gt;GRJ9BB%33Z598.L,F*F972/1UZ4,GRW9WRB=X-!!!#A&amp;Q#!!!!Q!!!)!!1!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!$3WD_,Y$GJ#GZG;XX^!Q@-!!!!1!!!!%!!!!!#'!YY('C%&amp;3[I?02=J,_8*V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!S$W[%M?.?U3S&lt;2V`T"(:2!%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"$MI^R8Q#&gt;;@:%!_2IPHHCN!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!)A!!!"JYH'.A9W"K9,D!!-3-1-T5Q01$S0\!Q-!!!&amp;;6"O9!!!!!!%5!!!%9?*RD9-!%`Y%!3$%S-$#&gt;!.)M;/*A'M;G*M"F,C[\I/,-5$?S1E1:A7*-?Y!-*J!=KF[1@]"_1A&amp;M7-Q'!(&lt;V+"5!!!!!!!!-!!&amp;73524!!!!!!!$!!!":!!!!MRYH%NA:'$).,9QWQ#EG9&amp;9H+'")4E`*:7,!=BHA)!N4!Q5AQ#I?6JIYI9($K="A2[`@!O9X`S'J^N&amp;2;#Z2E7#K63EWU&gt;&amp;J..(B;742?8&amp;H````T=@Y4H=\:&amp;TX.%'J,;&lt;!SB_X%7&amp;!]1"UCQA_H^A"EA6T,Q!I'E=$28+$#5MBA?C$B^P-''%7!QT-AL6`BUA[=0)\O%!/E,AY%/7\E9.),^X)IA%#P&amp;UBH")((@BU"%$]BF0&gt;!+N\_3"O:)$&lt;H]9S)!3&amp;9&amp;/%Z",75#GA^6UMRVXU!#\WU%%1G6!K!I)61"W$.A&amp;2TDC$M0$;_XL?\N!Y=C'&amp;)9/1.Q!R+!Y2-:[$)Q-)!O:A'1N6+U.E-U%&amp;90&amp;"9B^!=L71.,T"=F]E"[1T"KI')C^#=JOA,I(*0983%_!ME'_49#SO9(M"6#W%*!N!'6,!NE0I'QZ+(M$.)JQU=\_,KZ)XI?H4Q!V(8+!!!!!$"=!A"%!!!1R.SYQ!!!!!!Q8!)!!!!!%-4=O-!!!!!!-&amp;Q#!%1!!"$%X,D!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Q8!)!2!!!%-4=O-!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9O[,E'*)WF"C4+O19%C+%'*/CBRA!!!!@````_!!!!"A!!!!9!!!!'!!!!"A(``!9"!!1'!1-%"A%,2!9((_1'!1T%"A%Y&gt;!9"/(='!1T%"A%@Z!9(#U1'!1-%"A%!"!9"``Q'!!!!"A!!!!9!!!!'!!!!"`````Q!!!A$`````````````````````_)C)C)C)C)C)C)C)C)C)D`C)_0`Y``DYC0D`_)_)C)`YC0C0C0C)`Y`Y_)_0C)C0_)DYDYD`C0DY_0`YDYC)D`C)C)_)_)DYC0DYC)_)C)`YC0C0C0`Y_)DY_)C0`YC0_)C)C)C)C)C)C)C)C)C)D``````````````````````]T-T-T-T-T-T-T-T-T-T0`-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-T-T-T-T-T-T0`-T-T-``````````T-T-T`T-T-T0!!!!!!!!$]T-T-`]T-T-TQ!!!.U!!!`-T-T0`-T-T-]!!0$&gt;$Q!0T-T-T`T-T--T!!X&gt;X&gt;X1$]T-T-`]T-T-TQ!!X1$&gt;!!`-T-T0`-T-T-]!X&gt;!!$&gt;U0T-T-T`T-T-T0!.X1!!X&gt;!T0-T-`]T-T-TQ!!X1$&gt;!!`-T-T0`-T-T-]!$&gt;X&gt;X&gt;!0T-T-T`T-T-:G!!$QX1]!$]T-T-`]T-T-TQ!!!.U!!!`-T-T0`-T-T-]!!!!!!!!0T-T-T`T-T-T0`````````]T-T-`]T-T-T-T-T-T-T-T-T-T0`-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-T-T-T-T-T-T0`````````````````````Q!!"!$```````````````````````````````````````````^R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8(``X&amp;R=@^R````=@```X(`=8&amp;R`X(```^R=@^R=8&amp;R=@``=8&amp;R`X&amp;R`X&amp;R`X&amp;R=@``=@``=@^R=@^R`X&amp;R=8&amp;R``^R=8(`=8(`=8(``X&amp;R`X(`=@^R````=8(`=8&amp;R=8(``X&amp;R=8&amp;R=@^R=@^R=8(`=8&amp;R`X(`=8&amp;R=@^R=8&amp;R=@``=8&amp;R`X&amp;R`X&amp;R````=@^R=8(`=@^R=8&amp;R````=8&amp;R``^R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8(`````````````````````````````````````````````+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML+````````````````````SML+SML+SP``SML+SML+SML`Q!!!!!!!!!!!!!!!!$`+SML+SML+```+SML+SML+SP`!!!!!!!!A@I!!!!!!0]L+SML+SML``]L+SML+SML+`]!!!!!`!#"A1$]!!!!`SML+SML+SP``SML+SML+S-D)Q!!!)'"A9'"A9'"!!$`+SML+SML+```+SML+SML+SP`!!!!!)'"!!#"A1!!!0]L+SML+SML``]L+SML+SML+`]!!0K"A1!!!!#"A9%!`SML+SML+SP``SML+SML+SML`Q!!A9'"!!!!!)'"_A!D)S-L+SML+```+SML+SML+SP`!!!!!)'"!!#"A1!!!0]L+SML+SML``]L+SML+SML+`]!!!#"A9'"A9'"A1!!`SML+SML+SP``SML+SML+^,3UA!!!!$]!)'"!0Q!!!$`+SML+SML+```+SML+SML+SP`!!!!!!!!_I%!!!!!!0]L+SML+SML``]L+SML+SML+`]!!!!!!!!!!!!!!!!!`SML+SML+SP``SML+SML+SML````````````````````+SML+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML````````````````````````````````````````````!!!!$!!"2F")5!!!!!!!!Q!!!H]!!!4J?*SNF%VI%U%5R^_%L5R#C\/RN1UU**:N,'*!&amp;,5'0YK&gt;#N63F,4A)&gt;1%NXZ!;T1@YKG^,%)/P2D-1?AN"$RZ#-7\"#^\M#=&amp;?YA.?0&gt;3&amp;/RG@4PJ\C9JRIO\-#T,`.\`T@P`'1$J/RPR.'(.!-,W]'0/!*_K%Y";F-,"%XE*&lt;*(]"D)9)!:-U57WYWG3510[64V#T`!#`-$&gt;ZD@T&amp;&lt;QGB/XCVC-MA-6]"BR6^7(ZJF*HSLN2J&gt;"H6Z5BS$:)UX.8#@WE'VI;"5'&lt;M&amp;9Z3JJ!_!F*UM:OJZYM;YLVVROF!6(3;Q$D_E"'K:`%CCD^1:1E3V!G;&lt;ME9-E*K&amp;;L,C3XI)BI9^*CSFBWCSTV90R=$W;6_GH"_!3$/FOW$A]VCV&lt;P&amp;N3.(O-[1R3Z&amp;`;2S\97$RF4AGMU'MDB?M!^-G"1K6_H!&lt;KL@OU&lt;O+/^"Q+E&gt;I_;8]SX&amp;C`07S[)P8YU9DR'7"S`YQ;=UH20EEGW6B[+QA&lt;*NO'S:=-.99/8&amp;RCY0B4`Y1/8;G0"[:6].L?=#;=@B/_PJ,,:].0-Y_?JX(*94?63BRW[QP8!/?PUFJA)"QS""-]AW4\N.&amp;1K&amp;2Q!LCZ[&amp;&gt;&amp;BJ?ZQMDC.[ER=&gt;3&gt;OK&lt;K4OY;4Y\]7(FL4'Y^Z\-S#G^;TG&amp;;:&gt;4@5E&gt;&lt;T`T_N&amp;T"%;VVJB13_KTW3&gt;\%&amp;N;5VA&gt;1K*(IQE]CM&gt;[26K.A[P&gt;*[[8";%\;7E^:3K&gt;4"Y=RD4FIF1FJJV&lt;&lt;.@8-@-YPDF3!*3:&amp;:]-[X&gt;5ZX0O;&gt;SY8'&amp;7\3Q.]XM"E57T&gt;AC%`\0\&amp;:L.:0:_E-WU28]4I[4D@J8OWT=`X5ZBR3WY9UO;7]#&gt;'20Z9&amp;:T%!!!!!"!!!!#!!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!"Y!!!!!9!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!2"=!A!!!!!!"!!A!-0````]!!1!!!!!!+!!!!!%!)%"1!!!92%EO6'&amp;T;SZ3:7&amp;E4E./5SZM&gt;G.M98.T!!!"!!!!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!*2=!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!!!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!VLW-LA!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$7P9SO!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!%18!)!!!!!!!1!)!$$`````!!%!!!!!!#A!!!!"!#"!5!!!'%2*,F2B=WMO5G6B:%Z$4F-O&lt;(:D&lt;'&amp;T=Q!!!1!!!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:&amp;Q#!!!!!!!%!"1!$!!!"!!!!!!!!!!!!!!!!!!1!!A!)!!!!"!!!!%!!!!!I!!!!!A!!"!!!!!!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!05!!!&amp;Q?*S.DUV/!E%1B&lt;_G5@Y516WY-?G&amp;#V?T]1*D)#:M*IDON;6H$,(*'+9B,$W!:_%'8M.T[!UM"IQ,.O9FF;J86?^6!3&gt;-_@\]?&amp;]"OJU-(PL8N^.FZ"&gt;_]H47(U4XNHC*2KFV33_Z%XLM&lt;6'=\D4'Q8-:$_']Z_&gt;&amp;3'=GTUQZ;VZHEY5.K8%W709%.('APF"3['X*F;3.O)&lt;/`$0V_%XHI\!BUDSD)^J;RP?J[&lt;H,;+[^&gt;&amp;A[W2Z3I5[$CX`Z6SBF*#BE\6'MK^S)C/'!1S(8;)P:,^17O]R@Z[D%ZB&amp;&amp;2R32AR#TKL!NOBR,&lt;0U!)IA^ZQ!!!!!!!'Q!!1!#!!-!"!!!!%A!$Q)!!!!!$Q$9!.5!!!"2!!]#!!!!!!]!W!$6!!!!7A!0!A!!!!!0!.A!V1!!!'-!$Q1!!!!!$Q$9!.5)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65F35V*$$1I!!UR71U.-1F:8!!!1[!!!"$U!!!!A!!!1S!!!!!!!!!!!!!!!)!!!!$1!!!1I!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!!!!!"W%2'2&amp;-!!!!!!!!"\%R*:(-!!!!!!!!#!&amp;:*1U1!!!!!!!!#&amp;(:F=H-!!!!%!!!#+&amp;.$5V)!!!!!!!!#D%&gt;$5&amp;)!!!!!!!!#I%F$4UY!!!!!!!!#N'FD&lt;$1!!!!!!!!#S'FD&lt;$A!!!!!!!!#X%R*:H!!!!!!!!!#]%:13')!!!!!!!!$"%:15U5!!!!!!!!$'&amp;:12&amp;!!!!!!!!!$,%R*9G1!!!!!!!!$1%*%3')!!!!!!!!$6%*%5U5!!!!!!!!$;&amp;:*6&amp;-!!!!!!!!$@%253&amp;!!!!!!!!!$E%V6351!!!!!!!!$J%B*5V1!!!!!!!!$O&amp;:$6&amp;!!!!!!!!!$T%:515)!!!!!!!!$Y!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-!!!!!!!!!!!`````Q!!!!!!!!$5!!!!!!!!!!$`````!!!!!!!!!/A!!!!!!!!!!0````]!!!!!!!!!]!!!!!!!!!!!`````Q!!!!!!!!%=!!!!!!!!!!$`````!!!!!!!!!31!!!!!!!!!!0````]!!!!!!!!"4!!!!!!!!!!!`````Q!!!!!!!!'9!!!!!!!!!!$`````!!!!!!!!!;A!!!!!!!!!"0````]!!!!!!!!$%!!!!!!!!!!(`````Q!!!!!!!!-A!!!!!!!!!!D`````!!!!!!!!!T!!!!!!!!!!#@````]!!!!!!!!$1!!!!!!!!!!+`````Q!!!!!!!!.1!!!!!!!!!!$`````!!!!!!!!!W!!!!!!!!!!!0````]!!!!!!!!$?!!!!!!!!!!!`````Q!!!!!!!!/-!!!!!!!!!!$`````!!!!!!!!""!!!!!!!!!!!0````]!!!!!!!!'&amp;!!!!!!!!!!!`````Q!!!!!!!!I9!!!!!!!!!!$`````!!!!!!!!#CA!!!!!!!!!!0````]!!!!!!!!-L!!!!!!!!!!!`````Q!!!!!!!!SU!!!!!!!!!!$`````!!!!!!!!$,Q!!!!!!!!!!0````]!!!!!!!!-T!!!!!!!!!!!`````Q!!!!!!!!UU!!!!!!!!!!$`````!!!!!!!!$4Q!!!!!!!!!!0````]!!!!!!!!0)!!!!!!!!!!!`````Q!!!!!!!!]I!!!!!!!!!!$`````!!!!!!!!$T!!!!!!!!!!!0````]!!!!!!!!08!!!!!!!!!#!`````Q!!!!!!!""9!!!!!"2%33Z598.L,F*F972/1UZ4,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"Z!!!!!B*.&lt;W2V&lt;'5A1W^S:3ZM&gt;GRJ9H!-6'&amp;T;SZM&gt;G.M98.T5&amp;2)-!!!!%U!!1!*!!!!!!!9)5.P&lt;8"J&lt;'6E)%:S97VF&gt;W^S;S"-;7*T%EVP:(6M:3"$&lt;X*F,GRW&lt;'FC=!J598.L8W.M98.T$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="DI.Task.ReadNCNS.ctl" Type="Class Private Data" URL="DI.Task.ReadNCNS.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Data Types" Type="Folder">
		<Item Name="Constructor Data.ctl" Type="VI" URL="../Constructor Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="Return Data.ctl" Type="VI" URL="../Return Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0(7P:DP!!!!!Q]B6%6.5%R"6%5O&lt;(:M;7)7)62&amp;46"-162&amp;8V2"5UMO&lt;(:D&lt;'&amp;T=RR$&lt;WZT&gt;(*V9X2P=C"%982B)*=A[O\P[0]O9X2M!"J!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082401280</Property>
		</Item>
		<Item Name="Used Local Variables.ctl" Type="VI" URL="../Used Local Variables.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0(7P:DP!!!!!Q]B6%6.5%R"6%5O&lt;(:M;7)7)62&amp;46"-162&amp;8V2"5UMO&lt;(:D&lt;'&amp;T=RR$&lt;WZT&gt;(*V9X2P=C"%982B)*=A[O\P[0]O9X2M!"J!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082401280</Property>
		</Item>
		<Item Name="Used Sequence Variables.ctl" Type="VI" URL="../Used Sequence Variables.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0(7P:DP!!!!!Q]B6%6.5%R"6%5O&lt;(:M;7)7)62&amp;46"-162&amp;8V2"5UMO&lt;(:D&lt;'&amp;T=RR$&lt;WZT&gt;(*V9X2P=C"%982B)*=A[O\P[0]O9X2M!"J!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082401280</Property>
		</Item>
		<Item Name="Created Sequence Variables.ctl" Type="VI" URL="../Created Sequence Variables.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="Menu Tags.ctl" Type="VI" URL="../Menu Tags.ctl">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"^!!!!!1"V!0%!!!!!!!!!!QZ/36^%16&amp;N?#ZM&gt;GRJ9BB%33Z598.L,F*F972/1UZ4,GRW9WRB=X-.476O&gt;3"597&gt;T,G.U&lt;!!X1"5!!QF":'2#:7:P=G5.2'6M:82F27RF&lt;76O&gt;!F$&lt;'6B=ERJ=X1!#%VF&lt;H5A6'&amp;H!!!"!!!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
	</Item>
	<Item Name="Get Data Types.vi" Type="VI" URL="../Get Data Types.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5R"$&lt;WZT&gt;(*V9X2P=C"%982B!!!11&amp;-,5G6U&gt;8*O)%2B&gt;'%!'E"4&amp;&amp;6T:71A4'^D97QA6G&amp;S;7&amp;C&lt;'6T!!!=1&amp;-868.F:#"4:8&amp;V:7ZD:3"798*J97*M:8-!)%"4'E.S:7&amp;U:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!!!=1&amp;!!"1!&amp;!!9!"Q!)!!E+2'&amp;U93"5?8"F=Q!!0%"Q!"Y!!#E/4EF@2%&amp;2&lt;8AO&lt;(:M;7)92%EO6'&amp;T;SZ3:7&amp;E4E./5SZM&gt;G.M98.T!!B598.L)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1![1(!!(A!!+1Z/36^%16&amp;N?#ZM&gt;GRJ9BB%33Z598.L,F*F972/1UZ4,GRW9WRB=X-!"V2B=WMA;7Y!6!$Q!!Q!!Q!%!!I!#Q!%!!1!"!!%!!Q!"!!%!!U#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!/!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
	</Item>
	<Item Name="Get Hierarchy Path.vi" Type="VI" URL="../Get Hierarchy Path.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!=1%!!!@````]!"1Z);76S98*D;(EA='&amp;U;!!!0%"Q!"Y!!#E/4EF@2%&amp;2&lt;8AO&lt;(:M;7)92%EO6'&amp;T;SZ3:7&amp;E4E./5SZM&gt;G.M98.T!!B598.L)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1![1(!!(A!!+1Z/36^%16&amp;N?#ZM&gt;GRJ9BB%33Z598.L,F*F972/1UZ4,GRW9WRB=X-!"V2B=WMA;7Y!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!%!!E$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!+!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">276832272</Property>
	</Item>
	<Item Name="Get Properties.vi" Type="VI" URL="../Get Properties.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!("!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!)1&gt;)98-A2F!`!":!-0````]-68.F=C"'5#"/97VF!!!51$$`````#U2F=W.S;8"U;7^O!&amp;A!]1!!!!!!!!!$%EVP:(6M:3"$&lt;X*F,GRW&lt;'FC=!R598.L,GRW9WRB=X-46'&amp;T;S"1=G^Q:8*U;76T,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ1=G^Q:8*U;76T!!!]1(!!(A!!+1Z/36^%16&amp;N?#ZM&gt;GRJ9BB%33Z598.L,F*F972/1UZ4,GRW9WRB=X-!#&amp;2B=WMA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!J$EZ*8U2"57VY,GRW&lt;'FC'%2*,F2B=WMO5G6B:%Z$4F-O&lt;(:D&lt;'&amp;T=Q!(6'&amp;T;S"J&lt;A"5!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!Q!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
	</Item>
	<Item Name="Constructor.vi" Type="VI" URL="../Constructor.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'%!!!!#Q!%!!!!)%"4'E.S:7&amp;U:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!!!;1&amp;-54G6X)%.P&lt;H.U=H6D&gt;'^S)%2B&gt;'%!!$B!=!!?!!!J$EZ*8U2"57VY,GRW&lt;'FC'%2*,F2B=WMO5G6B:%Z$4F-O&lt;(:D&lt;'&amp;T=Q!%6'&amp;T;Q!!%E!B$6.B&gt;G5A9WBB&lt;G&gt;F=T]!)%"1!!%!""&gt;$&lt;WZT&gt;(*V9X2P=C"$&lt;'^T:3"&amp;&gt;G6O&gt;!!C1(!!'1!"!!581W^O=X2S&gt;7.U&lt;X)A1WRP=W5A28:F&lt;H1!,%"Q!"=!!!!"!!%!!!0I!!9:28:F&lt;H1A5G6H;8.U=G&amp;U;7^O)&amp;*F:GZV&lt;1!=1&amp;-868.F:#"4:8&amp;V:7ZD:3"798*J97*M:8-!&amp;E"4%%.P&lt;H.U=H6D&gt;'^S)%2B&gt;'%!!&amp;1!]!!-!!!!!!!"!!)!!!!!!!!!!Q!!!!=!#!!*!Q!!?!!!!!!!!!!!!!!*!!!!#1!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!%!!!!")!!!!3!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">262400</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">276832272</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="Execute.vi" Type="VI" URL="../Execute.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'N!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!=1&amp;!!!Q!!!!%!!AZ3:82V=GZF:#"&amp;=H*P=A!!"!!!!"*!5QV3:82V=GZF:#"%982B!$R!=!!?!!!J$EZ*8U2"57VY,GRW&lt;'FC'%2*,F2B=WMO5G6B:%Z$4F-O&lt;(:D&lt;'&amp;T=Q!)6'&amp;T;S"P&gt;81!!":!-0````]-68.F=C"'5#"/97VF!!!-1#%(5WBP&gt;S"'5!!71&amp;-11W^O=X2S&gt;7.U&lt;X)A2'&amp;U91!!&amp;%"4$URP9W&amp;M)&amp;:B=GFB9GRF=Q!91&amp;-35W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!!!?1&amp;!!!Q!*!!I!#R""9X2V97QA6'&amp;T;S"%982B!!![1(!!(A!!+1Z/36^%16&amp;N?#ZM&gt;GRJ9BB%33Z598.L,F*F972/1UZ4,GRW9WRB=X-!"V2B=WMA;7Y!6!$Q!!Q!!Q!%!!5!"A!(!!1!#!!%!!1!"!!-!!U$!!"Y!!!*!!!!!!!!!!E!!!#.#Q!#%!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!!%!!!!*)!!!!!!1!/!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">276832272</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
</LVClass>
