﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassBodyIcon" Type="Str">Actor</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6618880</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">Elprotronic_FlashPro430.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../Elprotronic_FlashPro430.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,!!!!*Q(C=T:3R&lt;B."%)&lt;`1R3U,CEC)5&gt;56'B?!)%F/DK`QK2/Z2+\]Q/EG4:3'D]!D;P1U%S4^(Y&amp;^\'1T,@LN:5%RR931?T?X*X`W:HZ&lt;H?^5GWPJ*&gt;;(WPH"RP_-G*&gt;X]\L](J6M4BX6V08^`T&lt;_06DVT&lt;_8PZ$]1`L\^Z;`30]R^K\A_X@_@&gt;DX@0L@HP30VE&gt;]P`5:-,N&gt;``J[7E*&lt;OX*_/)KNN_`KK[3:&lt;^`MP7P2,U`TX_!\`&lt;W^O$X\?:H.4E]@U`0\Z(VW&gt;_?XX_E]3XDYX_#ZWV3JX)199E&amp;ZJCJO.;*HOC*HOC*HOC"(OC"(OC"(OC/\OC/\OC/\OC'&lt;OC'&lt;OC'&lt;OCVIQN&gt;[%*H6J,C3;%E;:)A'1S+EFP#E`!E0!E00Z8Q*$Q*4]+4]$"%#5`#E`!E0!E095JY%J[%*_&amp;*?%B63&lt;*W&gt;(A3(N)LY!FY!J[!*_#BJ!+?!#!I&amp;C1/EI#BQ"G]"$Q"4]$$KQ+?A#@A#8A#(NQ+?!+?A#@A#8A)K&lt;-3F;:U&gt;(B))Y@(Y8&amp;Y("[(B^2S?"Q?B]@B=8AI*Y@(Y8%AH)*/=B$E"$E$H"]/D]0$1Q[0Q_0Q/$Q/$[[[1FZHJN#5DA[0Q70Q'$Q'D]&amp;$#BE]"I`"9`!90+36Q70Q'$Q'D]&amp;$+2E]"I`"9Y!923EP)ZE2;!QS")/(K_Y7K[M5F=2K,U&gt;%X&lt;RKGV*NM[FN)L8.I&lt;&lt;I;IOJNEBKE[]WK7K4J49*;B_H"KU'IV:%,&lt;A-V*,\!JND-WS+D&lt;!B.M$[7+_%`O7"S_63C]6#]`F=M^F-U_F5I^&amp;)Q_&amp;1A]&amp;!`8Z@P6ZP&gt;[K_I7^;NTW8RLR@HLU^_8:RV:N&gt;8,W_Z(FZ^HJ2^+X`A8V&gt;&gt;_-0(8&lt;&gt;D4`?&gt;*-@&gt;^XYU`NO`/7EW(6^`XT84&lt;\@&lt;-;5M38G5:\.O@1`H)V[I=VT.U?`!,YV30%!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"@@5F.31QU+!!.-6E.$4%*76Q!!%ZA!!!2(!!!!)!!!%XA!!!!R!!!!!BV&amp;&lt;("S&lt;X2S&lt;WZJ9V^'&lt;'&amp;T;&amp;"S&lt;T1T-#ZM&gt;GRJ9AZ.&lt;W2V&lt;'5O&lt;(:D&lt;'&amp;T=Q!!!!!!!+!8!)!!!$!!!!A!"!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!BP`1\.&amp;:9%?:#@]EE.]0VQ!!!"!!!!!1!!!!!08O'4JS'FR'BK=;&amp;P'*3[`5(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!#+GH@1[,WT4*AT5LQC?-,E!1!!!0`````5(9T:DQ#S"/G!#:DM_%*_!!!!%!@\_,&amp;8E`%[%;@565C!&amp;\U!!!!%!!!!!!!!!#=!!5R71U-!!!!"!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!-!!!!!!A!"!!!!!!!A!!!!'(C=9W"D9'JAO-!!R)R!&amp;J-'E07"19!"!$IB".9!!!"'!!!"'(C=9W$!"0_"!%AR-D!Q81$3,'DC9"L'JC&lt;!:3YOO[$CT&amp;!XMM+%A?\?![3:1(*1.6!`-*U"YB0IZH"A-2M!A!=I*A!!!!!!$!!"6EF%5Q!!!!!!!Q!!!91!!!-5?*RL9'2AS$3W-,M!J*G"7*SBA3%Z0S76CQ():Y#!&amp;UQ-&amp;)-!K(F;;/+'"Q[H!9%?PXQ,G.`]BK@&lt;257AO5:&amp;AKF5J.N(2;442Y7FEU8FR:````]X(_%ZX/W2=^T2"K3WGQ-I@NR&amp;B10%!&gt;)M)0J`9!:)&amp;=S]!+"J(!U6SAQF,)9(IAY@&lt;T"BB&amp;A--T)+G`USH5$4A,1%EA6Q"[#JZQ![5O$A1Z&lt;O2AUAPX=CC!1+]83'=%A=&gt;_(1%10S'5_!$/TEA@G#!_[_-*!"*3I#H39ACVA992:VMRVXU!$\SU%%1G6!K!I)61#C&gt;I"&gt;=)1D\D!]0.?_PL=,&amp;!ZM3'(M!-1.1!S+9W3MR]$)!,+1#5D71N8;!.F-5$&amp;98)(9(["M$31^)N#A:'3QBYON2L+4#3T(S(#'!;;/E?%3F.U!&gt;3.)4";I?1+5L1*E*U$:WE$W!3D&lt;#-A7A,)N'=%--.M/SLY!&gt;1MOWNH@R25J3-"J'J;M/9%Y/&lt;@!Q%#P/A!5$A#;^),]!!!!DQ!!!.RYH(.A9'$).,9Q;Q$3T)Q-$/)-$1T*_3GJ$%BA#Q.OU0R'I.N&amp;2;44297HWU&gt;&amp;IN.(B;/&lt;I:M43,.UMKC]_00````7!`R4$P"P/\%$;$Q$P_P"ZC-#B_([V\[_NQMETIBEJA0),1S]9$%G)*:(%A=":X]86X4XA&gt;2S!H&amp;S&lt;I'"A6ZV1#W1$1$XE#,K!!!!!!Q8!)!&amp;!!!%-4=O-!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$"=!A!5!!!1R.SYQ!!!!!!Q8!)!!!!!%-4=O-!!!!!!-&amp;Q#!"1!!"$%X,D!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'C:S5&gt;NJ3F%;K5J2GCF+52IG=:X9!!!!(`````A!!!!9!!!!'!!!!"A!!!!9"``Q'!1!%"A%$"!9"#U1'"R`E"A%-R!9"/(1'!4BX"A%-R!9"(_1'"QN%"A%$"!9"!!1'!@`]"A!!!!9!!!!'!!!!"A!!!!@````]!!!)!``````````````````````C)C)C)C)C)C)C)C)C)C)`Y_)DYD`C0`YDYDY_)D`_0_0_0_0C0DYDY_)_0C)_)D`DY_0DYDY_)_0C0DYC0_)`Y_)DY_)_0C0DYDY_)DYC0_0C)_)`YD`_)D`C0`Y``D`C)C)C)C)C)C)C)C)C)C)```````````````````````-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-T-T-T-T-T-T0`-T-T-T-T-T-T-T-T-T-T`T-T-T0`````````]T-T-`]T-T-TQ!!!!!!!!`-T-T0`-T-T-]!!!$&gt;!!!0T-T-T`T-T-T0!!$QX1]!$]T-T-`]T-T$-Q!.X&gt;X&gt;U!`-T-T0`-T-T-]!!.U!X1!0T-T-T`T-T-T0!.X1!!X&gt;$]T-T-`]T-T-TQ$&gt;U!!.X1-TT-T0`-T-T-]!!.U!X1!0T-T-T`T-T-T0!!X&gt;X&gt;X1$]T-T-`]T-T':A!!].U0!!`-T-T0`-T-T-]!!!$&gt;!!!0T-T-T`T-T-T0!!!!!!!!$]T-T-`]T-T-T``````````-T-T0`-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-T-T-T-T-T-T0`-T-T-T-T-T-T-T-T-T-T`````````````````````]!!!1!````````````````````````````````````````````=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R``^R`X&amp;R=@^R=@``=8(```^R=@^R=@^R`X&amp;R=@```X(``X(``X(``X(`=8(`=@^R=@^R`X&amp;R`X(`=8&amp;R`X&amp;R=@``=@^R`X(`=@^R=@^R`X&amp;R`X(`=8(`=@^R=8(``X&amp;R``^R`X&amp;R=@^R`X&amp;R`X(`=8(`=@^R=@^R`X&amp;R=@^R=8(``X(`=8&amp;R`X&amp;R``^R=@```X&amp;R=@``=8(```^R````=@``=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R`````````````````````````````````````````````SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SP```````````````````]L+SML+SML``]L+SML+SML+`]!!!!!!!!!!!!!!!!!`SML+SML+SP``SML+SML+SML`Q!!!!!!!)([!!!!!!$`+SML+SML+```+SML+SML+SP`!!!!!0Q!A9%!`!!!!0]L+SML+SML``]L+SML+SMD)S-!!!#"A9'"A9'"A1!!`SML+SML+SP``SML+SML+SML`Q!!!!#"A1!!A9%!!!$`+SML+SML+```+SML+SML+SP`!!$[A9%!!!!!A9'"!0]L+SML+SML``]L+SML+SML+`]!!)'"A1!!!!#"A@I!)S-D+SML+SP``SML+SML+SML`Q!!!!#"A1!!A9%!!!$`+SML+SML+```+SML+SML+SP`!!!!A9'"A9'"A9%!!0]L+SML+SML``]L+SML+SP3UN)!!!!!`!#"A1$]!!!!`SML+SML+SP``SML+SML+SML`Q!!!!!!!0K"!!!!!!$`+SML+SML+```+SML+SML+SP`!!!!!!!!!!!!!!!!!0]L+SML+SML``]L+SML+SML+````````````````````SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML``]L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+````````````````````````````````````````````Q!!!!Q!!5:13&amp;!!!!!!!!-!!!0/!!!)SHC=N6:,;".2&amp;,VP/CEP&lt;9JPKN6'7G9;*\&amp;%CS+IN@[+@67LJ8Z;Q1_IQ61L"#NN&amp;5&amp;1&amp;U/B"6V9S%*R7Z=+8&lt;A6#&gt;H-IKY5&gt;"%-@J:W5Z4K:,RPJD.*7BN&gt;;"&lt;$%-[ZZ\ZTT\M-!-WS2KE!&gt;SQA&lt;"Z@?CWI3:I%)..'9@%8'Q&gt;WCPQ!MC:-,/CEJ^B\K5#;,!AFT2D&gt;SC&gt;A$N(WL(U4JEET_Y$1;B&lt;'9D57L%K;[Z1D?I\JTZLUC9"869&amp;G&gt;J]5J./[_IX?.Y:2%)R7]64;3!%)&lt;Z&amp;F)X)]=7X1U-7`Q49;&gt;EI',7$=L"P2=ROR)EK`=EK3.'4*5[]E9-F7G*G:+:)5FR2TWGA8H#S7@5P3&amp;4DVX'Q?V8/&lt;(5[.QU'&gt;NZY/6QN4IH&gt;"7EJ&gt;T5W'6/4&gt;]I[=^&lt;3Y;H5[P(Q_DTR],P+',&amp;CDZQ\1-0W1@"?I/W']!!)E=U&amp;9_VDQF4YR"1&gt;&lt;DY/)&gt;B$7D_`^&amp;M1.5\I)MK@6"60/''2P$(NQ$/3A-Y9AHW"1H-05(_&lt;!Z5SEO3NV9X2M=%1&lt;PKR&gt;3C6'2\8L)V&gt;P*M9'N72C,,&amp;]1HOZ'&gt;QJ4C`%H("!!]CQ(R[5ODU-U^04;!!_C^2^3&amp;WHZXS?YJTGI?^YMOCY5#U[NR_&gt;Y^](LADXIBV3?7;X&amp;$/\$4/LM+6NF76W_\`0\![-UJUFG96*C-0D#PH&lt;[:*+-BN(VG[9L-"J2]\&gt;MMT'57?XJV-JM\O7:T&lt;O;@G:4;@4:4RUPM00L%S)GVHDN@X4`CG3OW$HI2W?FS4X()YD;1;=$7.`2:%QCAD`K?&gt;`VW*94SY,[_&lt;+!^!E=!.,?3KF85_-$&lt;G49&amp;&amp;8(5'ANR*M.OI0"`7YG(@@\S\(`^(LRIRPUX/V:&gt;?D$G)QZ*FN+Y\:#QM,;$9_84WD"?ZBR(4R4S7*E!5(=&gt;WBR#&lt;X2#%BU1A"C%#N/TV&amp;2&lt;IE[&amp;6;2H707Q06BC:?5!=B,,,93YB'2/'6I,:N_^";!1WKR5Q&gt;YG9!.Q&amp;R_XDJ8&lt;E9B$RHZV?+Y_(@&lt;))YJ0[Q#9Z:U*-UKY0!=WJ`S4Q6O);H&gt;Y-G?3Y&gt;29F[0&gt;@A3&lt;B$&lt;SLNRT&amp;+,D_^A'A/J-QI[]NSIV;#@PZ\[+?`BXYMMR_P:C^?T?A:C*Y&amp;`DX5[;\'+FS.Y_&gt;&amp;D3,O7!FO)"_^,&lt;F1/4:?J=SB]K3MSJL21C3]N:(1]9($7\%,7&lt;1C&lt;Z$VY"SK^J5M*@I_?]0`?K$^/L=L!6AX.H(8AA&lt;?64`,?N$P%/WBX?Q*,GT=X7PJ%TK@??.`8W2[@;&lt;R'E[3^@IDF4&lt;_!O8.;VY!!!!!!!1!!!!O!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!G!!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!%Y8!)!!!!!!!1!)!$$`````!!%!!!!!!$)!!!!#!"*!-P````])2'RM)("B&gt;'A!!"B!5!!"!!!/47^E&gt;7RF,GRW9WRB=X-!!!%!!1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!J&amp;Q#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!"!!!!!!!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!VO?$N1!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$7ZY/V!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!%Y8!)!!!!!!!1!)!$$`````!!%!!!!!!$)!!!!#!"*!-P````])2'RM)("B&gt;'A!!"B!5!!"!!!/47^E&gt;7RF,GRW9WRB=X-!!!%!!1!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'2=!A!!!!!!"!!5!!Q!!!1!!!!!!"!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%!!!"'&amp;Q#!!!!!!!)!%E!S`````QB%&lt;'QA='&amp;U;!!!'%"1!!%!!!Z.&lt;W2V&lt;'5O&lt;(:D&lt;'&amp;T=Q!!!1!"5&amp;2)-!!!!!1!!!!!!!!!!!!!!!1!"!!)!!!!"!!!!%]!!!!I!!!!!A!!"!!!!!!'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Q!!!'4?*S.DTV/QU!5B$^\]_/%*)4QUU5S$15&amp;1I1$7!,3)6F=!%RM%[-6'^HLC$)3:_)K8)%74A!PDB%&amp;$4P33$0T.0M?=-!I/0O3ZVVK\3]C/Y&gt;(0N`?8VY".&lt;\3C^T9X$RFM^OJDIJZG*PTS?G*8OLM@H"NYF)H)G93&amp;&gt;V;TKTG/!BR9(SBS])GO7^3PZLR&amp;XGWD'TCRZ'.;!LI%I0TA3N#V:)*$4J"'Z8K"\RAJ=S.X2C*32F+O[*&amp;'U_6=9J(&gt;1/^[N?OMM_R^)23W7',IX`OYC,57J.-?^T*'AWG5O-T9&amp;PC.9&lt;C`=#N]&gt;@Z489K&lt;)ZS')GT&lt;DW5N2QZI5G00LPM#@@:BW_X05KM!!!!@A!"!!)!!Q!&amp;!!!!7!!0!A!!!!!0!.A!V1!!!'%!$Q)!!!!!$Q$9!.5!!!"K!!]#!!!!!!]!W!$6!!!!=Q!0"!!!!!!0!.A!V1!!!(S!!)1!A!!!$Q$9!.5)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-!!!5F.31QU+!!.-6E.$4%*76Q!!%ZA!!!2(!!!!)!!!%XA!!!!!!!!!!!!!!#!!!!!U!!!%0!!!!"R-35*/!!!!!!!!!7"-6F.3!!!!!!!!!8236&amp;.(!!!!!!!!!9B$1V.5!!!!!!!!!:R-38:J!!!!!!!!!&lt;"$4UZ1!!!!!!!!!=2544AQ!!!!!!!!!&gt;B%2E24!!!!!!!!!?R-372T!!!!!!!!!A"735.%!!!!!1!!!B2W:8*T!!!!"!!!!DR41V.3!!!!!!!!!K"(1V"3!!!!!!!!!L2*1U^/!!!!!!!!!MBJ9WQU!!!!!!!!!NRJ9WQY!!!!!!!!!P"-37:Q!!!!!!!!!Q2'5%BC!!!!!!!!!RB'5&amp;.&amp;!!!!!!!!!SR75%21!!!!!!!!!U"-37*E!!!!!!!!!V2#2%BC!!!!!!!!!WB#2&amp;.&amp;!!!!!!!!!XR73624!!!!!!!!!Z"%6%B1!!!!!!!!![2.65F%!!!!!!!!!\B)36.5!!!!!!!!!]R71V21!!!!!!!!!_"'6%&amp;#!!!!!!!!!`1!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$A!!!!!!!!!!0````]!!!!!!!!!X!!!!!!!!!!!`````Q!!!!!!!!$Q!!!!!!!!!!$`````!!!!!!!!!0A!!!!!!!!!!0````]!!!!!!!!"*!!!!!!!!!!!`````Q!!!!!!!!%M!!!!!!!!!!$`````!!!!!!!!!6!!!!!!!!!!!0````]!!!!!!!!"H!!!!!!!!!!!`````Q!!!!!!!!'M!!!!!!!!!!(`````!!!!!!!!!T1!!!!!!!!!"0````]!!!!!!!!$S!!!!!!!!!!(`````Q!!!!!!!!09!!!!!!!!!!D`````!!!!!!!!!_A!!!!!!!!!#@````]!!!!!!!!$_!!!!!!!!!!+`````Q!!!!!!!!1)!!!!!!!!!!$`````!!!!!!!!""A!!!!!!!!!!0````]!!!!!!!!%-!!!!!!!!!!!`````Q!!!!!!!!2%!!!!!!!!!!$`````!!!!!!!!"-A!!!!!!!!!!0````]!!!!!!!!'T!!!!!!!!!!!`````Q!!!!!!!!L1!!!!!!!!!!$`````!!!!!!!!#O!!!!!!!!!!!0````]!!!!!!!!/N!!!!!!!!!!!`````Q!!!!!!!![]!!!!!!!!!!$`````!!!!!!!!$M1!!!!!!!!!!0````]!!!!!!!!/V!!!!!!!!!!!`````Q!!!!!!!!]]!!!!!!!!!!$`````!!!!!!!!$U1!!!!!!!!!!0````]!!!!!!!!2K!!!!!!!!!!!`````Q!!!!!!!"'Q!!!!!!!!!!$`````!!!!!!!!%&lt;A!!!!!!!!!!0````]!!!!!!!!2Z!!!!!!!!!#!`````Q!!!!!!!",U!!!!!!J.&lt;W2V&lt;'5O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7!!!!!BB.&lt;W2V&lt;'6T)%BJ:8*B=G.I?3ZM&gt;GRJ9H!35(*P:X*B&lt;7VF=CZM&gt;G.M98.T5&amp;2)-!!!!&amp;Y!!1!)!!!!!"AB1W^N='FM:71A2H*B&lt;76X&lt;X*L)%RJ9H-947^E&gt;7RF=S");76S98*D;(EO&lt;(:M;7*Q%&amp;"S&lt;W&gt;S97VN:8)A1WRB=X-35(*P:X*B&lt;7VF=CZM&gt;G.M98.T!!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Module.ctl" Type="Class Private Data" URL="Module.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="TypeDefs" Type="Folder">
		<Item Name="Module CMDs.ctl" Type="VI" URL="../TypeDefs/Module CMDs.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="API" Type="Folder">
		<Item Name="FlashPro Connect.vi" Type="VI" URL="../API/FlashPro Connect.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$8!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"P&gt;81!"!!!!"*!-P````])='&amp;U;#"P&gt;81!!":!5!!$!!!!!1!##%6S=G^S)'FO!!!31$,`````#'2M&lt;#"Q982I!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082929680</Property>
		</Item>
		<Item Name="FlashPro Create Error.vi" Type="VI" URL="../API/FlashPro Create Error.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$W!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"P&gt;81!"!!!!":!-0````].2H6O9X2J&lt;WYA&lt;G&amp;N:1!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"J&lt;A!!#U!$!!2$&lt;W2F!!!-1#%'28*S&lt;X)`!!!71$$`````$56S=G^S)'VF=X.B:W5!6!$Q!!Q!!Q!%!!1!"!!%!!1!"1!%!!9!"Q!)!!E#!!"Y!!!.#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%3!!!!!!!!!!I!!!!1!!!!%!!!!B!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082929664</Property>
		</Item>
		<Item Name="FlashPro Fuse.vi" Type="VI" URL="../API/FlashPro Fuse.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$\!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"P&gt;81!"!!!!""!-0````]'5G6T&gt;7RU!!!71$,`````$'2M&lt;#"Q982I)'^V&gt;!!!&amp;E"1!!-!!!!"!!))28*S&lt;X)A;7Y!!""!-0````]'1W^O:GFH!!!31$,`````#'2M&lt;#"Q982I!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="FlashPro Open Config.vi" Type="VI" URL="../API/FlashPro Open Config.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$L!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"P&gt;81!"!!!!":!-P````]-:'RM)("B&gt;'AA&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"J&lt;A!!%%!Q`````Q:$&lt;WZG;7=!!"*!-P````]):'RM)("B&gt;'A!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">2</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082929680</Property>
		</Item>
		<Item Name="FlashPro Open Firmware.vi" Type="VI" URL="../API/FlashPro Open Firmware.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$N!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"P&gt;81!"!!!!":!-P````]-:'RM)("B&gt;'AA&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"J&lt;A!!%E!Q`````QB';8*N&gt;W&amp;S:1!!%E!S`````QBE&lt;'QA='&amp;U;!!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!"!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">2</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082929680</Property>
		</Item>
		<Item Name="FlashPro Reset.vi" Type="VI" URL="../API/FlashPro Reset.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$Z!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"J!-0````]128BF9X6U;7^O)&amp;*F=X6M&gt;!!!%E!S`````QBQ982I)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!11$,`````"X"B&gt;'AA;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
		<Item Name="FlashPro Flash.vi" Type="VI" URL="../API/FlashPro Flash.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$^!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-P````]-:'RM)("B&gt;'AA&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"J&lt;A!!%E!Q`````QB';8*N&gt;W&amp;S:1!!%%!Q`````Q:$&lt;WZG;7=!!"*!-P````]):'RM)("B&gt;'A!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%!!!!"!!!!!1!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
	</Item>
	<Item Name="Get Hierarchy Path.vi" Type="VI" URL="../Get Hierarchy Path.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;P!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!=1%!!!@````]!"1Z);76S98*D;(EA='&amp;U;!!!2%"Q!"Y!!#Y&gt;27RQ=G^U=G^O;7.@2GRB=WB1=G]U-T!O&lt;(:M;7)/47^E&gt;7RF,GRW9WRB=X-!!!J.&lt;W2V&lt;'5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%*!=!!?!!!O(56M=(*P&gt;(*P&lt;GFD8U:M98.I5(*P.$-Q,GRW&lt;'FC$EVP:(6M:3ZM&gt;G.M98.T!!!*47^E&gt;7RF)'FO!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!"!!*!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Init.vi" Type="VI" URL="../Init.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%D!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%2!=!!?!!!O(56M=(*P&gt;(*P&lt;GFD8U:M98.I5(*P.$-Q,GRW&lt;'FC$EVP:(6M:3ZM&gt;G.M98.T!!!+47^E&gt;7RF)'^V&gt;!!!1E"Q!"Y!!#Y&gt;27RQ=G^U=G^O;7.@2GRB=WB1=G]U-T!O&lt;(:M;7)/47^E&gt;7RF,GRW9WRB=X-!!!F.&lt;W2V&lt;'5A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!1!"!!%!!9$!!"Y!!!*!!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*)!!!!!!1!(!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="Deinit.vi" Type="VI" URL="../Deinit.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%;!!!!"A!&lt;1!-!&amp;':J&lt;G&amp;M)'6S=G^S)'.P:'5A&lt;X6U!!!%!!!!2%"Q!"Y!!#Y&gt;27RQ=G^U=G^O;7.@2GRB=WB1=G]U-T!O&lt;(:M;7)/47^E&gt;7RF,GRW9WRB=X-!!!J.&lt;W2V&lt;'5A&lt;X6U!!!:1!-!%W:J&lt;G&amp;M)'6S=G^S)'.P:'5A;7Y!1E"Q!"Y!!#Y&gt;27RQ=G^U=G^O;7.@2GRB=WB1=G]U-T!O&lt;(:M;7)/47^E&gt;7RF,GRW9WRB=X-!!!F.&lt;W2V&lt;'5A;7Y!6!$Q!!Q!!!!"!!%!!A!"!!%!!1!"!!-!!1!"!!1$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!")!!!!!!!!!!!!!!*)!!!!!!1!&amp;!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="Handle Command.vi" Type="VI" URL="../Handle Command.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;O!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!J!5Q63:8"M?1"%1(!!(A!!,BV&amp;&lt;("S&lt;X2S&lt;WZJ9V^'&lt;'&amp;T;&amp;"S&lt;T1T-#ZM&gt;GRJ9AZ.&lt;W2V&lt;'5O&lt;(:D&lt;'&amp;T=Q!!#EVP:(6M:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!'%!Q`````QZ'&lt;'&amp;U&gt;'6O:71A2'&amp;U91!!#5!'!!.$451!1E"Q!"Y!!#Y&gt;27RQ=G^U=G^O;7.@2GRB=WB1=G]U-T!O&lt;(:M;7)/47^E&gt;7RF,GRW9WRB=X-!!!F.&lt;W2V&lt;'5A;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!%!!!!*)!!!!!!1!,!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Actor Core.vi" Type="VI" URL="../Actor Core.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%P!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!G&amp;E&amp;D&gt;'^S)%:S97VF&gt;W^S;SZM&gt;GRJ9H!.17.U&lt;X)O&lt;(:D&lt;'&amp;T=Q!!#5&amp;D&gt;'^S)'^V&gt;!!71&amp;!!!Q!!!!%!!ABF=H*P=C"J&lt;A!!1E"Q!"Y!!#Y&gt;27RQ=G^U=G^O;7.@2GRB=WB1=G]U-T!O&lt;(:M;7)/47^E&gt;7RF,GRW9WRB=X-!!!F.&lt;W2V&lt;'5A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
</LVClass>
