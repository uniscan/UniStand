﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="utf.calculate.project.code.coverage" Type="Bool">true</Property>
	<Property Name="utf.create.arraybrackets" Type="Str">[]</Property>
	<Property Name="utf.create.arraythreshold" Type="UInt">100</Property>
	<Property Name="utf.create.captureinputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.captureoutputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.codecoverage.flag" Type="Bool">false</Property>
	<Property Name="utf.create.codecoverage.value" Type="UInt">100</Property>
	<Property Name="utf.create.editor.flag" Type="Bool">false</Property>
	<Property Name="utf.create.editor.path" Type="Path"></Property>
	<Property Name="utf.create.nameseparator" Type="Str">/</Property>
	<Property Name="utf.create.precision" Type="UInt">6</Property>
	<Property Name="utf.create.repetitions" Type="UInt">1</Property>
	<Property Name="utf.create.testpath.flag" Type="Bool">false</Property>
	<Property Name="utf.create.testpath.path" Type="Path"></Property>
	<Property Name="utf.create.timeout.flag" Type="Bool">false</Property>
	<Property Name="utf.create.timeout.value" Type="UInt">0</Property>
	<Property Name="utf.create.type" Type="UInt">0</Property>
	<Property Name="utf.enable.RT.VI.server" Type="Bool">false</Property>
	<Property Name="utf.passwords" Type="Bin">&amp;Q#!!!!!!!)!%%!Q`````Q:4&gt;(*J&lt;G=!!":!1!!"`````Q!!#6"B=X.X&lt;X*E=Q!"!!%!!!!"!!!!#F652E&amp;-4&amp;.516)!!!!!</Property>
	<Property Name="utf.report.atml.create" Type="Bool">false</Property>
	<Property Name="utf.report.atml.path" Type="Path">ATML report.xml</Property>
	<Property Name="utf.report.atml.view" Type="Bool">false</Property>
	<Property Name="utf.report.details.errors" Type="Bool">false</Property>
	<Property Name="utf.report.details.failed" Type="Bool">false</Property>
	<Property Name="utf.report.details.passed" Type="Bool">false</Property>
	<Property Name="utf.report.errors" Type="Bool">true</Property>
	<Property Name="utf.report.failed" Type="Bool">true</Property>
	<Property Name="utf.report.html.create" Type="Bool">false</Property>
	<Property Name="utf.report.html.path" Type="Path">HTML report.html</Property>
	<Property Name="utf.report.html.view" Type="Bool">false</Property>
	<Property Name="utf.report.passed" Type="Bool">true</Property>
	<Property Name="utf.report.skipped" Type="Bool">true</Property>
	<Property Name="utf.report.sortby" Type="UInt">1</Property>
	<Property Name="utf.report.stylesheet.flag" Type="Bool">false</Property>
	<Property Name="utf.report.stylesheet.path" Type="Path"></Property>
	<Property Name="utf.report.summary" Type="Bool">true</Property>
	<Property Name="utf.report.txt.create" Type="Bool">false</Property>
	<Property Name="utf.report.txt.path" Type="Path">ASCII report.txt</Property>
	<Property Name="utf.report.txt.view" Type="Bool">false</Property>
	<Property Name="utf.run.changed.days" Type="UInt">1</Property>
	<Property Name="utf.run.changed.outdated" Type="Bool">false</Property>
	<Property Name="utf.run.changed.timestamp" Type="Bin">&amp;Q#!!!!!!!%!%%"5!!9*2'&amp;U:3^U;7VF!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	<Property Name="utf.run.days.flag" Type="Bool">false</Property>
	<Property Name="utf.run.includevicallers" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.overwrite" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.path" Type="Path">test execution log.txt</Property>
	<Property Name="utf.run.modified.last.run.flag" Type="Bool">true</Property>
	<Property Name="utf.run.priority.flag" Type="Bool">false</Property>
	<Property Name="utf.run.priority.value" Type="UInt">5</Property>
	<Property Name="utf.run.statusfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.statusfile.path" Type="Path">test status log.txt</Property>
	<Property Name="utf.run.timestamp.flag" Type="Bool">false</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Core Libs" Type="Folder">
			<Item Name="Actor Framework.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp">
				<Item Name="AF Debug" Type="Folder">
					<Item Name="Client Interface" Type="Folder">
						<Item Name="Get Actor Handles.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Get Actor Handles.vi"/>
						<Item Name="Get Registry Update Event.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Get Registry Update Event.vi"/>
						<Item Name="Protected Actor Handle.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Protected Actor Handle/Protected Actor Handle.lvclass"/>
					</Item>
					<Item Name="Debug Messages" Type="Folder">
						<Item Name="Ping Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Ping msg/Ping Msg.lvclass"/>
						<Item Name="Register Actor Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Register Actor Msg/Register Actor Msg.lvclass"/>
					</Item>
					<Item Name="support" Type="Folder">
						<Item Name="Actor Registry.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Actor Registry.vi"/>
						<Item Name="Generate Trace.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace.vi"/>
						<Item Name="Get Clone Name.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Get Clone Name.vi"/>
						<Item Name="Localized Strings.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Localized Strings.vi"/>
						<Item Name="Registration Mode.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Registration Mode.ctl"/>
						<Item Name="TDM Registration Mode.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/TDM Registration Mode.ctl"/>
						<Item Name="TDM Registry.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/TDM Registry.vi"/>
						<Item Name="This Actor.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/This Actor.ctl"/>
					</Item>
					<Item Name="Trace Generation" Type="Folder">
						<Item Name="Generate Custom Trace.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Custom Trace.vi"/>
						<Item Name="Generate Trace for Dropped Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for Dropped Message.vi"/>
						<Item Name="Generate Trace for Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for Message.vi"/>
						<Item Name="Generate Trace for New Actor.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for New Actor.vi"/>
						<Item Name="Generate Trace for New Time Delayed Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for New Time Delayed Message.vi"/>
						<Item Name="Generate Trace for Received Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for Received Message.vi"/>
						<Item Name="Generate Trace for Skipped Time-Delayed Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for Skipped Time-Delayed Message.vi"/>
						<Item Name="Generate Trace for Stopped Actor.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for Stopped Actor.vi"/>
						<Item Name="Generate Trace for Stopped Time-Delayed Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/AFDebug (source)/Generate Trace for Stopped Time-Delayed Message.vi"/>
					</Item>
				</Item>
				<Item Name="Messages" Type="Folder">
					<Item Name="Time-Delayed Send Message" Type="Folder">
						<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
						<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
						<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
					</Item>
					<Item Name="Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Message/Message.lvclass"/>
					<Item Name="Stop Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Stop Msg/Stop Msg.lvclass"/>
					<Item Name="Last Ack.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/LastAck/Last Ack.lvclass"/>
					<Item Name="Launch Nested Actor Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Launch Nested Actor Msg/Launch Nested Actor Msg.lvclass"/>
					<Item Name="Batch Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Batch Msg/Batch Msg.lvclass"/>
					<Item Name="Reply Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Reply Msg/Reply Msg.lvclass"/>
					<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Report Error Msg/Report Error Msg.lvclass"/>
					<Item Name="Self-Addressed Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Self-Addressed Msg/Self-Addressed Msg.lvclass"/>
				</Item>
				<Item Name="Actor.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Actor/Actor.lvclass"/>
				<Item Name="Message Priority Queue.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Message Priority Queue/Message Priority Queue.lvclass"/>
				<Item Name="Message Enqueuer.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Message Enqueuer/Message Enqueuer.lvclass"/>
				<Item Name="Message Dequeuer.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Message Dequeuer/Message Dequeuer.lvclass"/>
				<Item Name="Message Queue.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Message Queue/Message Queue.lvclass"/>
				<Item Name="Init Actor Queues FOR TESTING ONLY.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Actor/Init Actor Queues FOR TESTING ONLY.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/1abvi3w/vi.lib/Utility/High Resolution Relative Seconds.vi"/>
			</Item>
			<Item Name="Thread.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Thread.lvlibp">
				<Item Name="Messages to Thread" Type="Folder">
					<Item Name="Command.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Thread.lvlibp/Messages to Thread/Command/Command.lvclass"/>
					<Item Name="Command with Reply.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Thread.lvlibp/Messages to Thread/Command with Reply/Command with Reply.lvclass"/>
					<Item Name="Report-to-Caller.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Thread.lvlibp/Messages to Thread/Report-to-Caller/Report-to-Caller.lvclass"/>
				</Item>
				<Item Name="Thread.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Thread.lvlibp/Thread_class/Thread.lvclass"/>
			</Item>
			<Item Name="Module Core.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Module Core.lvlibp">
				<Item Name="Messages to Task" Type="Folder">
					<Item Name="Init Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Init Message/Init Message.lvclass"/>
					<Item Name="Prepare-to-Exec Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Prepare-to-Exec Message/Prepare-to-Exec Message.lvclass"/>
					<Item Name="Execute Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Execute Message/Execute Message.lvclass"/>
					<Item Name="Free Memory Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Free Memory Message/Free Memory Message.lvclass"/>
					<Item Name="Terminate Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Terminate Message/Terminate Message.lvclass"/>
					<Item Name="Read Self-Actor Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Read Self-Actor Message/Read Self-Actor Message.lvclass"/>
					<Item Name="Write Self-Actor Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Write Self-Actor Message/Write Self-Actor Message.lvclass"/>
					<Item Name="Set CloneFPName Message.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Messages to Task/Set CloneFPName Message/Set CloneFPName Message.lvclass"/>
				</Item>
				<Item Name="Task.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Task_class/Task.lvclass"/>
				<Item Name="Module.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Module_class/Module.lvclass"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Name.vi"/>
			</Item>
			<Item Name="Modules Hierarchy.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp">
				<Item Name="OVERRIDE ONLY &quot;GET HIERARCHY PATH&quot; METHOD" Type="Folder"/>
				<Item Name="Root" Type="Folder">
					<Item Name="Children" Type="Folder">
						<Item Name="Computer Processing" Type="Folder">
							<Item Name="Computer Processing.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Computer Processing Class/Computer Processing.lvclass"/>
						</Item>
						<Item Name="Database" Type="Folder">
							<Item Name="Database.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Database Class/Database.lvclass"/>
						</Item>
						<Item Name="Device" Type="Folder">
							<Item Name="Children" Type="Folder">
								<Item Name="Complex" Type="Folder">
									<Item Name="Complex.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Complex Class/Complex.lvclass"/>
								</Item>
								<Item Name="Digital Multimeter" Type="Folder">
									<Item Name="Digital Multimeter.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Digital Multimeter Class/Digital Multimeter.lvclass"/>
								</Item>
								<Item Name="Electronic Load" Type="Folder">
									<Item Name="Electronic Load.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Electronic Load Class/Electronic Load.lvclass"/>
								</Item>
								<Item Name="Interface Adapter" Type="Folder">
									<Item Name="Interface Adapter.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Interface Adapter Class/Interface Adapter.lvclass"/>
								</Item>
								<Item Name="Multiplexer" Type="Folder">
									<Item Name="Multiplexer.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Multiplexer Class/Multiplexer.lvclass"/>
								</Item>
								<Item Name="Power Supply Unit" Type="Folder">
									<Item Name="Power Supply Unit.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Power Supply Unit Class/Power Supply Unit.lvclass"/>
								</Item>
								<Item Name="Programmer" Type="Folder">
									<Item Name="Programmer.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Programmer Class/Programmer.lvclass"/>
								</Item>
								<Item Name="RF Instruments" Type="Folder">
									<Item Name="Children" Type="Folder">
										<Item Name="RF Power Meter" Type="Folder">
											<Item Name="RF Power Meter.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/RF Power Meter Class/RF Power Meter.lvclass"/>
										</Item>
										<Item Name="Spectrum Analyzer" Type="Folder">
											<Item Name="RF Spectrum Analyzer.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/RF Spectrum Analyzer Class/RF Spectrum Analyzer.lvclass"/>
										</Item>
									</Item>
									<Item Name="RF Instrument.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/RF Instrument Class/RF Instrument.lvclass"/>
								</Item>
								<Item Name="Step Motor" Type="Folder">
									<Item Name="Step Motor.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Step Motor Class/Step Motor.lvclass"/>
								</Item>
							</Item>
							<Item Name="Device.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Device Class/Device.lvclass"/>
						</Item>
						<Item Name="Test Modules" Type="Folder">
							<Item Name="Test Modules.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Test Modules Class/Test Modules.lvclass"/>
						</Item>
						<Item Name="User Interface" Type="Folder">
							<Item Name="User Interface.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/User Interface Class/User Interface.lvclass"/>
						</Item>
					</Item>
					<Item Name="Root Module Directory.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp/Root Module Directory Class/Root Module Directory.lvclass"/>
				</Item>
			</Item>
			<Item Name="Shared.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Shared.lvlibp">
				<Item Name="Changelog Tools" Type="Folder">
					<Item Name="Changelog File Add Build.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Changelog Tools/Changelog File Add Build.vi"/>
					<Item Name="Changelog File Readout.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Changelog Tools/Changelog File Readout.vi"/>
					<Item Name="Changelog New Build Window.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Changelog Tools/Changelog New Build Window.vi"/>
					<Item Name="Check Build Update.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Changelog Tools/Check Build Update.vi"/>
					<Item Name="Post-Build Window Call.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Changelog Tools/Post-Build Window Call.vi"/>
					<Item Name="Version.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Changelog Tools/Version.ctl"/>
				</Item>
				<Item Name="Debug" Type="Folder">
					<Item Name="High Precision Timer (AnyWire).vim" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Debug/High Precision Timer (AnyWire).vim"/>
					<Item Name="Wait (AnyWire).vim" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Debug/Wait (AnyWire).vim"/>
				</Item>
				<Item Name="Error Management" Type="Folder">
					<Item Name="Build Error.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Error Management/Build Error.vi"/>
					<Item Name="Clear All Errors.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Error Management/Clear All Errors.vi"/>
					<Item Name="Convert Error.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Error Management/Convert Error.vi"/>
					<Item Name="Filter Error Code.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Error Management/Filter Error Code.vi"/>
					<Item Name="Filter Multiple Error Codes.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Error Management/Filter Multiple Error Codes.vi"/>
					<Item Name="Wait.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Error Management/Wait.vi"/>
				</Item>
				<Item Name="Mouse" Type="Folder">
					<Item Name="Mouse Re-Click.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Mouse/Mouse Re-Click.vi"/>
				</Item>
				<Item Name="Other" Type="Folder">
					<Item Name="BCD To Decimal.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Other/BCD To Decimal.vi"/>
					<Item Name="Miliseconds to TimeString.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Other/Miliseconds to TimeString.vi"/>
				</Item>
				<Item Name="Paths" Type="Folder">
					<Item Name="Compiled Libs Path.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Paths/Compiled Libs Path.vi"/>
					<Item Name="Fix Path.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Paths/Fix Path.vi"/>
				</Item>
				<Item Name="Registry" Type="Folder">
					<Item Name="Backlash Fix.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Registry/Backlash Fix.vi"/>
					<Item Name="Enum Registry Key.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Registry/Enum Registry Key.vi"/>
					<Item Name="Read Registry Value (Binary).vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Registry/Read Registry Value (Binary).vi"/>
					<Item Name="Unistand Enum Directory.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Registry/Unistand Enum Directory.vi"/>
					<Item Name="UniStand Format Local Variable Path.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Registry/UniStand Format Local Variable Path.vi"/>
					<Item Name="Unistand Read Local Variable.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Registry/Unistand Read Local Variable.vi"/>
					<Item Name="Unistand Write Local Variable.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Registry/Unistand Write Local Variable.vi"/>
					<Item Name="Write Registry Value (Binary).vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Registry/Write Registry Value (Binary).vi"/>
				</Item>
				<Item Name="String" Type="Folder">
					<Item Name="Hex String to Normal String.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/String/Hex String to Normal String.vi"/>
					<Item Name="Normal String to Hex String.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/String/Normal String to Hex String.vi"/>
					<Item Name="String Levenshtein Distance.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/String/String Levenshtein Distance.vi"/>
				</Item>
				<Item Name="UI Refs" Type="Folder">
					<Item Name="Get Array Active Element Index.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/UI Refs/Get Array Active Element Index.vi"/>
				</Item>
				<Item Name="User Messages" Type="Folder">
					<Item Name="Button Dialog.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/User Messages/Button Dialog.vi"/>
					<Item Name="MaxDisplaySizes.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/User Messages/Support/MaxDisplaySizes.vi"/>
					<Item Name="Non-Blocking Message CORE.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/User Messages/Non-Blocking Message CORE.vi"/>
					<Item Name="Non-Blocking Message.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/User Messages/Non-Blocking Message.vi"/>
					<Item Name="TipStrip.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/User Messages/TipStrip.vi"/>
				</Item>
				<Item Name="VISA" Type="Folder">
					<Item Name="VISA Clear Port Data.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/VISA/VISA Clear Port Data.vi"/>
					<Item Name="VISA Set DisableErrorReplacement.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/VISA/VISA Set DisableErrorReplacement.vi"/>
				</Item>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Create Registry Key.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Create Registry Key.vi"/>
				<Item Name="Enum Registry Keys.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Enum Registry Keys.vi"/>
				<Item Name="Enum Registry Values Simple.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Enum Registry Values Simple.vi"/>
				<Item Name="Enum Registry Values.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Enum Registry Values.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="LVFontTypeDef.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVFontTypeDef.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Query Registry Key Info.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Query Registry Key Info.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write Registry Value Simple STR.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Write Registry Value Simple STR.vi"/>
				<Item Name="Write Registry Value STR.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Write Registry Value STR.vi"/>
			</Item>
		</Item>
		<Item Name="Post-Build Action.vi" Type="VI" URL="../Post-Build Action.vi"/>
		<Item Name="PSU_GWINSTEK_GPD-73303S.lvlib" Type="Library" URL="../PSU_GWINSTEK_GPD-73303S.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Select Event Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/Select Event Type.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
			</Item>
			<Item Name="Post-Build. Move Build (.lvlibp).vi" Type="VI" URL="../../../Post-Build Action VIs/Post-Build. Move Build (.lvlibp).vi"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="user32.dll" Type="Document" URL="user32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="PSU_GWINSTEK_GPD-73303S" Type="Packed Library">
				<Property Name="Bld_buildCacheID" Type="Str">{E882488C-0F99-4060-82D8-8CB6D40A3187}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">PSU_GWINSTEK_GPD-73303S</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../Modules</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_postActionVIID" Type="Ref">/My Computer/Post-Build Action.vi</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{68A38F2B-5226-4401-98AA-2CDA05EACD8F}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">PSU_GWINSTEK_GPD-73303S.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">../Modules/NI_AB_PROJECTNAME.lvlibp</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../Dependencies</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[2].destName" Type="Str">Core Libs</Property>
				<Property Name="Destination[2].path" Type="Path">..</Property>
				<Property Name="Destination[2].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{084C8312-4238-4426-9024-2AECD9220CE8}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[1].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[1].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Core Libs</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Core Libs/Actor Framework.lvlibp</Property>
				<Property Name="Source[2].preventRename" Type="Bool">true</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Core Libs/Thread.lvlibp</Property>
				<Property Name="Source[3].preventRename" Type="Bool">true</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Core Libs/Module Core.lvlibp</Property>
				<Property Name="Source[4].preventRename" Type="Bool">true</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Core Libs/Modules Hierarchy.lvlibp</Property>
				<Property Name="Source[5].preventRename" Type="Bool">true</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/PSU_GWINSTEK_GPD-73303S.lvlib</Property>
				<Property Name="Source[6].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[6].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[6].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[6].preventRename" Type="Bool">true</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[6].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">7</Property>
				<Property Name="TgtF_fileDescription" Type="Str">PSU_GWINSTEK_GPD-73303S</Property>
				<Property Name="TgtF_internalName" Type="Str">PSU_GWINSTEK_GPD-73303S</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2016 </Property>
				<Property Name="TgtF_productName" Type="Str">PSU_GWINSTEK_GPD-73303S</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{87C8F61C-58B6-4975-9749-E7BE763F6654}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">PSU_GWINSTEK_GPD-73303S.lvlibp</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
