﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">3524608</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16751729</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)5!!!*Q(C=\&gt;4.58*"%)8BI_8#,2F9J.!JE!)J&gt;!JMPS5JH$)$5C#&amp;4I%53/([TND&amp;RU*EIV77Z1S$X$.`DX-P3,-]3U^;\J884]OF@YY=&amp;]N\W$0`TV`?L]:L8#S8Z,K`[_X_VY`[L`?_O@]N`ZWCP`*8@H@Z&gt;`&gt;,]/X@AQ?.(S*;U5R,7GBMOR2ZE2&gt;ZE2&gt;ZE:P=Z#9XO=F.HO2*HO2*HO2*(O2"(O2"(O2"0COZS%5O=ED&amp;ZM6'R;,&amp;!M6A+#L?#E`B+4S&amp;BUM6HM*4?!J0Y7')#E`B+4S&amp;J`!Q497H]"3?QF.Y7'J+;F:S0)7(Z75]RG-]RG-]&lt;#HD-1#TG6H9,!*$JN.]-"\D-2Y_SHC-RXC-RXDIFP%9D`%9D`%Q::[+JW:5=DQMI]34?"*0YEE],+X%EXA34_**0'SHR*.Y%E3S9&lt;)Y"#74EA(*2?**00R2YEE]C3@R*"[[ZBX+?4*$-SIZHM!4?!*0Y!E],+(!%XA#4_!*0#SLQ".Y!E`A#4RMJ=!4?!*0!!EW:8M&amp;CQ54AU&amp;"%(BYT;=FZFXSF-3M5D_][I&gt;3`&lt;#J(S,VQ['_[?K&lt;K&lt;Z*[M.8([L[M.3(I0\HV'AV2LW*?P)9K$0P*^K2&gt;K$N;4P;FL;BL7GL-@7,"Z\0:ZV/*RW02RU/"_XX?_VW/WWX7WUW'[X8;[V7K]P0Q!PV]I0Q%X[8@I4B5&gt;&gt;\=%:PA3[PSA!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Tasks" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="UI TypeDefs" Type="Folder">
			<Item Name="Channel.ctl" Type="VI" URL="../Task Classes/Channel.ctl"/>
			<Item Name="Mode.ctl" Type="VI" URL="../Task Classes/Mode.ctl"/>
			<Item Name="Parameter.ctl" Type="VI" URL="../Task Classes/Parameter.ctl"/>
		</Item>
		<Item Name="Init.lvclass" Type="LVClass" URL="../Task Classes/Init/Init.lvclass"/>
		<Item Name="Deinit.lvclass" Type="LVClass" URL="../Task Classes/Deinit/Deinit.lvclass"/>
		<Item Name="Track.lvclass" Type="LVClass" URL="../Task Classes/Track/Track.lvclass"/>
		<Item Name="Set Value.lvclass" Type="LVClass" URL="../Task Classes/Set Value/Set Value.lvclass"/>
		<Item Name="Enable.lvclass" Type="LVClass" URL="../Task Classes/Enable/Enable.lvclass"/>
		<Item Name="Disable.lvclass" Type="LVClass" URL="../Task Classes/Disable/Disable.lvclass"/>
		<Item Name="Get Actual Ch Value.lvclass" Type="LVClass" URL="../Task Classes/Get Actual Ch Value/Get Actual Ch Value.lvclass"/>
		<Item Name="Read Values.lvclass" Type="LVClass" URL="../Task Classes/Read Values/Read Values.lvclass"/>
		<Item Name="Get Channel Settings.lvclass" Type="LVClass" URL="../Task Classes/Get Channel Settings/Get Channel Settings.lvclass"/>
	</Item>
	<Item Name="Module.lvclass" Type="LVClass" URL="../Module Class/Module.lvclass"/>
</Library>
