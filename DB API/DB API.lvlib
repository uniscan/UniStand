﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">57327</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)Z!!!*Q(C=\&gt;7^47."&amp;-8RMWC$4&gt;U"&gt;AMHI1#XY"9O*4AF1-)&amp;E.RQ*3*8A(!,NQ7XY*41_Z^B&lt;%"#?!.!KR8T00&lt;TG;]@]ZY@5C_`J*`;HSI0\Z;H^NZN@TA`@(`2XF\\B_0L]0;[P:`X*@^C`/PWZ`8@&lt;(`0@[,IOXS8`\N=H@Q2@0LPY)@;AYB;V+1'V7L,\IO]S)O]S)O]S*-]S:-]S:-]S9-]S)-]S)-]S%VO=J/&lt;X/1G\Q?ZS%5O=ED&amp;YM6#R;4&amp;"%6H+#L?#E`B+4S&amp;B[]K0)7H]"3?QE-8&amp;:\#5XA+4_&amp;BG!J0Y3E]B;@Q-&amp;786$`)]21?JF@C34S**`%E(J:5YEE!S7,*R-EE-*1U*C?**`%E(E[6?"*0YEE]C9&gt;G*:\%EXA34_*B3._6\*JWE/.B'A7?Q".Y!E`A97I&amp;HM!4?!*0Y'%Z":\!%S##"90*)3A9&amp;(1)PA3?Q-/(!E`A#4S"*`$1V+^1^*VJGH;1YT%?YT%?YT%?JJ$R')`R')`R-+W-RXC-RXC-B[6E0-:D0!:C&amp;G6ZG=H-1.0*"-&lt;$K^]N\F=JO]4^E-&lt;.KX&amp;4;NRM'D?2RMWB=&gt;%V,K&lt;'2&gt;,9@)V.V&gt;AMD5X1_/-UU"I9D55U"L?/WP'_J7[I;_K+OK1OK(0KD$JJ1T_YYW[XUX;\V7;TU8K^VGKVUH+ZV'+RU(Q_VWQWUW1S/4Y'TDG/$Y3HZ^)6ZT=8F^0LO^P@V`?0\8.[=X%\&lt;@E8`(`_"Z[./N0,.&gt;CD0]/2N!]!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="TYPEDEFS before &quot;DB Var to Data&quot; are disconnected! THX LV" Type="Folder"/>
	<Item Name="MF DB" Type="Folder">
		<Item Name="ID TypeDefs" Type="Folder">
			<Item Name="Stand Type ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Stand Type ID.ctl"/>
			<Item Name="Stand ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Stand ID.ctl"/>
			<Item Name="Stand Change ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Stand Change ID.ctl"/>
			<Item Name="Stand Parameter ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Stand Parameter ID.ctl"/>
			<Item Name="Stand Maintenance Type ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Stand Maintenance Type ID.ctl"/>
			<Item Name="Stand Maintenance Schedule ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Stand Maintenance Schedule ID.ctl"/>
			<Item Name="Stand Maintenance ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Stand Maintenance ID.ctl"/>
			<Item Name="Stand Test Table ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Stand Test Table ID.ctl"/>
			<Item Name="Stand Test Type ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Stand Test Type ID.ctl"/>
			<Item Name="Device Type ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Device Type ID.ctl"/>
			<Item Name="Device ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Device ID.ctl"/>
			<Item Name="Device Serial Number ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Device Serial Number ID.ctl"/>
			<Item Name="Device Assembly Option ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Device Assembly Option ID.ctl"/>
			<Item Name="Device Assembly ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Device Assembly ID.ctl"/>
			<Item Name="User Group ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/User Group ID.ctl"/>
			<Item Name="User ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/User ID.ctl"/>
			<Item Name="Workstation ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Workstation ID.ctl"/>
			<Item Name="Test ID.ctl" Type="VI" URL="../MF DB/ID TypeDefs/Test ID.ctl"/>
		</Item>
		<Item Name="Stands" Type="Folder">
			<Item Name="MFDB_standTypes INSERT.vi" Type="VI" URL="../MF DB/Stands/MFDB_standTypes INSERT.vi"/>
			<Item Name="MFDB_standTypes SELECT All.vi" Type="VI" URL="../MF DB/Stands/MFDB_standTypes SELECT All.vi"/>
			<Item Name="MFDB_standTypes SELECT All (full).vi" Type="VI" URL="../MF DB/Stands/MFDB_standTypes SELECT All (full).vi"/>
			<Item Name="MFDB_standTypes SELECT by ID.vi" Type="VI" URL="../MF DB/Stands/MFDB_standTypes SELECT by ID.vi"/>
			<Item Name="MFDB_standTypes UPDATE.vi" Type="VI" URL="../MF DB/Stands/MFDB_standTypes UPDATE.vi"/>
			<Item Name="MFDB_standTypes DELETE.vi" Type="VI" URL="../MF DB/Stands/MFDB_standTypes DELETE.vi"/>
			<Item Name="MFDB_stands INSERT.vi" Type="VI" URL="../MF DB/Stands/MFDB_stands INSERT.vi"/>
			<Item Name="MFDB_stands SELECT All.vi" Type="VI" URL="../MF DB/Stands/MFDB_stands SELECT All.vi"/>
			<Item Name="MFDB_stands UPDATE.vi" Type="VI" URL="../MF DB/Stands/MFDB_stands UPDATE.vi"/>
			<Item Name="MFDB_stands DELETE.vi" Type="VI" URL="../MF DB/Stands/MFDB_stands DELETE.vi"/>
			<Item Name="MFDB_stands SELECT by TypeID.vi" Type="VI" URL="../MF DB/Stands/MFDB_stands SELECT by TypeID.vi"/>
			<Item Name="MFDB_stands SELECT by TestTypeID.vi" Type="VI" URL="../MF DB/Stands/MFDB_stands SELECT by TestTypeID.vi"/>
			<Item Name="MFDB_standChanges INSERT.vi" Type="VI" URL="../MF DB/Stands/MFDB_standChanges INSERT.vi"/>
			<Item Name="MFDB_standChanges SELECT All.vi" Type="VI" URL="../MF DB/Stands/MFDB_standChanges SELECT All.vi"/>
			<Item Name="MFDB_standChanges UPDATE.vi" Type="VI" URL="../MF DB/Stands/MFDB_standChanges UPDATE.vi"/>
			<Item Name="MFDB_standChanges DELETE.vi" Type="VI" URL="../MF DB/Stands/MFDB_standChanges DELETE.vi"/>
			<Item Name="MFDB_standParameters INSERT UPDATE.vi" Type="VI" URL="../MF DB/Stands/MFDB_standParameters INSERT UPDATE.vi"/>
			<Item Name="MFDB_standParameters SELECT by StandID.vi" Type="VI" URL="../MF DB/Stands/MFDB_standParameters SELECT by StandID.vi"/>
			<Item Name="MFDB_standParameters DELETE.vi" Type="VI" URL="../MF DB/Stands/MFDB_standParameters DELETE.vi"/>
			<Item Name="MFDB_standMaintenanceTypes INSERT.vi" Type="VI" URL="../MF DB/Stands/MFDB_standMaintenanceTypes INSERT.vi"/>
			<Item Name="MFDB_standMaintenanceTypes SELECT All.vi" Type="VI" URL="../MF DB/Stands/MFDB_standMaintenanceTypes SELECT All.vi"/>
			<Item Name="MFDB_standMaintenanceTypes UPDATE.vi" Type="VI" URL="../MF DB/Stands/MFDB_standMaintenanceTypes UPDATE.vi"/>
			<Item Name="MFDB_standMaintenanceTypes DELETE.vi" Type="VI" URL="../MF DB/Stands/MFDB_standMaintenanceTypes DELETE.vi"/>
			<Item Name="MFDB_standMaintenanceSchedule INSERT.vi" Type="VI" URL="../MF DB/Stands/MFDB_standMaintenanceSchedule INSERT.vi"/>
			<Item Name="MFDB_standMaintenanceSchedule SELECT All.vi" Type="VI" URL="../MF DB/Stands/MFDB_standMaintenanceSchedule SELECT All.vi"/>
			<Item Name="MFDB_standMaintenanceSchedule UPDATE.vi" Type="VI" URL="../MF DB/Stands/MFDB_standMaintenanceSchedule UPDATE.vi"/>
			<Item Name="MFDB_standMaintenanceSchedule DELETE.vi" Type="VI" URL="../MF DB/Stands/MFDB_standMaintenanceSchedule DELETE.vi"/>
			<Item Name="MFDB_standMaintenance INSERT.vi" Type="VI" URL="../MF DB/Stands/MFDB_standMaintenance INSERT.vi"/>
			<Item Name="MFDB_standMaintenance SELECT All.vi" Type="VI" URL="../MF DB/Stands/MFDB_standMaintenance SELECT All.vi"/>
			<Item Name="MFDB_standMaintenance UPDATE.vi" Type="VI" URL="../MF DB/Stands/MFDB_standMaintenance UPDATE.vi"/>
			<Item Name="MFDB_standMaintenance DELETE.vi" Type="VI" URL="../MF DB/Stands/MFDB_standMaintenance DELETE.vi"/>
			<Item Name="MFDB_standTestTables INSERT.vi" Type="VI" URL="../MF DB/Stands/MFDB_standTestTables INSERT.vi"/>
			<Item Name="MFDB_standTestTables DELETE.vi" Type="VI" URL="../MF DB/Stands/MFDB_standTestTables DELETE.vi"/>
			<Item Name="MFDB_standTestTables SELECT All.vi" Type="VI" URL="../MF DB/Stands/MFDB_standTestTables SELECT All.vi"/>
			<Item Name="MFDB_standTestTypes INSERT.vi" Type="VI" URL="../MF DB/Stands/MFDB_standTestTypes INSERT.vi"/>
			<Item Name="MFDB_standTestTypes SELECT All.vi" Type="VI" URL="../MF DB/Stands/MFDB_standTestTypes SELECT All.vi"/>
			<Item Name="MFDB_standTestTypes SELECT by DeviceTypeID.vi" Type="VI" URL="../MF DB/Stands/MFDB_standTestTypes SELECT by DeviceTypeID.vi"/>
			<Item Name="MFDB_standTestTypes SELECT by StandID.vi" Type="VI" URL="../MF DB/Stands/MFDB_standTestTypes SELECT by StandID.vi"/>
			<Item Name="MFDB_standTestTypes SELECT by TestTypeID.vi" Type="VI" URL="../MF DB/Stands/MFDB_standTestTypes SELECT by TestTypeID.vi"/>
			<Item Name="MFDB_standTestTypes UPDATE.vi" Type="VI" URL="../MF DB/Stands/MFDB_standTestTypes UPDATE.vi"/>
			<Item Name="MFDB_standTestTypes DELETE.vi" Type="VI" URL="../MF DB/Stands/MFDB_standTestTypes DELETE.vi"/>
		</Item>
		<Item Name="Devices" Type="Folder">
			<Item Name="MFDB_deviceTypes INSERT.vi" Type="VI" URL="../MF DB/Devices/MFDB_deviceTypes INSERT.vi"/>
			<Item Name="MFDB_deviceTypes SELECT All.vi" Type="VI" URL="../MF DB/Devices/MFDB_deviceTypes SELECT All.vi"/>
			<Item Name="MFDB_deviceTypes SELECT by ID.vi" Type="VI" URL="../MF DB/Devices/MFDB_deviceTypes SELECT by ID.vi"/>
			<Item Name="MFDB_deviceTypes SELECT All (with Config).vi" Type="VI" URL="../MF DB/Devices/MFDB_deviceTypes SELECT All (with Config).vi"/>
			<Item Name="MFDB_deviceTypes UPDATE.vi" Type="VI" URL="../MF DB/Devices/MFDB_deviceTypes UPDATE.vi"/>
			<Item Name="MFDB_deviceTypes DELETE.vi" Type="VI" URL="../MF DB/Devices/MFDB_deviceTypes DELETE.vi"/>
			<Item Name="MFDB_deviceTypes SELECT by DeviceID.vi" Type="VI" URL="../MF DB/Devices/MFDB_deviceTypes SELECT by DeviceID.vi"/>
			<Item Name="MFDB_devices INSERT.vi" Type="VI" URL="../MF DB/Devices/MFDB_devices INSERT.vi"/>
			<Item Name="MFDB_device SELECT by TypeID.vi" Type="VI" URL="../MF DB/Devices/MFDB_device SELECT by TypeID.vi"/>
			<Item Name="MFDB_device SELECT by SerialNumber.vi" Type="VI" URL="../MF DB/Devices/MFDB_device SELECT by SerialNumber.vi"/>
			<Item Name="MFDB_deviceSerialNumbers INSERT.vi" Type="VI" URL="../MF DB/Devices/MFDB_deviceSerialNumbers INSERT.vi"/>
			<Item Name="MFDB_deviceAssemblyOptions INSERT.vi" Type="VI" URL="../MF DB/Devices/MFDB_deviceAssemblyOptions INSERT.vi"/>
			<Item Name="MFDB_deviceAssemblyOptions SELECT All.vi" Type="VI" URL="../MF DB/Devices/MFDB_deviceAssemblyOptions SELECT All.vi"/>
			<Item Name="MFDB_deviceAssemblyOptions UPDATE.vi" Type="VI" URL="../MF DB/Devices/MFDB_deviceAssemblyOptions UPDATE.vi"/>
			<Item Name="MFDB_deviceAssemblyOptions DELETE.vi" Type="VI" URL="../MF DB/Devices/MFDB_deviceAssemblyOptions DELETE.vi"/>
			<Item Name="MFDB_deviceAssembly INSERT.vi" Type="VI" URL="../MF DB/Devices/MFDB_deviceAssembly INSERT.vi"/>
			<Item Name="MFDB_deviceAssembly SELECT Components.vi" Type="VI" URL="../MF DB/Devices/MFDB_deviceAssembly SELECT Components.vi"/>
		</Item>
		<Item Name="Users" Type="Folder">
			<Item Name="Access Flags Support" Type="Folder">
				<Item Name="Access Flags.ctl" Type="VI" URL="../MF DB/Users/Access Flags Support/Access Flags.ctl"/>
				<Item Name="Access Flags List.vi" Type="VI" URL="../MF DB/Users/Access Flags Support/Access Flags List.vi"/>
				<Item Name="Access Flags Flatten.vi" Type="VI" URL="../MF DB/Users/Access Flags Support/Access Flags Flatten.vi"/>
				<Item Name="Access Flags Unflatten.vi" Type="VI" URL="../MF DB/Users/Access Flags Support/Access Flags Unflatten.vi"/>
				<Item Name="AccessCheck_CheckFlag.vi" Type="VI" URL="../MF DB/Users/Access Flags Support/AccessCheck_CheckFlag.vi"/>
				<Item Name="AccessCheck_TestingUtilities.vi" Type="VI" URL="../MF DB/Users/Access Flags Support/AccessCheck_TestingUtilities.vi"/>
				<Item Name="AccessCheck_MFDBEditFull.vi" Type="VI" URL="../MF DB/Users/Access Flags Support/AccessCheck_MFDBEditFull.vi"/>
				<Item Name="AccessCheck_UniStandEdit.vi" Type="VI" URL="../MF DB/Users/Access Flags Support/AccessCheck_UniStandEdit.vi"/>
				<Item Name="AccessCheck_MFDBAnalitycs.vi" Type="VI" URL="../MF DB/Users/Access Flags Support/AccessCheck_MFDBAnalitycs.vi"/>
				<Item Name="AccessCheck_MFDBStandMaintenance.vi" Type="VI" URL="../MF DB/Users/Access Flags Support/AccessCheck_MFDBStandMaintenance.vi"/>
				<Item Name="AccessCheck.vi" Type="VI" URL="../MF DB/Users/Access Flags Support/AccessCheck.vi"/>
			</Item>
			<Item Name="MFDB_userGroups INSERT.vi" Type="VI" URL="../MF DB/Users/MFDB_userGroups INSERT.vi"/>
			<Item Name="MFDB_userGroups SELECT All.vi" Type="VI" URL="../MF DB/Users/MFDB_userGroups SELECT All.vi"/>
			<Item Name="MFDB_userGroups UPDATE.vi" Type="VI" URL="../MF DB/Users/MFDB_userGroups UPDATE.vi"/>
			<Item Name="MFDB_userGroups DELETE.vi" Type="VI" URL="../MF DB/Users/MFDB_userGroups DELETE.vi"/>
			<Item Name="MFDB_users INSERT.vi" Type="VI" URL="../MF DB/Users/MFDB_users INSERT.vi"/>
			<Item Name="MFDB_users SELECT.vi" Type="VI" URL="../MF DB/Users/MFDB_users SELECT.vi"/>
			<Item Name="MFDB_users SELECT All.vi" Type="VI" URL="../MF DB/Users/MFDB_users SELECT All.vi"/>
			<Item Name="MFDB_users UPDATE.vi" Type="VI" URL="../MF DB/Users/MFDB_users UPDATE.vi"/>
			<Item Name="MFDB_users DELETE.vi" Type="VI" URL="../MF DB/Users/MFDB_users DELETE.vi"/>
		</Item>
		<Item Name="Workstations" Type="Folder">
			<Item Name="MFDB_workstations INSERT.vi" Type="VI" URL="../MF DB/Workstations/MFDB_workstations INSERT.vi"/>
			<Item Name="MFDB_workstations SELECT All.vi" Type="VI" URL="../MF DB/Workstations/MFDB_workstations SELECT All.vi"/>
			<Item Name="MFDB_workstations UPDATE.vi" Type="VI" URL="../MF DB/Workstations/MFDB_workstations UPDATE.vi"/>
			<Item Name="MFDB_workstations DELETE.vi" Type="VI" URL="../MF DB/Workstations/MFDB_workstations DELETE.vi"/>
		</Item>
		<Item Name="Tests" Type="Folder">
			<Item Name="MFDB_tests INSERT.vi" Type="VI" URL="../MF DB/Tests/MFDB_tests INSERT.vi"/>
			<Item Name="MFDB_tests UPDATE.vi" Type="VI" URL="../MF DB/Tests/MFDB_tests UPDATE.vi"/>
			<Item Name="MFDB_tests SELECT by ID.vi" Type="VI" URL="../MF DB/Tests/MFDB_tests SELECT by ID.vi"/>
		</Item>
		<Item Name="Test Tables" Type="Folder">
			<Item Name="MFDB_testTables INSERT Value.vi" Type="VI" URL="../MF DB/Test Tables/MFDB_testTables INSERT Value.vi"/>
			<Item Name="MFDB_testTables SELECT Table Columns.vi" Type="VI" URL="../MF DB/Test Tables/MFDB_testTables SELECT Table Columns.vi"/>
		</Item>
		<Item Name="DB VIewer" Type="Folder">
			<Item Name="Analytics" Type="Folder">
				<Item Name="MFDB_DBViewer Analytics Tests Count.vi" Type="VI" URL="../MF DB/DB Viewer/MFDB_DBViewer Analytics Tests Count.vi"/>
				<Item Name="MFDB_DBViewer Analytics Tests Table Columns.vi" Type="VI" URL="../MF DB/DB Viewer/MFDB_DBViewer Analytics Tests Table Columns.vi"/>
				<Item Name="MFDB_DBViewer Analytics Test Column Values.vi" Type="VI" URL="../MF DB/DB Viewer/MFDB_DBViewer Analytics Test Column Values.vi"/>
			</Item>
			<Item Name="MFDB_DBViewer Service Columns.ctl" Type="VI" URL="../MF DB/DB Viewer/MFDB_DBViewer Service Columns.ctl"/>
			<Item Name="MFDB_DBViewer Column Parameters.ctl" Type="VI" URL="../MF DB/DB Viewer/MFDB_DBViewer Column Parameters.ctl"/>
			<Item Name="MFDB_DBViewer Status Flag Condition.ctl" Type="VI" URL="../MF DB/DB Viewer/MFDB_DBViewer Status Flag Condition.ctl"/>
			<Item Name="MFDB_DBViewer INSERT Parameters.vi" Type="VI" URL="../MF DB/DB Viewer/MFDB_DBViewer INSERT Parameters.vi"/>
			<Item Name="MFDB_DBViewer SELECT Parameters.vi" Type="VI" URL="../MF DB/DB Viewer/MFDB_DBViewer SELECT Parameters.vi"/>
			<Item Name="MFDB_DBViewer Format Test Data.vi" Type="VI" URL="../MF DB/DB Viewer/MFDB_DBViewer Format Test Data.vi"/>
			<Item Name="MFDB_DBViewer SELECT Test Data.vi" Type="VI" URL="../MF DB/DB Viewer/MFDB_DBViewer SELECT Test Data.vi"/>
		</Item>
		<Item Name="UniStand Gate" Type="Folder">
			<Item Name="MFDB_UniStandGate SELECT Stands Info.vi" Type="VI" URL="../MF DB/UniStand Gate/MFDB_UniStandGate SELECT Stands Info.vi"/>
		</Item>
		<Item Name="UniStand" Type="Folder">
			<Item Name="MFDB_UniStand Identify User and Workstation.vi" Type="VI" URL="../MF DB/UniStand/MFDB_UniStand Identify User and Workstation.vi"/>
		</Item>
		<Item Name="Complex" Type="Folder">
			<Item Name="MFDB_complex Check IDs as Components.vi" Type="VI" URL="../MF DB/Complex/MFDB_complex Check IDs as Components.vi"/>
			<Item Name="MFDB_complex Check SNs as Components.vi" Type="VI" URL="../MF DB/Complex/MFDB_complex Check SNs as Components.vi"/>
			<Item Name="MFDB_complex Check SN by Test Type.vi" Type="VI" URL="../MF DB/Complex/MFDB_complex Check SN by Test Type.vi"/>
			<Item Name="MFDB_complex Check ID by Test Type.vi" Type="VI" URL="../MF DB/Complex/MFDB_complex Check ID by Test Type.vi"/>
			<Item Name="MFDB_complex New ID by Test Type.vi" Type="VI" URL="../MF DB/Complex/MFDB_complex New ID by Test Type.vi"/>
			<Item Name="MFDB_complex Search ID by SN.vi" Type="VI" URL="../MF DB/Complex/MFDB_complex Search ID by SN.vi"/>
			<Item Name="MFDB_complex Get Stands Maintenance Info (Runs).vi" Type="VI" URL="../MF DB/Complex/MFDB_complex Get Stands Maintenance Info (Runs).vi"/>
			<Item Name="MFDB_complex Get Stands Maintenance Info (Days).vi" Type="VI" URL="../MF DB/Complex/MFDB_complex Get Stands Maintenance Info (Days).vi"/>
		</Item>
		<Item Name="MF DB Open.vi" Type="VI" URL="../MF DB/MF DB Open.vi"/>
		<Item Name="MF DB Close.vi" Type="VI" URL="../MF DB/MF DB Close.vi"/>
	</Item>
</Library>
