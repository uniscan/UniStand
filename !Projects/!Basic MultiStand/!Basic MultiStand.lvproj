﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Libs" Type="Folder">
			<Item Name="Actor Framework.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp"/>
			<Item Name="Data Parser.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp"/>
			<Item Name="Module Core.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Module Core.lvlibp"/>
			<Item Name="Modules Hierarchy.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Modules Hierarchy.lvlibp"/>
			<Item Name="Sequence.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Sequence.lvlibp"/>
			<Item Name="Shared.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Shared.lvlibp"/>
			<Item Name="Thread.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Thread.lvlibp"/>
			<Item Name="Sequence Controller.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/Sequence Controller.lvlibp"/>
			<Item Name="DB API.lvlibp" Type="LVLibp" URL="../../../!Compiled Framework Libs/DB API.lvlibp"/>
		</Item>
		<Item Name="Messages to UniStand Class" Type="Folder">
			<Item Name="Pre-Start Check.lvclass" Type="LVClass" URL="../Messages to UniStand Class/Pre-Start Check/Pre-Start Check.lvclass"/>
			<Item Name="ID-SN Switch.lvclass" Type="LVClass" URL="../Messages to UniStand Class/ID-SN Switch/ID-SN Switch.lvclass"/>
			<Item Name="Restore Last ID-SN.lvclass" Type="LVClass" URL="../Messages to UniStand Class/Restore Last ID-SN/Restore Last ID-SN.lvclass"/>
		</Item>
		<Item Name="UniStand.lvclass" Type="LVClass" URL="../UniStand Class/UniStand.lvclass"/>
		<Item Name="UniStand.ico" Type="Document" URL="../UniStand.ico"/>
		<Item Name="!Basic MultiStand.ini" Type="Document" URL="../!Basic MultiStand.ini"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="LVSelectionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVSelectionTypeDef.ctl"/>
				<Item Name="LVStringsAndValuesArrayTypeDef_U64.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVStringsAndValuesArrayTypeDef_U64.ctl"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
			</Item>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Actor.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Actor/Actor.lvclass"/>
			<Item Name="MF DB Open.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/MF DB Open.vi"/>
			<Item Name="MFDB_UniStandGate SELECT Stands Info.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/UniStand Gate/MFDB_UniStandGate SELECT Stands Info.vi"/>
			<Item Name="Mouse Re-Click.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Mouse/Mouse Re-Click.vi"/>
			<Item Name="Compiled Libs Path.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Paths/Compiled Libs Path.vi"/>
			<Item Name="MFDB_standTestTypes SELECT by StandID.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Stands/MFDB_standTestTypes SELECT by StandID.vi"/>
			<Item Name="Fix Path.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/Paths/Fix Path.vi"/>
			<Item Name="MFDB_standParameters SELECT by StandID.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Stands/MFDB_standParameters SELECT by StandID.vi"/>
			<Item Name="MFDB_UniStand Identify User and Workstation.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/UniStand/MFDB_UniStand Identify User and Workstation.vi"/>
			<Item Name="MFDB_users SELECT.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Users/MFDB_users SELECT.vi"/>
			<Item Name="MF DB Close.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/MF DB Close.vi"/>
			<Item Name="Convert Data To Image.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Convert Data To Image.vi"/>
			<Item Name="Get Image Names.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Get Image Names.vi"/>
			<Item Name="Get Array of Name Strings.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Get Array of Name Strings.vi"/>
			<Item Name="Get Image Values.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Get Image Values.vi"/>
			<Item Name="Get Data Names.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Get Data Names.vi"/>
			<Item Name="Set Data Values.vi" Type="VI" URL="../../../!Compiled Framework Libs/Data Parser.lvlibp/Set Data Values.vi"/>
			<Item Name="Executor.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Sequence Controller.lvlibp/Executor Class/Executor.lvclass"/>
			<Item Name="Read Self Enqueuer.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Actor/Read Self Enqueuer.vi"/>
			<Item Name="Read Caller Enqueuer.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Actor/Read Caller Enqueuer.vi"/>
			<Item Name="Send AppClose.vi" Type="VI" URL="../../../!Compiled Framework Libs/Sequence Controller.lvlibp/Executor Class/Send AppClose.vi"/>
			<Item Name="Message Enqueuer.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Message Enqueuer/Message Enqueuer.lvclass"/>
			<Item Name="Send Message And Wait For Response.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Reply Msg/Send Message And Wait For Response.vi"/>
			<Item Name="Send Start.vi" Type="VI" URL="../../../!Compiled Framework Libs/Sequence Controller.lvlibp/Executor Class/Send Start.vi"/>
			<Item Name="Send Stop.vi" Type="VI" URL="../../../!Compiled Framework Libs/Sequence Controller.lvlibp/Executor Class/Send Stop.vi"/>
			<Item Name="Node ID.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Sequence.lvlibp/Sequence_class/Public API/Type Defs/Node ID.ctl"/>
			<Item Name="Sequence Execution Result.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Sequence Controller.lvlibp/Executor Class/Sequence Execution Result.ctl"/>
			<Item Name="Task Actual Data.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Task_class/Task Actual Data.ctl"/>
			<Item Name="Task Parameters.ctl" Type="VI" URL="../../../!Compiled Framework Libs/Module Core.lvlibp/Task_class/Task Parameters.ctl"/>
			<Item Name="Task Get ClassQualifiedName.vi" Type="VI" URL="../../../!Compiled Framework Libs/Sequence.lvlibp/Sequence_class/Public API/Task/Core Data/Task Get ClassQualifiedName.vi"/>
			<Item Name="Task Set SavedConstructorData.vi" Type="VI" URL="../../../!Compiled Framework Libs/Sequence.lvlibp/Sequence_class/Public API/Task/Core Data/Task Set SavedConstructorData.vi"/>
			<Item Name="Get Node Tree ID Path.vi" Type="VI" URL="../../../!Compiled Framework Libs/Sequence.lvlibp/Sequence_class/Public API/Get Node Tree ID Path.vi"/>
			<Item Name="Branch Get CustomName.vi" Type="VI" URL="../../../!Compiled Framework Libs/Sequence.lvlibp/Sequence_class/Public API/Branch/Core Data/Branch Get CustomName.vi"/>
			<Item Name="Node Get Metainfo.vi" Type="VI" URL="../../../!Compiled Framework Libs/Sequence.lvlibp/Sequence_class/Public API/Node/Core Data/Node Get Metainfo.vi"/>
			<Item Name="Node Execution Status (Override).ctl" Type="VI" URL="../../../!Compiled Framework Libs/Sequence Controller.lvlibp/Executor Class/Node Execution Status (Override).ctl"/>
			<Item Name="Sequence Controller.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Sequence Controller.lvlibp/Sequence Controller Class/Sequence Controller.lvclass"/>
			<Item Name="AccessCheck.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Users/Access Flags Support/AccessCheck.vi"/>
			<Item Name="AccessCheck_UniStandEdit.vi" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/Users/Access Flags Support/AccessCheck_UniStandEdit.vi"/>
			<Item Name="Launch Root Actor.vi" Type="VI" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Actor/Launch Root Actor.vi"/>
			<Item Name="Hex String to Normal String.vi" Type="VI" URL="../../../!Compiled Framework Libs/Shared.lvlibp/String/Hex String to Normal String.vi"/>
			<Item Name="Stand ID.ctl" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/ID TypeDefs/Stand ID.ctl"/>
			<Item Name="Stand Test Type ID.ctl" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/ID TypeDefs/Stand Test Type ID.ctl"/>
			<Item Name="Device ID.ctl" Type="VI" URL="../../../!Compiled Framework Libs/DB API.lvlibp/MF DB/ID TypeDefs/Device ID.ctl"/>
			<Item Name="Reply Msg.lvclass" Type="LVClass" URL="../../../!Compiled Framework Libs/Actor Framework.lvlibp/ActorFramework (source)/Reply Msg/Reply Msg.lvclass"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="!Basic MultiStand" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{F1901AA6-AC67-4ABB-97CA-55B5CF0E7D51}</Property>
				<Property Name="App_INI_GUID" Type="Str">{D03B26D4-93ED-431A-8618-E14FCE707B94}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/!Basic MultiStand.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{3D633582-CD6D-486C-863F-9FDC8FB239EB}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">!Basic MultiStand</Property>
				<Property Name="Bld_excludeDependentPPLs" Type="Bool">true</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/D/My Local Work Dir/Git Repo/UniStand/!Executables/NI_AB_PROJECTNAME</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{5280D15B-35BF-4D77-B0C5-64B0358296DD}</Property>
				<Property Name="Bld_version.build" Type="Int">98</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">!Basic MultiStand.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/D/My Local Work Dir/Git Repo/UniStand/!Executables/NI_AB_PROJECTNAME/!Basic MultiStand.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/D/My Local Work Dir/Git Repo/UniStand/!Executables/NI_AB_PROJECTNAME</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/UniStand.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{6FACC9A7-8EF9-4BAD-A8C1-F585A0DEBC5A}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/UniStand.lvclass/Launcher.vi</Property>
				<Property Name="Source[1].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[1].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[1].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/UniStand.lvclass/Override/Event Task Starting.vi</Property>
				<Property Name="Source[10].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[10].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[10].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[10].type" Type="Str">VI</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/UniStand.lvclass/Override/Event Task Returned Results.vi</Property>
				<Property Name="Source[11].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[11].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[11].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[11].type" Type="Str">VI</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/UniStand.lvclass/Override/Event Branch Starting.vi</Property>
				<Property Name="Source[12].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[12].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[12].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[12].type" Type="Str">VI</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/UniStand.lvclass/Override/Event Branch Finished.vi</Property>
				<Property Name="Source[13].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[13].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[13].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[13].type" Type="Str">VI</Property>
				<Property Name="Source[14].Container.applyProperties" Type="Bool">true</Property>
				<Property Name="Source[14].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[14].itemID" Type="Ref">/My Computer/UniStand.lvclass/Private</Property>
				<Property Name="Source[14].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[14].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[14].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[14].type" Type="Str">Container</Property>
				<Property Name="Source[15].Container.applyProperties" Type="Bool">true</Property>
				<Property Name="Source[15].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[15].itemID" Type="Ref">/My Computer/UniStand.lvclass/Override</Property>
				<Property Name="Source[15].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[15].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[15].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[15].type" Type="Str">Container</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/UniStand.lvclass</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/UniStand.lvclass/Override/Init.vi</Property>
				<Property Name="Source[3].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[3].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[3].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/UniStand.lvclass/Override/Deinit.vi</Property>
				<Property Name="Source[4].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[4].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[4].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/UniStand.lvclass/Override/Actor Core.vi</Property>
				<Property Name="Source[5].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[5].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[5].properties[1].type" Type="Str">Remove front panel</Property>
				<Property Name="Source[5].properties[1].value" Type="Bool">false</Property>
				<Property Name="Source[5].properties[2].type" Type="Str">Remove block diagram</Property>
				<Property Name="Source[5].properties[2].value" Type="Bool">true</Property>
				<Property Name="Source[5].propertiesCount" Type="Int">3</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/UniStand.lvclass/Override/Event Application Launched.vi</Property>
				<Property Name="Source[6].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[6].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[6].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[6].type" Type="Str">VI</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/UniStand.lvclass/Override/Event Application Closed.vi</Property>
				<Property Name="Source[7].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[7].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[7].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[7].type" Type="Str">VI</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/UniStand.lvclass/Override/Event Sequence Started.vi</Property>
				<Property Name="Source[8].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[8].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[8].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[8].type" Type="Str">VI</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/UniStand.lvclass/Override/Event Sequence Stopped.vi</Property>
				<Property Name="Source[9].properties[0].type" Type="Str">Allow debugging</Property>
				<Property Name="Source[9].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[9].propertiesCount" Type="Int">1</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">16</Property>
				<Property Name="TgtF_companyName" Type="Str">Uniscan Research</Property>
				<Property Name="TgtF_fastFileFormat" Type="Bool">true</Property>
				<Property Name="TgtF_fileDescription" Type="Str">!Basic MultiStand</Property>
				<Property Name="TgtF_internalName" Type="Str">!Basic MultiStand</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2018 Uniscan Research</Property>
				<Property Name="TgtF_productName" Type="Str">!Basic MultiStand</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{51A4236E-DC4B-40D9-8463-8E7BDE9BC30D}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">!Basic MultiStand.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
