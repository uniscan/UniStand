﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">ForestGreenFull</Property>
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">EndevoGOOP400</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">9868950</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">8992512</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">2</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Bin">&amp;Q#!!!!!!!A!4A$R!!!!!!!!!!%;2U244(:$&lt;'&amp;T=V^1&lt;(6H37Z5?8"F=SZD&gt;'Q!+U!7!!).2'6T;7&gt;O5'&amp;U&gt;'6S&lt;AN.:82I&lt;W25?8"F=Q!%6(FQ:1!!$%!Q`````Q**2!!!%%!Q`````Q&gt;7:8*T;7^O!!Z!-P````]%5'&amp;U;!!!$E!Q`````Q2/97VF!!!+1&amp;-%2'&amp;U91!!0!$R!!!!!!!!!!%62U244(:$&lt;'&amp;T=V^1&lt;(6H37YO9X2M!"Z!5!!'!!!!!1!#!!-!"!!&amp;"F"M&gt;7&gt;*&lt;A!!&amp;%"!!!(`````!!9(5'RV:UFO=Q!"!!=!!!!!!!!!!!</Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">G4SubTemplate_Simple_6x8x6</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str">1.0.0.9</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">Sequence.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../Sequence.lvlib</Property>
	<Property Name="NI.Lib.Description" Type="Str">Node child class. Represents a task node (can not have its own indent items in the sequence tree)</Property>
	<Property Name="NI.Lib.FriendGUID" Type="Str">1b9ad075-76dd-4be4-9383-fb2b95ec0437</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,$!!!*Q(C=T:3R&lt;BJ"%):`IEB*3ZX#%?W6U[:"YB6)H],4O5:+96+#]A4T#IDO3JI]Q,Q#EOM5V+YAXRU,ZL!R+3,&amp;=Z[\Y`^H:T`PLEZKY[0U8F_PR?WL=@#LKGJOZ;VZ\`J&amp;KP:[&gt;&lt;#\`L&amp;*6:XX0YYPIV]=XVL63?7*@Z(`7F3PRLE`(,\G$_NZ81]P_&lt;A.5K@CS3`O7=82@X,X&amp;?@_`*J@H`;PZS`Y^9(PIF`8Z8(2L^_K8Z`&amp;=LHMLP`]N-.]W5:X`_&lt;0X00^&lt;SNOFU`WM`-T\\D0@?,%@&gt;(PR.8DT8@AR_Y`B^34GO&gt;OFW314JI;;Z@IC:\IC:\IC2\IA2\IA2\IA?\IDO\IDO\IDG\IBG\IBG\IBNZ?[%)8ON":F74S:++E;&gt;)A+1:&amp;S3XB38A3HI3(HUJY%J[%*_&amp;*?#B2QJ0Q*$Q*4],$-#5]#5`#E`!E0,2K3&lt;+^U/&amp;*?'CPA#@A#8A#HI#(+28Q"!$":%(DI!E9#MTA*?!*?!)?8B8Q"$Q"4]!4]'!LY!FY!J[!*_"B3,MKU&gt;)U&amp;TI]N*($Y`!Y0![0QU.L/4Q/D]0D]$A]4#?(R_&amp;R)*Q*H?9AS"HE&amp;$A`("[(BY==(I@(Y8&amp;Y("[M&gt;I?]8:G'JLH1Y4&amp;Y$"[$R_!R?'ABA]@A-8A-(I/(ND*Y$"[$R_!R?*B+"I`"9`!9)-;E4#_DG4(1+$)%AY?`^L29OUP2EFB\3?8QKBR+F=/G=IB5$I@+JKNMJMIGK3S_SK+K,*&lt;+)KD]=SL1+D!KE[A-&lt;AKVY&lt;YG6_3#H*%4=ES/S!(:&lt;Y&lt;_Y],.:K0V?KX6;K8&amp;9K(:&lt;+&lt;*:+,R?+T2;+4"9+"_P[^$@/&lt;;2[`Z,D5PU^VO?W]`\[&gt;XX\@4&lt;\'&gt;XPUG0^UX_H&amp;=D^)0$^,.)Q/W:?H`+B_9Y&amp;%X0$`M@KH8,.U8H=1&lt;_$&lt;KH@&lt;0YRL^!9U)`;I!!!!!</Property>
	<Property Name="NI.Lib.LocalName" Type="Str">Task</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6*0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DEX.$!Z-D1],V:B&lt;$Y.#DQP64-S0AU+0&amp;5T-DY.#DR/97VF0E*B9WNH=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DAW/41W.D)],V:B&lt;$Y.#DQP64-S0AU+0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z';7RM)&amp;"B&gt;(2F=GY],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$!],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$-],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$9],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D%],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!5Y0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%T-4%Y.TQP6G&amp;M0AU+0#^6-T)_$1I]64-S0AU+0%ZB&lt;75_1G&amp;D;W&gt;S&lt;X6O:#"$&lt;WRP=DQP4G&amp;N:4Y.#DR797Q_.49W.T1R-4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$)],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$5],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0#^$&lt;(6T&gt;'6S0AU+0%ER.DY.#DR/97VF0F&gt;J:(2I0#^/97VF0AU+0&amp;:B&lt;$YT0#^797Q_$1I],UER.DY.#DR&amp;6TY.#DR/97VF0EVP:'5],UZB&lt;75_$1I]1WBP;7.F0E.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0E*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^S)%6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP26=_$1I]25Q_$1I]4G&amp;N:4Z4&gt;(FM:4QP4G&amp;N:4Y.#DR$;'^J9W5_5W^M;71],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1A2'^U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z';7RM)&amp;*V&lt;'5],UZB&lt;75_$1I]1WBP;7.F0E6W:7YA4W2E0#^$;'^J9W5_$1I]1WBP;7.F0F&gt;J&lt;G2J&lt;G=],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E6O:#"$98"T0#^/97VF0AU+0%.I&lt;WFD:4Z%:7:B&gt;7RU0#^$;'^J9W5_$1I]1WBP;7.F0E:M981],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DQP1WRV=X2F=DY.#A!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"&gt;&amp;5F.31QU+!!.-6E.$4%*76Q!!%P1!!!22!!!!)!!!%N1!!!!A!!!!!AZ4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!!!#A&amp;Q#!!!!Q!!!I!!1!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!-^NP/?'_P**J&gt;*2[#RD?;!!!!!-!!!!%!!!!!#[[/(1O&lt;WZ4L@QB2\Z"55@V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"!!!!!!!!!!!!!!!!!!!!!!!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!*A!!!#:YH'0A:'"K9,D!!-3-1-T5Q01$S0Y!YD-)=%"F'!$(^!LR!!!!!!"+!!!"'(C=9W$!"0_"!%AR-D!Q81$3,'DC9"L'JC&lt;!:3YOO[$CT&amp;!XME*%'9&amp;C4(O!$#;1(&amp;1.#^A\$%RHA0A%ODH-7-Q'!)!8+#-!!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!'(!!!#\(C=;W"E9-AUND#\!+3:A6C=I9%B/4]FF9M"S'?!A#N-$"3$!+BZ7GDCBA=/JQ'"(L^]#ZD@`);HWU6&amp;I,F'29+J6+4&lt;2U7EUU?&amp;J:.&amp;Z=7@````.R`B/&gt;TNE80=U1;ENJM$+(\=295$R!(3,#$[@W!'3"8-P!#A;2Q.&amp;=I-*3S'"[)/(W]Q9929$$-S#N6_I(+?&lt;J#.D4*!ZX;(K!BU4F1!M2CMG@CH(/$@&gt;G)(S!"_VY.!+VR!\O5!+B=Y_*#FOV%$+.%\%51#B8A[1TAEDLNQ[)A"_9QH/I(/[_3"_9)$\LYQE!%F1&amp;N-1$ZB!:E.6N0.&gt;NR"!_QP"R%)F1'B+C"5!9D;!1[R)RRRB_(BO@&lt;VP6WA=':$#G-()'Y!9F!=)W-^"E9'E)6-1,)7KN9'S';#CM(C#M4_!'6L)/E29539$^)$EDE$61&gt;C8Y+S'[$O!9H*!P6-A,*6A/Q%+&amp;M&lt;S$Y!:2M"W1*1NC5DG!&amp;GWU(:&amp;[$WYK+&gt;`6V=E&lt;Q04\]!_IJ\&amp;!!!!!!-&amp;Q#!%1!!"$%X,D!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Q8!)!2!!!%-4=O-!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$"=!A"%!!!1R.SYQ!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!"````_@D/,@H^N?PZ`9:H_@WXK`H^N'XZ````_!!!!!!!!!!!!!9!!!!@A!!!@_!!!@`Y!!0``!!$``Q!!``]!!0``!!$``Q!!``]!!0``!!$``Q!!``]!!0``!!$``_!!@``Y!"``Y!!(`]!!!@]!!!!]!!!!!!!!!!!!!!!#!.X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X;KKKKKKKKKKKKKKKKKKKNWKKKL-SKT+KMT+SKSKKKL&gt;KKKKL+L+L+SKKML+KKKKX;KKKKSKT-SKT+L-KKKKKNWKKKKMKMKMKKL+SMKKKKL&gt;KKKKL+L+L+T-KMKMKKKKX;KKKKKKKKKKKKKKKKKKKNX&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;T-T-T-T-T-T-T-T-T-T-X=T-T-T-T-SKT-T-T-T-T.X-T-T-T-SPL`L-T-T-T-T&gt;T-T-T-SPKKKP_MT-T-T-X=T-T-SPKKKKKK`[T-T-T.X-T-T0KKKKKKKKL`T-T-T&gt;T-T-SKKKKKKKKK`]T-T-X=T-T-L`KKKKKK``L-T-T.X-T-T+``_KKK```[T-T-T&gt;T-T-SP```[````_MT-T-X=T-T-L`````````L-T-T.X-T-T+`````````[T-T-T&gt;T-T-SP````````_MT-T-X=T-T-L`````````L-T-T.X-T-T+`````````[T-T-T&gt;T-T-T````````````]T-X=T-T-SK```````[````T.X-T-T-T+`````[````T-T&gt;T-T-T-T-L``[`````-T-X=T-T-T-T-SK`````-T-T.X-T-T-T-T-T-```-T-T-T&gt;T-T-T-T-T-T-T-T-T-T-X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;!!!%!&amp;:76F:76F:76F:76F:76F:76F:76F:76F:76F:76F:76G6F:76F:76F:76F:76F:76F:76F:76F:76F:76F:6:7:76F:76F+SML:75L+W6F:3ML+W5L:75L:76F:76F6F:F:76F:76F+W6F+W6F+W5L:76F:3NF+W6F:76F:7676G6F:76F:75L:75L+SML:75L+W6F+SNF:76F:76F:6:7:76F:76F:3NF:3NF:3NF:76F+W5L:3NF:76F:76F6F:F:76F:76F+W6F+W6F+W5L+SNF:3NF:3NF:76F:7676G6F:76F:76F:76F:76F:76F:76F:76F:76F:76F:6:76F:76F:76F:76F:76F:76F:76F:76F:76F:76F:76F9L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SN76CML+SML+SML+SML+SML15%L+SML+SML+SML+SML+V:7+SML+SML+SML+SML1&lt;/*M\."+SML+SML+SML+SML6F9L+SML+SML+SML1&lt;/*:76F:&lt;/T13ML+SML+SML+SN76CML+SML+SML1&lt;/*:76F:76F:77TMU%L+SML+SML+V:7+SML+SML+\/*:76F:76F:76F:76FM\-L+SML+SML6F9L+SML+SMLC9FF:76F:76F:76F:77TMSML+SML+SN76CML+SML+SO*M\/*:76F:76F:77TM\/*+SML+SML+V:7+SML+SML+YGTM\/TC76F:77TM\/TMYEL+SML+SML6F9L+SML+SMLC&lt;/TM\/TMYGTM\/TM\/TC3ML+SML+SN76CML+SML+SO*M\/TM\/TM\/TM\/TM\/*+SML+SML+V:7+SML+SML+YGTM\/TM\/TM\/TM\/TMYEL+SML+SML6F9L+SML+SMLC&lt;/TM\/TM\/TM\/TM\/TC3ML+SML+SN76CML+SML+SO*M\/TM\/TM\/TM\/TM\/*+SML+SML+V:7+SML+SML+YGTM\/TM\/TM\/TM\/TMYEL+SML+SML6F9L+SML+SMLM\/TM\/TM\/TM\/TM\/TM[SML#ML+SN76CML+SML+SMLC9GTM\/TM\/TM\/TMYGML+SML+QL+V:7+SML+SML+SML+YGTM\/TM\/TMYGML+SML+QL+SML6F9L+SML+SML+SML+SO*M\/TMW7ML+SML+SM+SML+SN76CML+SML+SML+SML+SMLC77ML+SML+SM+SML+SML+V:7+SML+SML+SML+SML+SML+SOML+SM+SML+SML+SML6F9L+SML+SML+SML+SML+SML+SML+SML+SML+SML+SN76F:76F:76F:76F:76F:76F:76F:76F:76F:76F:76F9!!!!-!!&amp;'5%B1!!!!!!!$!!!$F!!!"\NYH+V63WM452D^\G2;*T(&amp;/\5_AKW:B-GU6+/BIN;K@&gt;C*5+GCVN&lt;(2G-4(V#-.+GY5B&gt;$I9O#5/B#%&amp;RV*SY+&gt;N.&amp;E?"G.FJ"]!(2@[")M7!HYX=HG=QEV3BC&amp;J?&lt;Y4PH/`=\:_Y!&lt;&amp;CAW\E#X$/!U"8=$"LA3_I%)"=6I024*I%/ER^!GA,%A&amp;ZBG(\A#K4:!(^36Y39/A6@M.J]9L&lt;#'T*+0W.J01UAG=_!45F^GXB#TF0Z7&lt;-]67?TCN"#JUG"OS!(PQP47BI&lt;AN&lt;'6D&amp;+#E$5%-^LY&gt;/*7SF.:E_^53&amp;A58I.I+L?-#\H7Z%27\_Q+,F&gt;*-Q&gt;M3E"+&gt;NA@H\?!9F&amp;E',*[%1-#1/1@&gt;SO'JB'67`*S0H&gt;&amp;M:H9&lt;$00LO0'CT--/U-6!X&gt;L/I5I9C\7TISQ^IYI`?8O"M'.-HZ9U*!_*R]8^&gt;Q2FM!!C2X74#@GX=98DT&amp;8,"K'^%)73?2,E+(]/_1!?W;TFU".G'TA0X[9-:SAL?&gt;/-+=/'YZY67H+$B7T0T"#J80B6P[RS9SW&gt;3YF,YGD9YF-BHJ^PD./YFM3EIGMIHV*BV6^="".A$7T-I(&lt;!%??G$!0@!UT-X.Y1RQ&gt;;$&gt;#.UGZ]MY%;H\Y72Z[%FH?+SL-\Q?(*[[?OY['W#EC[O-\;!4WQ[-L5CL:68%&gt;P``D_U"4./^KNB#(.JBJ%9%$R:"4GSR(O!QR'NA/B&amp;TXRV&lt;R-12-`,HW"Z;&amp;VO'L9LN\/RM&gt;7S\SL(F#3H'6HNFLJFL,,RLZD@IAEOO]*Z'/T3^NZZ[7&amp;K`9J=&gt;W)5:M-%WY.RPU^J5WQ'J(FBCE_'TK7N6*HQS9$CJ]Y%//?_LS+50&amp;/CT4WG+VCG8FZ@RF,D;P"RI)8D!=ZL-HHKD7D93[\R&lt;IP9&lt;-)+R\:&lt;TH-5=MQ24=Q7;=22J7&amp;R=2$:=R3#?EK-"5P")O7$R4(\Q;Z*6%G)L$:&gt;M'B8#D(Z^+8-AR.&lt;+5G`158-?V;!L(J?;H;CG`&gt;`680V\.6=LV'QUY!+KQ;FP+JKZ:,^W#PB1T&lt;PX(V(.R/14_3'R".7R,IRI.6&lt;M;/XXOP:\80OIM``_WN57YXTR&amp;T?*+]_`PUGG-=_2CR#Z"/LK78`R0P%IEZQCA2)SF4#7?E_Z8E(BQ]O*]O&gt;3'**6MV9"D7/8_Q:M5@M&lt;F_A!?SW%!3&amp;/(_0VB$@66O'RM**\7`[AZA&lt;,3/U6NZ&amp;\+D]+#NN`!IP"+J=!!!!%!!!!+A!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!)X!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!"$&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!!H!!!!!A!*1!=!!V*F:A!71&amp;!!!1!!$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!!!1!"!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!#E8!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!%!!!!!!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$);&lt;$C!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!-BJM/)!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!!1R=!A!!!!!!"!!A!-0````]!!1!!!!!!*Q!!!!)!#5!(!!.3:79!&amp;E"1!!%!!!R598.L,GRW9WRB=X-!!!%!!1!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'2=!A!!!!!!"!!5!!Q!!!1!!!!!!"!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%!!!!T&amp;Q#!!!!!!!)!#5!(!!.3:79!&amp;E"1!!%!!!R598.L,GRW9WRB=X-!!!%!!1!!!!!!!!!!!!!!!!1!!Q!,!!!!"!!!!)U!!!!I!!!!!A!!"!!!!!!8!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!1!!!!!!!!!!!!!!!!!!!)!-EAE!!!!!!!.-!F66B5!!4A!*!!!!!A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+OK+D[LKCI_K[IK0KOK+DY!!!!!!!.-!A!!!$]!!!!!!!!!!!!!!0A!!!&amp;_?*S.DT&amp;/QU!12:_^)8;#%R))&gt;%B&lt;56$1=!&amp;,3+GDA*179[_2R1J$P)YI/2,(Y"S=!'\!:"V%19/_&gt;P&lt;0H^E`M]!*AT2#,5X*CK`X[OU$5/.L]^S;R^R=W)WN\J+&lt;L(E1GNOM;7+@Z-ZSHCY)Y04+NIUT;VW8WH@IJX7VS:T22?9S^A1-+3$Y**2%\6)O[8843XN0H,[K?OE[Q&gt;1F%X&amp;8^)G)66O5]D##R)]=+P&gt;3C-F#`!&lt;M=`&lt;025)E^,&gt;"OG.O:9=?=\(2D$G1]B94U8Y1\P"8_;V-0&lt;I@"2Q+S\VP)!&gt;'=CM:G!A\9C&lt;+D'0B)`A'/NIZ?1!!!'5!!1!#!!-!"!!!!%A!$Q)!!!!!$Q$9!.5!!!"2!!]#!!!!!!]!W!$6!!!!7A!0!A!!!!!0!.A!V1!!!'/!!)1!A!!!$Q$9!.5)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-!!!!&amp;*45E-.#A!$4&amp;:$1UR#6F=!!",U!!!%51!!!#!!!",5!!!!!!!!!!!!!!!A!!!!.!!!"%A!!!!&gt;4%F#4A!!!!!!!!&amp;M4&amp;:45A!!!!!!!!'!5F242Q!!!!!!!!'51U.46!!!!!!!!!'I4%FW;1!!!!!!!!']1U^/5!!!!!!!!!(16%UY-!!!!!!!!!(E2%:%5Q!!!!!!!!(Y4%FE=Q!!!!!!!!)-6EF$2!!!!!!!!!)A&gt;G6S=Q!!!!1!!!)U5U.45A!!!!!!!!+92U.15A!!!!!!!!+M35.04A!!!!!!!!,!;7.M.!!!!!!!!!,5;7.M/!!!!!!!!!,I4%FG=!!!!!!!!!,]2F")9A!!!!!!!!-12F"421!!!!!!!!-E6F"%5!!!!!!!!!-Y4%FC:!!!!!!!!!.-1E2)9A!!!!!!!!.A1E2421!!!!!!!!.U6EF55Q!!!!!!!!/)2&amp;2)5!!!!!!!!!/=466*2!!!!!!!!!/Q3%F46!!!!!!!!!0%5&amp;*5)!!!!!!!!!096E.55!!!!!!!!!0M2F2"1A!!!!!!!!1!!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!E!!!!!!!!!!$`````!!!!!!!!!-A!!!!!!!!!!0````]!!!!!!!!!X!!!!!!!!!!!`````Q!!!!!!!!$E!!!!!!!!!!$`````!!!!!!!!!2!!!!!!!!!!!0````]!!!!!!!!"'!!!!!!!!!!!`````Q!!!!!!!!&amp;%!!!!!!!!!!$`````!!!!!!!!!:1!!!!!!!!!!0````]!!!!!!!!"J!!!!!!!!!!%`````Q!!!!!!!!-Q!!!!!!!!!!@`````!!!!!!!!!U!!!!!!!!!!#0````]!!!!!!!!$5!!!!!!!!!!*`````Q!!!!!!!!.A!!!!!!!!!!L`````!!!!!!!!!X!!!!!!!!!!!0````]!!!!!!!!$A!!!!!!!!!!!`````Q!!!!!!!!/9!!!!!!!!!!$`````!!!!!!!!![Q!!!!!!!!!!0````]!!!!!!!!%-!!!!!!!!!!!`````Q!!!!!!!!9U!!!!!!!!!!$`````!!!!!!!!#DA!!!!!!!!!!0````]!!!!!!!!+3!!!!!!!!!!!`````Q!!!!!!!!XA!!!!!!!!!!$`````!!!!!!!!$?A!!!!!!!!!!0````]!!!!!!!!.]!!!!!!!!!!!`````Q!!!!!!!!Y!!!!!!!!!!!$`````!!!!!!!!$GA!!!!!!!!!!0````]!!!!!!!!/=!!!!!!!!!!!`````Q!!!!!!!"#M!!!!!!!!!!$`````!!!!!!!!%,1!!!!!!!!!!0````]!!!!!!!!1P!!!!!!!!!!!`````Q!!!!!!!"$I!!!!!!!!!!$`````!!!!!!!!%7Q!!!!!!!!!A0````]!!!!!!!!3;!!!!!!)6'&amp;T;SZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"'!!!!!AZ4:8&amp;V:7ZD:3ZM&gt;GRJ9AR/&lt;W2F,GRW9WRB=X.16%AQ!!!!(A!"!!1!!!J/&lt;W2F8W.M98.T$%ZP:'5O&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="NI_IconEditor" Type="Str">48 57 48 48 56 48 50 52 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 13 41 1 100 1 100 80 84 72 48 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 12 185 0 0 0 0 0 0 0 0 0 0 12 158 0 40 0 0 12 152 0 0 12 0 0 0 0 0 0 32 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 7 86 73 32 73 99 111 110 100 1 0 2 0 0 0 4 66 97 115 101 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 1 9 1 1

</Property>
	<Item Name="Friends List" Type="Friends List">
		<Item Name="Sequence.lvlib:Sequence.lvclass" Type="Friended Library" URL="../../Sequence_class/Sequence.lvclass"/>
	</Item>
	<Item Name="Task.ctl" Type="Class Private Data" URL="Task.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		<Item Name="ObjectAttributes.ctl" Type="VI" URL="../protected/ObjectAttributes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"E!!!!!A!21!I!#E&amp;U&gt;(*J9H6U:4%!!%M!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!31&amp;!!!1!!"%2B&gt;'%!!!%!!1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="Task Core Data.ctl" Type="VI" URL="../protected/Task Core Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="Task Run-Time Data.ctl" Type="VI" URL="../protected/Task Run-Time Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="Task_New.vi" Type="VI" URL="../protected/Task_New.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&lt;,!!!!-!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````],47^E&gt;7RF)%ZB&lt;75!(E!Q`````R2.&lt;W2V&lt;'5A6'BS:7&amp;E)&amp;.V:G:J?!!!(E!Q`````R2$&lt;'&amp;T=S"2&gt;7&amp;M;7:J:71A4G&amp;N:1!!$%!B"V.I&lt;X=A2F!!&amp;U!(!""3:82V=GYA2'6M98EM)'VT!!!71#%22'6M98EA&lt;WYA4G]N28*S&lt;X)!&amp;%!B$E2F&lt;'&amp;Z)'^O)%6S=G^S!!!=1&amp;-75W&amp;W:71A1W^O=X2S&gt;7.U&lt;X)A2'&amp;U91!!)E"4(6.B&gt;G6E)&amp;6T:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!#:!5S"498:F:#"$=G6B&gt;'6E)&amp;.F=86F&lt;G.F)&amp;:B=GFB9GRF=Q!!&amp;E!Q`````QR%:7:B&gt;7RU)%ZB&lt;75!!"*!-0````]*68.F=C"/97VF!#*!5!!#!!]!%"&gt;4:8&amp;V:7ZD:3"798*J97*M:3""&lt;'FB=Q!M1%!!!@````]!%2Z6=W6E)&amp;.F=86F&lt;G.F)&amp;:B=GFB9GRF)%&amp;M;7&amp;T:8-!!#Z!1!!"`````Q!2)5.S:7&amp;U:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'5A17RJ98.F=Q"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!11$$`````"F.P&gt;8*D:1!!&amp;%!Q`````QN%:8.U;7ZB&gt;'FP&lt;A!E1&amp;!!!Q!5!"5!&amp;B&gt;4:8&amp;V:7ZD:3"798*J97*M:3""&lt;'FB=Q!S1%!!!@````]!&amp;S6%982B)&amp;*F=86F=X2T)#B3:82V=GYN&gt;']N1W^O=X2S&gt;7.U&lt;X)J!'5!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=R*598.L)%.P=G5A2'&amp;U93ZD&gt;'Q!,E"1!!U!"1!'!!=!#!!*!!I!#Q!-!!U!$A!3!"-!'!F$&lt;X*F)%2B&gt;'%!3%"Q!"Y!!$%717.U&lt;X)A2H*B&lt;76X&lt;X*L,GRW&lt;'FC="B.:8.T97&gt;F)%6O=86F&gt;76S,GRW9WRB=X-!$62B=WMA27ZR&gt;76V:8)!$%!B"EBB=S"'5!!!(E"4'%2F:G&amp;V&lt;(1A1W^O=X2S&gt;7.U&lt;X)A2'&amp;U91!!'%"4%U2F:G&amp;V&lt;(1A5G6U&gt;8*O)%2B&gt;'%!)E"4(%2F:G&amp;V&lt;(1A68.F:#"-&lt;W.B&lt;#"798*J97*M:8-!!#2!5R^%:7:B&gt;7RU)&amp;6T:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!#B!5S*%:7:B&gt;7RU)%.S:7&amp;U:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!!!=1&amp;-817.U&gt;7&amp;M)%.P&lt;H.U=H6D&gt;'^S)%2B&gt;'%!(%"4&amp;E&amp;D&gt;(6B&lt;#"-&lt;W.B&lt;#"798*J97*M:8-!!"Z!5RF"9X2V97QA5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!"*!5QV3:82V=GZF:#"%982B!"R!5!!$!!!!!1!#$F*F&gt;(6S&lt;G6E)%6S=G^S!!"L!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-76'&amp;T;S"3&gt;7YN6'FN:3"%982B,G.U&lt;!!Q1&amp;!!$!!;!"M!(!!&gt;!"Y!(Q!A!#%!)A!D!#1!*1V3&gt;7YN6'FN:3"%982B!%U!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!51&amp;!!!A!:!#9%2'&amp;U91!!&amp;5"Q!#!!!1!H!!F%6F*3:7:0&gt;81!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!VS:7:F=G6O9W5A&lt;X6U!":!)2"/:8&gt;09GJF9X2$=G6B&gt;'6E!!"&gt;1"9!!BJ$=G6B&gt;'5A&lt;X)A&lt;'^P;S"V=#"F?'FT&gt;'FO:R"-&lt;W^L)(6Q)'6Y;8.U;7ZH!#B$=G6B&gt;'6.:82I&lt;W1I1X*F982F)'^S)'RP&lt;WMA&gt;8!A:8BJ=X2J&lt;G=J!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!-0````],4W*K:7.U)'ZB&lt;75!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!I!#E!"!!K!!1!+Q!M!!1!,1!O!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!*!!!!!!!!!!A!!!!+!!!!!!!!!AA!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!#]!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">276832272</Property>
		</Item>
		<Item Name="Task_GetAttributes.vi" Type="VI" URL="../protected/Task_GetAttributes.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!:#!!!!,1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;%!Q`````QN.&lt;W2V&lt;'5A4G&amp;N:1!?1$$`````&amp;%VP:(6M:3"5;(*F971A5X6G:GFY!!!?1$$`````&amp;%.M98.T)&amp;&amp;V97RJ:GFF:#"/97VF!!!-1#%(5WBP&gt;S"'5!!81!=!%&amp;*F&gt;(6S&lt;C"%:7RB?3QA&lt;8-!!":!)2&amp;%:7RB?3"P&lt;C"/&lt;SV&amp;=H*P=A!51#%/2'6M98EA&lt;WYA28*S&lt;X)!!"R!5R:498:F:#"$&lt;WZT&gt;(*V9X2P=C"%982B!!!C1&amp;-&gt;5W&amp;W:71A68.F:#"4:8&amp;V:7ZD:3"798*J97*M:8-!*E"4)&amp;.B&gt;G6E)%.S:7&amp;U:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!!!71$$`````$%2F:G&amp;V&lt;(1A4G&amp;N:1!!%E!Q`````QF6=W6S)%ZB&lt;75!)E"1!!)!$A!0&amp;V.F=86F&lt;G.F)&amp;:B=GFB9GRF)%&amp;M;7&amp;T!#R!1!!"`````Q!1(F6T:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'5A17RJ98.F=Q!!,E"!!!(`````!"!B1X*F982F:#"4:8&amp;V:7ZD:3"798*J97*M:3""&lt;'FB=W6T!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!""!-0````]'5W^V=G.F!!!51$$`````#U2F=X2J&lt;G&amp;U;7^O!#2!5!!$!"-!&amp;!!6&amp;V.F=86F&lt;G.F)&amp;:B=GFB9GRF)%&amp;M;7&amp;T!$*!1!!"`````Q!7*52B&gt;'%A5G6R&gt;76T&gt;(-A+&amp;*F&gt;(6S&lt;CVU&lt;SV$&lt;WZT&gt;(*V9X2P=CE!:1$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T%F2B=WMA1W^S:3"%982B,G.U&lt;!!O1&amp;!!$1!%!!5!"A!(!!A!#1!+!!M!$!!.!"%!%A!8#5.P=G5A2'&amp;U91")1(!!(A!!-2:"9X2P=C"'=G&amp;N:8&gt;P=GMO&lt;(:M;7*Q'%VF=X.B:W5A27ZR&gt;76V:8)O&lt;(:D&lt;'&amp;T=Q!.6'&amp;T;S"&amp;&lt;H&amp;V:86F=A!-1#%'3'&amp;T)%:1!!!?1&amp;-92'6G986M&gt;#"$&lt;WZT&gt;(*V9X2P=C"%982B!!!91&amp;-42'6G986M&gt;#"3:82V=GYA2'&amp;U91!C1&amp;-=2'6G986M&gt;#"6=W6E)%RP9W&amp;M)&amp;:B=GFB9GRF=Q!!*%"4(U2F:G&amp;V&lt;(1A68.F:#"4:8&amp;V:7ZD:3"798*J97*M:8-!+%"4)E2F:G&amp;V&lt;(1A1X*F982F:#"4:8&amp;V:7ZD:3"798*J97*M:8-!!"R!5R&gt;"9X2V97QA1W^O=X2S&gt;7.U&lt;X)A2'&amp;U91!=1&amp;-717.U&gt;7&amp;M)%RP9W&amp;M)&amp;:B=GFB9GRF=Q!!(E"4'5&amp;D&gt;(6B&lt;#"4:8&amp;V:7ZD:3"798*J97*M:8-!%E"4$6*F&gt;(6S&lt;G6E)%2B&gt;'%!(%"1!!-!!!!"!!)/5G6U&gt;8*O:71A28*S&lt;X)!!'M!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=R:598.L)&amp;*V&lt;CV5;7VF)%2B&gt;'%O9X2M!$"!5!!-!"E!'A!&lt;!"Q!(1!?!"]!)!!B!#)!)Q!E$6*V&lt;CV5;7VF)%2B&gt;'%!41$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"2!5!!#!"A!*12%982B!!!41(!!)!!"!#9!"U2B&gt;'&amp;3:79!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!#=!+!!J!#A!+!!I!#A!+A!I!#A!+Q-!!(A!!!U)!!!*!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!M!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">276832272</Property>
		</Item>
	</Item>
	<Item Name="properties" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="ActualConstructorData (swap)" Type="Folder">
			<Item Name="WriteActualConstructorData.vi" Type="VI" URL="../properties/WriteActualConstructorData.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Q!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!5RV"9X2V97QA1W^O=X2S&gt;7.U&lt;X)A2'&amp;U93!I&lt;WRE+1!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$8*F:G6S:7ZD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1&amp;-817.U&gt;7&amp;M)%.P&lt;H.U=H6D&gt;'^S)%2B&gt;'%!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!")!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
		</Item>
		<Item Name="ActualLocalVariables (swap)" Type="Folder">
			<Item Name="WriteActualLocalVariables.vi" Type="VI" URL="../properties/WriteActualLocalVariables.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Q!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!5RR"9X2V97QA4'^D97QA6G&amp;S;7&amp;C&lt;'6T)#BP&lt;'1J!!!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$8*F:G6S:7ZD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1&amp;-717.U&gt;7&amp;M)%RP9W&amp;M)&amp;:B=GFB9GRF=Q!!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!")!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
		</Item>
		<Item Name="ActualSequenceVariables (swap)" Type="Folder">
			<Item Name="WriteActualSequenceVariables.vi" Type="VI" URL="../properties/WriteActualSequenceVariables.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;U!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!5R^"9X2V97QA5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T)#BP&lt;'1J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"Z!5RF"9X2V97QA5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!3!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
		</Item>
		<Item Name="ReturnedData (swap)" Type="Folder">
			<Item Name="WriteReturnedData.vi" Type="VI" URL="../properties/WriteReturnedData.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;=!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!5R.3:82V=GZF:#"%982B)#BP&lt;'1J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!5QV3:82V=GZF:#"%982B!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!3!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
			</Item>
		</Item>
		<Item Name="ModuleName" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">ModuleName</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ModuleName</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadModuleName.vi" Type="VI" URL="../properties/ReadModuleName.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````],47^E&gt;7RF)%ZB&lt;75!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!VS:7:F=G6O9W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8400912</Property>
			</Item>
			<Item Name="WriteModuleName.vi" Type="VI" URL="../properties/WriteModuleName.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!-0````],47^E&gt;7RF)%ZB&lt;75!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!B!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8400912</Property>
			</Item>
		</Item>
		<Item Name="ModuleThreadSuffix" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">ModuleThreadSuffix</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ModuleThreadSuffix</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadModuleThreadSuffix.vi" Type="VI" URL="../properties/ReadModuleThreadSuffix.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"Z!-0````]547^E&gt;7RF)&amp;2I=G6B:#"4&gt;7:G;8A!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
			<Item Name="WriteModuleThreadSuffix.vi" Type="VI" URL="../properties/WriteModuleThreadSuffix.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"Z!-0````]547^E&gt;7RF)&amp;2I=G6B:#"4&gt;7:G;8A!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!)1!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
		</Item>
		<Item Name="ClassQualifiedName" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">ClassQualifiedName</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ClassQualifiedName</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadClassQualifiedName.vi" Type="VI" URL="../properties/ReadClassQualifiedName.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"Z!-0````]51WRB=X-A586B&lt;'FG;76E)%ZB&lt;75!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">269230096</Property>
			</Item>
			<Item Name="WriteClassQualifiedName.vi" Type="VI" URL="../properties/WriteClassQualifiedName.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"Z!-0````]51WRB=X-A586B&lt;'FG;76E)%ZB&lt;75!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!)1!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8400912</Property>
			</Item>
		</Item>
		<Item Name="ShowFP" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">ShowFP</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ShowFP</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadShowFP.vi" Type="VI" URL="../properties/ReadShowFP.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%_!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!)1&gt;4;'^X)%:1!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8400912</Property>
			</Item>
			<Item Name="WriteShowFP.vi" Type="VI" URL="../properties/WriteShowFP.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%_!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!R!)1&gt;4;'^X)%:1!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8400912</Property>
			</Item>
		</Item>
		<Item Name="ReturnDelay,Ms" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">ReturnDelay,Ms</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ReturnDelay,Ms</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadReturnDelay,Ms.vi" Type="VI" URL="../properties/ReadReturnDelay,Ms.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&gt;!"Q!15G6U&gt;8*O)%2F&lt;'&amp;Z,#"N=Q!!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!VS:7:F=G6O9W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8400912</Property>
			</Item>
			<Item Name="WriteReturnDelay,Ms.vi" Type="VI" URL="../properties/WriteReturnDelay,Ms.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"&gt;!"Q!15G6U&gt;8*O)%2F&lt;'&amp;Z,#"N=Q!!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8400912</Property>
			</Item>
		</Item>
		<Item Name="DelayOnNo-Error" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">DelayOnNo-Error</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">DelayOnNo-Error</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadDelayOnNo-Error.vi" Type="VI" URL="../properties/ReadDelayOnNo-Error.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!)2&amp;%:7RB?3"P&lt;C"/&lt;SV&amp;=H*P=A!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$8*F:G6S:7ZD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8400912</Property>
			</Item>
			<Item Name="WriteDelayOnNo-Error.vi" Type="VI" URL="../properties/WriteDelayOnNo-Error.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!)2&amp;%:7RB?3"P&lt;C"/&lt;SV&amp;=H*P=A!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8400912</Property>
			</Item>
		</Item>
		<Item Name="DelayOnError" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">DelayOnError</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">DelayOnError</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadDelayOnError.vi" Type="VI" URL="../properties/ReadDelayOnError.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!)1Z%:7RB?3"P&lt;C"&amp;=H*P=A!!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!VS:7:F=G6O9W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8400912</Property>
			</Item>
			<Item Name="WriteDelayOnError.vi" Type="VI" URL="../properties/WriteDelayOnError.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!)1Z%:7RB?3"P&lt;C"&amp;=H*P=A!!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8400912</Property>
			</Item>
		</Item>
		<Item Name="SavedConstructorData" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">SavedConstructorData</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">SavedConstructorData</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadSavedConstructorData.vi" Type="VI" URL="../properties/ReadSavedConstructorData.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"R!5R:498:F:#"$&lt;WZT&gt;(*V9X2P=C"%982B!!!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$8*F:G6S:7ZD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
			<Item Name="WriteSavedConstructorData.vi" Type="VI" URL="../properties/WriteSavedConstructorData.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"R!5R:498:F:#"$&lt;WZT&gt;(*V9X2P=C"%982B!!!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
		</Item>
		<Item Name="SavedUsedSequenceVariables" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">SavedUsedSequenceVariables</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">SavedUsedSequenceVariables</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadSavedUsedSequenceVariables.vi" Type="VI" URL="../properties/ReadSavedUsedSequenceVariables.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!5RV498:F:#"6=W6E)&amp;.F=86F&lt;G.F)&amp;:B=GFB9GRF=Q!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$8*F:G6S:7ZD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
			<Item Name="WriteSavedUsedSequenceVariables.vi" Type="VI" URL="../properties/WriteSavedUsedSequenceVariables.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#*!5RV498:F:#"6=W6E)&amp;.F=86F&lt;G.F)&amp;:B=GFB9GRF=Q!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
		</Item>
		<Item Name="SavedCreatedSequenceVariables" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">SavedCreatedSequenceVariables</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">SavedCreatedSequenceVariables</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadSavedCreatedSequenceVariables.vi" Type="VI" URL="../properties/ReadSavedCreatedSequenceVariables.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;9!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#:!5S"498:F:#"$=G6B&gt;'6E)&amp;.F=86F&lt;G.F)&amp;:B=GFB9GRF=Q!!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!VS:7:F=G6O9W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1343102992</Property>
			</Item>
			<Item Name="WriteSavedCreatedSequenceVariables.vi" Type="VI" URL="../properties/WriteSavedCreatedSequenceVariables.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;9!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#:!5S"498:F:#"$=G6B&gt;'6E)&amp;.F=86F&lt;G.F)&amp;:B=GFB9GRF=Q!!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1343102992</Property>
			</Item>
		</Item>
		<Item Name="UsedSequenceVariableAliases" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">UsedSequenceVariableAliases</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">UsedSequenceVariableAliases</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadUsedSequenceVariableAliases.vi" Type="VI" URL="../properties/ReadUsedSequenceVariableAliases.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'I!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````]-2'6G986M&gt;#"/97VF!!!31$$`````#66T:8)A4G&amp;N:1!C1&amp;!!!A!&amp;!!985W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'5A17RJ98-!,%"!!!(`````!!=?68.F:#"4:8&amp;V:7ZD:3"798*J97*M:3""&lt;'FB=W6T!!!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$8*F:G6S:7ZD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!A!#1!%!!1!"!!%!!I!"!!%!!M$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820560</Property>
			</Item>
			<Item Name="WriteUsedSequenceVariableAliases.vi" Type="VI" URL="../properties/WriteUsedSequenceVariableAliases.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'I!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-2'6G986M&gt;#"/97VF!!!31$$`````#66T:8)A4G&amp;N:1!C1&amp;!!!A!(!!A85W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'5A17RJ98-!,%"!!!(`````!!E?68.F:#"4:8&amp;V:7ZD:3"798*J97*M:3""&lt;'FB=W6T!!!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!+!!M$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!#%!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
		</Item>
		<Item Name="CreatedSequenceVariableAliases" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">CreatedSequenceVariableAliases</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">CreatedSequenceVariableAliases</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadCreatedSequenceVariableAliases.vi" Type="VI" URL="../properties/ReadCreatedSequenceVariableAliases.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'K!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````]-2'6G986M&gt;#"/97VF!!!31$$`````#66T:8)A4G&amp;N:1!C1&amp;!!!A!&amp;!!985W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'5A17RJ98-!,E"!!!(`````!!=B1X*F982F:#"4:8&amp;V:7ZD:3"798*J97*M:3""&lt;'FB=W6T!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
			<Item Name="WriteCreatedSequenceVariableAliases.vi" Type="VI" URL="../properties/WriteCreatedSequenceVariableAliases.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'K!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-2'6G986M&gt;#"/97VF!!!31$$`````#66T:8)A4G&amp;N:1!C1&amp;!!!A!(!!A85W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'5A17RJ98-!,E"!!!(`````!!EB1X*F982F:#"4:8&amp;V:7ZD:3"798*J97*M:3""&lt;'FB=W6T!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!)1!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
		</Item>
		<Item Name="DataRequests(Return-to-Constructor)" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">DataRequests(Return-to-Constructor)</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">DataRequests(Return-to-Constructor)</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadDataRequests(Return-to-Constructor).vi" Type="VI" URL="../properties/ReadDataRequests(Return-to-Constructor).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(R!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!""!-0````]'5W^V=G.F!!!51$$`````#U2F=X2J&lt;G&amp;U;7^O!#2!5!!$!!5!"A!(&amp;V.F=86F&lt;G.F)&amp;:B=GFB9GRF)%&amp;M;7&amp;T!$*!1!!"`````Q!)*52B&gt;'%A5G6R&gt;76T&gt;(-A+&amp;*F&gt;(6S&lt;CVU&lt;SV$&lt;WZT&gt;(*V9X2P=CE!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!VS:7:F=G6O9W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!*!!I!"!!%!!1!"!!,!!1!"!!-!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!U!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
			<Item Name="WriteDataRequests(Return-to-Constructor).vi" Type="VI" URL="../properties/WriteDataRequests(Return-to-Constructor).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(R!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!""!-0````]'5W^V=G.F!!!51$$`````#U2F=X2J&lt;G&amp;U;7^O!#2!5!!$!!=!#!!*&amp;V.F=86F&lt;G.F)&amp;:B=GFB9GRF)%&amp;M;7&amp;T!$*!1!!"`````Q!+*52B&gt;'%A5G6R&gt;76T&gt;(-A+&amp;*F&gt;(6S&lt;CVU&lt;SV$&lt;WZT&gt;(*V9X2P=CE!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!#Q!-!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!B!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!U!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
		</Item>
		<Item Name="TaskEnqueuer" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">TaskEnqueuer</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">TaskEnqueuer</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadTaskEnqueuer.vi" Type="VI" URL="../properties/ReadTaskEnqueuer.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;[!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%B!=!!?!!!R&amp;E&amp;D&gt;'^S)%:S97VF&gt;W^S;SZM&gt;GRJ9H!9476T=W&amp;H:3"&amp;&lt;H&amp;V:86F=CZM&gt;G.M98.T!!V598.L)%6O=86F&gt;76S!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
			<Item Name="WriteTaskEnqueuer.vi" Type="VI" URL="../properties/WriteTaskEnqueuer.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;[!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%B!=!!?!!!R&amp;E&amp;D&gt;'^S)%:S97VF&gt;W^S;SZM&gt;GRJ9H!9476T=W&amp;H:3"&amp;&lt;H&amp;V:86F=CZM&gt;G.M98.T!!V598.L)%6O=86F&gt;76S!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
		</Item>
		<Item Name="HasFP" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">HasFP</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">HasFP</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadHasFP.vi" Type="VI" URL="../properties/ReadHasFP.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%_!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!)1:)98-A2F!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
			<Item Name="WriteHasFP.vi" Type="VI" URL="../properties/WriteHasFP.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%_!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!R!)1:)98-A2F!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
		</Item>
		<Item Name="DefaultConstructorData" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">DefaultConstructorData</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">DefaultConstructorData</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadDefaultConstructorData.vi" Type="VI" URL="../properties/ReadDefaultConstructorData.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"Z!5RB%:7:B&gt;7RU)%.P&lt;H.U=H6D&gt;'^S)%2B&gt;'%!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
			<Item Name="WriteDefaultConstructorData.vi" Type="VI" URL="../properties/WriteDefaultConstructorData.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"Z!5RB%:7:B&gt;7RU)%.P&lt;H.U=H6D&gt;'^S)%2B&gt;'%!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
		</Item>
		<Item Name="DefaultReturnData" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">DefaultReturnData</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">DefaultReturnData</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadDefaultReturnData.vi" Type="VI" URL="../properties/ReadDefaultReturnData.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;+!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!5R.%:7:B&gt;7RU)&amp;*F&gt;(6S&lt;C"%982B!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
			<Item Name="WriteDefaultReturnData.vi" Type="VI" URL="../properties/WriteDefaultReturnData.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;+!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"B!5R.%:7:B&gt;7RU)&amp;*F&gt;(6S&lt;C"%982B!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1116209680</Property>
			</Item>
		</Item>
		<Item Name="DefaultUsedLocalVariables" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">DefaultUsedLocalVariables</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">DefaultUsedLocalVariables</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadDefaultUsedLocalVariables.vi" Type="VI" URL="../properties/ReadDefaultUsedLocalVariables.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!5RR%:7:B&gt;7RU)&amp;6T:71A4'^D97QA6G&amp;S;7&amp;C&lt;'6T!!!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$8*F:G6S:7ZD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
			<Item Name="WriteDefaultUsedLocalVariables.vi" Type="VI" URL="../properties/WriteDefaultUsedLocalVariables.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#*!5RR%:7:B&gt;7RU)&amp;6T:71A4'^D97QA6G&amp;S;7&amp;C&lt;'6T!!!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1116209680</Property>
			</Item>
		</Item>
		<Item Name="DefaultUsedSequenceVariables" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">DefaultUsedSequenceVariables</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">DefaultUsedSequenceVariables</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadDefaultUsedSequenceVariables.vi" Type="VI" URL="../properties/ReadDefaultUsedSequenceVariables.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;7!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!5R^%:7:B&gt;7RU)&amp;6T:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
			<Item Name="WriteDefaultUsedSequenceVariables.vi" Type="VI" URL="../properties/WriteDefaultUsedSequenceVariables.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;7!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#2!5R^%:7:B&gt;7RU)&amp;6T:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
		</Item>
		<Item Name="DefaultCreatedSequenceVariables" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">DefaultCreatedSequenceVariables</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">DefaultCreatedSequenceVariables</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadDefaultCreatedSequenceVariables.vi" Type="VI" URL="../properties/ReadDefaultCreatedSequenceVariables.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;3!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5RJ$=G6B&gt;'6E)&amp;.F=86F&lt;G.F)&amp;:B=GFB9GRF=Q!!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!VS:7:F=G6O9W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
			<Item Name="WriteDefaultCreatedSequenceVariables.vi" Type="VI" URL="../properties/WriteDefaultCreatedSequenceVariables.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;3!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#"!5RJ$=G6B&gt;'6E)&amp;.F=86F&lt;G.F)&amp;:B=GFB9GRF=Q!!.%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
		</Item>
		<Item Name="ActualConstructorData" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">ActualConstructorData</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ActualConstructorData</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadActualConstructorData.vi" Type="VI" URL="../properties/ReadActualConstructorData.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"R!5R&gt;"9X2V97QA1W^O=X2S&gt;7.U&lt;X)A2'&amp;U91!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$8*F:G6S:7ZD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
		</Item>
		<Item Name="ActualLocalVariables" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">ActualLocalVariables</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ActualLocalVariables</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadActualLocalVariables.vi" Type="VI" URL="../properties/ReadActualLocalVariables.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"R!5R:"9X2V97QA4'^D97QA6G&amp;S;7&amp;C&lt;'6T!!!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$8*F:G6S:7ZD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
		</Item>
		<Item Name="ActualSequenceVariables" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">ActualSequenceVariables</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ActualSequenceVariables</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadActualSequenceVariables.vi" Type="VI" URL="../properties/ReadActualSequenceVariables.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"Z!5RF"9X2V97QA5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342840848</Property>
			</Item>
		</Item>
		<Item Name="ReturnedData" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">ReturnedData</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ReturnedData</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadReturnedData.vi" Type="VI" URL="../properties/ReadReturnedData.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!5QV3:82V=GZF:#"%982B!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">269230096</Property>
			</Item>
		</Item>
		<Item Name="ReturnedError" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">ReturnedError</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ReturnedError</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="ReadReturnedError.vi" Type="VI" URL="../properties/ReadReturnedError.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"R!5!!$!!!!!1!#$F*F&gt;(6S&lt;G6E)%6S=G^S!!!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$8*F:G6S:7ZD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">269230096</Property>
			</Item>
			<Item Name="WriteReturnedError.vi" Type="VI" URL="../properties/WriteReturnedError.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"R!5!!$!!!!!1!#$F*F&gt;(6S&lt;G6E)%6S=G^S!!!U1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">269230096</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Task_Create.vi" Type="VI" URL="../Task_Create.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%S!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!!A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
	</Item>
	<Item Name="Destroy.vi" Type="VI" URL="../Destroy.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">44564496</Property>
	</Item>
	<Item Name="Save.vi" Type="VI" URL="../Save.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%V!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"J!-0````]15W6S;7&amp;M;8JF)&amp;.U=GFO:Q!!-%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-6'&amp;T;SZM&gt;G.M98.T!!B598.L)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!O1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!"V2B=WMA;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!*!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
	</Item>
	<Item Name="Load.vi" Type="VI" URL="../Load.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*J!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!41$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!:1!=!$F"B=G6O&gt;#"/&lt;W2F)%F%!!"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!Q1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR598.L,GRW9WRB=X-!#&amp;2B=WMA&lt;X6U!!!C1$$`````'&amp;*F=X1A&lt;W9A5W6S;7&amp;M;8JF)&amp;.U=GFO:Q!!"!!!!#*!-0````]:1WBJ&lt;'2S:7YA5W6S;7&amp;M;8JF)&amp;.U=GFO:Q"?!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T%E:P=GVB&gt;#"7:8*T;7^O,G.U&lt;!!D1"5!!1:W-3YQ,D!!!!Z'&lt;X*N981A6G6S=WFP&lt;A!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!;1$$`````%&amp;.F=GFB&lt;'F[:3"4&gt;(*J&lt;G=!!#Z!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$&amp;2B=WMO&lt;(:D&lt;'&amp;T=Q!(6'&amp;T;S"J&lt;A"5!0!!$!!$!!1!"1!'!!=!#!!*!!I!#Q!)!!Q!$1-!!(A!!!U)!!!*!!!!#1!!!)U,!!!.#A!!!!!!!!E!!!!1!!!!#A!!!!!!!!%+!!!!EA!!!!!"!!Y!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342713872</Property>
	</Item>
</LVClass>
