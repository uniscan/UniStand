﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">ForestGreenFull</Property>
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">EndevoGOOP400</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">9868950</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">1985280</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">2</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Bin">&amp;Q#!!!!!!!A!4A$R!!!!!!!!!!%;2U244(:$&lt;'&amp;T=V^1&lt;(6H37Z5?8"F=SZD&gt;'Q!+U!7!!).2'6T;7&gt;O5'&amp;U&gt;'6S&lt;AN.:82I&lt;W25?8"F=Q!%6(FQ:1!!$%!Q`````Q**2!!!%%!Q`````Q&gt;7:8*T;7^O!!Z!-P````]%5'&amp;U;!!!$E!Q`````Q2/97VF!!!+1&amp;-%2'&amp;U91!!0!$R!!!!!!!!!!%62U244(:$&lt;'&amp;T=V^1&lt;(6H37YO9X2M!"Z!5!!'!!!!!1!#!!-!"!!&amp;"F"M&gt;7&gt;*&lt;A!!&amp;%"!!!(`````!!9(5'RV:UFO=Q!"!!=!!!!!!!!!!!</Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">G4BaseTemplate_Singleton_6x8x6</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str">1.0.0.9</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">Sequence.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../Sequence.lvlib</Property>
	<Property Name="NI.Lib.Description" Type="Str">Class that represents the whole sequence and memory and provide various API methods for operating the sequence (including saving and loading from disk etc.)</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+T!!!*Q(C=V:1RDB."%%7`%&gt;+3_A1AJQYL*@16T"%K)X:I1E@E&gt;1!#,(-##YG):)L!"\#U*X#'N*'(.\W^O\/W&gt;Z9!#?BWT9T`\[Z[U^U;K&lt;28UEO^?[\N"FP0HU[HX95IPV.`7C`4&lt;MQ&amp;`W&amp;K'8(O6_P*`'83QZ!,]S`S0^?GA_X5&lt;ZIBP_F+.E`ZT2V5=]FP_ND.G8`[9M._/_CX\:$@NI._/_SX`Y7`WWUWG]@LX`.XR3YD_PN8`:Z\OP_X+490^NHZ??S?_\3??^&amp;`V*Y^XHQ(0L2`O5EDK&lt;OX&lt;2*"/'(KL$&lt;2%TX2%TX2%TX1!TX1!TX1!^X2(&gt;X2(&gt;X2(&gt;X1$&gt;X1$&gt;X1$&lt;VU&gt;+%,8?CM3F)]+:1E42)EAU&amp;2=EFY%J[%*_(BLR+?B#@B38A3(I9IY5FY%J[%*_&amp;BGB+?B#@B38A3(F)6ECQ&gt;(:[%B`1+?!+?A#@A#8AIK9!H!!C+"9G$*'!I-)/(A#@A#8BY6-!4]!1]!5`!A[W!*_!*?!+?A)=J:67CU(1&gt;(2\3S/&amp;R?"Q?B]@B)&lt;5=(I@(Y8&amp;Y("\+S?&amp;R?"Q)J[#4(!1ZEZQ"TB_(R_(B*I@(Y8&amp;Y("[("[PME*?6[7C[DA[0Q70Q'$Q'D]&amp;$#BE]"I`"9`!90+36Q70Q'$Q'D]&amp;$+2E]"I`"9Y!923EP)ZERU2BE#!90PX*;L/R3&amp;")L8;K(6`61KBYWV5/E?DB5.VVV-V5X388R62&gt;6&gt;&lt;&amp;5&amp;U(VZ63B67&amp;5C[B/\A&lt;KQ(60&lt;)EVM3)7R*S9%2.CX%X^QQ-0BY0W_\WWW[X7[\67KZ57CY8G]\FGMZEGEYH'Y\(OWBP[&lt;2NVX[8O9&gt;GWR_8X^]@FVU`(Z9`0R_60HD^_/8&lt;[`&lt;Q21[_OJ&gt;=X4$D7J@_NO+&lt;!D6ZTPWK`;&gt;1NX6PVWD`Q&lt;&gt;1,X&gt;\PV_A8Q;O-MA!!!!!</Property>
	<Property Name="NI.Lib.LocalName" Type="Str">Sequence</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6*0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DEX.$!Z-D1],V:B&lt;$Y.#DQP64-S0AU+0&amp;5T-DY.#DR/97VF0E*B9WNH=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DAW/41W.D)],V:B&lt;$Y.#DQP64-S0AU+0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z';7RM)&amp;"B&gt;(2F=GY],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$!],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$-],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$9],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D%],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!5Y0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%T-4%Y.TQP6G&amp;M0AU+0#^6-T)_$1I]64-S0AU+0%ZB&lt;75_1G&amp;D;W&gt;S&lt;X6O:#"$&lt;WRP=DQP4G&amp;N:4Y.#DR797Q_.49W.T1R-4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$)],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$5],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0#^$&lt;(6T&gt;'6S0AU+0%ER.DY.#DR/97VF0F&gt;J:(2I0#^/97VF0AU+0&amp;:B&lt;$YT0#^797Q_$1I],UER.DY.#DR&amp;6TY.#DR/97VF0EVP:'5],UZB&lt;75_$1I]1WBP;7.F0E.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0E*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^S)%6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP26=_$1I]25Q_$1I]4G&amp;N:4Z4&gt;(FM:4QP4G&amp;N:4Y.#DR$;'^J9W5_5W^M;71],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1A2'^U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z';7RM)&amp;*V&lt;'5],UZB&lt;75_$1I]1WBP;7.F0E6W:7YA4W2E0#^$;'^J9W5_$1I]1WBP;7.F0F&gt;J&lt;G2J&lt;G=],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E6O:#"$98"T0#^/97VF0AU+0%.I&lt;WFD:4Z%:7:B&gt;7RU0#^$;'^J9W5_$1I]1WBP;7.F0E:M981],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DQP1WRV=X2F=DY.#A!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"&gt;:5F.31QU+!!.-6E.$4%*76Q!!%Q1!!!26!!!!)!!!%O1!!!!E!!!!!AZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!!!I"=!A!!!-!!!+!!%!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!"/0/Y%R^?F1I.,O&lt;W[$*V$!!!!%!!!!"!!!!!!.VUSQ;)F?5/*[0@4CQQ%L&gt;1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!*Q!"4&amp;:$1Q!!!!%!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!Q!!!!!#!!%!!!!!!#9!!!!G?*RDY'2A;G#YQ!$%D%$-V-$U!]D_!/)T#("!:2A!R`1+]1!!!!!!3A!!!2BYH'.AQ!4`A1")-4)Q-&amp;U!UCRIYG!;RK9GQ'5O,LOAYMR1.\*#2"G"9ER\A!QGE"R5$1P9/QR-:Y$Y",IZT&amp;D-"A#!&amp;SAD!!!!!!!-!!&amp;73524!!!!!!!$!!!"BQ!!!ORYH'NA:'$).,9QOQ#EG9&amp;9H+'")4E`*:7,!=BHA)!L4!Q5AQ#I?6JIYI9($K="A2[`@!O9X`S'J^N&amp;2;#Z2E7#K63EWU&gt;&amp;J..(B;742?8&amp;H````T=@Y4H=\:&amp;TX.%'J,;&lt;!SB_X%7&amp;!]1"UCQA_H^A"EA6T,Q!I'E=$28+$#5MBA?C$B^P-''%7!QT-AL6@K"SHG[1D9US1/&gt;WB[A)&gt;%Z5!,%9L*HYJRTAXX:C"]A!@N?$1#N=1/\F!#I8/0C1J&lt;N2!SD2/R&amp;%!I6Y/E-Y*)[\=/C)!@G-*TK"TOPEA@G#!_[_-*!"*5"&lt;4%!_91':$6&lt;4T8&lt;=110M,Q=2#*5"I3IA6!')WA%/M3-==9@BY&lt;HW^&lt;V&gt;I("G1QJD"S"O!'*1(#.D01:'"J#&amp;4%#S&amp;KL7"MBGAIL"YAL%`A"F;S$J%7&amp;%G!`3!Z)Z!V5(9F_#MBOA\A'*S1,V4)#S69$M"#B&lt;']A_!'5&lt;!&gt;E#5,9F)ZA":NN"W2?A^O+CH@V&gt;8*']$U_`!0K+?R1!!!!!$"=!A"%!!!1R.SYQ!!!!!!Q8!)!!!!!%-4=O-!!!!!!-&amp;Q#!%1!!"$%X,D!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Q8!)!2!!!%-4=O-!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!@````H`'&lt;@Z`P;8_@]WJ`H`V&lt;@Z`DKX_@````A!!!!!!!!!!!!'!!!!(Y!!!(`A!!(`_!!$``Q!!``]!!0``!!$``Q!!``]!!0``!!$``Q!!``]!!0``!!$``Q!!```A!(``_!!@`_!!"``!!!(`!!!!0!!!!!!!!!!!!!!!!A$&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X````````````````````&gt;``````T-`]T`T`T`````X@`````0``T`T]T]`````^X``````-`]`]`0T0`````&gt;```````]`0T`T`T`````X@`````-T``0T]`]`````^X````````````````````&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X=T-T-T-T-T-T-T-T-T-T.X-T-T-T-T-`]T-T-T-T-T&gt;T-T-T-T-````T-T-T-T-X=T-T-T-```````-T-T-T.X-T-T-`````````]T-T-T&gt;T-T-T``````````]T-T-X=T-T-```````````-T-T.X-T-T0``````````T-T-T&gt;T-T-T``````````]T-T-X=T-T-```````````-T-T.X-T-T0``````````T-T-T&gt;T-T-T``````````]T-T-X=T-T-```````````-T-T.X-T-T0``````````T-T-T&gt;T-T-T``````````]T-T-X=T-T-`````````````-T.X-T-T-`````````````]T&gt;T-T-T-T``````````]T-X=T-T-T-T0````````T-T.X-T-T-T-T-``````T-T-T&gt;T-T-T-T-T-T0``T-T-T-X=T-T-T-T-T-T-T-T-T-T.X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X1!!"!"76F:76F:76F:76F:76F:76F:76F:76F:76F:76F:76F&lt;2U&gt;(2U&gt;(2U&gt;(2U&gt;(2U&gt;(2U&gt;(2U&gt;(2U&gt;(2U&gt;(2U&gt;&amp;76N(2U&gt;(2U&gt;(2U3ML+^(2+SP2U3P2U3P2U&gt;(2U&gt;(2U6:7U&gt;(2U&gt;(2U&gt;%LU&gt;(2U3P2U3P2+SP2+^(2U&gt;(2U&gt;(26F&lt;2U&gt;(2U&gt;(2U&gt;%L+^(2+^(2+^%LU3MLU&gt;(2U&gt;(2U&gt;&amp;76N(2U&gt;(2U&gt;(2U&gt;(2+^%LU3P2U3P2U3P2U&gt;(2U&gt;(2U6:7U&gt;(2U&gt;(2U&gt;%L+SP2U&gt;%LU3P2+^(2+^(2U&gt;(2U&gt;(26F&lt;2U&gt;(2U&gt;(2U&gt;(2U&gt;(2U&gt;(2U&gt;(2U&gt;(2U&gt;(2U&gt;(2U&gt;&amp;76F:76F:76F:76F:76F:76F:76F:76F:76F:76F:76F:7+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML6F9L+SML+SML+SML+SML+]P,+SML+SML+SML+SML+SN76CML+SML+SML+SML+]P2U&gt;(2SSML+SML+SML+SML+V:7+SML+SML+SML+]P2U&gt;(2U&gt;(2U=ML+SML+SML+SML6F9L+SML+SML+]P2U&gt;(2U&gt;(2U&gt;(2U&gt;(,+SML+SML+SN76CML+SML+SP2U&gt;(2U&gt;(2U&gt;(2U&gt;(2U&gt;(2+SML+SML+V:7+SML+SML+^(2U&gt;(2U&gt;(2U&gt;(2U&gt;(2`^%L+SML+SML6F9L+SML+SMLU&gt;(2U&gt;(2U&gt;(2U&gt;(2````U3ML+SML+SN76CML+SML+SP2U&gt;(2U&gt;(2U&gt;(2```````2+SML+SML+V:7+SML+SML+^(2U&gt;(2U&gt;(2U@```````^%L+SML+SML6F9L+SML+SMLU&gt;(2U&gt;(2U&gt;(`````````U3ML+SML+SN76CML+SML+SP2U&gt;(2U&gt;(2U@`````````2+SML+SML+V:7+SML+SML+^(2U&gt;(2U&gt;(2`````````^%L+SML+SML6F9L+SML+SMLU&gt;(2U&gt;(2U&gt;(`````````U3ML+SML+SN76CML+SML+SP2U&gt;(2U&gt;(2U@`````````2+SML+SML+V:7+SML+SML+^(2U&gt;(2U&gt;(2````````U&gt;'ML+QL+SML6F9L+SML+SML+^(2U&gt;(2U&gt;(`````U&gt;(2L+SML+SM+SN76CML+SML+SML+SP2U&gt;(2U@``U&gt;(2L+SML+SM+SML+V:7+SML+SML+SML+SMLU&gt;(2U&gt;(2L+SML+SML#ML+SML6F9L+SML+SML+SML+SML+^(2L+SML+SML#ML+SML+SN76CML+SML+SML+SML+SML+SMLL+SML#ML+SML+SML+V:7+SML+SML+SML+SML+SML+SML+SML+SML+SML+SML6F:76F:76F:76F:76F:76F:76F:76F:76F:76F:76F:7!!!!$!!"2F")5!!!!!!!!Q!!!Z1!!!?\?*SN65NL%V%9`?ZE7C=RR4OV0I+NG94*N&amp;3DI;,7KHX9C6#JIN&lt;7RU:D%R^1D$3JO&amp;)81[',AF$I1B"=&gt;3=O#H&lt;42:(A:D:;1@!"U8_A3,&amp;A*_.X*ZH-*.5I9B;8G_%\ZTPX/W@O!'R9I.OZ!NQTA.!6X!Q;Y%PK"#!8&amp;;$U5S;"$J-@1*I#R)"?9:B_Y!KEW1"`5F?%G$I&amp;8\$;@'+WQBMS3D^D;4U.)*H0A%V*@:NY1MZ4_6GT0&amp;6HMYL11K&gt;*A&lt;MA"\],UVI;'Y,7RF9R3AJ!V"$0;_(4C6MJ47:0P6%B9&amp;&amp;[$;#KXD!OZVO2%6O`M#CZ834-(&lt;%J!3H&lt;9(Z_XA'*2:"CS?B%$!E$E(X=LBK92F6PS=DZX2&lt;':W'QTT[\DRIMT$$N$&amp;1.X;TK&amp;+')OVM[-M0;/+0XF\A&lt;"D4*_7.#10C=@&amp;`8=%:&lt;!!)E&gt;VEQHZNX'&amp;Y]R6SQ;BP2#&amp;EHE3Z#B`$PE!(NGMZ&gt;!4:BMY$^_G$'=I+XH4D#H$BO/?&amp;6JSAY6MT]Q1K6TY6&lt;_M=G-NH5O*3_*IW/*4):[@&lt;YT4O*&lt;%J+*L+*^39&gt;6@8!149!VMT+"WQ"(HJAQ$XQ.-T.T?%-=(7AX1D&gt;*O@,/"'J__&amp;E??B*:XCMKT/](BS?OHLO/BNAJ)OLD/WA%^M/D+V)KW66R(&lt;``Y`N!5T4P;L91BT;9;2'"!]715ZMM2\A--2L9$I2=^]&gt;7]4%%40SZ^A?7B&gt;&lt;BKW+\?TM&lt;(6MO]KRZ1EJRF:\:;[:;SS];_9X[)*,LP#?2DMUP&lt;???FB;PW+8(&gt;C&amp;'&lt;$".O$=&lt;^0;6.M"K2Z99J0BM[FL63:]-G!YK@/"$DHPK]CF$R4IMU^JCN9JFZ?8]:3YWLQ=;#&amp;YQ(/;T*Z[IVIW%OO]7[,W'T##M?W7]ZT&amp;(,-%5X-&amp;GH%5;6B=8%1W8-5AHJ+D!6,Q3,FA]5R_]'O362*C+QW8&lt;"I6QIR_@3FT)-47SF*PU&amp;&amp;T(N7A+R[8GJWIJPX@V6T^?T68+^2M./!#KM'J&lt;SK;O73`&gt;ALY5-W\^R^2T=4E%`EBM146M3[-;$67\'DN^\LW?VT\K,0``NL6&amp;O.]]2=XC3P0P\^*JD(0E9M1O14K[FF`]4\R+*/=)I%3-J5QFHJ0O6Z"Y=0,C@,H5BC36&lt;.7!9VDF`M'&lt;&amp;(\'Z@I!(MNB!%B4B`D^91XV6&lt;BM&lt;#3?VP_I/9'SUDN&amp;&lt;?2?SI`#AL&lt;@Q+,Q3K8!!!!"!!!!#I!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!#1Q!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!2R=!A!!!!!!"!!A!-0````]!!1!!!!!!+Q!!!!)!#5!(!!.3:79!'E"1!!%!!""4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!!"!!%!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!+2=!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!1!!!!!!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!-BJM.M!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!S'GQWQ!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!"(&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!!L!!!!!A!*1!=!!V*F:A!;1&amp;!!!1!!%&amp;.F=86F&lt;G.F,GRW9WRB=X-!!!%!!1!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'2=!A!!!!!!"!!5!!Q!!!1!!!!!!"!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%!!!!X&amp;Q#!!!!!!!)!#5!(!!.3:79!'E"1!!%!!""4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!!"!!%!!!!!!!!!!!!!!!!%!!-!#Q!!!!1!!!#.!!!!+!!!!!)!!!1!!!!!'A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!%!!!!!!!!!!!!!!!!!!!#!$*)*!!!!!!!$4!*6696!!%Y!#1!!!!)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#LKCI_K[IK0KOK+D[LKCI_!!!!!!!$4!)!!!!`!!!!!!!!!!!!!!$V!!!"BHC=D9^.4M-Q%)7`R+6*3VI#&amp;(:)8L&amp;AQ99,2%,KOCI(A*$9+**&amp;I(%KFBS*9X!(,A%X9*K5(YE.?P*YZMXYT4.QT#C,5%NDO?&lt;DN8JZ!^4USDSWZLYQZW\NKNPU6VGYP'G3&lt;[,QDL.M11!HF[ZNP&amp;HJWOJO3D_MKH8OD3ZTH\-D9%Q*Q4OB&amp;'J&lt;=M'A&gt;W(&gt;(8(WL/KF\QF47V*26QS*C&amp;6&lt;7HE91&gt;+N(#P`6)L)1P2'\(,[4S-B%I;&lt;).-R._*BQ&amp;RE.&amp;0WJ,V"+NQ8QCX_-D_&gt;`1\^DQ)/*#M[X5!/4/27MD#2\*#:-$//**`!*R&amp;$0-Y!!!!!!!"F!!%!!A!$!!1!!!")!!]#!!!!!!]!W!$6!!!!51!0!A!!!!!0!.A!V1!!!&amp;I!$Q)!!!!!$Q$9!.5!!!"DA!#%!)!!!!]!W!$6#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4!!!!"35V*$$1I!!UR71U.-1F:8!!!4"!!!"&amp;5!!!!A!!!3Z!!!!!!!!!!!!!!!)!!!!$1!!!2)!!!!(5R*1EY!!!!!!!!"&lt;%R75V)!!!!!!!!"A&amp;*55U=!!!!!!!!"F%.$5V1!!!!!!!!"K%R*&gt;GE!!!!!!!!"P%.04F!!!!!!!!!"U&amp;2./$!!!!!!!!!"Z%2'2&amp;-!!!!!!!!"_%R*:(-!!!!!!!!#$&amp;:*1U1!!!!!!!!#)(:F=H-!!!!%!!!#.&amp;.$5V)!!!!!!!!#G%&gt;$5&amp;)!!!!!!!!#L%F$4UY!!!!!!!!#Q'FD&lt;$1!!!!!!!!#V'FD&lt;$A!!!!!!!!#[%R*:H!!!!!!!!!#`%:13')!!!!!!!!$%%:15U5!!!!!!!!$*&amp;:12&amp;!!!!!!!!!$/%R*9G1!!!!!!!!$4%*%3')!!!!!!!!$9%*%5U5!!!!!!!!$&gt;&amp;:*6&amp;-!!!!!!!!$C%253&amp;!!!!!!!!!$H%V6351!!!!!!!!$M%B*5V1!!!!!!!!$R&amp;"36#!!!!!!!!!$W&amp;:$6&amp;!!!!!!!!!$\%:515)!!!!!!!!%!!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!`````Q!!!!!!!!$-!!!!!!!!!!$`````!!!!!!!!!/!!!!!!!!!!!0````]!!!!!!!!![!!!!!!!!!!!`````Q!!!!!!!!%5!!!!!!!!!!$`````!!!!!!!!!2Q!!!!!!!!!!0````]!!!!!!!!"3!!!!!!!!!!!`````Q!!!!!!!!'9!!!!!!!!!!$`````!!!!!!!!!;A!!!!!!!!!"0````]!!!!!!!!$.!!!!!!!!!!(`````Q!!!!!!!!.%!!!!!!!!!!D`````!!!!!!!!!V1!!!!!!!!!#@````]!!!!!!!!$:!!!!!!!!!!+`````Q!!!!!!!!.U!!!!!!!!!!$`````!!!!!!!!!Y1!!!!!!!!!!0````]!!!!!!!!$H!!!!!!!!!!!`````Q!!!!!!!!/Q!!!!!!!!!!$`````!!!!!!!!"$1!!!!!!!!!!0````]!!!!!!!!'/!!!!!!!!!!!`````Q!!!!!!!!I]!!!!!!!!!!$`````!!!!!!!!#EQ!!!!!!!!!!0````]!!!!!!!!.Z!!!!!!!!!!!`````Q!!!!!!!!XM!!!!!!!!!!$`````!!!!!!!!$@1!!!!!!!!!!0````]!!!!!!!!/"!!!!!!!!!!!`````Q!!!!!!!!ZM!!!!!!!!!!$`````!!!!!!!!$H1!!!!!!!!!!0````]!!!!!!!!1P!!!!!!!!!!!`````Q!!!!!!!"$%!!!!!!!!!!$`````!!!!!!!!%-Q!!!!!!!!!!0````]!!!!!!!!1_!!!!!!!!!!!`````Q!!!!!!!"&amp;]!!!!!!!!!)$`````!!!!!!!!%HA!!!!!$&amp;.F=86F&lt;G.F,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="NI_IconEditor" Type="Str">48 57 48 48 56 48 50 52 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 13 41 1 100 1 100 80 84 72 48 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 12 185 0 0 0 0 0 0 0 0 0 0 12 158 0 40 0 0 12 152 0 0 12 0 0 0 0 0 0 32 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 102 153 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 7 86 73 32 73 99 111 110 100 1 0 2 0 0 0 4 66 97 115 101 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 1 9 1 1

</Property>
	<Item Name="Sequence.ctl" Type="Class Private Data" URL="Sequence.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="ObjectAttributes.ctl" Type="VI" URL="../private/ObjectAttributes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"I!!!!!A!21!I!#E&amp;U&gt;(*J9H6U:4%!!%]!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!%E"1!!%!!!2%982B!!!"!!%!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="Sequence Core Data.ctl" Type="VI" URL="../private/Sequence Core Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="Sequence Run-Time Data.ctl" Type="VI" URL="../private/Sequence Run-Time Data.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="GetInstance.vi" Type="VI" URL="../private/GetInstance.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!4F!!!!)A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;E!B%%ZF&gt;U^C;G6D&gt;%.S:7&amp;U:71!!&amp;Y!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-32G^S&lt;7&amp;U)&amp;:F=H.J&lt;WYO9X2M!#.!&amp;1!""H9R,D!O-!!!$E:P=GVB&gt;#"7:8*T;7^O!!!11$$`````"V:F=H.J&lt;WY!%E"5!!9+6'FN:3"4&gt;'&amp;N=!!!%%!Q`````Q&gt;$;'&amp;O:W6T!"2!5!!$!!9!"Q!)"E.I97ZH:1!!&amp;E"!!!(`````!!E*1WBB&lt;G&gt;F&lt;'^H!"R!5R&gt;-&lt;W.B&lt;#"798*J97*M:8-A5X2P=G&amp;H:1!51&amp;!!!Q!&amp;!!I!#Q:):7&amp;E:8)!!#R!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$%ZP:'5O&lt;(:D&lt;'&amp;T=Q!%37ZJ&gt;!!!,%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-4G^E:3ZM&gt;G.M98.T!!2#&lt;W2Z!!!O1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR/&lt;W2F,GRW9WRB=X-!"E2F;7ZJ&gt;!!!'%"1!!-!$1!/!!],27ZU=HEA4G^E:8-!(%!S`````R*4:8&amp;V:7ZD:3"';7RF)&amp;"B&gt;'A!!&amp;E!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-75W6R&gt;76O9W5A1W^S:3"%982B,G.U&lt;!!;1&amp;!!!Q!-!"!!%1F$&lt;X*F)%2B&gt;'%!%E"4$%ZP:'6T)%RP&lt;WNV=!!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"URB=X1A351!%%!B#E6Y:7.V&gt;'FO:T]!!":!5R"&amp;?'6D&gt;82J&lt;WYA4'^P;X6Q!!!=1&amp;-84'&amp;V&lt;G.I:71A47^E&gt;7RF=S"-&lt;W^L&gt;8!!'E"4&amp;5RP9W&amp;M)&amp;:B=GFB9GRF)&amp;:B&lt;(6F=Q"H!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T'F.F=86F&lt;G.F)&amp;*V&lt;CV5;7VF)%2B&gt;'%O9X2M!#2!5!!'!"-!&amp;!!6!"9!&amp;Q!9$6*V&lt;CV5;7VF)%2B&gt;'%!51$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!51&amp;!!!A!3!"E%2'&amp;U91!!&amp;5"Q!#!!!1!;!!F%6F*3:7:0&gt;81!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!%!!!!'%!Q`````Q^09GJF9X22&gt;76V:5ZB&lt;75!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!&lt;!"Q!(1!?!"U!(1!@!"U!(1!A!Q!!?!!!$1A!!!E!!!!*!!!!"1M!!!!!!!!"!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!#!!!.!!!!$!!!!!!!!!!!!!!"!#%!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="New.vi" Type="VI" URL="../private/New.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!67!!!!*!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;E!B%%ZF&gt;U^C;G6D&gt;%.S:7&amp;U:71!!&amp;Y!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-32G^S&lt;7&amp;U)&amp;:F=H.J&lt;WYO9X2M!#.!&amp;1!""H9R,D!O-!!!$E:P=GVB&gt;#"7:8*T;7^O!!!11$$`````"V:F=H.J&lt;WY!%E"5!!9+6'FN:3"4&gt;'&amp;N=!!!%%!Q`````Q&gt;$;'&amp;O:W6T!"2!5!!$!!9!"Q!)"E.I97ZH:1!!&amp;E"!!!(`````!!E*1WBB&lt;G&gt;F&lt;'^H!"R!5R&gt;-&lt;W.B&lt;#"798*J97*M:8-A5X2P=G&amp;H:1!51&amp;!!!Q!&amp;!!I!#Q:):7&amp;E:8)!!#R!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$%ZP:'5O&lt;(:D&lt;'&amp;T=Q!%37ZJ&gt;!!!,%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-4G^E:3ZM&gt;G.M98.T!!2#&lt;W2Z!!!O1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR/&lt;W2F,GRW9WRB=X-!"E2F;7ZJ&gt;!!!'%"1!!-!$1!/!!],27ZU=HEA4G^E:8-!(%!S`````R*4:8&amp;V:7ZD:3"';7RF)&amp;"B&gt;'A!!&amp;E!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-75W6R&gt;76O9W5A1W^S:3"%982B,G.U&lt;!!;1&amp;!!!Q!-!"!!%1F$&lt;X*F)%2B&gt;'%!%E"4$%ZP:'6T)%RP&lt;WNV=!!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"URB=X1A351!%%!B#E6Y:7.V&gt;'FO:T]!!":!5R"&amp;?'6D&gt;82J&lt;WYA4'^P;X6Q!!!=1&amp;-84'&amp;V&lt;G.I:71A47^E&gt;7RF=S"-&lt;W^L&gt;8!!'E"4&amp;5RP9W&amp;M)&amp;:B=GFB9GRF)&amp;:B&lt;(6F=Q"H!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T'F.F=86F&lt;G.F)&amp;*V&lt;CV5;7VF)%2B&gt;'%O9X2M!#2!5!!'!"-!&amp;!!6!"9!&amp;Q!9$6*V&lt;CV5;7VF)%2B&gt;'%!51$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!51&amp;!!!A!3!"E%2'&amp;U91!!&amp;5"Q!#!!!1!;!!F%6F*3:7:0&gt;81!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!%!!!!'%!Q`````Q^09GJF9X22&gt;76V:5ZB&lt;75!85!7!!);1X*F982F)'^S)'RP&lt;WMA&gt;8!A:8BJ=X2J&lt;G=14'^P;S"V=#"F?'FT&gt;'FO:Q!I1X*F982F476U;'^E+%.S:7&amp;U:3"P=C"M&lt;W^L)(6Q)'6Y;8.U;7ZH+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!51$$`````#U^C;G6D&gt;#"O97VF!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!"M!(!!&gt;!"Y!(1!@!#!!(1!B!#)$!!"Y!!!.#!!!#1!!!!E!!!!.#Q!!!!!!!!%!!!!!!!!!#!!!!!I!!!!!!!!##!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!)Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="NodesLookup Columns.ctl" Type="VI" URL="../private/NodesLookup Columns.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="Next Free ID.vi" Type="VI" URL="../private/Next Free ID.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%%!!!!"1!%!!!!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$%ZF?(1A2H*F:3"*2!!!%E"4$%ZP:'6T)%RP&lt;WNV=!!!2Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!41!=!#&amp;.U98*U)%F%!!"5!0!!$!!!!!!!!!!"!!!!!!!!!!)!!!!!!!!!!Q-!!(A!!!!!!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!!%!!!!!!"!!1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
		</Item>
		<Item Name="NodesLookup Set Node.vi" Type="VI" URL="../private/NodesLookup Set Node.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'D!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!":!5R"/&lt;W2F=S"-&lt;W^L&gt;8!A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$%ZP:'5O&lt;(:D&lt;'&amp;T=Q!%4G^E:1!!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$6.U98*U)%ZP:'5A351!&amp;%"4$UZP:'6T)%RP&lt;WNV=#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!)!!E!#A-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!A!!!!1!!!!%A!!!!!"!!M!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">663568</Property>
		</Item>
		<Item Name="NodesLookup Get Node.vi" Type="VI" URL="../private/NodesLookup Get Node.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'P!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!6Q$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QV/&lt;W2F)&amp;2Z='5O9X2M!#&amp;!&amp;1!#"&amp;2B=WM'1H*B&lt;G.I!!F/&lt;W2F)&amp;2Z='5!,%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-4G^E:3ZM&gt;G.M98.T!!2/&lt;W2F!!!71&amp;-14G^E:8-A4'^P;X6Q)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!&amp;%"4$UZP:'6T)%RP&lt;WNV=#"J&lt;A"5!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%!!!!!!"!!M!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">278929424</Property>
		</Item>
		<Item Name="NodesLookup Delete Node.vi" Type="VI" URL="../private/NodesLookup Delete Node.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%M!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5R"/&lt;W2F=S"-&lt;W^L&gt;8!A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!"2!5Q^/&lt;W2F=S"-&lt;W^L&gt;8!A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">663568</Property>
		</Item>
		<Item Name="NodesLookup Set ParentID.vi" Type="VI" URL="../private/NodesLookup Set ParentID.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Z!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5R"/&lt;W2F=S"-&lt;W^L&gt;8!A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%U!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!'5!(!!Z198*F&lt;H1A4G^E:3"*2!!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!&amp;%"4$UZP:'6T)%RP&lt;WNV=#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!A!#1)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!"!!!!!1!!!!%A!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">8388608</Property>
		</Item>
		<Item Name="NodesLookup Get ParentID.vi" Type="VI" URL="../private/NodesLookup Get ParentID.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Z!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%U!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!'5!(!!Z198*F&lt;H1A4G^E:3"*2!!!&amp;E"4%%ZP:'6T)%RP&lt;WNV=#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!&amp;%"4$UZP:'6T)%RP&lt;WNV=#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">8388608</Property>
		</Item>
		<Item Name="NodesLookup Set ChildIDs.vi" Type="VI" URL="../private/NodesLookup Set ChildIDs.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'&lt;!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5R"/&lt;W2F=S"-&lt;W^L&gt;8!A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V$;'FM:#"/&lt;W2F)%F%!"R!1!!"`````Q!($E.I;7RE)%ZP:'5A352T!!".!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"F!"Q!/1H*B&lt;G.I)%ZP:'5A351!!"2!5Q^/&lt;W2F=S"-&lt;W^L&gt;8!A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!#!!*!!I#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!)1!!!!%!!!!")!!!!!!1!,!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">8388608</Property>
		</Item>
		<Item Name="NodesLookup Get ChildIDs.vi" Type="VI" URL="../private/NodesLookup Get ChildIDs.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'&lt;!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V$;'FM:#"/&lt;W2F)%F%!"R!1!!"`````Q!&amp;$E.I;7RE)%ZP:'5A352T!!!71&amp;-14G^E:8-A4'^P;X6Q)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1".!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"F!"Q!/1H*B&lt;G.I)%ZP:'5A351!!"2!5Q^/&lt;W2F=S"-&lt;W^L&gt;8!A;7Y!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!"!!!!!!!1!,!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">8388608</Property>
		</Item>
		<Item Name="NodesLookup Set LinkedIDs.vi" Type="VI" URL="../private/NodesLookup Set LinkedIDs.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'4!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5R"/&lt;W2F=S"-&lt;W^L&gt;8!A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V$;'FM:#"/&lt;W2F)%F%!"R!1!!"`````Q!($URJ&lt;GNF:#"/&lt;W2F)%F%=Q"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!51&amp;-04G^E:8-A4'^P;X6Q)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!A!#1!+!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!#%!!!!"!!!!!3!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">8388608</Property>
		</Item>
		<Item Name="NodesLookup Get LinkedIDs.vi" Type="VI" URL="../private/NodesLookup Get LinkedIDs.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'4!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V$;'FM:#"/&lt;W2F)%F%!"R!1!!"`````Q!&amp;$URJ&lt;GNF:#"/&lt;W2F)%F%=Q!71&amp;-14G^E:8-A4'^P;X6Q)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!51&amp;-04G^E:8-A4'^P;X6Q)'FO!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!1!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">8388608</Property>
		</Item>
		<Item Name="Branch Insert Child Node.vi" Type="VI" URL="../private/Branch Insert Child Node.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;=!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5R"/&lt;W2F=S"-&lt;W^L&gt;8!A&lt;X6U!!!:1!-!%EFO:'6Y)#BB&gt;#"U;'5A:7ZE+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!M1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR/&lt;W2F,GRW9WRB=X-!"%ZP:'5!!$"!=!!?!!!@$F.F=86F&lt;G.F,GRW&lt;'FC$E*S97ZD;#ZM&gt;G.M98.T!!:#=G&amp;O9WA!!"2!5Q^/&lt;W2F=S"-&lt;W^L&gt;8!A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!'!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!#A!!!!I!!!!)!!!!%!!!!")!!!!!!1!,!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">663568</Property>
		</Item>
		<Item Name="Branch Remove Child Node.vi" Type="VI" URL="../private/Branch Remove Child Node.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;C!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5R"/&lt;W2F=S"-&lt;W^L&gt;8!A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V$;'FM:#"/&lt;W2F)%F%!$"!=!!?!!!@$F.F=86F&lt;G.F,GRW&lt;'FC$E*S97ZD;#ZM&gt;G.M98.T!!:#=G&amp;O9WA!!"2!5Q^/&lt;W2F=S"-&lt;W^L&gt;8!A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"Q!)!!E$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!%!!!!")!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">663568</Property>
		</Item>
		<Item Name="Add Child Node.vi" Type="VI" URL="../private/Add Child Node.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!-=!!!!&amp;!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$5!(!!&gt;/&lt;W2F)%F%!"F!"Q!41W^O:'FU;7^O97QA4G^E:3"*2!#.!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T(UZP:'5A28BF9X6U;7^O)&amp;.U982V=S"&amp;&gt;G6O&gt;#ZD&gt;'Q!25!6!!9(28BF9X6U:1:';7ZJ=WA(5X6D9W6T=Q&gt;'97FM&gt;8*F"E.B&lt;G.F&lt;!24;WFQ!!!-5X2B&gt;(6T)%6W:7ZU!!!=1%!!!@````]!"A^$&lt;WZE;82J&lt;WZT)#B05CE!(E"1!!-!"!!&amp;!!=228BF9X6U:3"$&lt;WZE;82J&lt;WY!)%"!!!(`````!!A35G6N&lt;X:F:#"$&lt;WZE;82J&lt;WZT!!"0!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"N!"Q!118.T;7&gt;O:71A4G^E:3"*2!!!&amp;E"4%%ZP:'6T)%RP&lt;WNV=#"P&gt;81!!!1!!!!:1!-!%EFO:'6Y)#BB&gt;#"U;'5A:7ZE+1!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"URB=X1A351!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"0!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"N!"Q!15'&amp;S:7ZU)%*S97ZD;#"*2!!!,%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-4G^E:3ZM&gt;G.M98.T!!2/&lt;W2F!!!51&amp;-04G^E:8-A4'^P;X6Q)'FO!&amp;1!]!!-!!-!#1!+!!M!$!!.!!Q!$A!0!"!!%1!3!Q!!?!!!$1A!!!E!!!!."Q!!$1M!!!!!!!!+!!!!!!!!!")!!!!+!!!!%!!!!"!!!!!3!!!!!!%!%Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">279191568</Property>
		</Item>
		<Item Name="Reparent Node.vi" Type="VI" URL="../private/Reparent Node.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+B!!!!%A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"Q!(4G^E:3"*2!!:1!=!%U.P&lt;G2J&gt;'FP&lt;G&amp;M)%ZP:'5A351!D1$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=R^/&lt;W2F)%6Y:7.V&gt;'FP&lt;C"4&gt;'&amp;U&gt;8-A28:F&lt;H1O9X2M!%6!&amp;1!'"U6Y:7.V&gt;'5'2GFO;8.I"V.V9W.F=X-(2G&amp;J&lt;(6S:1:$97ZD:7Q%5WNJ=!!!$&amp;.U982V=S"&amp;&gt;G6O&gt;!!!(%"!!!(`````!!=01W^O:'FU;7^O=S!I4V)J!"Z!5!!$!!5!"A!)%56Y:7.V&gt;'5A1W^O:'FU;7^O!#"!1!!"`````Q!*%F*F&lt;7^W:71A1W^O:'FU;7^O=Q!!&amp;E"4%%ZP:'6T)%RP&lt;WNV=#"P&gt;81!!"F!!Q!337ZE:8AA+'&amp;U)(2I:3"F&lt;G1J!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%]!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!'U!(!""598*H:81A1H*B&lt;G.I)%F%!!"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!51&amp;-04G^E:8-A4'^P;X6Q)'FO!&amp;1!]!!-!!-!"!!+!!M!"!!%!!1!$!!.!!Y!$Q!1!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!I!!!!+!!!!%!!!!"!!!!!3!!!!!!%!%1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">279191568</Property>
		</Item>
		<Item Name="Delete Node by ID.vi" Type="VI" URL="../private/Delete Node by ID.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%M!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5R"/&lt;W2F=S"-&lt;W^L&gt;8!A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!"2!5Q^/&lt;W2F=S"-&lt;W^L&gt;8!A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">41943056</Property>
		</Item>
		<Item Name="Node Check Execute Condition.vi" Type="VI" URL="../private/Node Check Execute Condition.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)"!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1J3:7&amp;D;'&amp;C&lt;'5`!!!71&amp;-14G^E:8-A4'^P;X6Q)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!#.!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T(UZP:'5A28BF9X6U;7^O)&amp;.U982V=S"&amp;&gt;G6O&gt;#ZD&gt;'Q!25!6!!9(28BF9X6U:1:';7ZJ=WA(5X6D9W6T=Q&gt;'97FM&gt;8*F"E.B&lt;G.F&lt;!24;WFQ!!!-5X2B&gt;(6T)%6W:7ZU!!!=1%!!!@````]!#1^$&lt;WZE;82J&lt;WZT)#B05CE!(%"1!!)!#!!+%56Y:7.V&gt;'5A1W^O:'FU;7^O!"2!5Q^/&lt;W2F=S"-&lt;W^L&gt;8!A;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#Q!)!!Q$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!%!!!!"!!!!!!!1!.!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">278929424</Property>
		</Item>
		<Item Name="Node Re-Check Execute Conditions.vi" Type="VI" URL="../private/Node Re-Check Execute Conditions.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*E!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!&amp;%!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!(5!(!".$&lt;WZE;82J&lt;WZB&lt;#"/&lt;W2F)%F%!)U!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-@4G^E:3"&amp;?'6D&gt;82J&lt;WYA5X2B&gt;(6T)%6W:7ZU,G.U&lt;!"&amp;1"5!"A&gt;&amp;?'6D&gt;82F"E:J&lt;GFT;!&gt;4&gt;7.D:8.T"U:B;7RV=G5'1W&amp;O9W6M"&amp;.L;8!!!!R4&gt;'&amp;U&gt;8-A28:F&lt;H1!!"R!1!!"`````Q!($U.P&lt;G2J&gt;'FP&lt;H-A+%^3+1!?1&amp;!!!Q!&amp;!!9!#"&amp;&amp;?'6D&gt;82F)%.P&lt;G2J&gt;'FP&lt;A!A1%!!!@````]!#2*3:7VP&gt;G6E)%.P&lt;G2J&gt;'FP&lt;H-!!":!5R"/&lt;W2F=S"-&lt;W^L&gt;8!A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!5Q^/&lt;W2F=S"-&lt;W^L&gt;8!A;7Y!6!$Q!!Q!!Q!%!!I!#Q!%!!1!"!!%!!Q!"!!&amp;!!U$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!!!!1!/!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">41943056</Property>
		</Item>
		<Item Name="Node Save Recursion.vi" Type="VI" URL="../private/Node Save Recursion.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"J!-0````]15W6S;7&amp;M;8JF)&amp;.U=GFO:Q!!(E"!!!(`````!!515W6S;7&amp;M;8JF:#"/&lt;W2F=Q!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!51&amp;-04G^E:8-A4'^P;X6Q)'FO!&amp;1!]!!-!!-!"!!%!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!1!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Sequence Nodes Load.vi" Type="VI" URL="../private/Sequence Nodes Load.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!7+!!!!)Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V5&lt;X!A4'^B:'6E)%F%!&amp;Y!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-32G^S&lt;7&amp;U)&amp;:F=H.J&lt;WYO9X2M!#.!&amp;1!""H9R,D!O-!!!$E:P=GVB&gt;#"7:8*T;7^O!!!11$$`````"V:F=H.J&lt;WY!%E"5!!9+6'FN:3"4&gt;'&amp;N=!!!%%!Q`````Q&gt;$;'&amp;O:W6T!"2!5!!$!!=!#!!*"E.I97ZH:1!!&amp;E"!!!(`````!!I*1WBB&lt;G&gt;F&lt;'^H!"R!5R&gt;-&lt;W.B&lt;#"798*J97*M:8-A5X2P=G&amp;H:1!51&amp;!!!Q!'!!M!$!:):7&amp;E:8)!!#R!=!!?!!!&gt;$F.F=86F&lt;G.F,GRW&lt;'FC$%ZP:'5O&lt;(:D&lt;'&amp;T=Q!%37ZJ&gt;!!!,%"Q!"Y!!"U/5W6R&gt;76O9W5O&lt;(:M;7)-4G^E:3ZM&gt;G.M98.T!!2#&lt;W2Z!!!O1(!!(A!!(1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9AR/&lt;W2F,GRW9WRB=X-!"E2F;7ZJ&gt;!!!'%"1!!-!$A!0!"!,27ZU=HEA4G^E:8-!(%!S`````R*4:8&amp;V:7ZD:3"';7RF)&amp;"B&gt;'A!!&amp;E!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-75W6R&gt;76O9W5A1W^S:3"%982B,G.U&lt;!!;1&amp;!!!Q!.!"%!%AF$&lt;X*F)%2B&gt;'%!%E"4$%ZP:'6T)%RP&lt;WNV=!!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"URB=X1A351!%%!B#E6Y:7.V&gt;'FO:T]!!":!5R"&amp;?'6D&gt;82J&lt;WYA4'^P;X6Q!!!=1&amp;-84'&amp;V&lt;G.I:71A47^E&gt;7RF=S"-&lt;W^L&gt;8!!'E"4&amp;5RP9W&amp;M)&amp;:B=GFB9GRF)&amp;:B&lt;(6F=Q"H!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T'F.F=86F&lt;G.F)&amp;*V&lt;CV5;7VF)%2B&gt;'%O9X2M!#2!5!!'!"1!&amp;1!7!"=!'!!:$6*V&lt;CV5;7VF)%2B&gt;'%!81$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!A1&amp;!!!A!4!"I25W6R&gt;76O9W5A2'&amp;U93"P&gt;81!'5!$!"**&lt;G2F?#!I981A&gt;'BF)'6O:#E!!#F!&amp;A!##&amp;.F=86F&lt;G.F$&amp;.F=86F&lt;G.F5'&amp;S&gt;!!,4'^B:#"T9WBF&lt;75!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"6!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!#&amp;!"Q!85'&amp;S:7ZU)%F%)#BG&lt;X)A&lt;X*Q;'&amp;O=SE!'E!Q`````R"4:8*J97RJ?G5A5X2S;7ZH!!"&gt;!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!#"!5!!#!"-!'B"4:8&amp;V:7ZD:3"%982B)'FO!!"5!0!!$!!$!!1!"1!&lt;!!1!(!!&gt;!!9!(A!@!#!!)1-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!#!!!!"!!!!!1!!!!#A!!!!A!!!%3!!!!%A!!!!!"!#)!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
		</Item>
		<Item Name="Node Lock Recursion.vi" Type="VI" URL="../private/Node Lock Recursion.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%]!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5R"/&lt;W2F=S"-&lt;W^L&gt;8!A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!J!)16-&lt;W.L0Q",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.5X2B=H1A4G^E:3"*2!!51&amp;-04G^E:8-A4'^P;X6Q)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%!!!!"!!!!!3!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
		</Item>
		<Item Name="Save to File.vi" Type="VI" URL="../private/Save to File.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$N!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!'E!Q`````R"4:8*J97RJ?G5A5X2S;7ZH!!!71$,`````$&amp;"B&gt;'AA+#IO=X&amp;O+1!!6!$Q!!Q!!Q!%!!1!"!!%!!1!"!!%!!5!"!!'!!=$!!"Y!!!.#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!#%A!!!"!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Load from File.vi" Type="VI" URL="../private/Load from File.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$N!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"J!-0````]15W6S;7&amp;M;8JF)&amp;.U=GFO:Q!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$,`````$&amp;"B&gt;'AA+#IO=X&amp;O+1!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
	</Item>
	<Item Name="Public API" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Type Defs" Type="Folder">
			<Item Name="Node ID.ctl" Type="VI" URL="../Public API/Type Defs/Node ID.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
			</Item>
			<Item Name="Node Execution Status.ctl" Type="VI" URL="../Public API/Type Defs/Node Execution Status.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#3!!!!!1#+!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T$EZP:'64&gt;'&amp;U&gt;8-O9X2M!&amp;.!&amp;1!)#66O:'6G;7ZF:!2*:'RF#56Y:7.V&gt;'FO:Q2%&lt;WZF#%.B&lt;G.F&lt;'6E"V.L;8"Q:71)5X6D9W6E:71'2G&amp;J&lt;'6E!!!'5X2B&gt;(6T!!!"!!!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
			</Item>
			<Item Name="Node Execution Status Event.ctl" Type="VI" URL="../Public API/Type Defs/Node Execution Status Event.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#*!!!!!1#"!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T%UZP:'64&gt;'&amp;U&gt;8.&amp;&gt;G6O&gt;#ZD&gt;'Q!25!6!!9(28BF9X6U:1:';7ZJ=WA'1W&amp;O9W6M"&amp;.L;8!(5X6D9W6T=Q&gt;'97FM&gt;8*F!!!-5X2B&gt;(6T)%6W:7ZU!!!"!!!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
			</Item>
			<Item Name="Node Retry Condition.ctl" Type="VI" URL="../Public API/Type Defs/Node Retry Condition.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#'!!!!!1"_!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T&amp;EZP:'63:82S?5.P&lt;G2J&gt;'FP&lt;CZD&gt;'Q!0U!6!!-.67ZU;7QA=X6D9W6T=QV6&lt;H2J&lt;#"G97FM&gt;8*F"E&amp;O?8&gt;B?1!!$V*F&gt;(*Z)%.P&lt;G2J&gt;'FP&lt;A!"!!!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
			</Item>
			<Item Name="Node Condition Fail Action.ctl" Type="VI" URL="../Public API/Type Defs/Node Condition Fail Action.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
			</Item>
			<Item Name="Format Version.ctl" Type="VI" URL="../Public API/Type Defs/Format Version.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
			</Item>
			<Item Name="Node Type.ctl" Type="VI" URL="../Public API/Type Defs/Node Type.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
			</Item>
			<Item Name="Node Metainfo.ctl" Type="VI" URL="../Public API/Type Defs/Node Metainfo.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
			</Item>
			<Item Name="Local Variable Definition.ctl" Type="VI" URL="../Public API/Type Defs/Local Variable Definition.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
			</Item>
			<Item Name="Local Variable Storage.ctl" Type="VI" URL="../Public API/Type Defs/Local Variable Storage.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
			</Item>
		</Item>
		<Item Name="Sequence" Type="Folder">
			<Item Name="Core Data" Type="Folder">
				<Item Name="Sequence Get Changelog.vi" Type="VI" URL="../Public API/Sequence/Core Data/Sequence Get Changelog.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'5!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````](6G6S=WFP&lt;A!31&amp;1!"AJ5;7VF)&amp;.U97VQ!!!11$$`````"U.I97ZH:8-!&amp;%"1!!-!"1!'!!='1WBB&lt;G&gt;F!!!71%!!!@````]!#!F$;'&amp;O:W6M&lt;W=!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!*!!I!"!!%!!1!"!!,!!1!"!!-!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!U!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">268972560</Property>
				</Item>
				<Item Name="Sequence Set Changelog.vi" Type="VI" URL="../Public API/Sequence/Core Data/Sequence Set Changelog.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'5!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!11$$`````"V:F=H.J&lt;WY!%E"5!!9+6'FN:3"4&gt;'&amp;N=!!!%%!Q`````Q&gt;$;'&amp;O:W6T!"2!5!!$!!=!#!!*"E.I97ZH:1!!&amp;E"!!!(`````!!I*1WBB&lt;G&gt;F&lt;'^H!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!#Q!-!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!B!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!U!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Sequence Get LocalVariableStorage.vi" Type="VI" URL="../Public API/Sequence/Core Data/Sequence Get LocalVariableStorage.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*`!!!!%1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$%!B"E:P&gt;7ZE0Q!!%E!S`````QF%;8*F9X2P=HE!$E!Q`````Q2/97VF!!"J!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T'ERP9W&amp;M)&amp;:B=GFB9GRF)&amp;.U&lt;X*B:W5O9X2M!#:!5!!#!!5!"B:-&lt;W.B&lt;#"798*J97*M:3"4&gt;'^S97&gt;F!!!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-4'FC=G&amp;S?3"/97VF!!!71$$`````$6:B=GFB9GRF)%ZB&lt;75!&amp;E!Q`````QV5;(*F971A5X6G:GFY!(!!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-&gt;4'^D97QA6G&amp;S;7&amp;C&lt;'5A2'6G;7ZJ&gt;'FP&lt;CZD&gt;'Q!+E"1!!-!#Q!-!!U:4'^D97QA6G&amp;S;7&amp;C&lt;'5A2'6G;7ZJ&gt;'FP&lt;A!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"Q!)!!E!#1!*!!E!#A!*!!Y!$Q-!!(A!!!U)!!!*!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!1!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">268972560</Property>
				</Item>
				<Item Name="Sequence Set LocalVariableStorage.vi" Type="VI" URL="../Public API/Sequence/Core Data/Sequence Set LocalVariableStorage.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*T!!!!%!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!31$,`````#52J=G6D&gt;'^S?1!/1$$`````"%ZB&lt;75!!'E!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-;4'^D97QA6G&amp;S;7&amp;C&lt;'5A5X2P=G&amp;H:3ZD&gt;'Q!*E"1!!)!"Q!)&amp;ERP9W&amp;M)&amp;:B=GFB9GRF)&amp;.U&lt;X*B:W5!!":!-0````]-4'FC=G&amp;S?3"/97VF!!!71$$`````$6:B=GFB9GRF)%ZB&lt;75!&amp;E!Q`````QV5;(*F971A5X6G:GFY!(!!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-&gt;4'^D97QA6G&amp;S;7&amp;C&lt;'5A2'6G;7ZJ&gt;'FP&lt;CZD&gt;'Q!+E"1!!-!#A!,!!Q:4'^D97QA6G&amp;S;7&amp;C&lt;'5A2'6G;7ZJ&gt;'FP&lt;A!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!*!!U!$A-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!"!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!0!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Sequence Clear LocalVariablesStorage.vi" Type="VI" URL="../Public API/Sequence/Core Data/Sequence Clear LocalVariablesStorage.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%Y!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
			</Item>
			<Item Name="Run-Time Data" Type="Folder">
				<Item Name="Sequence Get Executing.vi" Type="VI" URL="../Public API/Sequence/Run-Time Data/Sequence Get Executing.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1J&amp;?'6D&gt;82J&lt;G=`!!!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Sequence Set Executing.vi" Type="VI" URL="../Public API/Sequence/Run-Time Data/Sequence Set Executing.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!11#%+28BF9X6U;7ZH0Q!!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Sequence Get ExecutionLookup.vi" Type="VI" URL="../Public API/Sequence/Run-Time Data/Sequence Get ExecutionLookup.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5R"&amp;?'6D&gt;82J&lt;WYA4'^P;X6Q!!!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Sequence Set ExecutionLookup.vi" Type="VI" URL="../Public API/Sequence/Run-Time Data/Sequence Set ExecutionLookup.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71&amp;-128BF9X6U;7^O)%RP&lt;WNV=!!!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Sequence Get LaunchedModulesLookup.vi" Type="VI" URL="../Public API/Sequence/Run-Time Data/Sequence Get LaunchedModulesLookup.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"R!5R&gt;-986O9WBF:#".&lt;W2V&lt;'6T)%RP&lt;WNV=!!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Sequence Set LaunchedModulesLookup.vi" Type="VI" URL="../Public API/Sequence/Run-Time Data/Sequence Set LaunchedModulesLookup.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1&amp;-84'&amp;V&lt;G.I:71A47^E&gt;7RF=S"-&lt;W^L&gt;8!!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Sequence Get LocalVariableValue.vi" Type="VI" URL="../Public API/Sequence/Run-Time Data/Sequence Get LocalVariableValue.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)1!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$%!B"E:P&gt;7ZE0Q!!'E"4&amp;%RP9W&amp;M)&amp;:B=GFB9GRF)&amp;:B&lt;(6F!!!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-4'FC=G&amp;S?3"/97VF!!!71$$`````$6:B=GFB9GRF)%ZB&lt;75!&amp;E!Q`````QV5;(*F971A5X6G:GFY!(!!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-&gt;4'^D97QA6G&amp;S;7&amp;C&lt;'5A2'6G;7ZJ&gt;'FP&lt;CZD&gt;'Q!+E"1!!-!#1!+!!M:4'^D97QA6G&amp;S;7&amp;C&lt;'5A2'6G;7ZJ&gt;'FP&lt;A!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!Q!$1-!!(A!!!U)!!!*!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!/!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Sequence Set LocalVariableValue.vi" Type="VI" URL="../Public API/Sequence/Run-Time Data/Sequence Set LocalVariableValue.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)%!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!;1&amp;-54'^D97QA6G&amp;S;7&amp;C&lt;'5A6G&amp;M&gt;75!!":!-0````]-4'FC=G&amp;S?3"/97VF!!!71$$`````$6:B=GFB9GRF)%ZB&lt;75!&amp;E!Q`````QV5;(*F971A5X6G:GFY!(!!]1!!!!!!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-&gt;4'^D97QA6G&amp;S;7&amp;C&lt;'5A2'6G;7ZJ&gt;'FP&lt;CZD&gt;'Q!+E"1!!-!#!!*!!I:4'^D97QA6G&amp;S;7&amp;C&lt;'5A2'6G;7ZJ&gt;'FP&lt;A!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!M!$!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!"!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!.!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Sequence Clear LocalVariableValues.vi" Type="VI" URL="../Public API/Sequence/Run-Time Data/Sequence Clear LocalVariableValues.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%Y!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Node" Type="Folder">
			<Item Name="Core Data" Type="Folder">
				<Item Name="Node Get Metainfo.vi" Type="VI" URL="../Public API/Node/Core Data/Node Get Metainfo.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!-L!!!!%!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!6Q$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QV/&lt;W2F)&amp;2Z='5O9X2M!#&amp;!&amp;1!#"&amp;2B=WM'1H*B&lt;G.I!!F/&lt;W2F)&amp;2Z='5!41$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!:1!=!$F"B=G6O&gt;#"/&lt;W2F)%F%!!",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.1WBJ&lt;'1A4G^E:3"*2!!=1%!!!@````]!"AZ$;'FM:#"/&lt;W2F)%F%=Q!!7!$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=R&amp;/&lt;W2F)%VF&gt;'&amp;J&lt;G:P,G.U&lt;!!?1&amp;!!!Q!%!!5!"QV/&lt;W2F)%VF&gt;'&amp;J&lt;G:P!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!)!!E!#A!,!!M!#Q!,!!Q!#Q!.!!Y$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get DefaultName.vi" Type="VI" URL="../Public API/Node/Core Data/Node Get DefaultName.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(?!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;E!Q`````QR%:7:B&gt;7RU)%ZB&lt;75!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set DefaultName.vi" Type="VI" URL="../Public API/Node/Core Data/Node Set DefaultName.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(?!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$%2F:G&amp;V&lt;(1A4G&amp;N:1!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!)1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get DirectExecuteConditions.vi" Type="VI" URL="../Public API/Node/Core Data/Node Get DirectExecuteConditions.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+Z!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!D1$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=R^/&lt;W2F)%6Y:7.V&gt;'FP&lt;C"4&gt;'&amp;U&gt;8-A28:F&lt;H1O9X2M!%6!&amp;1!'"U6Y:7.V&gt;'5'2GFO;8.I"V.V9W.F=X-(2G&amp;J&lt;(6S:1:$97ZD:7Q%5WNJ=!!!$&amp;.U982V=S"&amp;&gt;G6O&gt;!!!(%"!!!(`````!!501W^O:'FU;7^O=S!I4V)J!"R!5!!#!!1!"B&amp;&amp;?'6D&gt;82F)%.P&lt;G2J&gt;'FP&lt;A!M1%!!!@````]!"R^%;8*F9X1A28BF9X6U:3"$&lt;WZE;82J&lt;WZT)#B"4E1J!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!)!!E!#A!,!!M!#Q!,!!Q!#Q!%!!U$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Add Direct Execute Condition.vi" Type="VI" URL="../Public API/Node/Core Data/Node Add Direct Execute Condition.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+6!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!#.!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T(UZP:'5A28BF9X6U;7^O)&amp;.U982V=S"&amp;&gt;G6O&gt;#ZD&gt;'Q!25!6!!9(28BF9X6U:1:';7ZJ=WA(5X6D9W6T=Q&gt;'97FM&gt;8*F"E.B&lt;G.F&lt;!24;WFQ!!!-5X2B&gt;(6T)%6W:7ZU!!!=1%!!!@````]!#1^$&lt;WZE;82J&lt;WZT)#B05CE!*%"1!!)!#!!+'%2J=G6D&gt;#"&amp;?'6D&gt;82F)%.P&lt;G2J&gt;'FP&lt;A!!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#Q!)!!Q$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Remove DirectExecuteCondition.vi" Type="VI" URL="../Public API/Node/Core Data/Node Remove DirectExecuteCondition.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(@!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!81!-!%5FO:'6Y)#BD&lt;'6B=C"B&lt;'QJ!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get PreviousExecuteCondition.vi" Type="VI" URL="../Public API/Node/Core Data/Node Get PreviousExecuteCondition.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*^!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!D1$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=R^/&lt;W2F)%6Y:7.V&gt;'FP&lt;C"4&gt;'&amp;U&gt;8-A28:F&lt;H1O9X2M!%6!&amp;1!'"U6Y:7.V&gt;'5'2GFO;8.I"V.V9W.F=X-(2G&amp;J&lt;(6S:1:$97ZD:7Q%5WNJ=!!!$&amp;.U982V=S"&amp;&gt;G6O&gt;!!!+%"!!!(`````!!1;5(*F&gt;GFP&gt;8-A28BF9X6U:3"$&lt;WZE;82J&lt;WY!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!&amp;!!9!"Q!)!!A!#!!)!!E!#!!+!!M$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set PreviousExecuteCondition.vi" Type="VI" URL="../Public API/Node/Core Data/Node Set PreviousExecuteCondition.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*^!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1#.!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T(UZP:'5A28BF9X6U;7^O)&amp;.U982V=S"&amp;&gt;G6O&gt;#ZD&gt;'Q!25!6!!9(28BF9X6U:1:';7ZJ=WA(5X6D9W6T=Q&gt;'97FM&gt;8*F"E.B&lt;G.F&lt;!24;WFQ!!!-5X2B&gt;(6T)%6W:7ZU!!!I1%!!!@````]!#"J1=G6W;7^V=S"&amp;?'6D&gt;82F)%.P&lt;G2J&gt;'FP&lt;A!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#1!+!!M$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!)1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get OverwritePreviousExecuteCondition.vi" Type="VI" URL="../Public API/Node/Core Data/Node Get OverwritePreviousExecuteCondition.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(S!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!+E!B*%^W:8*X=GFU:3"1=G6W;7^V=S"&amp;?'6D&gt;82F)%.P&lt;G2J&gt;'FP&lt;A!!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set OverwritePreviousExecuteCondition.vi" Type="VI" URL="../Public API/Node/Core Data/Node Set OverwritePreviousExecuteCondition.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(S!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!K1#%E4X:F=H&gt;S;82F)&amp;"S:8:J&lt;X6T)%6Y:7.V&gt;'5A1W^O:'FU;7^O!!"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!)!!E!#A-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!"!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get ConditionFailAction.vi" Type="VI" URL="../Public API/Node/Core Data/Node Get ConditionFailAction.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*%!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!@!$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=RZ/&lt;W2F)%.P&lt;G2J&gt;'FP&lt;C"'97FM)%&amp;D&gt;'FP&lt;CZD&gt;'Q!.5!6!!-'1W&amp;O9W6M"&amp;.L;8!(28BF9X6U:1!61W^O:'FU;7^O)%:B;7QA17.U;7^O!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set ConditionFailAction.vi" Type="VI" URL="../Public API/Node/Core Data/Node Set ConditionFailAction.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*%!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"]!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T(EZP:'5A1W^O:'FU;7^O)%:B;7QA17.U;7^O,G.U&lt;!!V1"5!!Q:$97ZD:7Q%5WNJ=!&gt;&amp;?'6D&gt;82F!"6$&lt;WZE;82J&lt;WYA2G&amp;J&lt;#""9X2J&lt;WY!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get FailureTerminatesSequence.vi" Type="VI" URL="../Public API/Node/Core Data/Node Get FailureTerminatesSequence.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(I!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!)%!B'U:B;7RV=G5A6'6S&lt;7FO982F=S"4:8&amp;V:7ZD:1",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.4G^E:3"*2#!I:(6Q+1!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!#1!+!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set FailureTerminatesSequence.vi" Type="VI" URL="../Public API/Node/Core Data/Node Set FailureTerminatesSequence.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(I!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!A1#%&lt;2G&amp;J&lt;(6S:3"5:8*N;7ZB&gt;'6T)&amp;.F=86F&lt;G.F!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get SkipNode.vi" Type="VI" URL="../Public API/Node/Core Data/Node Get SkipNode.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(7!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$E!B#6.L;8!A4G^E:1",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.4G^E:3"*2#!I:(6Q+1!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!#1!+!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set SkipNode.vi" Type="VI" URL="../Public API/Node/Core Data/Node Set SkipNode.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(7!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!/1#%*5WNJ=#"/&lt;W2F!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get Breakpoint.vi" Type="VI" URL="../Public API/Node/Core Data/Node Get Breakpoint.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(9!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%%!B#E*S:7&amp;L='^J&lt;H1!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set Breakpoint.vi" Type="VI" URL="../Public API/Node/Core Data/Node Set Breakpoint.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(9!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!11#%+1H*F97NQ&lt;WFO&gt;!!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get RetryCount.vi" Type="VI" URL="../Public API/Node/Core Data/Node Get RetryCount.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(:!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%5!(!!N3:82S?3"$&lt;X6O&gt;!",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.4G^E:3"*2#!I:(6Q+1!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!#1!+!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set RetryCount.vi" Type="VI" URL="../Public API/Node/Core Data/Node Set RetryCount.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(:!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!21!=!#V*F&gt;(*Z)%.P&gt;7ZU!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get RetryCondition.vi" Type="VI" URL="../Public API/Node/Core Data/Node Get RetryCondition.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*)!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!A!$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=RB/&lt;W2F)&amp;*F&gt;(*Z)%.P&lt;G2J&gt;'FP&lt;CZD&gt;'Q!0U!6!!-.67ZU;7QA=X6D9W6T=QV6&lt;H2J&lt;#"G97FM&gt;8*F"E&amp;O?8&gt;B?1!!$V*F&gt;(*Z)%.P&lt;G2J&gt;'FP&lt;A",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.4G^E:3"*2#!I:(6Q+1!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!#1!+!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set RetryCondition.vi" Type="VI" URL="../Public API/Node/Core Data/Node Set RetryCondition.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*)!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1#!!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T'%ZP:'5A5G6U=HEA1W^O:'FU;7^O,G.U&lt;!!`1"5!!QV6&lt;H2J&lt;#"T&gt;7.D:8.T$66O&gt;'FM)':B;7RV=G5'17ZZ&gt;W&amp;Z!!!05G6U=HEA1W^O:'FU;7^O!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get LockedState.vi" Type="VI" URL="../Public API/Node/Core Data/Node Get LockedState.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(9!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%%!B#URP9WNF:#"/&lt;W2F!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set LockedState.vi" Type="VI" URL="../Public API/Node/Core Data/Node Set LockedState.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(9!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!11#%,4'^D;W6E)%ZP:'5!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get Description.vi" Type="VI" URL="../Public API/Node/Core Data/Node Get Description.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(=!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;%!Q`````QN%:8.D=GFQ&gt;'FP&lt;A",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.4G^E:3"*2#!I:(6Q+1!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!#1!+!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set Description.vi" Type="VI" URL="../Public API/Node/Core Data/Node Set Description.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(=!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!51$$`````#U2F=W.S;8"U;7^O!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!#%!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get NoErrorDelegation.vi" Type="VI" URL="../Public API/Node/Core Data/Node Get NoErrorDelegation.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(A!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'%!B%UZP)%6S=G^S)%2F&lt;'6H982J&lt;WY!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
				<Item Name="Node Set NoErrorDelegation.vi" Type="VI" URL="../Public API/Node/Core Data/Node Set NoErrorDelegation.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(A!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!91#%44G]A28*S&lt;X)A2'6M:7&gt;B&gt;'FP&lt;A"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!)!!E!#A-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!"!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
				</Item>
			</Item>
			<Item Name="Run-Time Data" Type="Folder">
				<Item Name="Node Get DisplayedName.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Get DisplayedName.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(A!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'%!Q`````QZ%;8.Q&lt;'&amp;Z:71A4G&amp;N:1!!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set DisplayedName.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Set DisplayedName.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(A!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!91$$`````$E2J=X"M98FF:#"/97VF!!"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!)!!E!#A-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!B!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get Status.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Get Status.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*4!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!CQ$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=RF/&lt;W2F)%6Y:7.V&gt;'FP&lt;C"4&gt;'&amp;U&gt;8-O9X2M!%F!&amp;1!(#66O:'6G;7ZF:!2*:'RF#56Y:7.V&gt;'FO:Q2%&lt;WZF"E:B;7RF:!B$97ZD:7RF:!&gt;4;WFQ='6E!!:4&gt;'&amp;U&gt;8-!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set Status.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Set Status.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*4!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1#,!0%!!!!!!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T'5ZP:'5A28BF9X6U;7^O)&amp;.U982V=SZD&gt;'Q!35!6!!=*67ZE:7:J&lt;G6E"%FE&lt;'5*28BF9X6U;7ZH"%2P&lt;G5'2G&amp;J&lt;'6E#%.B&lt;G.F&lt;'6E"V.L;8"Q:71!"F.U982V=Q!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get WasExecuted.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Get WasExecuted.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(;!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%E!B$&amp;&gt;B=S"&amp;?'6D&gt;82F:!!!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set WasExecuted.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Set WasExecuted.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(;!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!31#%-6W&amp;T)%6Y:7.V&gt;'6E!!"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!)!!E!#A-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!"!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get WasTerminated.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Get WasTerminated.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(=!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;%!B$F&gt;B=S"5:8*N;7ZB&gt;'6E!!",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.4G^E:3"*2#!I:(6Q+1!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!#1!+!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set WasTerminated.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Set WasTerminated.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(=!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!51#%/6W&amp;T)&amp;2F=GVJ&lt;G&amp;U:71!!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get MetDirectConditions.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Get MetDirectConditions.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(O!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!B!#*!1!!"`````Q!%&amp;5VF&gt;#"%;8*F9X1A1W^O:'FU;7^O=Q",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.4G^E:3"*2#!I:(6Q+1!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"1!'!!=!#!!)!!A!#!!*!!A!#A!,!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set MetDirectConditions.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Set MetDirectConditions.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(O!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!%!#%!)E"!!!(`````!!A6476U)%2J=G6D&gt;#"$&lt;WZE;82J&lt;WZT!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!E!#A!,!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!#%!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get MetPreviousCondition.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Get MetPreviousCondition.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(E!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(%!B&amp;EVF&gt;#"1=G6W;7^V=S"$&lt;WZE;82J&lt;WY!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set MetPreviousCondition.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Set MetPreviousCondition.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(E!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1#%7476U)&amp;"S:8:J&lt;X6T)%.P&lt;G2J&gt;'FP&lt;A!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get ConditionsFailed.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Get ConditionsFailed.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(?!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;E!B%5.P&lt;G2J&gt;'FP&lt;H-A2G&amp;J&lt;'6E!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set ConditionsFailed.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Set ConditionsFailed.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(?!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71#%21W^O:'FU;7^O=S"'97FM:71!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Get RetriesMade.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Get RetriesMade.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(&lt;!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%U!(!!R3:82S;76T)%VB:'5!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Node Set RetriesMade.vi" Type="VI" URL="../Public API/Node/Run-Time Data/Node Set RetriesMade.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(&lt;!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!41!=!$&amp;*F&gt;(*J:8-A47&amp;E:1!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Task" Type="Folder">
			<Item Name="Core Data" Type="Folder">
				<Item Name="Task Get ModuleName.vi" Type="VI" URL="../Public API/Task/Core Data/Task Get ModuleName.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(=!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;%!Q`````QN.&lt;W2V&lt;'5A4G&amp;N:1",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.4G^E:3"*2#!I:(6Q+1!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!#1!+!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set ModuleName.vi" Type="VI" URL="../Public API/Task/Core Data/Task Set ModuleName.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(=!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!51$$`````#UVP:(6M:3"/97VF!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!#%!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get ModuleThreadSuffix.vi" Type="VI" URL="../Public API/Task/Core Data/Task Get ModuleThreadSuffix.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(G!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(E!Q`````R2.&lt;W2V&lt;'5A6'BS:7&amp;E)&amp;.V:G:J?!!!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set ModuleThreadSuffix.vi" Type="VI" URL="../Public API/Task/Core Data/Task Set ModuleThreadSuffix.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(G!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!?1$$`````&amp;%VP:(6M:3"5;(*F971A5X6G:GFY!!"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!)!!E!#A-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!B!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get ClassQualifiedName.vi" Type="VI" URL="../Public API/Task/Core Data/Task Get ClassQualifiedName.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(G!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(E!Q`````R2$&lt;'&amp;T=S"2&gt;7&amp;M;7:J:71A4G&amp;N:1!!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set ClassQualifiedName.vi" Type="VI" URL="../Public API/Task/Core Data/Task Set ClassQualifiedName.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(G!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!?1$$`````&amp;%.M98.T)&amp;&amp;V97RJ:GFF:#"/97VF!!"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!)!!E!#A-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!B!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get ShowFP.vi" Type="VI" URL="../Public API/Task/Core Data/Task Get ShowFP.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(5!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$%!B"V.I&lt;X=A2F!!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set ShowFP.vi" Type="VI" URL="../Public API/Task/Core Data/Task Set ShowFP.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(5!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!-1#%(5WBP&gt;S"'5!"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!)!!E!#A-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!"!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get ReturnDelay.vi" Type="VI" URL="../Public API/Task/Core Data/Task Get ReturnDelay.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(@!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;U!(!""3:82V=GYA2'6M98EM)'VT!!",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.4G^E:3"*2#!I:(6Q+1!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!#1!+!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set ReturnDelay.vi" Type="VI" URL="../Public API/Task/Core Data/Task Set ReturnDelay.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(@!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!81!=!%&amp;*F&gt;(6S&lt;C"%:7RB?3QA&lt;8-!!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get DelayOnNoError.vi" Type="VI" URL="../Public API/Task/Core Data/Task Get DelayOnNoError.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(?!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;E!B%52F&lt;'&amp;Z)'^O)%ZP,56S=G^S!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set DelayOnNoError.vi" Type="VI" URL="../Public API/Task/Core Data/Task Set DelayOnNoError.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(?!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71#%22'6M98EA&lt;WYA4G]N28*S&lt;X)!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get DelayOnError.vi" Type="VI" URL="../Public API/Task/Core Data/Task Get DelayOnError.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(=!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;%!B$E2F&lt;'&amp;Z)'^O)%6S=G^S!!",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.4G^E:3"*2#!I:(6Q+1!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!#1!+!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set DelayOnError.vi" Type="VI" URL="../Public API/Task/Core Data/Task Set DelayOnError.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(=!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!51#%/2'6M98EA&lt;WYA28*S&lt;X)!!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get SavedConstructorData.vi" Type="VI" URL="../Public API/Task/Core Data/Task Get SavedConstructorData.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(E!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(%"4&amp;F.B&gt;G6E)%.P&lt;H.U=H6D&gt;'^S)%2B&gt;'%!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set SavedConstructorData.vi" Type="VI" URL="../Public API/Task/Core Data/Task Set SavedConstructorData.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(E!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1&amp;-75W&amp;W:71A1W^O=X2S&gt;7.U&lt;X)A2'&amp;U91!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!3!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get SavedUsedSequenceVariables.vi" Type="VI" URL="../Public API/Task/Core Data/Task Get SavedUsedSequenceVariables.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(K!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!)E"4(6.B&gt;G6E)&amp;6T:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set SavedUsedSequenceVariables.vi" Type="VI" URL="../Public API/Task/Core Data/Task Set SavedUsedSequenceVariables.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(K!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!C1&amp;-&gt;5W&amp;W:71A68.F:#"4:8&amp;V:7ZD:3"798*J97*M:8-!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!3!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get SavedCreatedSequenceVariables.vi" Type="VI" URL="../Public API/Task/Core Data/Task Get SavedCreatedSequenceVariables.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(O!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!*E"4)&amp;.B&gt;G6E)%.S:7&amp;U:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'6T!!",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.4G^E:3"*2#!I:(6Q+1!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!#1!+!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set SavedCreatedSequenceVariables.vi" Type="VI" URL="../Public API/Task/Core Data/Task Set SavedCreatedSequenceVariables.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(O!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!G1&amp;-A5W&amp;W:71A1X*F982F:#"4:8&amp;V:7ZD:3"798*J97*M:8-!!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%A!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get UsedSequenceVariablesAliases.vi" Type="VI" URL="../Public API/Task/Core Data/Task Get UsedSequenceVariablesAliases.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)_!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;E!Q`````QR%:7:B&gt;7RU)%ZB&lt;75!!"*!-0````]*68.F=C"/97VF!#*!5!!#!!1!"2&gt;4:8&amp;V:7ZD:3"798*J97*M:3""&lt;'FB=Q!M1%!!!@````]!"BZ6=W6E)&amp;.F=86F&lt;G.F)&amp;:B=GFB9GRF)%&amp;M;7&amp;T:8-!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!(!!A!#1!+!!I!#A!+!!M!#A!-!!U$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set UsedSequenceVariablesAliases.vi" Type="VI" URL="../Public API/Task/Core Data/Task Set UsedSequenceVariablesAliases.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)_!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$%2F:G&amp;V&lt;(1A4G&amp;N:1!!%E!Q`````QF6=W6S)%ZB&lt;75!)E"1!!)!#!!*&amp;V.F=86F&lt;G.F)&amp;:B=GFB9GRF)%&amp;M;7&amp;T!#R!1!!"`````Q!+(F6T:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'5A17RJ98.F=Q!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#Q!-!!U$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!)1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get CreatedSequenceVariablesAliases.vi" Type="VI" URL="../Public API/Task/Core Data/Task Get CreatedSequenceVariablesAliases.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*!!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;E!Q`````QR%:7:B&gt;7RU)%ZB&lt;75!!"*!-0````]*68.F=C"/97VF!#*!5!!#!!1!"2&gt;4:8&amp;V:7ZD:3"798*J97*M:3""&lt;'FB=Q!O1%!!!@````]!"C&amp;$=G6B&gt;'6E)&amp;.F=86F&lt;G.F)&amp;:B=GFB9GRF)%&amp;M;7&amp;T:8-!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!=!#!!*!!I!#A!+!!I!#Q!+!!Q!$1-!!(A!!!U)!!!*!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!/!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set CreatedSequenceVariablesAliases.vi" Type="VI" URL="../Public API/Task/Core Data/Task Set CreatedSequenceVariablesAliases.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*!!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$%2F:G&amp;V&lt;(1A4G&amp;N:1!!%E!Q`````QF6=W6S)%ZB&lt;75!)E"1!!)!#!!*&amp;V.F=86F&lt;G.F)&amp;:B=GFB9GRF)%&amp;M;7&amp;T!#Z!1!!"`````Q!+)5.S:7&amp;U:71A5W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'5A17RJ98.F=Q"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!,!!Q!$1-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!B!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!/!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get DataRequests(Return-to-Constructor).vi" Type="VI" URL="../Public API/Task/Core Data/Task Get DataRequests(Return-to-Constructor).vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*#!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!%%!Q`````Q:4&lt;X6S9W5!!"2!-0````],2'6T&gt;'FO982J&lt;WY!*%"1!!-!"!!&amp;!!985W6R&gt;76O9W5A6G&amp;S;7&amp;C&lt;'5A17RJ98-!-E"!!!(`````!!=F2'&amp;U93"3:8&amp;V:8.U=S!I5G6U&gt;8*O,82P,5.P&lt;H.U=H6D&gt;'^S+1",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.4G^E:3"*2#!I:(6Q+1!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!#!!*!!I!#Q!,!!M!#Q!-!!M!"!!.!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!Y!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set DataRequests(Return-to-Constructor).vi" Type="VI" URL="../Public API/Task/Core Data/Task Set DataRequests(Return-to-Constructor).vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*#!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!11$$`````"F.P&gt;8*D:1!!&amp;%!Q`````QN%:8.U;7ZB&gt;'FP&lt;A!E1&amp;!!!Q!)!!E!#B&gt;4:8&amp;V:7ZD:3"798*J97*M:3""&lt;'FB=Q!S1%!!!@````]!#S6%982B)&amp;*F=86F=X2T)#B3:82V=GYN&gt;']N1W^O=X2S&gt;7.U&lt;X)J!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!Q!#!!.!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!#%!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!Y!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
			</Item>
			<Item Name="Run-Time Data" Type="Folder">
				<Item Name="Task Get TaskEnqueuer.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Get TaskEnqueuer.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)1!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!3%"Q!"Y!!$%717.U&lt;X)A2H*B&lt;76X&lt;X*L,GRW&lt;'FC="B.:8.T97&gt;F)%6O=86F&gt;76S,GRW9WRB=X-!$62B=WMA27ZR&gt;76V:8)!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set TaskEnqueuer.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Set TaskEnqueuer.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)1!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1")1(!!(A!!-2:"9X2P=C"'=G&amp;N:8&gt;P=GMO&lt;(:M;7*Q'%VF=X.B:W5A27ZR&gt;76V:8)O&lt;(:D&lt;'&amp;T=Q!.6'&amp;T;S"&amp;&lt;H&amp;V:86F=A"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!)!!E!#A-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!"!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get HasFP.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Get HasFP.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(5!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$%!B"EBB=S"'5!!!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set HasFP.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Set HasFP.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(5!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!-1#%'3'&amp;T)%:1!!"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!)!!E!#A-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!"!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get DefaultConstructorData.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Get DefaultConstructorData.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(G!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(E"4'%2F:G&amp;V&lt;(1A1W^O=X2S&gt;7.U&lt;X)A2'&amp;U91!!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set DefaultConstructorData.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Set DefaultConstructorData.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(G!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!?1&amp;-92'6G986M&gt;#"$&lt;WZT&gt;(*V9X2P=C"%982B!!"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!)!!E!#A-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get DefaultReturnData.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Get DefaultReturnData.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(A!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'%"4%U2F:G&amp;V&lt;(1A5G6U&gt;8*O)%2B&gt;'%!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set DefaultReturnData.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Set DefaultReturnData.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(A!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!91&amp;-42'6G986M&gt;#"3:82V=GYA2'&amp;U91"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!)!!E!#A-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get DefaultUsedLocalVariables.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Get DefaultUsedLocalVariables.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(K!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!)E"4(%2F:G&amp;V&lt;(1A68.F:#"-&lt;W.B&lt;#"798*J97*M:8-!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set DefaultUsedLocalVariables.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Set DefaultUsedLocalVariables.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(K!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!C1&amp;-=2'6G986M&gt;#"6=W6E)%RP9W&amp;M)&amp;:B=GFB9GRF=Q!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!3!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get DefaultUsedSequenceVariables.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Get DefaultUsedSequenceVariables.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(M!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!*%"4(U2F:G&amp;V&lt;(1A68.F:#"4:8&amp;V:7ZD:3"798*J97*M:8-!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set DefaultUsedSequenceVariables.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Set DefaultUsedSequenceVariables.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(M!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!E1&amp;-@2'6G986M&gt;#"6=W6E)&amp;.F=86F&lt;G.F)&amp;:B=GFB9GRF=Q"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!)!!E!#A-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get DefaultCreatedSequenceVariables.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Get DefaultCreatedSequenceVariables.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(Q!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!+%"4)E2F:G&amp;V&lt;(1A1X*F982F:#"4:8&amp;V:7ZD:3"798*J97*M:8-!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set DefaultCreatedSequenceVariables.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Set DefaultCreatedSequenceVariables.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(Q!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!I1&amp;-C2'6G986M&gt;#"$=G6B&gt;'6E)&amp;.F=86F&lt;G.F)&amp;:B=GFB9GRF=Q!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!3!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get ActualConstructorData.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Get ActualConstructorData.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(E!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(%"4&amp;U&amp;D&gt;(6B&lt;#"$&lt;WZT&gt;(*V9X2P=C"%982B!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set ActualConstructorData.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Set ActualConstructorData.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)'!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!)E"4(5&amp;D&gt;(6B&lt;#"$&lt;WZT&gt;(*V9X2P=C"%982B)#BP&lt;'1J!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!(%"4&amp;U&amp;D&gt;(6B&lt;#"$&lt;WZT&gt;(*V9X2P=C"%982B!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!E!#A!,!Q!!?!!!$1A!!!U*!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%A!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get ActualLocalVariables.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Get ActualLocalVariables.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(E!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(%"4&amp;E&amp;D&gt;(6B&lt;#"-&lt;W.B&lt;#"798*J97*M:8-!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set ActualLocalVariables.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Set ActualLocalVariables.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)'!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!)E"4(%&amp;D&gt;(6B&lt;#"-&lt;W.B&lt;#"798*J97*M:8-A+'^M:#E!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!(%"4&amp;E&amp;D&gt;(6B&lt;#"-&lt;W.B&lt;#"798*J97*M:8-!!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!E!#A!,!Q!!?!!!$1A!!!U*!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%A!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get ActualSequenceVariables.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Get ActualSequenceVariables.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(G!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(E"4'5&amp;D&gt;(6B&lt;#"4:8&amp;V:7ZD:3"798*J97*M:8-!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set ActualSequenceVariables.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Set ActualSequenceVariables.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)+!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!*%"4(U&amp;D&gt;(6B&lt;#"4:8&amp;V:7ZD:3"798*J97*M:8-A+'^M:#E!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!?1&amp;-:17.U&gt;7&amp;M)&amp;.F=86F&lt;G.F)&amp;:B=GFB9GRF=Q"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!*!!I!#Q-!!(A!!!U)!!!.#1!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get ReturnedData.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Get ReturnedData.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(;!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%E"4$6*F&gt;(6S&lt;G6E)%2B&gt;'%!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set ReturnedData.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Set ReturnedData.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(S!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'%"4%V*F&gt;(6S&lt;G6E)%2B&gt;'%A+'^M:#E!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!31&amp;-.5G6U&gt;8*O:71A2'&amp;U91"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!*!!I!#Q-!!(A!!!U)!!!.#1!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Get ReturnedError.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Get ReturnedError.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(E!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(%"1!!-!!!!"!!)/5G6U&gt;8*O:71A28*S&lt;X)!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Task Set ReturnedError.vi" Type="VI" URL="../Public API/Task/Run-Time Data/Task Set ReturnedError.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(E!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1&amp;!!!Q!!!!%!!AZ3:82V=GZF:#"&amp;=H*P=A!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Branch" Type="Folder">
			<Item Name="Core Data" Type="Folder">
				<Item Name="Branch Get CustomName.vi" Type="VI" URL="../Public API/Branch/Core Data/Branch Get CustomName.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(=!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;%!Q`````QN$&gt;8.U&lt;WUA4G&amp;N:1",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.4G^E:3"*2#!I:(6Q+1!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!#1!+!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Branch Set CustomName.vi" Type="VI" URL="../Public API/Branch/Core Data/Branch Set CustomName.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(=!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!51$$`````#U.V=X2P&lt;3"/97VF!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!#%!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Branch Get BranchReference.vi" Type="VI" URL="../Public API/Branch/Core Data/Branch Get BranchReference.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)#!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$%!B"EFT)&amp;*F:A!!%E!S`````QB3:79A5'&amp;U;!!!(%"1!!)!"!!&amp;%%*S97ZD;#"3:7:F=G6O9W5!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!'!!=!#!!*!!E!#1!*!!I!#1!,!!Q$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Branch Create Reference.vi" Type="VI" URL="../Public API/Branch/Core Data/Branch Create Reference.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(A!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$,`````$5&amp;C=W^M&gt;82F)&amp;"B&gt;'A!2Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!41!=!#5*S97ZD;#"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!)!!E!#A-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!"!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Branch Load Reference.vi" Type="VI" URL="../Public API/Branch/Core Data/Branch Load Reference.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)"!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V5&lt;X!A4'^B:'6E)%F%!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!'5!$!"**&lt;G2F?#!I981A&gt;'BF)'6O:#E!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!(E!S`````R2"9H.P&lt;(6U:3"P=C"3:79A5'&amp;U;!!!2Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!41!=!#5*S97ZD;#"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!=!#!!*!!I!#Q-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!)!!!!#A!!!"!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Branch Remove Reference.vi" Type="VI" URL="../Public API/Branch/Core Data/Branch Remove Reference.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!()!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!+!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Branch Get TreeClosed.vi" Type="VI" URL="../Public API/Branch/Core Data/Branch Get TreeClosed.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(9!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%%!B#V2S:75A1WRP=W6E!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!*!!I$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Branch Set TreeClosed.vi" Type="VI" URL="../Public API/Branch/Core Data/Branch Set TreeClosed.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(9!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!11#%,6(*F:3"$&lt;'^T:71!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
			</Item>
			<Item Name="Run-Time Data" Type="Folder">
				<Item Name="Branch Get BranchReferenceStatus.vi" Type="VI" URL="../Public API/Branch/Run-Time Data/Branch Get BranchReferenceStatus.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(W!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$E!B#%FT)&amp;:B&lt;'FE!!!A1&amp;!!!1!%&amp;U*S97ZD;#"3:7:F=G6O9W5A5X2B&gt;(6T!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!&amp;!!9!"Q!)!!A!#!!)!!E!#!!+!!M$!!"Y!!!.#!!!#1!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
				<Item Name="Branch Set BranchReferenceStatus.vi" Type="VI" URL="../Public API/Branch/Run-Time Data/Branch Set BranchReferenceStatus.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(W!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V/&lt;W2F)%F%)#BE&gt;8!J!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!/1#%)38-A6G&amp;M;71!!#"!5!!"!!A81H*B&lt;G.I)&amp;*F:G6S:7ZD:3"4&gt;'&amp;U&gt;8-!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#1!+!!M$!!"Y!!!.#!!!!!!!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Init.vi" Type="VI" URL="../Public API/Init.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$R!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#6*F:G6S:7ZD:1!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!%!Q!!?!!!$1A!!!!!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!!!%!"Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Deinit.vi" Type="VI" URL="../Public API/Deinit.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#^!!!!"Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!6!$Q!!Q!!Q!%!!1!"!!%!!1!"!!%!!5!"!!%!!1$!!"Y!!!.#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!!!1!'!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Get Entry Node IDs.vi" Type="VI" URL="../Public API/Get Entry Node IDs.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)@!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%-!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!$U!(!!2*&lt;GFU!!"$!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!!^!"Q!%1G^E?1!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"E2F;7ZJ&gt;!!!(%"1!!-!"1!'!!=/27ZU=HEA4G^E:3"*2(-!!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Get Node ID List.vi" Type="VI" URL="../Public API/Get Node ID List.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!':!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V$;'FM:#"/&lt;W2F)%F%!":!1!!"`````Q!&amp;#%ZP:'5A352T!!!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!%!!E$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Get Node Tree ID Path.vi" Type="VI" URL="../Public API/Get Node Tree ID Path.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)Z!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5.I;7RE)%ZP:'5A351!*E"!!!(`````!!1:6(*F:3"*2#"1982I)#BJ&lt;G.M,C"T:7RG+1",!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&gt;!"Q!.4G^E:3"*2#!I:(6Q+1!Y1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!R4:8&amp;V:7ZD:3"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%5!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!%5!(!!&gt;/&lt;W2F)%F%!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"1!'!!=!#!!)!!A!#!!*!!A!#A!,!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Add Node.vi" Type="VI" URL="../Public API/Add Node.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*#!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V":'2F:#"/&lt;W2F)%F%!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!'5!$!"**&lt;G2F?#!I981A&gt;'BF)'6O:#E!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!6Q$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QV/&lt;W2F)&amp;2Z='5O9X2M!#&amp;!&amp;1!#"&amp;2B=WM'1H*B&lt;G.I!!F/&lt;W2F)&amp;2Z='5!4Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!&lt;1!=!%&amp;"B=G6O&gt;#"#=G&amp;O9WAA351!!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"Q!)!!E!#A!,!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!I!!!!+!!!!%!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Remove Node.vi" Type="VI" URL="../Public API/Remove Node.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;^!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&amp;!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"&amp;!"Q!(4G^E:3"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Move Node.vi" Type="VI" URL="../Public API/Move Node.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!.I!!!!%A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!51$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!&gt;1!=!%U.P&lt;G2J&gt;'FP&lt;G&amp;M)%ZP:'5A351!D1$R!!!!!!!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=R^/&lt;W2F)%6Y:7.V&gt;'FP&lt;C"4&gt;'&amp;U&gt;8-A28:F&lt;H1O9X2M!%6!&amp;1!'"U6Y:7.V&gt;'5'2GFO;8.I"V.V9W.F=X-(2G&amp;J&lt;(6S:1:$97ZD:7Q%5WNJ=!!!$&amp;.U982V=S"&amp;&gt;G6O&gt;!!!(%"!!!(`````!!901W^O:'FU;7^O=S!I4V)J!"Z!5!!$!!1!"1!(%56Y:7.V&gt;'5A1W^O:'FU;7^O!#"!1!!"`````Q!)%F*F&lt;7^W:71A1W^O:'FU;7^O=Q!!3Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!81!=!$5ZP:'5A351A+'2V=#E!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!%!!!!'5!$!"**&lt;G2F?#!I981A&gt;'BF)'6O:#E!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!4Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!&lt;1!=!%&amp;2B=G&gt;F&gt;#"#=G&amp;O9WAA351!!$:!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!#V.F=86F&lt;G.F)'FO!'%!]!!-!!-!#1!+!!M!$!!-!!Q!$1!/!!]!"!!1!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!!!!!!!I!!!!+!!!!%!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!"%!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Copy Node.vi" Type="VI" URL="../Public API/Copy Node.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)S!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%U!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!'5!(!!Z$&lt;X"J:71A4G^E:3"*2!!!/%"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!-5W6R&gt;76O9W5A&lt;X6U!!!:1!-!%EFO:'6Y)#BB&gt;#"U;'5A:7ZE+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"0!0(70;S.!!!!!QZ4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T#UZP:'5A351O9X2M!"N!"Q!16'&amp;S:W6U)%*S97ZD;#"*2!!!21$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!21!=!"UZP:'5A351!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!(!!A!#1!+!!M$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!#!!!!!I!!!!1!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Save Sequence.vi" Type="VI" URL="../Public API/Save Sequence.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$,`````$&amp;"B&gt;'AA+#IO=X&amp;O+1!!.E"Q!"Y!!#%/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=Q!,5W6R&gt;76O9W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Load Sequence.vi" Type="VI" URL="../Public API/Load Sequence.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%,!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$,`````$&amp;"B&gt;'AA+#IO=X&amp;O+1!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Save SequencePart.vi" Type="VI" URL="../Public API/Save SequencePart.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'6!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$,`````$6"B&gt;'AA+#IO=X&amp;O=#E!2Q$RVDWMD1!!!!-/5W6R&gt;76O9W5O&lt;(:M;7)15W6R&gt;76O9W5O&lt;(:D&lt;'&amp;T=QN/&lt;W2F)%F%,G.U&lt;!!41!=!#5*S97ZD;#"*2!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!A!#1-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!"!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Load SequencePart.vi" Type="VI" URL="../Public API/Load SequencePart.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)"!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%M!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!&amp;U!(!!V5&lt;X!A4'^B:'6E)%F%!$B!=!!?!!!B$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-!$&amp;.F=86F&lt;G.F)'^V&gt;!!!'5!$!"**&lt;G2F?#!I981A&gt;'BF)'6O:#E!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E!S`````QV1982I)#AK,H.R&lt;H!J!%]!]&gt;9^L)U!!!!$$F.F=86F&lt;G.F,GRW&lt;'FC%&amp;.F=86F&lt;G.F,GRW9WRB=X-,4G^E:3"*2#ZD&gt;'Q!'U!(!""198*F&lt;H1A1H*B&lt;G.I)%F%!!!W1(!!(A!!)1Z4:8&amp;V:7ZD:3ZM&gt;GRJ9B"4:8&amp;V:7ZD:3ZM&gt;G.M98.T!!N4:8&amp;V:7ZD:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!=!#!!*!!I!#Q-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!)!!!!#A!!!"!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
	</Item>
</LVClass>
