﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Serialize Functions" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Get Format Version.vi" Type="VI" URL="../Serialize Functions/Get Format Version.vi"/>
		<Item Name="Serialize Parameter.vi" Type="VI" URL="../Serialize Functions/Serialize Parameter.vi"/>
		<Item Name="Deserialize Parameter.vi" Type="VI" URL="../Serialize Functions/Deserialize Parameter.vi"/>
		<Item Name="Wrap Class.vi" Type="VI" URL="../Serialize Functions/Wrap Class.vi"/>
		<Item Name="Unwrap Class.vi" Type="VI" URL="../Serialize Functions/Unwrap Class.vi"/>
		<Item Name="Wrap Node.vi" Type="VI" URL="../Serialize Functions/Wrap Node.vi"/>
		<Item Name="Unwrap Node.vi" Type="VI" URL="../Serialize Functions/Unwrap Node.vi"/>
		<Item Name="Preview Node.vi" Type="VI" URL="../Serialize Functions/Preview Node.vi"/>
		<Item Name="Wrap Sequence Nodes.vi" Type="VI" URL="../Serialize Functions/Wrap Sequence Nodes.vi"/>
		<Item Name="Unwrap Sequence Nodes.vi" Type="VI" URL="../Serialize Functions/Unwrap Sequence Nodes.vi"/>
		<Item Name="Wrap Sequence.vi" Type="VI" URL="../Serialize Functions/Wrap Sequence.vi"/>
		<Item Name="Unwrap Sequence.vi" Type="VI" URL="../Serialize Functions/Unwrap Sequence.vi"/>
		<Item Name="Wrap SequencePart.vi" Type="VI" URL="../Serialize Functions/Wrap SequencePart.vi"/>
		<Item Name="Unwrap SequencePart.vi" Type="VI" URL="../Serialize Functions/Unwrap SequencePart.vi"/>
	</Item>
	<Item Name="Sequence.lvclass" Type="LVClass" URL="../Sequence_class/Sequence.lvclass"/>
	<Item Name="Node.lvclass" Type="LVClass" URL="../Node_class/Node.lvclass">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Task.lvclass" Type="LVClass" URL="../Task_class/Task.lvclass">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Branch.lvclass" Type="LVClass" URL="../Branch_class/Branch.lvclass">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
</Library>
